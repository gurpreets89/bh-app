exports.checkAdminPermission = function(AllowedUsers, action) {
	var middleware = false; // start out assuming this is not a middleware call
	return function(req, res, next) {
		var userType = req.user.user_type;
		if (!userType) {
			return res.send(401, 'unauthorize');
		}

		if (AllowedUsers.indexOf(userType) > -1) {
			return next();
		} else {
			return res.send(401, 'unauthorize');
		}
	}
};
exports.requireAuthentication = function(req, res, next) {
	console.log('private route list!');
	next();
};
exports.logger = function(req, res, next) {
	console.log('Original request hit : ' + req.originalUrl, JSON.stringify(req.user));
	next();
}