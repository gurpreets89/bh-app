var userObj         = require('./../../models/users/users.js');
var constantObj     = require('./../../../constants.js');
var mongoose        = require('mongoose');
var moment          = require('moment');


/**
 * List all patients
 * Input: 
 * Output: User json object
 * Created By : RaJesh
 * Date : 28 June 2016
 */


/* UTC -> XXX change timezone from UTC timezone to user timezone */
function changeToPatientZone(ts, serverOffset, patientOffset){
    var newts = parseInt(ts) + ((parseInt(patientOffset) - parseInt(serverOffset)) * 60 );
    return newts;
}


exports.list = function() {
    var inputJSON = {};
    inputJSON.user_type = { $in: [ 3, 4 ] },
    inputJSON.device_id = {$exists:true};
    inputJSON.next_appointment_date = {$exists:true};
    inputJSON.next_appointment_time = {$exists:true};
    inputJSON.next_assessment_date = {$exists:true};
    inputJSON.is_deleted = false;

    //console.log(JSON.stringify(inputJSON));
    userObj.find(inputJSON, function(err, data) {
    	//console.log("DATA ---------->> ", JSON.stringify(data));
        if (err) {
            //console.log("ERROR");
        } else {
            for(var i = 0; i < data.length; i++){
			    var currentTimeStamp = moment().unix();
			    //console.log('moment = ',moment()._d);
			    var serverOffset = moment().utcOffset();

			    var endTimeStamp = moment(moment.unix(currentTimeStamp).add(10, 'm')._d).unix();
			    var query = {};
		        //console.log("currentTimeStamp -->>", currentTimeStamp);
		        //console.log("endTimeStamp -->>", endTimeStamp);
		        //console.log("serverOffset -->>", serverOffset);
		        //console.log("user offset = ", data[i].time_zone);
		        // console.log("userOffset -->>", ((-1)*(data[i].time_zone)));
		        var userTimeStamp = (-1)*(data[i].time_zone);
		        if (userTimeStamp != serverOffset) {
		        	//console.log("HERE--------");
					query.currentTimeStamp	= changeToPatientZone(currentTimeStamp, serverOffset, userTimeStamp);
					query.endTimeStamp	= changeToPatientZone(endTimeStamp, serverOffset, userTimeStamp);
				}else{
					query.currentTimeStamp	= currentTimeStamp;
					query.endTimeStamp	= endTimeStamp;
				}
				//console.log('+ currentTimeStamp = ', query.currentTimeStamp, ' endTimeStamp = ', query.endTimeStamp);

            	var newdate = data[i].next_appointment_date + " " + data[i].next_appointment_time;
            	//console.log("HERE in newdate", newdate);
            	var d = new Date(newdate);
            	//console.log("HERE in d", d);
				d.setDate(d.getDate() - 1);
				//console.log("HERE in d2", d);
				var userDateTimeStamp = Math.floor(d.getTime()/1000);

				//console.log("USER DATE Time Stamp ----------->>>" , userDateTimeStamp);

				if (userDateTimeStamp > query.currentTimeStamp && userDateTimeStamp < query.endTimeStamp) {
					//console.log("Time Stamp found to Send Normal Notification");
					var title = "BH App Notification";
					var deviceId = data[i].device_id;
					var platform_type= data[i].platform_type;
					var uniqKey = currentTimeStamp;
					var apptDate = data[i].next_appointment_date;
					var timeString = data[i].next_appointment_time;

					var stringTopass = apptDate + " " + timeString;
                    var newdateTime = new Date(stringTopass);
                    
					var asmtdate = data[i].next_assessment_date;
					var msg = "Next appointment is on " + apptDate + " at " + formatAMPM(newdateTime) +  ". \n Next assessment is on " + asmtdate + ".";	
					sendNotification(deviceId, platform_type, title, msg, uniqKey);
				}else{
					//console.log("Time Stamp NOT found to Send Notification Normal Notifications");
				}
			}
        }
    })
}

// List are Patients/Caregivers to send daily notifications
exports.dailyreminderNotification = function() {
    var inputJSON = {};
    inputJSON.user_type = { $in: [ 3, 4 ] },
    inputJSON.device_id = {$exists:true};
    inputJSON.daily_reminder_time = {$exists:true};

   // console.log("INPUT JSON DATA -- ", JSON.stringify(inputJSON));
    userObj.find(inputJSON, function(err, data) {
    	//console.log("DATA ---------->> ", JSON.stringify(data));
        if (err) {
            //console.log("ERROR");
        } else {
            for(var i = 0; i < data.length; i++){
			    var currentTimeStamp = moment().unix();
			    //console.log('moment = ',moment()._d);
			    var serverOffset = moment().utcOffset();

			    var endTimeStamp = moment(moment.unix(currentTimeStamp).add(10, 'm')._d).unix();
			    var query = {};
		        //console.log("currentTimeStamp -->>", currentTimeStamp);
		        //console.log("endTimeStamp -->>", endTimeStamp);
		        //console.log("serverOffset -->>", serverOffset);
		        //console.log("user offset = ", data[i].time_zone);
		        //console.log("userOffset -->>", ((-1)*(data[i].time_zone)));
		        var userTimeStamp = (-1)*(data[i].time_zone);
		        if (userTimeStamp != serverOffset) {
		        	//console.log("HERE--------");
					query.currentTimeStamp	= changeToPatientZone(currentTimeStamp, serverOffset, userTimeStamp);
					query.endTimeStamp	= changeToPatientZone(endTimeStamp, serverOffset, userTimeStamp);
				}else{
					query.currentTimeStamp	= currentTimeStamp;
					query.endTimeStamp	= endTimeStamp;
				}
			

            	var newdate = data[i].daily_reminder_time;
            	var d = new Date(newdate);
            //console.log("HERE in d", d);
				var userDateTimeStamp = Math.floor(d.getTime()/1000);
		//console.log('+ currentTimeStamp = ', query.currentTimeStamp, ' endTimeStamp = ', query.endTimeStamp, ' ==userDateTimeStamp= ', userDateTimeStamp);
				//console.log("USER DATE Time Stamp ----------->>>" , userDateTimeStamp);

				if (userDateTimeStamp > query.currentTimeStamp && userDateTimeStamp < query.endTimeStamp) {
					//console.log("Time Stamp found to Send Notification Daily Reminder Notification");
					var title = "BH App Daily Notification";
					var deviceId = data[i].device_id;
					var platform_type= data[i].platform_type;
					var uniqKey = currentTimeStamp;
					var msg = "Please fill in your Daily Check In data.";	
					var userId = data[i]._id;
					sendDailyReminderNotification(deviceId, platform_type, title, msg, uniqKey, userId, d);
				}else{
					//console.log("Time Stamp NOT found to Send Daily Notification");
				}
			}
        }
    })
}


// List are Patients/Caregivers to send weekly notifications
exports.weeklyreminderNotification = function() {
    var inputJSON = {};
    inputJSON.user_type = { $in: [ 3, 4 ] },
    inputJSON.device_id = {$exists:true};
    inputJSON.weekly_reminder_time = {$exists:true};

    //console.log("INPUT JSON DATA -- ", JSON.stringify(inputJSON));
    userObj.find(inputJSON, function(err, data) {
    	//console.log("DATA ---------->> ", JSON.stringify(data));
        if (err) {
            //console.log("ERROR");
        } else {
            for(var i = 0; i < data.length; i++){
			    var currentTimeStamp = moment().unix();
			    //console.log('moment = ',moment()._d);
			    var serverOffset = moment().utcOffset();

			    var endTimeStamp = moment(moment.unix(currentTimeStamp).add(10, 'm')._d).unix();
			    var query = {};
		        //console.log("currentTimeStamp -->>", currentTimeStamp);
		        ////console.log("endTimeStamp -->>", endTimeStamp);
		        //console.log("serverOffset -->>", serverOffset);
		        //console.log("user offset = ", data[i].time_zone);
		        // console.log("userOffset -->>", ((-1)*(data[i].time_zone)));
		        var userTimeStamp = (-1)*(data[i].time_zone);
		        if (userTimeStamp != serverOffset) {
		        	//console.log("HERE--------");
					query.currentTimeStamp	= changeToPatientZone(currentTimeStamp, serverOffset, userTimeStamp);
					query.endTimeStamp	= changeToPatientZone(endTimeStamp, serverOffset, userTimeStamp);
				}else{
					query.currentTimeStamp	= currentTimeStamp;
					query.endTimeStamp	= endTimeStamp;
				}
				//console.log('+ currentTimeStamp = ', query.currentTimeStamp, ' endTimeStamp = ', query.endTimeStamp);

            	var newdate = data[i].weekly_reminder_time;
            	var d = new Date(newdate);
            	//console.log("HERE in d", d);
				var userDateTimeStamp = Math.floor(d.getTime()/1000);

				//console.log("USER DATE Time Stamp ----------->>>" , userDateTimeStamp);

				if (userDateTimeStamp > query.currentTimeStamp && userDateTimeStamp < query.endTimeStamp) {
					//console.log("Time Stamp FOUND to Send Notification Weekly Reminder Notification");
					var title = "BH App Weekly Notification";
					var deviceId = data[i].device_id;
					var platform_type= data[i].platform_type;
					var uniqKey = currentTimeStamp;
					var msg = "Please fill in your Weekly Goals Check In data.";	
					var userId = data[i]._id;
					sendWeeklyReminderNotification(deviceId, platform_type, title, msg, uniqKey, userId, d);
				}else{
					//console.log("Time Stamp NOT found to Send Weekly Notification");
				}
			}
        }
    })
}


// function to convert time to AMPM 

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    hours = hours < 10 ? '0'+hours : hours;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

//function to send Notification to Android / IOS for 24 Hours before assessment OR Appointment
function sendNotification (deviceId, platform_type, title, msg, uniqKey) {
	//console.log('\n--In sendNotification unique key = ',uniqKey,' \n deviceId = ', deviceId, 'PLatform --' , platform_type, ' \n msg = ', msg);
	if(platform_type == 'android'){
		var gcm 	= require('android-gcm');
		var serverKey	= "AIzaSyAJdp3kRqq6yWY1eyLQhmDxuDTt2chuZ5o"; // type : server
		var senderId 	= "780443469261";
		
		// initialize new androidGcm object
		var gcmObject = new gcm.AndroidGcm(serverKey);
		// create new message
		var finalmessage = new gcm.Message({
		    registration_ids: [deviceId],
		    data: {
				key1: 'This is key1',
				title: title,
				message: msg,
				id:uniqKey
				// key2: 'key 2'
		    }
		});
		// send the message
		gcmObject.send(finalmessage, function(err, response) {
		    //console.log("\nGCM Response ---------------------------------------------------\n", err, response);
		});
	}else if(platform_type == 'ios'){
		var apns = require('apn');
		var path = require('path');
		var errorCallback=  function(err, notif){
		    //console.log('ERROR : ' + err + '\nNOTIFICATION : ' + notif);
		}
		var options = { 
			key: path.resolve('PushKey.pem'),
			cert: path.resolve('PushCert.pem'),
			certData: null,
			passphrase: "",
			keyData: null,
			ca: null,
			pfx: null,
			pfxData: null,
			gateway: 'gateway.sandbox.push.apple.com',
			port: 2195,
			rejectUnauthorized: true,
			enhanced: true,
			errorCallback: errorCallback,
			cacheLength: 100,
			autoAdjustCache: true,
			connectionTimeout: 0
		};

		var apnsConnection = new apns.Connection(options);
		var note = new apns.Notification();
		note.expiry = Math.floor(Date.now() / 1000) + 3600;
		note.badge = 1;
		note.sound = 'ping.aiff';
		note.alert = msg;
		note.payload = {'messageFrom': title};

		apnsConnection.pushNotification(note, deviceId);
		// Handling these events to confirm the notification gets
		// transmitted to the APN server or find error if any
		apnsConnection.on('error', log('error'));
		apnsConnection.on('transmitted', log('transmitted'));
		apnsConnection.on('timeout', log('timeout'));
		apnsConnection.on('connected', log('connected'));
		apnsConnection.on('disconnected', log('disconnected'));
		apnsConnection.on('socketError', log('socketError'));
		apnsConnection.on('transmissionError', log('transmissionError'));
		apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));

		function log(type) {
			return function() {
				//console.log(type, arguments , arguments.toString('utf-8') );
			}
		}
	}else{
		//console.log("ERROR -- >> In Sending Notification");
	}
}


//function to send Daily Notification to Android / IOS
function sendDailyReminderNotification (deviceId, platform_type, title, msg, uniqKey, userId, d) {
	//console.log('\n--In sendDailyReminderNotification unique key = ',uniqKey,' \n deviceId = ', deviceId, 'PLatform --' , platform_type, ' \n msg = ', msg, d);
	if(platform_type == 'android'){
		var gcm 	= require('android-gcm');
		var serverKey	= "AIzaSyAJdp3kRqq6yWY1eyLQhmDxuDTt2chuZ5o"; // type : server
		var senderId 	= "780443469261";
		
		// initialize new androidGcm object
		var gcmObject = new gcm.AndroidGcm(serverKey);
		// create new message
		var finalmessage = new gcm.Message({
		    registration_ids: [deviceId],
		    data: {
				key1: 'This is key1',
				title: title,
				message: msg,
				id:uniqKey
				// key2: 'key 2'
		    }
		});
		// send the message
		gcmObject.send(finalmessage, function(err, response) {
		    //console.log("\nGCM Response ---------------------------------------------------\n", response, err);
		    if(response.success == 1){
		    	var update_id = userId;
		    	var dataToUpdate = {};
		    	// add one day to send Notification on Next Day
		    	var newDATEObj = new Date(d.setDate(d.getDate() + 1));
		    	dataToUpdate.daily_reminder_time = newDATEObj;
		    	// Update user daily notification Flag
		    	//console.log("HERE2 ----", JSON.stringify(dataToUpdate));
			    userObj.update({ _id: update_id }, { $set: dataToUpdate }, function(err, data) {
			        if (err) {
			            //console.log("ERROR");
			        } else {
			            //console.log("SUCCESS");
			        }
			    });
		    }
		});
	}else if(platform_type == 'ios'){
		var apns = require('apn');
		var path = require('path');
		var errorCallback=  function(err, notif){
		    //console.log('ERROR : ' + err + '\nNOTIFICATION : ' + notif);
		}
		var options = { 
			key: path.resolve('PushKey.pem'),
			cert: path.resolve('PushCert.pem'),
			certData: null,
			passphrase: "",
			keyData: null,
			ca: null,
			pfx: null,
			pfxData: null,
			gateway: 'gateway.sandbox.push.apple.com',
			port: 2195,
			rejectUnauthorized: true,
			enhanced: true,
			errorCallback: errorCallback,
			cacheLength: 100,
			autoAdjustCache: true,
			connectionTimeout: 0
		};

		var apnsConnection = new apns.Connection(options);
		var note = new apns.Notification();
		note.expiry = Math.floor(Date.now() / 1000) + 3600;
		note.badge = 0;
		note.sound = 'ping.aiff';
		note.alert = msg;
		note.payload = {'messageFrom': title};

		apnsConnection.pushNotification(note, deviceId);
		// Handling these events to confirm the notification gets
		// transmitted to the APN server or find error if any
		apnsConnection.on('error', log('error'));
		apnsConnection.on('transmitted', log('transmitted'));
		apnsConnection.on('timeout', log('timeout'));
		apnsConnection.on('connected', log('connected'));
		apnsConnection.on('disconnected', log('disconnected'));
		apnsConnection.on('socketError', log('socketError'));
		apnsConnection.on('transmissionError', log('transmissionError'));
		apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));

		function log(type) {
			return function() {
				//console.log("HERE -----> ", type);
				if(type == "transmitted"){
					var update_id = userId;
			    	var dataToUpdate = {};
			    	// add one day to send Notification on Next Day
			    	var newDATEObj = new Date(d.setDate(d.getDate() + 1));
			    	dataToUpdate.daily_reminder_time = newDATEObj;
			    	// Update user daily notification Flag
			    	//console.log("HERE2 ----", JSON.stringify(dataToUpdate));
				    userObj.update({ _id: update_id }, { $set: dataToUpdate }, function(err, data) {
				        if (err) {
				            //console.log("ERROR");
				        } else {
				            //console.log("SUCCESS");
				        }
				    });
				}
			}
		}
	}else{
		//console.log("ERROR -- >> In Sending Notification");
	}
}


//function to send Weekly Notification to Android / IOS
function sendWeeklyReminderNotification (deviceId, platform_type, title, msg, uniqKey, userId, d) {
	//console.log('\n--In sendWeeklyReminderNotification unique key = ',uniqKey,' \n deviceId = ', deviceId, 'PLatform --' , platform_type, ' \n msg = ', msg, d);
	if(platform_type == 'android'){
		var gcm 	= require('android-gcm');
		var serverKey	= "AIzaSyAJdp3kRqq6yWY1eyLQhmDxuDTt2chuZ5o"; // type : server
		var senderId 	= "780443469261";
		
		// initialize new androidGcm object
		var gcmObject = new gcm.AndroidGcm(serverKey);
		// create new message
		var finalmessage = new gcm.Message({
		    registration_ids: [deviceId],
		    data: {
				key1: 'This is key1',
				title: title,
				message: msg,
				id:uniqKey
				// key2: 'key 2'
		    }
		});
		// send the message
		gcmObject.send(finalmessage, function(err, response) {
		    //console.log("\nGCM Response ---------------------------------------------------\n", response, err);
		    if(response.success == 1){
		    	var update_id = userId;
		    	var dataToUpdate = {};
		    	// add one day to send Notification on Next Day
		    	var newDATEObj = new Date(d.setDate(d.getDate() + 7));
		    	dataToUpdate.weekly_reminder_time = newDATEObj;
		    	// Update user daily notification Flag
		    	//console.log("HERE2 ----", JSON.stringify(dataToUpdate));
			    userObj.update({ _id: update_id }, { $set: dataToUpdate }, function(err, data) {
			        if (err) {
			            //console.log("ERROR");
			        } else {
			            //console.log("SUCCESS");
			        }
			    });
		    }
		});
	}else if(platform_type == 'ios'){
		var apns = require('apn');
		var path = require('path');
		var errorCallback=  function(err, notif){
		    //console.log('ERROR : ' + err + '\nNOTIFICATION : ' + notif);
		}
		var options = { 
			key: path.resolve('PushKey.pem'),
			cert: path.resolve('PushCert.pem'),
			certData: null,
			passphrase: "",
			keyData: null,
			ca: null,
			pfx: null,
			pfxData: null,
			gateway: 'gateway.sandbox.push.apple.com',
			port: 2195,
			rejectUnauthorized: true,
			enhanced: true,
			errorCallback: errorCallback,
			cacheLength: 100,
			autoAdjustCache: true,
			connectionTimeout: 0
		};

		var apnsConnection = new apns.Connection(options);
		var note = new apns.Notification();
		note.expiry = Math.floor(Date.now() / 1000) + 3600;
		note.badge = 0;
		note.sound = 'ping.aiff';
		note.alert = msg;
		note.payload = {'messageFrom': title};

		apnsConnection.pushNotification(note, deviceId);
		// Handling these events to confirm the notification gets
		// transmitted to the APN server or find error if any
		apnsConnection.on('error', log('error'));
		apnsConnection.on('transmitted', log('transmitted'));
		apnsConnection.on('timeout', log('timeout'));
		apnsConnection.on('connected', log('connected'));
		apnsConnection.on('disconnected', log('disconnected'));
		apnsConnection.on('socketError', log('socketError'));
		apnsConnection.on('transmissionError', log('transmissionError'));
		apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));

		function log(type) {
			return function() {
				//console.log("HERE -----> ", type);
				if(type == "transmitted"){
					var update_id = userId;
			    	var dataToUpdate = {};
			    	// add one day to send Notification on Next Day
			    	var newDATEObj = new Date(d.setDate(d.getDate() + 7));
			    	dataToUpdate.weekly_reminder_time = newDATEObj;
			    	// Update user daily notification Flag
			    	//console.log("HERE2 ----", JSON.stringify(dataToUpdate));
				    userObj.update({ _id: update_id }, { $set: dataToUpdate }, function(err, data) {
				        if (err) {
				            //console.log("ERROR");
				        } else {
				            //console.log("SUCCESS");
				        }
				    });
				}
			}
		}
	}else{
		//console.log("ERROR -- >> In Sending Notification");
	}
}


