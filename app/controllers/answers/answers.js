var answerObj = require('./../../models/answer/answer.js');
var constantObj = require('./../../../constants.js');
var mongoose = require('mongoose');

/**
 * Bulk update answer status
 * Input: asnwer json object
 * Output: Role json object
 * This function gets role json object from exports.role 
 */
exports.bulkUpdateAnswer = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	var roleLength = inputData.data.length;
	var bulk = answerObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var answerData = inputData.data[i];
		var id = mongoose.Types.ObjectId(answerData.id);
		bulk.find({
			_id: id
		}).update({
			$set: answerData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
		res.jsonp(outputJSON);
	});
};

exports.exportFile = function(req, res){
    
    var json2csv = require('json2csv');
    var fs = require('fs');
    var inputData = {};
	inputData.is_status = true;
	inputData.is_deleted = false;
    answerObj.find(inputData, function(errDetail, dataDetail){
        if(errDetail){
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        }
        else{

            var fields = ['answer_text', 'answer_value'];
            var mainData = new Array();
            
            for(i=0;i<dataDetail.length;i++){
                var obj = {};
                obj.answer_text = dataDetail[i]['answer_text'];
                obj.answer_value = dataDetail[i]['answer_value'];
                mainData.push(obj);
            }
            var csv = json2csv({ data: mainData, fields: fields});
            fs.writeFile('public/answerlist.csv', csv, function(err){
                if(err){
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': err
                    };
                }
                else{
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                    };
                }
                res.jsonp(outputJSON);
            });
        }
    });
}

/**
 * List all disease object
 * Input: 
 * Output: Disease json object
 * Created: Jatinder Singh
 * Currently used in BH application
 */
exports.answerlist = function(req, res) {
	var outputJSON = "";
	var inputData = {};
	inputData.is_status = true;
	inputData.is_deleted = false;
	 //console.log('in answer = ', req, res);
	answerObj.find(inputData, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	}).sort({'position' : 1}).lean();
}

/**
 * List all disease object in Admin section.
 * Input: 
 * Output: Disease json object
 * Created: Jatinder Singh
 * Currently used in BH application
 */
exports.getAllAnswerOptions = function(req, res) {
	var outputJSON = "";
	 //console.log('in answer = ', req, res);
	answerObj.find({}, function(err, data) {
		// console.log(err,data);
		// return;
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	}).sort({'position' : 1});
}


/**
 * Create new user object
 * Input: User object
 * Output: User json object with success
 * developer : Jatinder Singh
 */
exports.saveAnswerOption = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var answerModelObj = {};
	answerModelObj = req.body;
	if(typeof(req.body._id) != 'undefined'){

		var updateData = {};
    	updateData._id = req.body._id;
    	delete(req.body._id);
    	answerObj.update(updateData, {$set: req.body}, function(err, data) {
    		if (err) {
				//console.log('error in update answer option : ', err);
				switch (err.name) {
					case 'ValidationError':

						for (field in err.errors) {
							if (errorMessage == "") {
								errorMessage = field +""+ err.errors[field].message;
							} else {
								errorMessage += ", " +field+ ""+ err.errors[field].message;
							}
						} //for
						break;
				} //switch

				outputJSON = {
					'status': 'failure',
					'messageId': 401,
					'message': errorMessage
				};
			} //if
			else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.answerUpdateSuccess,
					'data': data
				};
			}
			// console.log('outputJSON = ', outputJSON);
			res.jsonp(outputJSON);

		});
	} else { // Add answer option
		answerObj(answerModelObj).save(req.body, function(err, data) {
			if (err) {
				//console.log('error in add answer option : ', err);
				switch (err.name) {
					case 'ValidationError':

						for (field in err.errors) {
							if (errorMessage == "") {
								errorMessage = field +""+ err.errors[field].message;
							} else {
								errorMessage += ", " +field+ ""+ err.errors[field].message;
							}
						} //for
						break;
				} //switch

				outputJSON = {
					'status': 'failure',
					'messageId': 401,
					'message': errorMessage
				};
			} //if
			else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.answerAddSuccess,
					'data': data
				};
			}
			// console.log('outputJSON = ', outputJSON);
			res.jsonp(outputJSON);

		});
	}
}

	/**
	* Show question by id
	* Input: Question json object
	* Output: Question json object
	* This function gets question json object from exports.question 
	*/
	exports.findOneAnswer = function(req, res) {
		var outputJSON = "";
		var inputData = {};
		inputData._id = req.body._id;
		answerObj.findOne(inputData, function(err, data) {
			if (err) {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
			} else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			}
			res.jsonp(outputJSON);
		});
	};


/**
 * Update user object
 * Input: User object
 * Output: User json object with success
 * developer : gurpreet (updating patient data.)
 */
exports.update = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var user = req.userData;
	
	user.first_name = req.body.first_name;
	user.last_name 	= req.body.last_name;
	user.phone 	= req.body.phone;
	user.country 	= req.body.country;
	user.country_code = req.body.country_code;
	
	user.save(function(err, data) {
		//console.log('err in update : ', err);
		//console.log('data in update : ', data);
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = field +""+ err.errors[field].message;
						} else {
							errorMessage += ", " +field+ ""+ err.errors[field].message;
						}
					} //for
					break;
			} //switch
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} //if
		else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userStatusUpdateSuccess
			};
		}
		res.jsonp(outputJSON);
	});
}


/**
 * Update user object(s) (Bulk update)
 * Input: user object(s)
 * Output: Success message
 * This function is used to for bulk updation for user object(s)
 */
/*exports.bulkUpdate = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	var roleLength = inputData.data.length;
	var bulk = diseaseObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var userData = inputData.data[i];
		var id = mongoose.Types.ObjectId(userData.id);
		bulk.find({
			_id: id
		}).update({
			$set: userData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
	});
	res.jsonp(outputJSON);
} 
*/

