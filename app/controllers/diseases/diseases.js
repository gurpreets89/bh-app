var diseaseObj = require('./../../models/disease/disease.js');
var constantObj = require('./../../../constants.js');
var mongoose = require('mongoose');
var userObj = require('./../../models/users/users.js');
var fs = require('fs');
/**
 * update admin disease object for admin
 * Input: 
 * Output: Disease json object
 * Created: Hitesh Jain
 * Currently used in BH application
 */
exports.updateAdminDiseases = function(req, res){
	var outputJSON = "";
	var arrDiseaseId = req.body.diseaseId;
	
	var savedDisease = req.user.disease;
	var operation = req.body.operation;
	if(operation == "1"){
		//active 
		for(var j=0;j<arrDiseaseId.length;j++){
			if(savedDisease.indexOf(arrDiseaseId[j]) == -1){
				savedDisease.push(arrDiseaseId[j]);
			}
		}
	}
	else{
		//iactive
		for(var j=0;j<arrDiseaseId.length;j++){
			var index = savedDisease.indexOf(arrDiseaseId[j]);
			if(index != -1){
				savedDisease.splice(index, 1);
			}
		}
	}
	userObj.update({_id:req.user._id}, {$set:{disease:savedDisease}}, function(errUpdate, dataUpdate){
		if (errUpdate) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.diseaseUpdateSuccess,
			}
		}
		res.jsonp(outputJSON);
	});
	
	
	
}

exports.exportFile = function(req, res){
	var json2csv = require('json2csv');
	var fs = require('fs');
	var whereJSON = {};
	whereJSON.is_deleted=false;
	diseaseObj.find(whereJSON, function(errDetail, dataDetail){
		if(errDetail){
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		}
		else{
			var fields = ['id', 'name', 'description', 'starting_probability_outpatient', 'starting_probability_pediatrician', 'low_severity_threshold', 'high_severity_threshold', 'low_dlr', 'neutral_dlr', 'high_dlr'];
			var mainData = new Array();
			for(i=0;i<dataDetail.length;i++){
				var obj = {};
				obj.id = dataDetail[i]['_id'].toString();
				obj.name = dataDetail[i]['title'];
				obj.description = dataDetail[i]['description'];
				obj.starting_probability_outpatient = dataDetail[i]['starting_probability_outpatient'];
				obj.starting_probability_pediatrician = dataDetail[i]['starting_probability_pediatrician'];
				obj.low_severity_threshold = dataDetail[i]['low_severity_threshold'];
				obj.high_severity_threshold = dataDetail[i]['high_severity_threshold'];
				obj.low_dlr = dataDetail[i]['low_dlr'];
				obj.neutral_dlr = dataDetail[i]['neutral_dlr'];
				obj.high_dlr = dataDetail[i]['high_dlr'];
				mainData.push(obj);
			}
			var csv = json2csv({ data: mainData, fields: fields});
			fs.writeFile('public/import_disorder.csv', csv, function(err){
				if(err){
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': err
					};
				}
				else{
					var outputJSON = {
						'status': 'success',
						'messageId': 200,
					};
				}
				res.jsonp(outputJSON);
			});
		}
	});
}

exports.importFile = function(req, res){

	var fs = require('fs');
    var csv = require("fast-csv");
	var stream = fs.createReadStream("./public/csv/" + req.file.filename);
	csv
	.fromStream(stream, { headers: true })
	.on("data", function(data) {
		var insertObj = {};
		if(typeof data.name != "undefined"){
			insertObj.title = data.name;
		}
		if(typeof data.description != "undefined"){
			insertObj.description = data.description;
		}
		if(typeof data.starting_probability_outpatient != "undefined"){
			insertObj.starting_probability_outpatient = data.starting_probability_outpatient;
		}
		if(typeof data.starting_probability_pediatrician != "undefined"){
			insertObj.starting_probability_pediatrician = data.starting_probability_pediatrician;
		}
		if(typeof data.low_severity_threshold != "undefined"){
			insertObj.low_severity_threshold = data.low_severity_threshold;
		}
		if(typeof data.high_severity_threshold != "undefined"){
			insertObj.high_severity_threshold = data.high_severity_threshold;
		}
		if(typeof data.low_dlr != "undefined"){
			insertObj.low_dlr = data.low_dlr;
		}
		if(typeof data.neutral_dlr != "undefined"){
			insertObj.neutral_dlr = data.neutral_dlr;
		}
		if(typeof data.high_dlr != "undefined"){
			insertObj.high_dlr = data.high_dlr;
		}
		if(!(typeof data.id != "undefined" && data.id != "")){
			diseaseObj(insertObj).save(insertObj, function(err, data) {
				if (err) {
					//console.log('error in add disease : ', err);
					var errorMessage = "";
					switch (err.name) {
						case 'ValidationError':
							for (field in err.errors) {
								if (errorMessage == "") {
									errorMessage = field +""+ err.errors[field].message;
								} 
								else {
									errorMessage += ", " +field+ ""+ err.errors[field].message;
								}
							} //for
							break;
					} 
					//switch
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': errorMessage
					};
					res.jsonp(outputJSON);
				} //if
			});
		}
		else{
			console.log("=============update data=============================");
			console.log(insertObj);
			console.log("=============update data=============================");
			diseaseObj.update({'_id':data.id}, {$set:insertObj}, function(errUpdate, dataUpdate){
				if(errUpdate){
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': errorMessage
					};
					console.log()
					res.jsonp(outputJSON);
				}
			});	
		}
	})
	.on("end", function() {
		var outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.diseaseSuccess
		};
		console.log("===================");
		return res.json(outputJSON);
	});
}

/**
 * List all disease object for admin
 * Input: 
 * Output: Disease json object
 * Created: Hitesh Jain
 * Currently used in BH application
 */
exports.adminlist = function(req, res) {
	var outputJSON = "";
	diseaseObj.find({'is_deleted':false, 'is_status':true},'title description').lean().exec(function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			var returnData = new Array();
			for(var i=0;i<data.length;i++){
				var obj = {};
				obj.active = false;
				if(req.user.disease.indexOf(data[i]._id) != -1){
					obj.active = true;
				}
				obj._id = data[i]._id;
				obj.title = data[i].title;
				obj.description = data[i].description;
				returnData.push(obj);
			}
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': returnData
			}
		}
		res.jsonp(outputJSON);
	});
}


/**
 * List all disease object
 * Input: 
 * Output: Disease json object
 * Created: Jatinder Singh
 * Currently used in BH application
 */
exports.list = function(req, res) {
	var outputJSON = "";
	diseaseObj.find({}, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}


/**
 * Add New Disease
 * developer : gurpreet
 */
exports.add = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	//console.log("req body = ", req.body);
	diseaseObj(req.body).save(req.body, function(err, data) {
		if (err) {
			//console.log('error in add disease : ', err);
			switch (err.name) {
				case 'ValidationError':

					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = field +""+ err.errors[field].message;
						} else {
							errorMessage += ", " +field+ ""+ err.errors[field].message;
						}
					} //for
					break;
			} //switch

			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} //if
		else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.diseaseSuccess,
				'data': data
			};
		}
		res.jsonp(outputJSON);

	});

}



/**
 * Update user object(s) (Bulk update)
 * Input: user object(s)
 * Output: Success message
 * This function is used to for bulk updation for user object(s)
 */
exports.bulkUpdateDiseases = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	//console.log("inputJson",inputData);
	var roleLength = inputData.data.length;
	var bulk = diseaseObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var userData = inputData.data[i];
		var id = mongoose.Types.ObjectId(userData.id);
		bulk.find({
			_id: id
		}).update({
			$set: userData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
		res.jsonp(outputJSON);
	});
} 


/**
 * Show disease by name
 * Input: Disease json object
 * Output: Disease json object
 * This function gets disease json object
 * created by : Jatinder Singh 07-06-2016
 * 
 */
exports.findDiseaseDetail = function(req, res) {
	var outputJSON = "";
	var query = {};
	query = req.params;
	diseaseObj.findOne(query, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
		    outputJSON = {
				'status': 'success',
				'messageId': 200,
				'data' : data,
				'message': constantObj.messages.successRetreivingData
			}
		}
		res.jsonp(outputJSON);
	});
};

/**
 * FindOne Disease
 * created by : Gurpreet Singh
 */
exports.findOne = function(req, res) {
	//console.log("req = ", req.body, req.params); return;
	var outputJSON = ""; var query = {};
	query._id = req.params.id;
	diseaseObj.findOne(query, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
		    outputJSON = {
				'status': 'success',
				'messageId': 200,
				'data' : data,
				'message': constantObj.messages.successRetreivingData
			}
		}
		res.jsonp(outputJSON);
	});
};


/**
* Update Disease By ID
* Developer : Gurpreet Singh
**/
exports.updateDisease = function(req, res) {
	var errorMessage = ""; var outputJSON = ""; var searchCriteria = {};
	var updateData = {};
	updateData = req.body;
	//console.log("req body = ", updateData);
	searchCriteria._id = req.body._id;
	delete updateData._id;
	diseaseObj.update(searchCriteria, updateData, function(err, data) {
		//console.log(err);
		if(err) {
			switch(err.name) {
				case 'ValidationError':
				for(field in err.errors) {
					if(errorMessage == "") {
					  errorMessage = err.errors[field].message;
					}
					else {              
					  errorMessage+="\r\n" + err.errors[field].message;
					}
				}//for
				break;
			}//switch
			outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
		}//if
		else {
			outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.diseaseUpdateSuccess};
		}
		res.jsonp(outputJSON);
	});
}

/**
* Delete Disease
* Created :Gurpreet Singh
**/
exports.deleteDisease = function(req, res) {
	var outputJSON = ""; var search = {}; var updateData = {};
	search= {'_id' : req.body};
	updateData = {$set:{'is_deleted':true}};
	diseaseObj.update(search, updateData, function(err, data) {
		//console.log(err, data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.diseaseDeleteSuccess,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}