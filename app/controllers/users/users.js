var userObj = require('./../../models/users/users.js');
var userInviteObj = require('./../../models/users/userInvites.js');
var otpObj = require('./../../models/otp/otp.js');
var constantObj = require('./../../../constants.js');
var emailService = require('./../email/emailService.js');
var mongoose = require('mongoose');
var fs = require("fs");
var CryptoJS = require("crypto-js");
var moment   = require('moment'); 

/**
    find user by username
    author : gurpreet
**/
exports.checkUser = function(req, res) {
    var outputJSON = "";
    var query = {};
    query.username = req.params.name;
    userObj.find(query, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            if (data.length > 0) {
                outputJSON = {
                    'status': 'warning',
                    'messageId': 200,
                    'message': constantObj.messages.warningRetreivingUser
                        //'data': data
                }
                res.jsonp(outputJSON);
            } else {
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.successRetreivingData
                }
                res.jsonp(outputJSON);
            }
        }
    });
}


exports.exportFile = function(req, res){
  
    var json2csv = require('json2csv');
    var fs = require('fs');
    var whereJSON = {};
    whereJSON.is_deleted=false;
    userObj.find(whereJSON, function(errDetail, dataDetail){
        if(errDetail){
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        }
        else{
            var fields = ['first_name', 'last_name', 'email', 'username', 'clinic_name', 'phone', 'country_code', 'country'];
            var mainData = new Array();
            for(i=0;i<dataDetail.length;i++){
                var obj = {};
                obj.first_name = dataDetail[i]['first_name'];
                obj.last_name = dataDetail[i]['last_name'];
                obj.email = dataDetail[i]['email'];
                obj.username = dataDetail[i]['username'];
                obj.clinic_name = dataDetail[i]['clinic_name'];
                obj.phone = dataDetail[i]['phone'];
                obj.country_code = dataDetail[i]['country_code'];
                obj.country = dataDetail[i]['country'];
                mainData.push(obj);
            }
            var csv = json2csv({ data: mainData, fields: fields});
            fs.writeFile('public/adminlist.csv', csv, function(err){
                if(err){
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': err
                    };
                }
                else{
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                    };
                }
                res.jsonp(outputJSON);
            });
        }
    });
}


/*
 * This function is used to validate and check for the existing username and email
 * Created by : Jatinder Singh
 * Modules using this code : Clinician Signup, Add Clinician from admin profile, Add patient/caregiver from Patient Profile.
 *
 */

exports.checkUserAndEmail = function(req, res) {
    var outputJSON = "";
    //First Check for Username 
    if(req.body.hasOwnProperty('_id')){
    userObj.find({ username:  {$regex: req.body.username,'$options': 'i'} , '_id':{$ne:req.body._id}}, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            if (data.length > 0) {
                outputJSON = {
                    'status': 'warning-username',
                    'messageId': 200,
                    'message': constantObj.messages.userNameExist
                        //'data': data
                }
                res.jsonp(outputJSON);
            } else {
                // Now check for email exist 
                userObj.find({ email: req.body.email , '_id':{$ne:req.body._id}}, function(err, data) {

                    if (err) {
                        outputJSON = {
                            'status': 'failure',
                            'messageId': 203,
                            'message': constantObj.messages.errorRetreivingData
                        };
                        res.jsonp(outputJSON);
                    } else {
                        if (data.length > 0) {
                            outputJSON = {
                                'status': 'warning-email',
                                'messageId': 200,
                                'message': constantObj.messages.userEmailExist
                                    //'data': data
                            }
                            res.jsonp(outputJSON);
                        } else {
                            outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': constantObj.messages.successRetreivingData
                            }
                            res.jsonp(outputJSON);
                        }
                    }
                });
            }
        }
    });
}else{
    userObj.find({ username: {$regex: req.body.username,'$options': 'i'} }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            if (data.length > 0) {
                outputJSON = {
                    'status': 'warning-username',
                    'messageId': 200,
                    'message': constantObj.messages.userNameExist
                        //'data': data
                }
                res.jsonp(outputJSON);
            } else {
                // Now check for email exist 
                userObj.find({ email: req.body.email }, function(err, data) {

                    if (err) {
                        outputJSON = {
                            'status': 'failure',
                            'messageId': 203,
                            'message': constantObj.messages.errorRetreivingData
                        };
                        res.jsonp(outputJSON);
                    } else {
                        if (data.length > 0) {
                            outputJSON = {
                                'status': 'warning-email',
                                'messageId': 200,
                                'message': constantObj.messages.userEmailExist
                                    //'data': data
                            }
                            res.jsonp(outputJSON);
                        } else {
                            outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': constantObj.messages.successRetreivingData
                            }
                            res.jsonp(outputJSON);
                        }
                    }
                });
            }
        }
    });
}
}




/*
 * This function is used to check the clinician who is signing Up is invited or not
 * Created by : Rajesh
 *
*/

exports.checkInvitedClinicianSignup = function(req, res) {
    var outputJSON = "";
    //console.log("DATA to send ===> ", JSON.stringify(req.body));
    userInviteObj.find(req.body, function(err, data) {
        //console.log("ERROR --- >" ,err);
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            //console.log("DATA -- > ",  JSON.stringify(data));
            if (data.length > 0) {
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.successRetreivingData,
                    'data': data
                }
            res.jsonp(outputJSON);
            } else {
                outputJSON = {
                    'status': 'warning',
                    'messageId': 201,
                    'message': constantObj.messages.errorRetreivingData
                };
            res.jsonp(outputJSON);
            }
        }
    });
}


/*
 * This function is used to update the invited user status  once he/she signed up
 * Created by : Rajesh
 *
*/

exports.updateInvitedClinicianStatus = function(req, res) {
    var outputJSON = "";
    //console.log("DATA to send ===> ", JSON.stringify(req.body));
    userInviteObj.find(req.body, function(err, data) {
        //console.log("ERROR --- >" ,err);
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            //console.log("DATA -- > ",  JSON.stringify(data));
            if (data.length > 0) {
                var inviteData = {};
                inviteData.is_signed_up = true;
                userInviteObj.update({ _id: data[0]._id }, { $set: inviteData }, function(err1, resp) {
                    if(err1){
                        //console.log("ERROR in Invited clinician save.");
                        outputJSON = {
                            'status': 'warning',
                            'messageId': 200,
                            'message': constantObj.messages.warningRetreivingUser
                        }
                        res.jsonp(outputJSON);
                    }
                    else{
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': constantObj.messages.successRetreivingData
                        };
                        res.jsonp(outputJSON);
                    }
                });
            } else {
                outputJSON = {
                    'status': 'warning',
                    'messageId': 201,
                    'message': constantObj.messages.errorRetreivingData
                };
            res.jsonp(outputJSON);
            }
        }
    });
}

/**
 * Login Authentication of the Patient from Ionic Application
 * Input: username, password
 * Output: user data with access token
 * Created By: Jatinder Singh
 * Date : 23 May 2016
 */
exports.authenticate = function(req, res, done) {
    var data = req.user.user;
    res.jsonp({ 'status': 'success', 'messageId': 200, 'message': 'Patient logged in successfully', "data": data, "access_token": req.user.token });
}

/**
 * Find role by id
 * Input: roleId
 * Output: Role json object
 * This function gets called automatically whenever we have a roleId parameter in route. 
 * It uses load function which has been define in role model after that passes control to next calling function.
 */
exports.user = function(req, res, next, id) {
    userObj.load(id, function(err, user) {
        if (err) {
            res.jsonp(err);
        } else if (!user) {
            res.jsonp({
                err: 'Failed to load roleeeeeeee ' + id
            });
        } else {

            req.userData = user;
            next();
        }
    });
};


/**
 * Show user by id
 * Input: User json object
 * Output: Role json object
 * This function gets role json object from exports.role 
 */
exports.findOne = function(req, res) {
    if (!req.userData) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.successRetreivingData,
            'data': req.userData
        }
    }
    res.jsonp(outputJSON);
};

/**
 * Show user History by id
 * Input: User json object
 * Output: User json object
 */
exports.findUserHistory = function(req, res) {
    if (!req.params.id) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        subscriptionObj.find({ subscriber_id: req.params.id }).populate('subscriber_id plan_id').exec(function(err, data) {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        });
    }
    res.jsonp(outputJSON);
};


/**
 * List all user object
 * Input: 
 * Output: User json object
 */
exports.list = function(req, res) {
    var outputJSON = "";
    userObj.find({ is_deleted: false }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}


/**
 * Create new user object
 * Input: User object
 * Output: User json object with success
 * developer : gurpreet
 */
exports.add = function(req, res) {
    var decryptedData = CryptoJS.AES.decrypt(req.body.inputData, constantObj.messages.encryptionKey);
    req.body = '';
    req.body = decryptedData.toString(CryptoJS.enc.Utf8);
    req.body = JSON.parse(req.body);

    //console.log("DATA here ---> ", JSON.stringify(req.body));
    var errorMessage = "";
    var outputJSON = "";
    var findUserModelObj = {};
    //To add patient/caregiver : code start
    if (req.body.user_type == 3 || req.body.user_type == 4) {
        if (typeof(req.user) != 'undefined') {
            req.body.parent = req.user._id;
        }
    } else if (req.body.user_type == 2) {
        // To add clinician user from admin profile
        //console.log(typeof(req.user));
        if (typeof(req.user) != 'undefined') {
            req.body.parent = req.user._id;
        }
    }
    if (typeof(req.user) != 'undefined' && typeof(req.user.clinic_name) != 'undefined') {
        req.body.clinic_name = req.user.clinic_name;

    } else if (typeof(req.body.parent) != 'undefined') {
        var parentJson = {};
        parentJson = { _id: req.body.parent };

        userObj.findOne(parentJson, { "clinic_name": 1 }, function(err, data2) {
            if (data2) {
                req.body.clinic_name = data2.clinic_name;
                //console.log("Dasd");
                addOutsideClinician(req, res);
            }
        }).lean();
    }


    if (typeof(req.user) != 'undefined') {  
        //To add patient/caregiver : code end
        req.body.first_name = firstLetterCapital(req.body.first_name);
        req.body.last_name = firstLetterCapital(req.body.last_name);
        var userModelObj = new userObj(req.body);
        userModelObj.save(req.body, function(err, data1) {
            if (err) {
                switch (err.name) {
                    case 'ValidationError':

                        for (field in err.errors) {
                            if (errorMessage == "") {
                                errorMessage = field + " " + err.errors[field].message;
                            } else {
                                errorMessage += ", " + field + " " + err.errors[field].message;
                            }
                        } //for
                        break;
                } //switch

                outputJSON = {
                    'status': 'failure',
                    'messageId': 401,
                    'message': errorMessage
                };
                res.jsonp(outputJSON);
            } //if
            else {
                if (req.body.user_type == 3) { //If adding patient
                    if (typeof(req.body.caregiver) != 'undefined') { //If Caregiver selected
                        findUserModelObj._id = req.body.caregiver;
                        findUserModelObj.user_type = 4;
                        findUserModelObj.parent = req.user._id;
                        userObj.findOne(findUserModelObj, function(err, data2) {
                            if (data2) {
                                var updateData = {};
                                updateData.caregivers_patient_id = data1._id;
                                userObj.update({ _id: data2._id }, { $set: updateData }, function(err, data3) {
                                    if (data3) {
                                        outputJSON = {
                                            'status': 'success',
                                            'messageId': 200,
                                            'message': constantObj.messages.userSuccess,
                                            'data': data1
                                        };

                                        /* Send Email to Patient */
                                        var userDetails = {};
                                        userDetails.email = userModelObj.email;
                                        userDetails.username = userModelObj.username;
                                        //userDetails.pass = userModelObj.password;
                                        userDetails.pass = req.body.originalPassword;
                                        userDetails.firstname = userModelObj.first_name;
                                        userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
                                        // adminDetail.password = common.encrypt(userDetails.pass);
                                        var frm = 'BH app<noreply@bh-app.com>';
                                        var emailSubject = 'Welcome to BH app!';
                                        if (req.body.user_type == 3) {
                                            var emailTemplate = 'in_patient_signup.html';
                                        } else if (req.body.user_type == 4) {
                                            var emailTemplate = 'in_caregiver_signup.html';
                                        }
                                        emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
                                        res.jsonp(outputJSON);
                                    }
                                })
                            }
                            // return;
                        });
                    } else { //If caregiver not selected
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': constantObj.messages.userSuccess,
                            'data': data1
                        };

                        /* Send Email to Patient */
                        var userDetails = {};
                        userDetails.email = userModelObj.email;
                        userDetails.username = userModelObj.username;
                        //userDetails.pass = userModelObj.password;
                        userDetails.pass = req.body.originalPassword;
                        userDetails.firstname = userModelObj.first_name;
                        userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
                        // adminDetail.password = common.encrypt(userDetails.pass);
                        var frm = 'BH app<noreply@bh-app.com>';
                        var emailSubject = 'Welcome to BH app!';
                        if (req.body.user_type == 3) {
                            var emailTemplate = 'in_patient_signup.html';
                        } else if (req.body.user_type == 4) {
                            var emailTemplate = 'in_caregiver_signup.html';
                        }
                        emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
                        res.jsonp(outputJSON);
                    }
                } else if (req.body.user_type == 4) { //If adding Caregiver

                    if (typeof(req.body.caregivers_patient_id) != 'undefined') { // If patient selected

                        findUserModelObj._id = req.body.caregivers_patient_id;
                        findUserModelObj.user_type = 3;
                        findUserModelObj.parent = req.user._id;
                        userObj.findOne(findUserModelObj, function(err, data2) {
                            if (data2) {
                                var updateData = {};
                                updateData.caregiver = data1._id;
                                userObj.update({ _id: data2._id }, { $set: updateData }, function(err, data3) {
                                    if (data3) {
                                        outputJSON = {
                                            'status': 'success',
                                            'messageId': 200,
                                            'message': constantObj.messages.userSuccess,
                                            'data': data1
                                        };

                                        /* Send Email to Caregiver */
                                        var userDetails = {};
                                        userDetails.email = userModelObj.email;
                                        userDetails.username = userModelObj.username;
                                        //userDetails.pass = userModelObj.password;
                                        userDetails.pass = req.body.originalPassword;
                                        userDetails.firstname = userModelObj.first_name;
                                        userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
                                        // adminDetail.password = common.encrypt(userDetails.pass);
                                        var frm = 'BH app<noreply@bh-app.com>';
                                        var emailSubject = 'Welcome to BH app!';
                                        if (req.body.user_type == 3) {
                                            var emailTemplate = 'in_patient_signup.html';
                                        } else if (req.body.user_type == 4) {
                                            var emailTemplate = 'in_caregiver_signup.html';
                                        }
                                        emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
                                        res.jsonp(outputJSON);
                                    }
                                })
                            }
                            // return;
                        });
                    } else { // If patient not selected
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': constantObj.messages.userSuccess,
                            'data': data1
                        };

                        // Send Email to Caregiver 
                        var userDetails = {};
                        userDetails.email = userModelObj.email;
                        userDetails.username = userModelObj.username;
                        //userDetails.pass = userModelObj.password;
                        userDetails.pass = req.body.originalPassword;
                        userDetails.firstname = userModelObj.first_name;
                        userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
                        // adminDetail.password = common.encrypt(userDetails.pass);
                        var frm = 'BH app<noreply@bh-app.com>';
                        var emailSubject = 'Welcome to BH app!';
                        if (req.body.user_type == 3) {
                            var emailTemplate = 'in_patient_signup.html';
                        } else if (req.body.user_type == 4) {
                            var emailTemplate = 'in_caregiver_signup.html';
                        }
                        emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
                        res.jsonp(outputJSON);
                    }
                } else if (req.body.user_type == 2) { //If Sign-up Clinician from front/Add clinician by Admin
                    //console.log("HEERREREE");
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.userSuccess,
                        'data': data1
                    };

                    // Send Email to Clinician 
                    var userDetails = {};
                    userDetails.email = userModelObj.email;
                    userDetails.username = userModelObj.username;
                    //userDetails.pass = userModelObj.password;
                    userDetails.pass = req.body.originalPassword;
                    userDetails.firstname = userModelObj.first_name;
                    userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
                    // adminDetail.password = common.encrypt(userDetails.pass);
                    var frm = 'BH app<noreply@bh-app.com>';
                    var emailSubject = 'Welcome to BH app!';
                    var emailTemplate = 'clinician_signup.html';
                    emailService.send(userDetails, emailSubject, emailTemplate, frm, req);

                    res.jsonp(outputJSON);
                }
            }

        });
    }
}


/**
 * Update user object
 * Input: User object
 * Output: User json object with success
 * developer : gurpreet (updating patient data.)
 */
exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";

    var user = req.userData;

    //console.log("USER DATA ---> ",  JSON.stringify(user));
    if (req.body.first_name) {
        user.first_name = firstLetterCapital(req.body.first_name);
    }
    if (req.body.last_name) {
        user.last_name = firstLetterCapital(req.body.last_name);
    }
    if (req.body.phone) {
        user.phone = req.body.phone;
    }
    if (req.body.country) {
        user.country = req.body.country;
    }
    if (req.body.country_id) {
        user.country_id = req.body.country_id;
    }
    if (req.body.country_code) {
        user.country_code = req.body.country_code;
    }
    if (req.body.dob) {
        user.dob = req.body.dob;
    }
    if (req.body.emr_no) {
        user.emr_no = req.body.emr_no;
    }

    // state of mind fields update here
    if (req.body.is_appointment_showed) {
        user.is_appointment_showed = req.body.is_appointment_showed;
    }
    if (req.body.is_status != 'undefined') {

        user.is_status = req.body.is_status;
    }
    if (req.body.is_deleted != 'undefined') {

        user.is_deleted = req.body.is_deleted;
    }
    if (req.body.next_appointment_date) {

        user.next_appointment_date = req.body.next_appointment_date;
    }

    if (req.body.next_appointment_time) {

        user.next_appointment_time = req.body.next_appointment_time;
    }

    if (req.body.next_assessment_date) {
        user.next_assessment_date = req.body.next_assessment_date;
    }

    if (req.body.next_assessment_available != 'undefined') {
        user.next_assessment_available = req.body.next_assessment_available;
    }
    if (req.body.caregivers_patient_id != 'undefined') {
        user.caregivers_patient_id = req.body.caregivers_patient_id;
    }
    if (req.body.caregiver != 'undefined') {
        user.caregiver = req.body.caregiver;
    }
    if (req.body.level_of_service) {
        user.level_of_service = req.body.level_of_service;
    }
    if (req.body.assessment_type) {
        user.assessment_type = req.body.assessment_type;
    }


    user.save(function(err, data) {

        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = field + "" + err.errors[field].message;
                        } else {
                            errorMessage += ", " + field + "" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.userStatusUpdateSuccess
            };
            // Only execute when clinician is logged in

            if (req.user.user_type == 2) {
                //console.log("REQUEST BODY", JSON.stringify(req.body));
                // Check if Caregiver assigned to Patient or Not
                // var userdataReq = req.body;
                // console.log("REQUEST BODY", JSON.stringify(userdataReq));
                // console.log('userdataReq.caregiver = ', userdataReq.caregiver);
                // if(userdataReq.caregiver != '' ){
                //     console.log("HERE------------ Caregiver assigned.");
                // }else{
                //     console.log("HERE -------------- Caregiver not assigned.");
                // }

                var title = "BH App Notification";
                var apptDate = req.body.next_appointment_date;
                var timeString = req.body.next_appointment_time;
                if (typeof timeString != 'undefined') {
                    var stringTopass = apptDate + " " + timeString;
                    var newdateTime = new Date(stringTopass);
                
                    var asmtdate = req.body.next_assessment_date;
                    var msg = "Next appointment is on " + apptDate + " at " + formatAMPM(newdateTime) + ". \n Next assessment is on " + asmtdate + ".";
                    var d = new Date();
                    var uniqKey = d.getTime();
                    sendNotification(req.body.device_id, req.body.platform_type, title, msg, uniqKey);
                }
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Update user object(s) (Bulk update)
 * Input: user object(s)
 * Output: Success message
 * This function is used to for bulk updation for user object(s)
 */
exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var roleLength = inputData.data.length;
    var bulk = userObj.collection.initializeUnorderedBulkOp();
    for (var i = 0; i < roleLength; i++) {
        var userData = inputData.data[i];
        var id = mongoose.Types.ObjectId(userData.id);
        bulk.find({
            _id: id
        }).update({
            $set: userData
        });
    }
    bulk.execute(function(data) {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.userStatusUpdateSuccess
        };
        res.jsonp(outputJSON);
    });
}

exports.subscriberList = function(req, res) {
    var outputJSON = "";
    var query = {};
    query.type = 2;

    userObj.find(query, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}


exports.addSubscriber = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var userModelObj = {};
    userModelObj = req.body;
    userModelObj.type = 2;
    userObj(userModelObj).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = field + "" + err.errors[field].message;
                        } else {
                            errorMessage += ", " + field + "" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {

            var subscriberModelObj = {};
            subscriberModelObj.subscriber_id = data._id;
            subscriberModelObj.plan_id = data.plan;
            subscriptionObj(subscriberModelObj).save();
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.userSuccess,
                'data': data
            };
        }

        res.jsonp(outputJSON);

    });

}

/**
 * Send a new invite to out-patient
 * Input: User object
 * Output: User json object with success
 * developer : Jatinder Singh
 */
exports.inviteOutPatient = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var userModelObj = {};
    userModelObj = req.body;

    //Check email exits or not code starts
    userObj.find(userModelObj, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            if (data.length > 0) {
                outputJSON = {
                    'status': 'warning',
                    'messageId': 200,
                    'message': constantObj.messages.warningRetreivingUser
                        //'data': data
                }
                res.jsonp(outputJSON);
                //Check email exits or not code ends
            } else {

                // Send Email to out-Patient 
                var userDetails = {};
                userDetails.email = userModelObj.email;
                userDetails.reference_code = req.user._id;
                userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
                // adminDetail.password = common.encrypt(userDetails.pass);
                var frm = 'BH app<noreply@bh-app.com>';
                var emailSubject = 'Welcome to BH app!';
                var emailTemplate = 'out_patient_signup.html';
                emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.userSuccess
                };
                res.jsonp(outputJSON);
            }
        }
    });
}

/**
 * Get Logged-in user profile data
 * Input: 
 * Output: User json object
 */
exports.getUserProfileData = function(req, res) {
    var data = req.user;
    var outputJSON = "";
    outputJSON = {
        'status': 'success',
        'messageId': 200,
        'message': constantObj.messages.successRetreivingData,
        'data': data
    }
    res.jsonp(outputJSON);
}


/**
 * Update Logged-in user profile data into users collection
 * Input: User json oject
 * Output: true/false
    This function is also used to update the information of any user type using primary key
    Also using on mobile app when submitting assessment.
 */
/* UTC -> XXX change timezone from UTC timezone to user timezone */
function changeToPatientZone(ts, serverOffset, patientOffset){
    var newts = parseInt(ts) + ((parseInt(patientOffset) - parseInt(serverOffset)) * 60 );
    return newts;
}

exports.updateUserProfile = function(req, res) {
    var update_id = req.body._id;
    //console.log('req.body = ', req.body)
    if(req.body.time_zone && req.body.daily_reminder_time){
        var currentTimeStamp = moment().unix();
        //console.log('moment = ',moment()._d);
        var serverOffset = moment().utcOffset();
        var userOffset = (-1)*(parseInt(req.body.time_zone));
        
        //console.log('data ------------------------ \n timezone = ', req.body.time_zone)
        //console.log('\n currentTimeStamp = ', currentTimeStamp)
        //console.log('\n serverOffset = ', serverOffset)
        //console.log('\n userOffset = ', userOffset)
        //console.log('\n daily_reminder_time = ', req.body.daily_reminder_time)
        //console.log('\n daily_reminder_time = ', req.body.daily_reminder_time)
        
        if (userOffset != serverOffset) {
            var tt = new Date(req.body.daily_reminder_time);
            var wt = new Date(req.body.weekly_reminder_time);
            var ttm = moment(req.body.daily_reminder_time).unix();
            var wtm = moment(req.body.weekly_reminder_time).unix();
            
            var newDailyTime = changeToPatientZone(ttm, serverOffset, userOffset);
            req.body.daily_reminder_time = new Date(newDailyTime*1000);
            
            var newWeeklyTime = changeToPatientZone(wtm, serverOffset, userOffset);
            req.body.weekly_reminder_time = new Date(newWeeklyTime*1000);
            
            //console.log("HERE--------", tt, '/', ttm)
            //console.log("newDailyTime--------",newDailyTime, new Date(newDailyTime*1000))
            //console.log("req.body.daily_reminder_time--------",req.body.daily_reminder_time)
        }
    }
    
    
    delete req.body._id;
    if(req.body.first_name){
        req.body.first_name = firstLetterCapital(req.body.first_name);
    }
    if(req.body.last_name){
        req.body.last_name = firstLetterCapital(req.body.last_name);
    }
    //console.log("HERE2 ----", JSON.stringify(req.body));
    userObj.update({ _id: update_id }, { $set: req.body }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/*
 |------------------------------------------------------
 | Update Profile Pic for User | Developer : gurpreet
 |------------------------------------------------------
*/
exports.updateProfilePic = function(req, res) {

    var base64Data = req.body.img.replace(/^data:image\/png;base64,/, "");
    var timestamp = new Date().getTime().toString();
    var imgName = "img_" + timestamp;
    var imgPath = "public/images/profile/" + imgName;
    fs.writeFile(imgPath, base64Data, 'base64', function(err) {
        if (!err) {
            outputJSON = { 'status': 'success', 'imgName': imgName };
            var updateJSON = {};
            updateJSON = { 'profile_pic': imgName };
            userObj.update({ _id: req.user._id }, { $set: updateJSON }, function(err, data) {
                if (err) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 203,
                        'message': constantObj.messages.errorRetreivingData
                    };
                }
                //else {
                //  outputJSON = {
                //      'status': 'success',
                //      'messageId': 200,
                //      'message': constantObj.messages.successRetreivingData,
                //      'data': data
                //  }
                // }
            })
        } else {
            outputJSON = { 'status': 'failure', 'error': err };
        }
        res.jsonp(outputJSON);
    });
}

// Find patient for caregiver id
exports.findPatientByCaregiverId = function(req, res) {
    userObj.findOne(req.body, { first_name: 1, last_name: 1 }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

// Find User Counts for caregiver id
exports.findUserCount = function(req, res) {
    var jsonData = req.body;
    var inputJSONString = {};
    if (jsonData.user_type == 1) {
        // Admin Login
        inputJSONString = {
            //user_type: { $in: [ 2, 3 ] }
            $or: [
                { $and: [{ user_type: 2 }, { parent: jsonData._id }] }
                //{ $and : [ { user_type : 3 }, { hospital_id : jsonData._id } ] }
            ]
        }
    } else if (jsonData.user_type == 2) {
        // Clinician Login
        inputJSONString = {
            parent: jsonData._id,
            user_type: { $in: [3, 4] }
        }
    } else if (jsonData.user_type == 5) {
        // Super Admin Login
        inputJSONString = {
            user_type: { $in: [1] }
        }
    }
    userObj.find(inputJSONString, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

// Find Patient Count for Admin
exports.findAdminPatient = function(req, res) {
    var jsonData = req.body;
    var inputJSONString = {};
    inputJSONString = {
        //parent: { $in: [ 2, 3 ] }
        $or: [
            { $and: [{ user_type: 2 }, { parent: jsonData._id }] }
            //{ $and : [ { user_type : 3 }, { hospital_id : jsonData._id } ] }
        ]
    }
    userObj.find(inputJSONString, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            var clinicianIdArray = [];
            for (var i = 0; i < data.length; i++) {
                clinicianIdArray.push(data[i]._id);
            }
            var inputJSON = {};
            inputJSON = {
                parent: { $in: clinicianIdArray }
            }
            userObj.find(inputJSON, function(err, data) {
                //console.log('in clinican patient = ', err);
                if (err) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 203,
                        'message': constantObj.messages.errorRetreivingData
                    };
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.successRetreivingData,
                        'data': data
                    }
                }
                res.jsonp(outputJSON);
            }).populate("parent", 'first_name last_name').populate("caregiver").populate("caregivers_patient_id");
        }
    });
}

//function to send Notification to Android. /
function sendNotification(deviceId, platform_type, title, msg, uniqKey) {
    //console.log('\n--In sendNotification unique key = ', uniqKey, ' \n deviceId = ', deviceId, 'PLatform --', platform_type, ' \n msg = ', msg);
    if (platform_type == 'android') {
        //console.log("HERE in android device ------------->>");
        var gcm = require('android-gcm');
        var serverKey = "AIzaSyAJdp3kRqq6yWY1eyLQhmDxuDTt2chuZ5o"; // type : server
        var senderId = "780443469261";

        // initialize new androidGcm object
        var gcmObject = new gcm.AndroidGcm(serverKey);
        // create new message
        var finalmessage = new gcm.Message({
            registration_ids: [deviceId],
            data: {
                key1: 'This is key1',
                title: title,
                message: msg,
                id: uniqKey
                    // key2: 'key 2'
            }
        });
        // send the message
        gcmObject.send(finalmessage, function(err, response) {
            console.log("\nGCM Response ---------------------------------------------------\n", err, response);
        });

    } else if (platform_type == 'ios') {
        //console.log("HERE in IOS device ------------->>");
        var apns = require('apn');
        var path = require('path');
        var errorCallback = function(err, notif) {
            //console.log('ERROR : ' + err + '\nNOTIFICATION : ' + notif);
        }
        var options = {
            key: path.resolve('PushKey.pem'),
            cert: path.resolve('PushCert.pem'),
            certData: null,
            passphrase: "",
            keyData: null,
            ca: null,
            pfx: null,
            pfxData: null,
            gateway: 'gateway.sandbox.push.apple.com',
            port: 2195,
            rejectUnauthorized: true,
            enhanced: true,
            errorCallback: errorCallback,
            cacheLength: 100,
            autoAdjustCache: true,
            connectionTimeout: 0
        };

        var apnsConnection = new apns.Connection(options);
        var note = new apns.Notification();
        note.expiry = Math.floor(Date.now() / 1000) + 3600;
        note.badge = 1;
        note.sound = 'ping.aiff';
        note.alert = msg;
        note.payload = { 'messageFrom': title };

        apnsConnection.pushNotification(note, deviceId);
        // Handling these events to confirm the notification gets
        // transmitted to the APN server or find error if any
        apnsConnection.on('error', log('error'));
        apnsConnection.on('transmitted', log('transmitted'));
        apnsConnection.on('timeout', log('timeout'));
        apnsConnection.on('connected', log('connected'));
        apnsConnection.on('disconnected', log('disconnected'));
        apnsConnection.on('socketError', log('socketError'));
        apnsConnection.on('transmissionError', log('transmissionError'));
        apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));

        function log(type) {
            return function() {
                //console.log(type, arguments , arguments.toString('utf-8') );
            }
        }
    } else {
        //console.log("ERROR -- >> In Sending Notification");
    }
}


// Sign Up Patient/Caregiver for Mobile

exports.signUpPatient = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var userModelObj = {};
    var findUserModelObj = {};
    
    var decryptedData = CryptoJS.AES.decrypt(req.body.inputData, constantObj.messages.encryptionKey);
    req.body = '';
    req.body = decryptedData.toString(CryptoJS.enc.Utf8);
    req.body = JSON.parse(req.body);
    

    userModelObj = req.body;
    userModelObj.first_name = firstLetterCapital(userModelObj.first_name);
    userModelObj.last_name = firstLetterCapital(userModelObj.last_name);
    // console.log(JSON.stringify(userModelObj));
    userObj(userModelObj).save(req.body, function(err, data1) {
        if (err) {
            // console.log('error in add user : ', err);
            switch (err.name) {
                case 'ValidationError':

                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = field + "" + err.errors[field].message;
                        } else {
                            errorMessage += ", " + field + "" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch

            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data1
            }
        }
        /* Send Email to Patient/Caregiver */
        var userDetails = {};
        userDetails.email = userModelObj.email;
        userDetails.username = userModelObj.username;
        //userDetails.pass = userModelObj.password;
        userDetails.pass = userModelObj.originalPassword;
        userDetails.firstname = userModelObj.first_name;
        userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
        // adminDetail.password = common.encrypt(userDetails.pass);
        var frm = 'BH app<noreply@bh-app.com>';
        var emailSubject = 'Welcome to BH app!';
        if (userModelObj.user_type == 3) {
            var emailTemplate = 'in_patient_signup.html';
        } else if (userModelObj.user_type == 4) {
            var emailTemplate = 'in_caregiver_signup.html';
        }
        emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
        res.jsonp(outputJSON);
    });
}

/**
 * List all Admin object
 * Input: 
 * Output: User json object
 * Created By : Rajesh
 * Date : 19 July 2016
 */

exports.listAdmin = function(req, res) {
    var outputJSON = "";
    var jsonData = {};
    jsonData.user_type = 1;
    //console.log("jsonData = ", jsonData);
    userObj.find(jsonData, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Add Admin object
 * Input: 
 * Output: User json object
 * Created By : Rajesh
 * Date : 26 July 2016
 */
exports.addAdmin = function(req, res) {
    // streamSource: fs.createReadStream(req.files.image.path)
    var decryptedData = CryptoJS.AES.decrypt(req.body.inputData, constantObj.messages.encryptionKey);
    req.body = '';
    req.body = decryptedData.toString(CryptoJS.enc.Utf8);
    req.body = JSON.parse(req.body);
    
    if(req.body.hasOwnProperty('_id')){
    // edit mode
    var errorMessage = "";
    var outputJSON = "";
    var userModelObj = {};
    userModelObj = req.body;
    userModelObj.first_name = firstLetterCapital(userModelObj.first_name);
    userModelObj.last_name = firstLetterCapital(userModelObj.last_name);
            
    var updatedata = {first_name:req.body.first_name, last_name:req.body.last_name, email:req.body.email, username:req.body.username,
        phone:req.body.phone, clinic_name:req.body.clinic_name, address:req.body.address, password:req.body.password, is_status:req.body.is_status};
    userObj.update({_id:req.body._id},{$set:updatedata}, function(err, data1) {

        if (err) {
            switch (err.name) {
                case 'ValidationError':

                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = field + " " + err.errors[field].message;
                        } else {
                            errorMessage += ", " + field + " " + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
            res.jsonp(outputJSON);
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.userSuccess,
                'data': data1
            };
            /* Send Email to Admin */
            var userDetails = {};
            userDetails.email = userModelObj.email;
            userDetails.username = userModelObj.username;
            //userDetails.pass = userModelObj.password;
            userDetails.pass = req.body.originalPassword;
            userDetails.firstname = userModelObj.first_name;
            userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
            var frm = 'BH app<noreply@bh-app.com>';
            var emailSubject = 'Welcome to BH app!';
            var emailTemplate = 'admin_signup.html';
            emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
            res.jsonp(outputJSON);
        }
    });

    }else{
        // add mode

    console.log(req.body)
    var errorMessage = "";
    var outputJSON = "";
    var userModelObj = {};
    userModelObj = req.body;
    userModelObj.first_name = firstLetterCapital(userModelObj.first_name);
    userModelObj.last_name = firstLetterCapital(userModelObj.last_name);
    // console.log("req.body",userModelObj);
    userObj(userModelObj).save(req.body, function(err, data1) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':

                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = field + " " + err.errors[field].message;
                        } else {
                            errorMessage += ", " + field + " " + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
            res.jsonp(outputJSON);
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.userSuccess,
                'data': data1
            };
            /* Send Email to Admin */
            var userDetails = {};
            userDetails.email = userModelObj.email;
            userDetails.username = userModelObj.username;
            //userDetails.pass = userModelObj.password;
            userDetails.pass = req.body.originalPassword;
            userDetails.firstname = userModelObj.first_name;
            userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
            var frm = 'BH app<noreply@bh-app.com>';
            var emailSubject = 'Welcome to BH app!';
            var emailTemplate = 'admin_signup.html';
            emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
            res.jsonp(outputJSON);
        }
    });
    }
    
}

// Check user exist in DB or Not 


/*
    Function: forgot password with Twilio mobile App 
    Created: Rajesh
*/

exports.forgotPassword = function(req, res) {
    var outputJSON = "";
    var inputJSON = {};
    inputJSON.email = req.body.email;
    inputJSON.user_type = { '$in': [3, 4] };
    userObj.findOne(inputJSON, function(err, data) {
        //console.log("forgotPassword = ", err, data);
        if (err) {
            outputJSON = {
                'status': 'error',
                'messageId': 401,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            if (!data) {
                outputJSON = {
                    'status': 'failure',
                    'messageId': 203,
                    'message': constantObj.messages.emailDoesnotExist
                }
                res.jsonp(outputJSON);
            } else {
                if (data.phone) {

                    var phoneNumber = data.country_code + data.phone;
                    //console.log(phoneNumber);
                    // OTP generation and saving in DB
                    var generated_time = new Date();
                    var expiry_time = new Date();
                    expiry_time.setMinutes(generated_time.getMinutes() + 15);
                    var otpModelObj = {};
                    otpModelObj.user = data._id;
                    otpModelObj.otp_code = randomOTP(6);
                    otpModelObj.generated_time = generated_time;
                    otpModelObj.expiry_time = expiry_time;


                    otpObj(otpModelObj).save(req.body, function(err, data1) {
                        if (err) {
                            //console.log("OTP ERROR = ", err);
                        } else {
                            //console.log("OTP SUCCESS = ", JSON.stringify(data1));
                            //console.log("Twilio Testing HERE ---->>>");
                            //var accountSid = 'AC66b55fb1cc7f274bd463e08d30caf04d';
                            //var authToken = '021ed9ff81a0f3286281473dcb4b0eb9';
                            var accountSid = 'AC594b6b7ff54a297094ce69e077bcf749';
                            var authToken = '53edea398ae23c64ce4349d7f9cfd37d';
                            //require the Twilio module and create a REST client 
                            var client = require('twilio')(accountSid, authToken);
                            client.messages.create({
                                to: phoneNumber,
                                from: "+14155992671",
                                body: "One time password for BH Patient App is :  " + data1.otp_code,
                            }, function(err1, message) {
                                console.log("ERROR ------------->>> ",err1);
                                console.log("SUCCESS MESSAGE ------------->>> ",message);
                            });

                            outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': constantObj.messages.successRetreivingData,
                                'data': data,
                                'otpdata': data1
                            }
                            res.jsonp(outputJSON);
                        }
                    })
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.successRetreivingData,
                        'data': data
                    }
                    res.jsonp(outputJSON);
                }
            }
        }
    })
}

exports.checkOtp = function(req, res) {
    var outputJSON = "";
    var inputJSON = {};
    inputJSON = req.body;
    otpObj.find(inputJSON, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'error',
                'messageId': 401,
                'message': constantObj.messages.errorRetreivingData
            }
            res.jsonp(outputJSON);
        } else {
            //console.log(JSON.stringify(data.length));
            if (data.length == 0) {
                outputJSON = {
                    'status': 'warning',
                    'messageId': 203,
                    'message': constantObj.messages.warningRetreivingOtp
                }
                res.jsonp(outputJSON);
            } else {
                //console.log(data[0].expiry_time);
                var currentDateTime = new Date();
                //console.log(currentDateTime);
                if (currentDateTime > data[0].expiry_time) {
                    outputJSON = {
                        'status': 'warning',
                        'messageId': 204,
                        'message': constantObj.messages.warningOtpExpiry
                    }
                    res.jsonp(outputJSON);
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.successRetreivingData,
                        'data': data
                    }
                    res.jsonp(outputJSON);
                }
            }
        }
    })
}

exports.resetPassword = function(req, res) {
    var decryptedData = CryptoJS.AES.decrypt(req.body.inputData, constantObj.messages.encryptionKey);
    req.body = '';
    req.body = decryptedData.toString(CryptoJS.enc.Utf8);
    req.body = JSON.parse(req.body);
    var outputJSON = "";
    var updatePassword = {};
    //console.log(JSON.stringify(req.body));
    updatePassword.password = req.body.password;

    userObj.findOne({ _id: req.body._id }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            userObj.update({ "_id": req.body._id }, { $set: updatePassword }, function(err, result) {
                if (err) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 203,
                        'message': constantObj.messages.errorRetreivingData
                    };
                    res.jsonp(outputJSON);
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.successRetreivingData,
                        'data': result
                    };
                    res.jsonp(outputJSON);
                }
            });
        }
    });
}

exports.getAdmin = function(req, res) {
    userObj.findOne({ _id: req.body.admin_id }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
            res.jsonp(outputJSON);
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            };
            res.jsonp(outputJSON);
        }
    })
    }

/*
|-------------------------------------------
|   Generate Random OTP function
|   developer : RaJesh
|-------------------------------------------
*/
function randomOTP(length) {
    var chars = "123456789";
    var otp = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        otp += chars.charAt(i);
    }
    return otp;
}
// register clinician at sign up page

function addOutsideClinician(req,res)
{
    req.body.first_name = firstLetterCapital(req.body.first_name);
    req.body.last_name = firstLetterCapital(req.body.last_name);
    //console.log("HERE --->>", JSON.stringify(req.body));
    var userModelObj = new userObj(req.body);
    var outputJSON = {};
    var errorMessage = "";
    userModelObj.save(req.body, function(err, data1) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':

                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = field + " " + err.errors[field].message;
                        } else {
                            errorMessage += ", " + field + " " + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch

            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } else {
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.userSuccess,
                    'data': data1
                };

                // Send Email to Clinician 
                var userDetails = {};
                userDetails.email = userModelObj.email;
                userDetails.username = userModelObj.username;
                //userDetails.pass = userModelObj.password;
                userDetails.pass = req.body.originalPassword;
                userDetails.firstname = userModelObj.first_name;
                userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
                // adminDetail.password = common.encrypt(userDetails.pass);
                var frm = 'BH app<noreply@bh-app.com>';
                var emailSubject = 'Welcome to BH app!';
                var emailTemplate = 'clinician_signup.html';
                emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
        }
        res.jsonp(outputJSON);
    });  

}

function firstLetterCapital(string) { return string.charAt(0).toUpperCase() + string.slice(1); }

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    hours = hours < 10 ? '0'+hours : hours;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

