var faqObj = require('./../../models/faq/faq.js');
var constantObj = require('./../../../constants.js');
var mongoose = require('mongoose');


/**
 * List all Faq object in Admin section.
 * Input: 
 * Output: Faq json object
 * Developer : RaJesh Thakur
 */
exports.getAllFaqs = function(req, res) {
	var outputJSON = "";
	faqObj.find({}, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}


exports.exportFile = function(req, res){
  
    var json2csv = require('json2csv');
    var fs = require('fs');
    
    faqObj.find({}, function(errDetail, dataDetail){
        if(errDetail){
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        }
        else{
            var fields = ['faq_question'];
            var mainData = new Array();
            for(i=0;i<dataDetail.length;i++){
                var obj = {};
                obj.faq_question = dataDetail[i]['faq_question'];
                
                mainData.push(obj);
            }
            var csv = json2csv({ data: mainData, fields: fields});
            fs.writeFile('public/faqlist.csv', csv, function(err){
                if(err){
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': err
                    };
                }
                else{
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                    };
                }
                res.jsonp(outputJSON);
            });
        }
    });
}



/**
 * Create new Faq object
 * Input: Faq object
 * Output: Faq json object with success
 * developer : RaJesh Thakur
 */
exports.saveFaq = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var FaqModelObj = {};
	FaqModelObj = req.body;
	if(typeof(req.body._id) != 'undefined'){
		var updateData = {};
    	updateData._id = req.body._id;
    	delete(req.body._id);
    	faqObj.update(updateData, {$set: req.body}, function(err, data) {
    		if (err) {
				//console.log('error in update answer option : ', err);
				switch (err.name) {
					case 'ValidationError':

						for (field in err.errors) {
							if (errorMessage == "") {
								errorMessage = field +""+ err.errors[field].message;
							} else {
								errorMessage += ", " +field+ ""+ err.errors[field].message;
							}
						} //for
						break;
				} //switch

				outputJSON = {
					'status': 'failure',
					'messageId': 401,
					'message': errorMessage
				};
			} //if
			else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.answerUpdateSuccess,
					'data': data
				};
			}
			// console.log('outputJSON = ', outputJSON);
			res.jsonp(outputJSON);

		});
	} else { // Add answer option
		faqObj(FaqModelObj).save(req.body, function(err, data) {
			if (err) {
				//console.log('error in add faq option : ', err);
				switch (err.name) {
					case 'ValidationError':

						for (field in err.errors) {
							if (errorMessage == "") {
								errorMessage = field +""+ err.errors[field].message;
							} else {
								errorMessage += ", " +field+ ""+ err.errors[field].message;
							}
						} //for
						break;
				} //switch
				outputJSON = {
					'status': 'failure',
					'messageId': 401,
					'message': errorMessage
				};
			}else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.answerAddSuccess,
					'data': data
				};
			}
			res.jsonp(outputJSON);
		});
	}
}

/**
* Show Faq by id
* Input: Faq json object
* Output: Faq json object
* This function gets Faq json object from exports.question 
* Developer : RaJesh Thakur
*/
exports.findOneFaq = function(req, res) {
	var outputJSON = "";
	var inputData = {};
	inputData._id = req.body._id;
	faqObj.findOne(inputData, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
};

/**
* Bulk Update Status of the FAQs
* Input: Faq json object
* Output: Faq json object
* This function Update single or multiple FAQs json object from exports.question 
* Developer : RaJesh Thakur
*/

exports.updateFaqStatus = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	var roleLength = inputData.data.length;
	var bulk = faqObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var faqData = inputData.data[i];
		var id = mongoose.Types.ObjectId(faqData.id);
		bulk.find({
			_id: id
		}).update({
			$set: faqData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
		res.jsonp(outputJSON);
	});
};



/**
* Delete the question object
* Input: _id
* Created : RaJesh Thakur
* Output: Question json object with success
*/

exports.deleteFaq = function(req, res) {
	var outputJSON = ""; var search = {}; var updateData = {};
	search= {'_id' : req.body};
	updateData = {$set:{'is_deleted':true, 'is_status': false}};
	faqObj.update(search, updateData, function(err, data) {
		//console.log(err, data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.diseaseDeleteSuccess,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}

/**
 * List all Faq object in Admin section.
 * Input: 
 * Output: Faq json object
 * Developer : RaJesh Thakur
 */
exports.getAllFaqsForMobile = function(req, res) {
	var outputJSON = "";
	var query = {};
    query.is_status = true;
    query.is_deleted = false;
	faqObj.find(query, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}

