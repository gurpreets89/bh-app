var path = require("path");
// var root=path.dirname(process.mainModule.filename);
var root = process.cwd();
var taskObj = require(path.resolve(root, './app/models/admin/tasks.js'));
var mongoose = require('mongoose');
var constantObj = require(path.resolve(root, 'constants.js'));

/**
 * Find diagnosis by id
 * Input: diagnosisId
 * Output: diagnosis json object
 * This function gets called automatically whenever we have a diagnosisId parameter in route. 
 * It uses load function which has been define in diagnosis model after that passes control to next calling function.
 */
exports.task = function(req, res, next, id) {
    taskObj.load(id, function(err, task) {
        if (err) {
            res.jsonp(err);
        } else if (!task) {
            res.jsonp({
                err: 'Failed to load task ' + id
            });
        } else {
            req.task = task;
            next();
        }
    });
};

/**
 * Show diagnosis by id
 * Input: diagnosis json object
 * Output: diagnosis json object
 * This function gets diagnosis json object from exports.role 
 */
exports.findOne = function(req, res) {
    if (!req.task) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.successRetreivingData,
            'data': req.task
        }
    }
    res.jsonp(outputJSON);
};

/**
 * List all diagnosis object
 * Input: 
 * Output: diagnosis json object
 */
exports.list = function(req, res) {
    var outputJSON = "";
    var query = {};
    query = {
        $and: [{
                is_deleted: false
            }
            /*, {
                        enable: true
                    }*/
        ]
    };
    //console.log(req.body);
    taskObj.find(query, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * List all filtered tasks object
 * Input: 
 * Output: tasks json object
 */
exports.filterlist = function(req, res) {
    var outputJSON = "";
    var query = {};
    var diag = req.body.diagnosis;
    var appttype = req.body.appointmentType;
    query = {is_deleted: false, enable: true,diagnosis : diag, appointmentType:appttype};
    //console.log(req.body);
    taskObj.find(query, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Create new diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.add = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var task = {};
    task.title = req.body.title;
    task.code = req.body.code;
    task.diagnosis = req.body.diagnosis;
    task.appointmentType = req.body.appointmentType;
    task.time = req.body.time;

    taskObj(task).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': errorMessage
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.taskSuccess
            };
        }

        res.jsonp(outputJSON);
    });
}

/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var task = req.task;
    task.title = req.body.title;
    task.code = req.body.code;
    task.diagnosis = req.body.diagnosis;
    task.appointmentType = req.body.appointmentType;
    task.time = req.body.time;
    task.enable = req.body.enable;
    task.save(function(err, data) {
        //console.log(err);
        //console.log(data);
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.taskUpdateSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}


/**
 * Update diagnosis object(s) (Bulk update)
 * Input: diagnosis object(s)
 * Output: Success message
 * This function is used to for bulk updation for role object(s)
 */
exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var taskLength = inputData.data.length;
    var bulk = taskObj.collection.initializeUnorderedBulkOp();

    if (!taskLength) return res.status(400).json({
        'status': 'failure',
        'messageId': 401,
        'message': "invalid operation"
    })

    for (var i = 0; i < taskLength; i++) {
        var taskData = inputData.data[i];
        var id = mongoose.Types.ObjectId(taskData.id);
        bulk.find({
            _id: id
        }).update({
            $set: taskData
        });
    }
    bulk.execute(function(data) {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.taskStatusUpdateSuccess
        };
    });
    res.jsonp(outputJSON);
}