var extraQuestionObj = require('./../../models/extraQuestion/extraQuestion.js');
var mongoose = require('mongoose');
var answerObj = require('./../../models/answer/answer.js');
var constantObj = require('./../../../constants.js');

/**
* Find question by id
* Input: questionId
* Output: Question json object
* This function gets called automatically whenever we have a questionId parameter in route. 
* It uses load function which has been define in role model after that passes control to next calling function.
*/
exports.question = function(req, res, next, id) {
	extraQuestionObj.load(id, function(err, question) {
	   if (err){
		   res.jsonp(err);
	   }
	   else if (!question){
		   res.jsonp({err:'Failed to load question ' + id});
	   }
	   else{
		   
		   req.questionData = question;
		   next();
	   }
   });
};


/**
* Show question by id
* Input: Question json object
* Output: Question json object
* This function gets question json object from exports.question 
*/
exports.findOne = function(req, res) {
   if(!req.questionData) {
	   outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
   }
   else {
	   outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
	   'data': req.questionData}
   }
   res.jsonp(outputJSON);
};




/**
 * Create new question object
 * Input: Question object
 * Output: Question json object with success
 */
exports.add = function(req, res) {
	var outputJSON = "";
	var errorMessage = "";
	var inputJsonString = req.body;
	var questionData = req.body;
   	questionData.clinic = req.user._id;
       	extraQuestionObj(questionData).save(questionData, function(err, data) {
			if (err) {
				outputJSON = {
					'status': 'failure',
					'messageId': 401,
					'message': errorMessage
				};
			} //if
			else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.userSuccess,
					'data': data
				};
			}
			res.jsonp(outputJSON);
		});
	};

function getAnswerDetail(answersArr, index, callback){
	answerObj.find({answer_text:{$in:answersArr}, is_status:true, is_deleted:false}, function(errAnswer, dataAnswer){
		if(errAnswer){
			console.log(errAnswer);
		}
		else{
			callback(dataAnswer, index);
		}

	});
}

exports.importFile = function(req, res){

	var fs = require('fs');
    var csv = require("fast-csv");
	var stream = fs.createReadStream("./public/csv/" + req.file.filename);
	var i = 0;
	var extraQuestionArr = {};
	csv
	.fromStream(stream, { headers: true })
	.on("data", function(data) {

		console.log("============================");
		var answersArr = new Array();
		if(typeof data.answers_1 != "undefined" && data.answers_1 != ""){
			answersArr.push(data.answers_1);
		}
		if(typeof data.answers_2 != "undefined" && data.answers_2 != ""){
			answersArr.push(data.answers_2);
		}
		if(typeof data.answers_3 != "undefined" && data.answers_3 != ""){
			answersArr.push(data.answers_3);
		}
		if(typeof data.answers_4 != "undefined" && data.answers_4 != ""){
			answersArr.push(data.answers_4);
		}
		if(typeof data.answers_5 != "undefined" && data.answers_5 != ""){
			answersArr.push(data.answers_5);
		}
		var insertObj = {};
		if(typeof data.question != "undefined"){
			insertObj.question = data.question;
		}
		insertObj.clinic = req.user._id;
		extraQuestionArr[i] = insertObj;
		getAnswerDetail(answersArr, i, function(answerData, index){
			extraQuestionArr[index].answers = new Array();
			if(answerData != null){
				for(p=0;p<answerData.length;p++){
					var answerObj = {};
					answerObj.answer_id = answerData[p]._id;
					answerObj.answer_text = answerData[p].answer_text;
					answerObj.answer_value = answerData[p].answer_value;		
					extraQuestionArr[index].answers.push(answerObj);
				}
			}
			extraQuestionObj(extraQuestionArr[index]).save(extraQuestionArr[index], function(err, data) {
				if (err) {
					switch (err.name) {
						case 'ValidationError':
							for (field in err.errors) {
								if (errorMessage == "") {
									errorMessage = field +""+ err.errors[field].message;
								} 
								else {
									errorMessage += ", " +field+ ""+ err.errors[field].message;
								}
							} //for
							break;
					} 
					//switch
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': errorMessage
					};
					res.jsonp(outputJSON);
				} //if
			});
		});
		i++;
		
		
	})
	.on("end", function() {
		var outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userSuccess
		};
		return res.json(outputJSON);
	});
}

/**
 * List all the question object
 * Input: Null
 * Created :Hitesh Jain
 * Output: Question json object with success
 */
exports.allQuestionlist = function(req, res) {
	var outputJSON = "";
	var queryJSON = {};
	queryJSON.clinic = req.user._id;
	queryJSON.is_deleted = false;
	extraQuestionObj.find(queryJSON, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData,
				'err' : err
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}


/**
* Delete the question object
* Input: Null
* Created :Hitesh Jain
* Output: Question json object with success
*/
exports.deleteQuestion = function(req, res) {
   var outputJSON = "";
   extraQuestionObj.remove({_id:req.body}, function(err, data) {
	   if (err) {
		   outputJSON = {
			   'status': 'failure',
			   'messageId': 203,
			   'message': constantObj.messages.errorRetreivingData
		   };
	   } else {
		   outputJSON = {
			   'status': 'success',
			   'messageId': 200,
			   'message': constantObj.messages.questionDeleteSuccess,
			   'data': data
		   }
	   }
	   res.jsonp(outputJSON);
   });
}

/**
 * Update question object
 * Input: Question object
 * Output: Question json object with success
 * Developer: Hitesh Jain
 * Date : 22-feb-2016
 */
exports.update = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var questions = req.questionData
	questions.answers = req.body.answers;
	questions.question = req.body.question;
	questions.is_status = req.body.is_status;

	questions.save(function(err, data) {
	if(err) {
	  switch(err.name) {
		case 'ValidationError':
		for(field in err.errors) {
		  if(errorMessage == "") {
			errorMessage = err.errors[field].message;
		  }
		  else {              
			errorMessage+="\r\n" + err.errors[field].message;
		  }
			  }//for
			  break;
		  }//switch
		  outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
		}//if
		else {
		  outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.userStatusUpdateSuccess};
		}
		res.jsonp(outputJSON);
	});
}

/**
 * Update question object(s) (Bulk update)
 * Input: Question object(s)
 * Output: Success message
 * This function is used to for bulk updation for question object(s)
 */
exports.bulkUpdate = function(req, res) {
		var outputJSON = "";
		var inputData = req.body;
		//console.log("inputData",inputData);
		var roleLength = inputData.data.length;
		var bulk = extraQuestionObj.collection.initializeUnorderedBulkOp();
		for (var i = 0; i < roleLength; i++) {
		var questionData = inputData.data[i];
		var id = mongoose.Types.ObjectId(questionData.id);
		bulk.find({
			_id: id
		}).update({
			$set: questionData
		});
		}
		bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.questionSuccess
		};
		res.jsonp(outputJSON);
		});
}