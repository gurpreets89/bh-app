var goalsObj = require('./../../models/goal/goal.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');
var userObj = require('./../../models/users/users.js');

/**
 * Find goal by id
 * Input : goalId
 * Output: Goal json object
 * This function gets called automatically whenever we have a goalId parameter in route. 
 * It uses load function which has been define in goal model after that passes control to next calling function.
 */
exports.goal = function(req, res, next, id) {
	goalsObj.load(id, function(err, goal) {
		if (err) {
			res.jsonp(err);
		} else if (!goal) {
			res.jsonp({
				err: 'Failed to load role ' + id
			});
		} else {
			req.goalData = goal;
			next();
		}
	});
};

/**
 * List patient goals in mobile app
 * Input: patient
 * Output: goals json object
 * Developer : Rajesh
 */
exports.patientGoal = function(req, res) {
	var outputJSON = "";
	//console.log("patientGoal = ", req.body);
	var inputData = {};
	inputData.patient = req.body.patient;
	inputData.is_status = true;
	inputData.is_deleted = false;
	goalsObj.find(inputData, function(err, data) {
		//console.log("in patientGoal = ", err, data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}

/**
   * List patient goals in mobile app for state of mind screen
   * Input: patient
   * Output: goals json object
   * Developer : Rajesh
   */

exports.patientGoalForState = function(req, res) {
  var outputJSON = "";
  //console.log("patientGoal = ", req.body);
  var inputData = {};
  inputData.patient = req.body.patient;
  inputData.is_status = true;
  inputData.is_deleted = false;
  goalsObj.find(inputData, function(err, data) {
    if(err) {
      outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
    }
    else {
      outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
      'data': data}
    }
    res.jsonp(outputJSON);
  });
}

   /**
   * List all goals object
   * Input: 
   * Output: goals json object
   * Developer : Rajesh
   */
   	exports.list = function(req, res) {
      var outputJSON = "";
      //console.log("HERE --------------" , JSON.stringify(req.body));
      var inputData = {};
      inputData.clinician = req.body.clinician;
      if(req.body.user_type){
          inputData.user_type = req.body.user_type;
      }
      inputData.is_deleted = false;
      goalsObj.find(inputData, function(err, data) {
      	//console.log(data);
	      if(err) {
		      outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
	      }
	      else {
		      outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		      'data': data}
	      }
	      res.jsonp(outputJSON);
      }).populate('patient');
   }


/**
 * List all goals object
 * Input: goal id
 * Output: goals json object
 * Developer : Rajesh
 */
  exports.deleteGoal = function(req, res) {
    var outputJSON = ""; var search = {}; var updateData = {};
    search= {'_id' : req.body};
    updateData = {$set:{'is_deleted':true}};
    goalsObj.update(search, updateData, function(err, data) {
      //console.log(err, data);
      if (err) {
        outputJSON = {
          'status': 'failure',
          'messageId': 203,
          'message': constantObj.messages.errorRetreivingData
        };
      } else {
        outputJSON = {
          'status': 'success',
          'messageId': 200,
          'message': constantObj.messages.questionDeleteSuccess,
          'data': data
        }
      }
      res.jsonp(outputJSON);
    });
  }

/**
 * List goal
 * Input: 
 * Output: goals json object
 * Developer : Rajesh
 */


exports.add = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	goalModelObj = req.body;
	goalsObj(goalModelObj).save(req.body, function(err, data) {
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += ", " + err.errors[field].message;
						}
					}
					break;
			}
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userSuccess,
				'data': data
			};
		}
		res.jsonp(outputJSON);
	});
}

/**
 * Find one goal
 * Input: 
 * Output: goals json object
 * Developer : Rajesh
 */

exports.findOne = function(req, res) {
	//console.log('in goalData = ', JSON.stringify(req.goalData));
	if (!req.goalData) {
		outputJSON = {
			'status': 'failure',
			'messageId': 203,
			'message': constantObj.messages.errorRetreivingData
		};
	} else {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.successRetreivingData,
			'data': req.goalData
		}
	}
	res.jsonp(outputJSON);
};

/**
 * Update Goal
 * Input: 
 * Output: goals json object
 * Developer : Rajesh
 */

exports.update = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var goal = req.goalData;

	//console.log(req.body.patient);
	goal.patient = req.body.patient;
	goal.clinician = req.body.clinician;
	goal.clinic = req.body.clinic;
	goal.title = req.body.title;
	goal.enable = req.body.enable;
	goal.created_date = req.body.created_date;
	goal.modified_date = req.body.modified_date;
	goal.is_achieved = req.body.is_achieved;
	goal.is_deleted = req.body.is_deleted;
	goal.goal_type_id = req.body.goal_type_id;
	goal.goal_question = req.body.goal_question;
	goal.goal_type = req.body.goal_type;

	goal.save(function(err, data) {
		//console.log(err);
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += "\r\n" + err.errors[field].message;
						}
					} //for
					break;
			} //switch
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} //if
		else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userStatusUpdateSuccess
			};
		}
		res.jsonp(outputJSON);
	});
}

/* * Update  Goal Status*/
exports.bulkUpdateGoal = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	var roleLength = inputData.data.length;
	var bulk = goalsObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var answerData = inputData.data[i];
		var id = mongoose.Types.ObjectId(answerData.id);
		bulk.find({
			_id: id
		}).update({
			$set: answerData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
		res.jsonp(outputJSON);
	});
};

/*
 * Created By : Gurpreet Singh
 * Function to get the Caregiver or Patients who are not assigned to anyone(patinet/caregiver resp.).
 *
 *
 */
exports.getUserByGoal = function(req, res) {
	var outputJSON = "";
	var inputJSON = {};
	inputJSON = req.body;

	inputJSON.parent = req.user._id;

	//console.log(inputJSON);
	// return;
	userObj.find(inputJSON, function(err, data) {
		// console.log(err,data);
		// return;
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if (data.length) {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			} else {
				outputJSON = {
					'status': 'warning',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
				}
			}
		}
		res.jsonp(outputJSON);
	});
}

/**
 * List patient next session goals in web & mobile app
 * Developer : Gurpreet
 * used on State Of Mind screen - both @ web & mobile app.
 */
exports.getPatientDailyGoals = function(req, res) {
	var outputJSON = ""; var inputData = {};
	inputData.patient = req.body.patient;
	inputData.is_status = true;
	inputData.is_deleted = false;
	inputData.goal_type_id = 1;
	
	goalsObj.find(inputData, function(err, data) {
		//console.log('getPatientDailyGoals -- \n', err, data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}