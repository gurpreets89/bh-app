var userObj = require('./../../models/users/users.js');
var patientLogObj = require('./../../models/patientLog/patientLog.js');
var constantObj = require('./../../../constants.js');
var mongoose = require('mongoose');
var emailService = require('./../email/emailService.js');
var userInviteObj = require('./../../models/users/userInvites.js');

/**
 * Find role by id
 * Input: roleId
 * Output: Role json object
 * This function gets called automatically whenever we have a roleId parameter in route. 
 * It uses load function which has been define in role model after that passes control to next calling function.
 */
exports.user = function(req, res, next, id) {
	userObj.load(id, function(err, user) {
		if (err) {
			res.jsonp(err);
		} else if (!user) {
			res.jsonp({
				err: 'Failed to load role ' + id
			});
		} else {

			req.userData = user;
			//console.log(req.user);
			next();
		}
	});
};


/**
 * Show user by id
 * Input: User json object
 * Output: Role json object
 * This function gets role json object from exports.role 
 */
exports.findOne = function(req, res) {
	// console.log('in findOne = ', req, res);
	if (!req.userData) {
		outputJSON = {
			'status': 'failure',
			'messageId': 203,
			'message': constantObj.messages.errorRetreivingData
		};
	} else {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.successRetreivingData,
			'data': req.userData
		}
	}
	res.jsonp(outputJSON);
};

/**
 * Show user History by id
 * Input: User json object
 * Output: User json object
 */
exports.findUserHistory = function(req, res) {
	if (!req.params.id) {
		outputJSON = {
			'status': 'failure',
			'messageId': 203,
			'message': constantObj.messages.errorRetreivingData
		};
	} else {
		subscriptionObj.find({subscriber_id:req.params.id}).populate('subscriber_id plan_id').exec(function(err, data){
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.successRetreivingData,
			'data': data
		}
		});
	}
	res.jsonp(outputJSON);
};


/**
 * List all clinician object
 * Input: 
 * Output: User json object
 * Created By : Jatinder Singh
 * Date : 24 May 2016
 */
exports.list = function(req, res) {
	var outputJSON = "";
	var jsonData = {};
	jsonData.user_type = 2;
	jsonData.parent = req.user._id;
	//console.log("jsonData = ", jsonData);
	userObj.find(jsonData, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
			res.jsonp(outputJSON);
		} else {
			if(data.length > 0){
				// Find Patients/Caregivers count under clincian
				getUserCount(data, data.length, 0, res);
			}else{
				outputJSON = {
					'status': 'nodata',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
				res.jsonp(outputJSON);
			}
		}
	}).lean();
}

// function to get count of users for particular clinician id.
function getUserCount(data, length, index, res){
	var currentData = data[index];
//console.log('currentData = ', currentData);
	var inputJSONString = {};
	inputJSONString = {
        parent: currentData._id,
        user_type: { $in: [3, 4] }
    }
	userObj.find(inputJSONString, function(err1, data1) {
        if (err1) {
			//console.log('Error in getUserCount at ', index);
			data[index].user_count = 0;
        	if(index+1 < length){
            	//console.log('index = ',index);
            	getUserCount(data, length, ++index, res);
            }else{
            	outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
				res.jsonp(outputJSON);
            }
        } else {
        	//console.log('\nin else');
            data[index].user_count = data1.length;
            if(index+1 < length){
            	//console.log('index = ',index);
            	getUserCount(data, length, ++index, res);
            }else{ // last case
            	outputJSON = {
	                'status': 'success',
	                'messageId': 200,
	                'message': constantObj.messages.successRetreivingData,
	                'data': data
	            }
            	//console.log("New Data = ", JSON.stringify(data));
            	res.jsonp(outputJSON);
            }
        }
    });
}


/**
 * Create new user object
 * Input: User object
 * Output: User json object with success
 */
exports.add = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var userModelObj = {};

	//console.log(req.body);
	userModelObj = req.body;
	userObj(userModelObj).save(req.body, function(err, data) {
		if (err) {
			switch (err.name) {
				case 'ValidationError':

					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += ", " + err.errors[field].message;
						}
					} //for
					break;
			} //switch

			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} //if
		else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userSuccess,
				'data': data
			};
		}

		res.jsonp(outputJSON);

	});

}


/**
 * Update user object
 * Input: User object
 * Output: User json object with success
 */
exports.update = function(req, res) {
	//console.log('update_user')
	var errorMessage = "";
	var outputJSON = "";
	var user = req.userData;
	user.company = req.body.company;
	user.first_name = firstLetterCapital(req.body.first_name);
	user.last_name = firstLetterCapital(req.body.last_name);
	user.email = req.body.email;
	user.user_name = req.body.user_name;
	if (req.body.userchangepassword) {
		user.password = req.body.password;
	}
	if (req.body.renewplan) {
		user.plan = req.body.plan;
	}
	user.display_name = req.body.display_name;
	user.role = req.body.role;
	user.enable = req.body.enable;
	user.save(function(err, data) {
		//console.log(err);
		//console.log(data);
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += "\r\n" + err.errors[field].message;
						}
					} //for
					break;
			} //switch
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} //if
		else {
			if (req.body.renewplan) {
				var subscriberModelObj = {};
				subscriberModelObj.subscriber_id = data._id;
				subscriberModelObj.plan_id = data.plan;
				subscriptionObj(subscriberModelObj).save();
			}
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userStatusUpdateSuccess
			};
		}
		res.jsonp(outputJSON);
	});
}


/**
 * Update user object(s) (Bulk update)
 * Input: user object(s)
 * Output: Success message
 * This function is used to for bulk updation for user object(s)
 * This function is in use of BH APP project.
 */
exports.bulkUpdateClinician = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	//console.log(inputData);
	var roleLength = inputData.data.length;
	var bulk = userObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var userData = inputData.data[i];
		var id = mongoose.Types.ObjectId(userData.id);
		bulk.find({
			_id: id
		}).update({
			$set: userData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
		res.jsonp(outputJSON);
	});
} 



/*
|-----------------------------------------------
| delete one Clinician, Developer : Gurpreet
|-----------------------------------------------
*/
exports.deleteClinician = function(req, res) {
		var outputJSON = "";
		userObj.remove({_id:req.body}, function(err, data) {
			if (err) {
				outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.clinicianDeleteError
				};
			} else {
				outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.clinicianDeleteSuccess,
				'data': data
				}
			}
			res.jsonp(outputJSON);
		});
}

/*
|-----------------------------------------------
| Check Clinician Patients, Developer : Gurpreet
|-----------------------------------------------
*/
	exports.checkClinicianPatients = function(req, res) {
		//console.log("----------------checkClinicianPatients------------\n", req.body, req.user);
		var outputJSON = ""; var jsonData = {}; var fields = {};
		jsonData.user_type = 3;
		jsonData.parent = req.body.parent;
		fields = {'username':1, 'email':1};
		userObj.find(jsonData, fields, function(err, data) {
			if (err) {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
			} else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			}
			res.jsonp(outputJSON);
		});
	}

/*
|-----------------------------------------------
|	Get Clinicians except current clinician
|	Developer : Gurpreet
|-----------------------------------------------
*/
	exports.getClinicians = function(req, res) {
		var outputJSON = ""; var jsonData = {}; var fields = {};
		jsonData.user_type = 2;
		jsonData.is_deleted = false;
		jsonData.is_status = true;
		jsonData.parent = req.user._id;
		jsonData._id = {$ne : req.body.parent};
		//console.log("jsonData = ", jsonData);
		fields = {'username':1, 'email':1};
		userObj.find(jsonData, fields, function(err, data) {
			if (err) {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
			} else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			}
			res.jsonp(outputJSON);
		});
	}

/*
|-----------------------------------------------
|	Update multiple Patient's Clinician
|	Developer : Gurpreet
|-----------------------------------------------
*/
	exports.updatePatientsClinician = function(req, res) {
		var searchJSON = {}; var updateJSON = {}; var outputJSON = {};
		searchJSON = {_id: {$in: req.body.patients}};
		updateJSON = {$set: {parent: req.body.clinician}};
		userObj.update(searchJSON, updateJSON, {multi: true}, function(err, data) {
			if (err){
			    //console.log(err);
			    outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.patientTransferError
			    };
			}
			else if(data){
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.patientTransferSuccess
				};
			}
			res.jsonp(outputJSON);
		});
		
	}
	
/*
|-----------------------------------------------
|	Get Patients info before transferring.
|	Developer : Gurpreet
|-----------------------------------------------
*/
	exports.getPatientsInfo = function(req, res) {
		var outputJSON = ""; var jsonData = {}; var fields = {};
		jsonData.user_type = 3;
		jsonData.parent = req.body.parent;
		jsonData._id = {$in : req.body.patients};
	//console.log("jsonData = ", jsonData);
		fields = {};
		userObj.find(jsonData, fields, function(err, data) {
			if (err) {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
			} else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			}
			res.jsonp(outputJSON);
		});
	}
	
/*
|-----------------------------------------------------------------------------
|	log multiple Patient's info before transferring to another clinician.
|	Developer : Gurpreet
|-----------------------------------------------------------------------------
*/
	exports.logPatientsInfo = function(req, res) {
		//console.log("logPatientsInfo = ", req.body, req.user); //return;
		var outputJSON = "";
		if (req.body.patients.length > 0){
		    for (var i=0; i < req.body.data.length; i++) {
			var inputJSON = {};
			var inputData = req.body.data[i];
			// console.log("inputData = ", inputData);
			inputJSON = inputData;
			inputJSON.patient_id = inputData._id;
			inputJSON.modified_by = req.user.first_name + ' ' + req.user.last_name;
			inputJSON.modified_by_id = req.user._id;
			delete inputJSON._id;
			patientLogObj(inputJSON).save(inputJSON, function(err, data) {
				if (err){
				    //console.log("error : ", err);
				}
				else if(data){
				    //console.log(err);
				}
			});
			if (i+1 == req.body.data.length){
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData
				}
				res.jsonp(outputJSON);
			}
		    }
		}
	}

function firstLetterCapital(string) { return string.charAt(0).toUpperCase() + string.slice(1); }

/**
 * Send a new invite to out-patient
 * Input: Patient object
 * Output: User json object with success
 * developer : Gurpreet Singh
 */
exports.invite = function(req, res) {
	var errorMessage = ""; var outputJSON = ""; var userModelObj = {};
	userModelObj.user_email = req.body.email;

	/*Check user table for email exits or not code starts*/
	userObj.find(req.body, function(err2, data2) {
		if (err2) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
			res.jsonp(outputJSON);
		} else {
			if (data2.length > 0) {
				outputJSON = {
					'status': 'warning',
					'messageId': 200,
					'message': constantObj.messages.warningRetreivingUser
				}
				res.jsonp(outputJSON);
				//console.log("HERE only");
			}else{
				/*Check invited table email exits or not code starts*/
				userInviteObj.find(userModelObj, function(err, data) {
					if (err) {
						outputJSON = {
							'status': 'failure',
							'messageId': 203,
							'message': constantObj.messages.errorRetreivingData
						};
						res.jsonp(outputJSON);
					} else {
						//console.log(data.length);
						if (data.length > 0) {
							if(data[0].is_signed_up == true){
								//console.log("USER is already invited and successfully signed up.");
								outputJSON = {
									'status': 'warning',
									'messageId': 200,
									'message': constantObj.messages.warningRetreivingUser
								}
								res.jsonp(outputJSON);
							}else{
								var inviteData = {};
								inviteData.reference_code = randomReferenceCode(6);
								inviteData.created_date = new Date();
								inviteData.modified_date = new Date();
								userInviteObj.update({ _id: data[0]._id }, { $set: inviteData }, function(err1, resp) {
									if(err1){
										//console.log("ERROR in Invited clinician save.");
										outputJSON = {
											'status': 'warning',
											'messageId': 200,
											'message': constantObj.messages.warningRetreivingUser
										}
										res.jsonp(outputJSON);
									}
									else{
										//console.log('Clinician Invitation saved ---------->', JSON.stringify(resp));
										/* Send Email to Clinician */
										var userDetails = {};
										userDetails.email = userModelObj.user_email;
										userDetails.reference_code = inviteData.reference_code;
										var logoUrl = "http://"+req.headers.host+"/#/users/signup";
										userDetails.app_link = "<a href='"+logoUrl+"'>Click Me</a>";
										var frm = 'BH app<noreply@bh-app.com>';
										var emailSubject = 'Welcome to BH app!';
										var emailTemplate = 'invite_clinician.html';
										emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
										outputJSON = {
											'status': 'success',
											'messageId': 200,
											'message': constantObj.messages.invitationResent
										};
										res.jsonp(outputJSON);
									}
								});
							}
						} else {
							//console.log("USER is not invited or not signed up yet", JSON.stringify(data));

							/* add invited user to UserInvites Table */
							var inviteData = {};
							inviteData.user_email = userModelObj.user_email;
							inviteData.parent = req.user._id;
							inviteData.user_type = 'clinician';
							inviteData.reference_code = randomReferenceCode(6);
							inviteData.is_signed_up = false;
							userInviteObj(inviteData).save(function(err1, resp){
								if(err1){
									//console.log("ERROR in Invited clinician save.");
									outputJSON = {
										'status': 'warning',
										'messageId': 200,
										'message': constantObj.messages.warningRetreivingUser
									}
									res.jsonp(outputJSON);
								}
								else{
									//console.log('Clinician Invitation saved ---------->', JSON.stringify(resp));
									/* Send Email to Clinician */
									var userDetails = {};
									userDetails.email = userModelObj.user_email;
									userDetails.reference_code = inviteData.reference_code;
									var logoUrl = "http://"+req.headers.host+"/#/users/signup";
									userDetails.app_link = "<a href='"+logoUrl+"'>Click Me</a>";
									var frm = 'BH app<noreply@bh-app.com>';
									var emailSubject = 'Welcome to BH app!';
									var emailTemplate = 'invite_clinician.html';
									emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
									outputJSON = {
										'status': 'success',
										'messageId': 200,
										'message': constantObj.messages.invitationSent
									};
									res.jsonp(outputJSON);
								}
							});
						}
					}
				});
			}
		}
	})
}

/**
 * List all invited clinicians
 * Output: User json object
 * Created By : Gurpreet
 */
exports.listInvitedUsers = function(req, res) {
	var outputJSON = "";
	var inputJSON = {};
	inputJSON.parent = req.user._id;
	inputJSON.user_type = 'clinician';
	userInviteObj.find(inputJSON, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	}).populate('parent');
}

/*
    |-------------------------------------------
    |   Generate Random Reference Code
    |   developer : RaJesh Thakur
    |-------------------------------------------
*/
function randomReferenceCode(length) {
    var chars = "123456789";
    var value = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        value += chars.charAt(i);
    }
    return value;
}