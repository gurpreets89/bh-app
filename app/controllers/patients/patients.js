var userObj = require('./../../models/users/users.js');
var userInviteObj = require('./../../models/users/userInvites.js');
var diseaseObj = require('./../../models/disease/disease.js');
var patientAssessmentObj = require('./../../models/patientAssessment/patientAssessment.js');
var checkinObj = require('./../../models/checkin/checkin.js');
var patientProbability = require('./../../models/patientProbability/patientProbability.js');
var constantObj = require('./../../../constants.js');
var emailService = require('./../email/emailService.js');
var mongoose = require('mongoose');
var CryptoJS = require("crypto-js");

/**
 * Find role by id
 * Input: roleId
 * Output: Role json object
 * This function gets called automatically whenever we have a roleId parameter in route. 
 * It uses load function which has been define in role model after that passes control to next calling function.
 */
exports.user = function(req, res, next, id) {
	userObj.load(id, function(err, user) {
		if (err) {
			res.jsonp(err);
		} else if (!user) {
			res.jsonp({
				err: 'Failed to load role ' + id
			});
		} else {

			req.userData = user;
			//console.log(req.user);
			next();
		}
	});
};


/**
 * List all patients
 * Input: 
 * Output: User json object
 * Created By : RaJesh
 * Date : 27 May 2016
 */
exports.list = function(req, res) {
	var outputJSON = "";
	var inputJSON = {};
	var clinician_id = req.user._id;
	inputJSON.parent = clinician_id;
	inputJSON.user_type = {
		'$in': [3, 4]
	};
	if (typeof req.params.id != 'undefined') {
		var filterId = req.params.id;
		inputJSON.last_name = new RegExp(filterId, 'i');
	}
	userObj.find(inputJSON, function(err, data) {
		//console.log("list = ", err,data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
			res.jsonp(outputJSON);
		} else {
			/** get patients missed check-in data **/
			getMissedCheckins(data, res, 0);

		}
	}).populate('caregiver', 'first_name last_name username email user_type patient_type').populate('caregivers_patient_id', 'first_name last_name username email dob emr_no user_type patient_type').lean();
}

/**
Private function to get Patients check in data & calculate missed check ins.
developer : Gurpreet
**/
function getMissedCheckins(data, res, n) {
	if (n < data.length) {
		var pData = data[n]; var inputJSON = {};
		//console.log("\n pData = ", pData.created_date);
		var cDate = pData.created_date;
		var cd = new Date(cDate);
		cd.getDate();
		//console.log("\n cd = ", cd);
		inputJSON.patient = pData._id;
		var d = new Date();
		d.setDate(d.getDate() - 9);
		//console.log("\n d = ", d);
		var newdate = '';
		if (d.getTime() < cd.getTime()) {
			//console.log("Date is <<<< then created date!");
			var newdate = (cd.getMonth() + 1) + '/' + cd.getDate() + '/' + cd.getFullYear();
		}else{
			//console.log("Date is >>> then created date!");
			var newdate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
		}
		//console.log('newdate = ', newdate);
		inputJSON.checkin_date = {
			$gt: newdate
		};
		//console.log("inputJSON = ", inputJSON);
		checkinObj.find(inputJSON, {
			'checkin_date': 1
		}, function(err, resp) {
			//console.log("checkinObj resp = ", err, resp);
			var allDates = [];
			for (var i = 0; i < 10; i++) {
				var d = new Date();
				d.setDate(d.getDate() - 9);
				d.setDate(d.getDate() + i);
				var tempdate2 = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
				//console.log("comparions = ", d, cd);
				if (d.getTime() >= cd.getTime()) {
					allDates.push(tempdate2);
				}
			}
			//console.log("allDates = \n", allDates);
			if (resp.length > 0) {
				for (var y = 0; y < resp.length; y++) {
					if (allDates.indexOf(resp[y].checkin_date) > -1) {
						allDates.splice(allDates.indexOf(resp[y].checkin_date), 1);
					}
				}
			}
			var missedDates = [];
			for (var z = 0; z < allDates.length; z++) {
				var d = new Date(allDates[z]);
				var d1 = new Date(allDates[z]);
				d.setDate(d.getDate() + 1);
				var newdate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
				//console.log("current = ",allDates[z] ," -----newdate----- ",newdate);
				d1.setDate(d1.getDate() - 1);
				var olddate = (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
				//console.log("-----olddate-----", olddate);
				if (z == 0 && newdate == allDates[z + 1]) {
					missedDates.push(allDates[z]);
				} else if (z > 0 && (z + 1 < allDates.length) && (olddate == allDates[z - 1] || newdate == allDates[z + 1])) {
					missedDates.push(allDates[z]);
				} else if (olddate == allDates[z - 1]) {
					missedDates.push(allDates[z]);
				}
			}
			//console.log(">>> missedDates = \n", missedDates);
			data[n].missedCheckIns = missedDates;
			n++;
			getMissedCheckins(data, res, n);
		});
	} else {
		var outputJSON = "";
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.successRetreivingData,
			'data': data
		}
		res.jsonp(outputJSON);
	}
}


/*
 * Created By : Gurpreet Singh
 * Function to get the Caregiver or Patients who are not assigned to anyone(patinet/caregiver resp.).
 *
 *
 */

exports.patientType = function(req, res) {
	var outputJSON = "";
	var inputJSON = {};
	inputJSON = req.body;

	inputJSON.parent = req.user._id;
	if (req.body.user_type == 3) {
		inputJSON.caregiver = null;
	} else if (req.body.user_type == 4) {
		inputJSON.caregivers_patient_id = null;
	}
	//console.log(inputJSON);
	userObj.find(inputJSON, function(err, data) {
		// console.log(err,data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if (data.length) {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			} else {
				outputJSON = {
					'status': 'warning',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
				}
			}
		}
		res.jsonp(outputJSON);
	});
}


/**
 * Save patient assessment data
 * Input: patient assessment data object
 * gurpreet
 */
exports.saveAssessment = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	questionnaireModelObj = req.body;
	//console.log("req.body ------------------------------------- ", req.body); //return;
	patientAssessmentObj(questionnaireModelObj).save(req.body, function(err, data) {
		//console.log("------",err, data);
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += ", " + err.errors[field].message;
						}
					}
					break;
			}
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
			res.jsonp(outputJSON);
		} else {
			/** step 1 : calculate severity score **/
			//console.log("saveAssessment = ", data); //return;
			calculateSeverity(data, res);
			//outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.userSuccess, 'data': data};
		}
		//console.log('outputJSON = ', outputJSON);
		//res.jsonp(outputJSON);
	});
}

/**
 *	calculate severity
 *	developer : gurpreet
 **/
function calculateSeverity(data, res) {
	//console.log("hi = ", data);
	var new_answers = {};
	var results = {};
	if (data.answers) {
		for (var index in data.answers) {
			//console.log("\n--", index);
			var ans = data.answers[index];
			var tracker = [];
			tracker['duration'] = 0;
			tracker['impairment'] = 0;
			// alert(tracker);
			//console.log("\nans = ", ans);
			//console.log("\nnew_answers = ", new_answers);

			if(typeof ans.disease != "undefined"){
				if (!new_answers[ans.disease]) { // item not found
					// new_answers.push(ans.disease);
					new_answers[ans.disease] = [];
					new_answers[ans.disease].push(ans);
					results[ans.disease] = [];
					results[ans.disease].disease_name = ans.disease_name;
					results[ans.disease].patient_score = 0;
					results[ans.disease].total_score = 0;
					results[ans.disease].patient_score += parseInt(ans.answer_score);
					results[ans.disease].total_score += 3;
					results[ans.disease].threshold_screening = 0;
					results[ans.disease].threshold_followup = 0;
					results[ans.disease].threshold_duration = 0;
					results[ans.disease].threshold_impairment = 0;
					results[ans.disease].meet_criteria_count = 0;
					if (ans.question_type == 1 && ans.answer_score > 1) { //screening
						results[ans.disease].threshold_screening = 1;
					} 
					else if (ans.question_type == 2 && ans.answer_score > 1) { //follow-up
						results[ans.disease].threshold_followup = 1;
					} 
					else if (ans.question_type == 3 && ans.answer_score >= 2) { //duration
						results[ans.disease].threshold_duration = 1;
						if (!tracker['duration'] && results[ans.disease].meet_criteria_count == 1) {
							results[ans.disease].meet_criteria_count += 1;
							tracker['duration'] = 1;
						} else if (!tracker['duration'] && results[ans.disease].meet_criteria_count == 0) {
							results[ans.disease].meet_criteria_count = 1;
							tracker['duration'] = 1;
						}
					} 
					else if (ans.question_type == 4 && ans.answer_score > 0) { //impairment
						results[ans.disease].threshold_impairment = 1;
						if (!tracker['impairment'] && results[ans.disease].meet_criteria_count == 1) {
							results[ans.disease].meet_criteria_count += 1;
							tracker['impairment'] = 1;
						} else if (!tracker['impairment'] && results[ans.disease].meet_criteria_count == 0) {
							results[ans.disease].meet_criteria_count = 1;
							tracker['impairment'] = 1;
						}


					}

				} 
				else {
					var key1 = ans.disease;
					//console.log("key1 = ", key1);
					new_answers[ans.disease].push(ans);
					//results[ans.disease]=[];
					results[ans.disease].patient_score += parseInt(ans.answer_score);
					results[ans.disease].total_score += 3;
					if (ans.question_type == 1 && ans.answer_score > 1) { //screening
						results[ans.disease].threshold_screening += 1;
					} 
					else if (ans.question_type == 2 && ans.answer_score > 1) { //follow-up
						results[ans.disease].threshold_followup += 1;
					} 
					else if (ans.question_type == 3 && ans.answer_score >= 2) { //duration
						results[ans.disease].threshold_duration += 1;
						/*if(!tracker['duration']){
							results[ans.disease].meet_criteria_count += 1;
							tracker['duration'] = 1;
						}*/
						if (!tracker['duration'] && results[ans.disease].meet_criteria_count == 1) {
							results[ans.disease].meet_criteria_count += 1;
							tracker['duration'] = 1;
						} 
						else if (!tracker['duration'] && results[ans.disease].meet_criteria_count == 0) {
							results[ans.disease].meet_criteria_count = 1;
							tracker['duration'] = 1;
						}

					} 
					else if (ans.question_type == 4 && ans.answer_score > 0) { //impairment
						results[ans.disease].threshold_impairment += 1;
						/*if(!tracker['impairment']){
							results[ans.disease].meet_criteria_count += 1;	
							tracker['impairment'] = 1;
						}*/
						if (!tracker['impairment'] && results[ans.disease].meet_criteria_count == 1) {
							results[ans.disease].meet_criteria_count += 1;
							tracker['impairment'] = 1;
						} else if (!tracker['impairment'] && results[ans.disease].meet_criteria_count == 0) {
							results[ans.disease].meet_criteria_count = 1;
							tracker['impairment'] = 1;
						}
					}
				}
			}
		}
	}
	//console.log("new_answers = ", new_answers);
	//console.log("results = ", results);
	var posterior_probability = 0;
	var j = 0;
	for (var index in results) {
		var val = results[index];
		results[index].severity_score = ((val.patient_score / val.total_score) * 100).toFixed(2);
		// for screening threshold
		results[index].threshold_screening_condition = 0;
		if (val.threshold_screening > 0) {
			results[index].threshold_screening_condition = 1;
			results[index].meet_criteria_count += 1;
		}
		// for other symptoms/follow up threshold
		results[index].threshold_followup_condition = 0;
		if (val.threshold_followup >= 3) {
			results[index].threshold_followup_condition = 1;
			results[index].meet_criteria_count += 1;
		}


	}
	//console.log("new results = ", results);
	calculateProbability(Object.keys(results), 0, Object.keys(results).length, results, data._id, res);

}


/**
 *	calculate probability
 *	developer : gurpreet
 **/

function calculateProbability(diseaseIdArray, currentIndex, length, results, assessmentId, res) {
	//console.log("assessmentId = ", assessmentId);
	var severityScore = 0;
	if (currentIndex < length) {
		var inputString = {};
		var diseaseData = {};
		var posterior_probability = 0;
		severityScore = results[diseaseIdArray[currentIndex]].severity_score;
		inputString = {
			"_id": diseaseIdArray[currentIndex]
		};
		diseaseObj.findOne(inputString, function(err, diseaseData) {
			if (diseaseData) {
				//console.log("diseaseData == \n", diseaseData, severityScore, diseaseData.high_severity_threshold);
				//High Severity Check
				if (severityScore >= parseInt(diseaseData.high_severity_threshold)) {
					//console.log("if", diseaseData.op_high_risk_posterior);
					posterior_probability = diseaseData.op_high_risk_posterior;
				}
				//Low Severity Check
				else if (severityScore >= parseInt(diseaseData.low_severity_threshold) && severityScore < parseInt(diseaseData.high_severity_threshold)) {
					//console.log("else if");
					posterior_probability = diseaseData.op_low_risk_posterior;
				} else {
					//console.log("else");
					posterior_probability = 0;
				}
				//console.log("posterior_probability = ", posterior_probability);
				results[diseaseIdArray[currentIndex]].posterior_probability = posterior_probability;
				//console.log("results => ", results);
				//console.log("currentIndex => ", currentIndex, "length => ", length);
			}

			currentIndex++;
			calculateProbability(diseaseIdArray, currentIndex, length, results, assessmentId, res);

		}).lean();

	} else {
		//console.log('------------------\n');
		//console.log('Result: ',results);
		var outputResults = [];
		for (x in results) {
			var tempObj = {};
			var result = results[x];
			results[x].disease = x;
			results[x].is_diagnosed = false;
			if (result.meet_criteria_count == 4) {
				results[x].is_diagnosed = true;
			}

			tempObj.disease = x;
			tempObj.severity_score = results[x].severity_score;
			tempObj.posterior_probability = results[x].posterior_probability;
			tempObj.is_diagnosed = results[x].is_diagnosed;

			outputResults.push(tempObj);
		}
		//console.log('New Result: ',results);
		//console.log('outputResults == ',outputResults);
		if (outputResults) { // update to patient Assessment.
			var outputJSON = "";
			patientAssessmentObj.update({
				"_id": assessmentId
			}, {
				$set: {
					"results": outputResults
				}
			}, function(err, data) {
				if (data) {
					outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': constantObj.messages.userSuccess,
						'data': data
					};
					res.jsonp(outputJSON);
				} else if (err) {
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': errorMessage
					};
					res.jsonp(outputJSON);
				}
				//console.log("update query", err, data);
			});
		}
	}
}



/**
 * Save patient severity score and assessment data
 * Input: patient assessment data object
 * Jatinder Singh
 */
exports.saveScoreAndProbability = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	ansObj = req.body;
	// console.log(ansObj);
	// return;

	patientAssessmentObj(ansObj).save(req.body, function(err, data) {
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += ", " + err.errors[field].message;
						}
					}
					break;
			}
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userSuccess,
				'data': data
			};
		}
		//console.log('outputJSON = ', outputJSON);
		res.jsonp(outputJSON);
	});
}

/**
 * Get Latest Probability of the patient
 * Input: patient id data object
 * Jatinder Singh
 */
exports.getLatestAssessmentOfPatient = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var reqObj = {};

	if (req.body.patient) {
		reqObj.patient = req.body.patient;
		reqObj.user = req.body.patient;
	}

	if (req.body._id) {
		reqObj._id = req.body._id;
	}

	var checkCaregiver = {};
	checkCaregiver._id = req.body.patient;

	//console.log("====>>>", reqObj)

	patientAssessmentObj.find(reqObj, function(err, data) {
			//console.log(err, data);
			if (err) {
				console.log("======1===============");
				console.log(err);
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
				res.jsonp(outputJSON);
			} else {
				if (data.length) {

					outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': constantObj.messages.successRetreivingData,
						'data': data
					}
					res.jsonp(outputJSON);
				} else {
					userObj.findOne(checkCaregiver, function(err2, data2) {
						if (err2) {
							console.log("======2===============");
							console.log(err2);
							outputJSON = {
								'status': 'failure',
								'messageId': 203,
								'message': constantObj.messages.errorRetreivingData
							};
							res.jsonp(outputJSON);
						} else {
							//console.log("DATA 2 ------------------>>", JSON.stringify(data2.caregiver));
							var dataToSend = {};
							dataToSend.patient = data2._id;
							dataToSend.user = data2.caregiver;
							patientAssessmentObj.find(dataToSend, function(err3, data3) {
								//console.log("DATA 3 ------------------>>", JSON.stringify(data3));
								if (err3) {
									console.log("======3===============");
									console.log(err3);
									outputJSON = {
										'status': 'failure',
										'messageId': 203,
										'message': constantObj.messages.errorRetreivingData
									};
								} else {
									outputJSON = {
										'status': 'success',
										'messageId': 200,
										'message': constantObj.messages.successRetreivingData,
										'data': data3
									};
								}
								res.jsonp(outputJSON);
							});
						}
					});
				}
			}
		})
		.populate('patient').populate('clinic').populate('results.disease', 'title').populate('answers.disease', 'title').sort({
			'_id': -1
		});
}


/**
 * Get History Probability of the patient
 * Input: patient id data object
 * Rajesh
 */
exports.getHistoryData = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var reqObj = {};
	reqObj.patient = req.body.patient;
	patientAssessmentObj.find(reqObj, function(err, data) {
		//console.log(err, data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if (data.length) {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				};
			} else {
				outputJSON = {
					'status': 'warning',
					'messageId': 200,
					'message': constantObj.messages.errorRetreivingData
				};
			}
		}
		res.jsonp(outputJSON);
	});
}

/**
 * Show Assessment by id
 */
exports.findOne = function(req, res) {
	//console.log("DATA ===> ", JSON.stringify(req.body));

	patientAssessmentObj.findOne(req.body, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}

/**
 * Save diagnose and target points for diseases
 * Input: patient diagnose and target data object
 * Jatinder Singh
 */
exports.saveDiagnoseTarget = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	ansObj = req.body;
	patientProbability(ansObj).save(req.body, function(err, data) {
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += ", " + err.errors[field].message;
						}
					}
					break;
			}
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.diagTargetSuccess,
				'data': data
			};
		}
		//console.log('outputJSON = ', outputJSON);
		res.jsonp(outputJSON);
	});
}

/*
	|------------------------------------------------------------
	| function : Filter Records on the basis of user type
	| developer :  RaJesh
	|------------------------------------------------------------
*/

exports.getPatientFilterList = function(req, res) {
	var outputJSON = "";
	var inputJSON = {};
	var clinician_id = req.user._id;
	inputJSON.parent = clinician_id;
	if (req.body.user_type) {
		inputJSON.user_type = req.body.user_type;
	}
	//console.log(inputJSON);
	userObj.find(inputJSON, function(err, data) {
		// console.log(err,data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			}
			res.jsonp(outputJSON);
		} else {
			/** get patients missed check-in data **/
			getMissedCheckins(data, res, 0);
		}
	}).populate('caregiver', 'first_name last_name username email user_type patient_type').populate('caregivers_patient_id', 'first_name last_name username email dob emr_no user_type patient_type').lean();
};


/**
 * Send a new invite to out-patient
 * Input: Patient object
 * Output: User json object with success
 * developer : Gurpreet Singh
 */



exports.inviteOutPatient = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var dataTosend = req.body;
	var userModelObj = {};
	userModelObj.user_email = req.body.email;
	var inputJSON = {};
	inputJSON.email = req.body.email;
 	/*Check user table for email exits or not code starts*/
	userObj.find(inputJSON, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
			res.jsonp(outputJSON);
		} else {
			if (data.length > 0) {
				// email already exist in DB
				outputJSON = {
					'status': 'warning',
					'messageId': 200,
					'message': constantObj.messages.warningRetreivingUser
				}
				res.jsonp(outputJSON);
			}else{
				/*Check invited table email exits or not code starts*/
				userInviteObj.find(userModelObj, function(err1, data1) {
					if (err) {
						outputJSON = {
							'status': 'failure',
							'messageId': 203,
							'message': constantObj.messages.errorRetreivingData
						};
						res.jsonp(outputJSON);
					} else {
						if (data1.length > 0) {
							if(data1[0].is_signed_up == true){
								//console.log("USER is already invited and successfully signed up.");
								outputJSON = {
									'status': 'warning',
									'messageId': 200,
									'message': constantObj.messages.warningRetreivingUser
								}
								res.jsonp(outputJSON);
							}else{
								var inviteData = {};
								inviteData.reference_code = randomReferenceCode(6);
								inviteData.user_type = dataTosend.user_type;
								inviteData.created_date = new Date();
								inviteData.modified_date = new Date();
								inviteData.emr_no = req.body.emr_no;
								userInviteObj.update({ _id: data1[0]._id }, { $set: inviteData }, function(err1, resp) {
									if(err1){
										outputJSON = {
											'status': 'warning',
											'messageId': 200,
											'message': constantObj.messages.warningRetreivingUser
										}
										res.jsonp(outputJSON);
									}
									else{
										//console.log('Patinet/Caregiver Invitation saved ---------->', JSON.stringify(resp));
										/* Send Email to Clinician */
										var userDetails = {};
										var textTosend = "";
										if(dataTosend.user_type == 'patient'){
											textTosend = "We are pleased to send you the registration email to join the BH app as an out-patient.";
										}else{
											textTosend = "We are pleased to send you the registration email to join the BH app as a caregiver.";
										}
										userDetails.email = userModelObj.user_email;
										userDetails.user_type = textTosend;
										userDetails.reference_code = inviteData.reference_code;
										userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
										var frm = 'BH app<noreply@bh-app.com>';
										var emailSubject = 'Welcome to BH app!';
										var emailTemplate = 'out_patient_signup.html';
										emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
										outputJSON = {
											'status': 'success',
											'messageId': 205,
											'message': constantObj.messages.invitationResent
										};
										res.jsonp(outputJSON);
									}
								});
							}
						}else{
							//console.log("USER is not invited or not signed up yet", JSON.stringify(data1));
							/* add invited user to UserInvites Table */
							var inviteData = {};
							inviteData.user_email = userModelObj.user_email;
							inviteData.parent = req.user._id;
							inviteData.user_type = dataTosend.user_type;
							inviteData.reference_code = randomReferenceCode(6);
							inviteData.is_signed_up = false;
							inviteData.emr_no = req.body.emr_no;
							userInviteObj(inviteData).save(function(err2, resp2){
								if(err2){
									outputJSON = {
										'status': 'warning',
										'messageId': 200,
										'message': constantObj.messages.warningRetreivingUser
									}
									res.jsonp(outputJSON);
								}
								else{
									//console.log('Patient/Caregiver Invitation saved ---------->', JSON.stringify(resp2));
									/* Send Email to Clinician */
									var userDetails = {};
									var textTosend = "";
									if(dataTosend.user_type == 'patient'){
										textTosend = "We are pleased to send you the registration email to join the BH app as an out-patient.";
									}else{
										textTosend = "We are pleased to send you the registration email to join the BH app as a caregiver.";
									}
									userDetails.email = userModelObj.user_email;
									userDetails.user_type = textTosend;
									userDetails.reference_code = inviteData.reference_code;
									userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
									var frm = 'BH app<noreply@bh-app.com>';
									var emailSubject = 'Welcome to BH app!';
									var emailTemplate = 'out_patient_signup.html';
									emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
									outputJSON = {
										'status': 'success',
										'messageId': 200,
										'message': constantObj.messages.invitationSent
									};
									res.jsonp(outputJSON);
								}
							});
						}
					}
				});
			}
		}
	})
};
// exports.inviteOutPatient = function(req, res) {
// 	var errorMessage = "";
// 	var outputJSON = "";
// 	var userModelObj = {};
// 	userModelObj = req.body;

// 	/*Check email exits or not code starts*/
// 	userObj.find(userModelObj, function(err, data) {
// 		if (err) {
// 			outputJSON = {
// 				'status': 'failure',
// 				'messageId': 203,
// 				'message': constantObj.messages.errorRetreivingData
// 			};
// 			res.jsonp(outputJSON);
// 		} else {
// 			if (data.length > 0) {
// 				outputJSON = {
// 					'status': 'warning',
// 					'messageId': 200,
// 					'message': constantObj.messages.warningRetreivingUser
// 						//'data': data
// 				}
// 				res.jsonp(outputJSON);
// 				/*Check email exits or not code ends*/
// 			} else {

// 				/* Send Email to out-Patient */
// 				var userDetails = {};
// 				userDetails.email = userModelObj.email;
// 				userDetails.reference_code = req.user._id;
// 				userDetails.app_link = "<a href='http://www.google.com'>Link</a>";
// 				// adminDetail.password = common.encrypt(userDetails.pass);
// 				var frm = 'BH app<noreply@bh-app.com>';
// 				var emailSubject = 'Welcome to BH app!';
// 				var emailTemplate = 'out_patient_signup.html';
// 				emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
// 				outputJSON = {
// 					'status': 'success',
// 					'messageId': 200,
// 					'message': constantObj.messages.userSuccess
// 				};
// 				/* add invited user to UserInvites Table */
// 				var inviteData = {};
// 				inviteData.user_email = userDetails.email;
// 				inviteData.parent = req.user._id;
// 				inviteData.user_type = 'patient';
// 				userInviteObj(inviteData).save(function(err, resp) {
// 					if(resp)
// 						console.log('...Patient Invitation saved.');
// 				});
// 				res.jsonp(outputJSON);
// 			}
// 		}
// 	});
// }


/**
 * List all invited users
 * Output: User json object
 * Created By : Gurpreet
 */
exports.listInvitedUsers = function(req, res) {
	var outputJSON = "";
	var inputJSON = {};
	inputJSON.parent = req.user._id;
	//inputJSON.user_type = 'patient';
	
	userInviteObj.find(inputJSON, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	}).populate('clinician');
}

/**
 * Update Patient By ID
 * Developer : Gurpreet Singh
 **/
exports.updatePatient = function(req, res) {
	// console.log("updateuser",req.body);
	// req.body.clinic_name=req.user.clinic_name;
	//    console.log(req.body);
	var errorMessage = "";
	var outputJSON = "";
	var searchCriteria = {};
	var updateData = {};
	var findUserModelObj = {};
	var decryptedData = CryptoJS.AES.decrypt(req.body.inputData, constantObj.messages.encryptionKey);
	req.body = '';
	req.body = decryptedData.toString(CryptoJS.enc.Utf8);
	req.body = JSON.parse(req.body);
	// req.body.clinic_name=req.user.clinic_name;
	// console.log(req.body);
	updateData = req.body;
	//console.log("updateData",updateData);
	// return;
	var editingUserId = req.body._id;
	searchCriteria = {'_id': req.body._id};
	updateData.first_name = firstLetterCapital(updateData.first_name);
	updateData.last_name = firstLetterCapital(updateData.last_name);

	delete updateData._id;
	userObj.update(searchCriteria, updateData, function(err, data1) {
	//console.log('\nuserObj.update = ',err, data1);
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += "\r\n" + err.errors[field].message;
						}
					} //for
					break;
			} //switch
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
			res.jsonp(outputJSON);
		} //if
		else {
			//console.log("else update\n", req.body);
			if (req.body.user_type == 3) { //If updating patient
				//console.log("CAREGIVER ------------------>>>>");
				//console.log(req.body.caregiver);
				if (typeof(req.body.caregiver) != 'undefined' && req.body.caregiver != null) { //If Caregiver selected
					if(req.body.caregiver._id){
						findUserModelObj._id = req.body.caregiver._id;
					}else{
						findUserModelObj._id = req.body.caregiver;
					}
					findUserModelObj.user_type = 4;
					findUserModelObj.parent = req.user._id;
					userObj.findOne(findUserModelObj, function(err, data2) {
						if (data2) {
							var updateData1 = {};
							updateData1.caregivers_patient_id = editingUserId;
							userObj.update({
								_id: data2._id
							}, {
								$set: updateData1
							}, function(err, data3) {
								if (data3) {
									outputJSON = {
										'status': 'success',
										'messageId': 200,
										'message': constantObj.messages.userSuccess
									};
									res.jsonp(outputJSON);
								}
							})
						}else{
							//console.log('err = ', err);
						}
					});
				} else { //If caregiver not selected
					outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': constantObj.messages.userSuccess
					};
					res.jsonp(outputJSON);
				}
			} else if (req.body.user_type == 4) { //If updating Caregiver
				if (typeof(req.body.caregivers_patient_id) != 'undefined' && req.body.caregivers_patient_id != null) { // If patient selected
					if(req.body.caregivers_patient_id._id){
						findUserModelObj._id = req.body.caregivers_patient_id._id;
					}else{
						findUserModelObj._id = req.body.caregivers_patient_id;
					}
					findUserModelObj.user_type = 3;
					findUserModelObj.parent = req.user._id;
					userObj.findOne(findUserModelObj, function(err, data2) {
						if (data2) {
							var updateData1 = {};
							updateData1.caregiver = editingUserId;
							userObj.update({
								_id: data2._id
							}, {
								$set: updateData1
							}, function(err, data3) {
								if (data3) {
									outputJSON = {
										'status': 'success',
										'messageId': 200,
										'message': constantObj.messages.userSuccess
									};
									res.jsonp(outputJSON);
								}
							})
						}
					});
				} else { // If patient not selected
					outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': constantObj.messages.userSuccess
					};
					res.jsonp(outputJSON);
				}
			}
		}
	});
}


/**
 * Get Patient Probability submitted data 
 * Input: patient id
 * Gurpreet Singh
 */
exports.getPatientProbability = function(req, res) {
	var outputJSON = "";
	var inputJSON = {};
	inputJSON.patient = req.body.patient;
	//console.log("inputJSON -------- ", inputJSON);
	patientProbability.find(inputJSON, function(err, data) {
		//console.log(err,data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	}).sort({'created_date':-1}).limit(5).populate('patient');
}

function firstLetterCapital(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Check clinician if appointment already available or Not
 * Input: parent, appointment date, appointment time
 * Rajesh
 */
exports.checkAppointmentForClinician = function(req, res) {
	var outputJSON = "";
	var inputJSON = {};
	inputJSON = req.body;
	
	//console.log("INPUT JSON -- >> ", JSON.stringify(inputJSON));
	userObj.find(inputJSON, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}


/*
    |-------------------------------------------
    |   Generate Random Reference Code
    |   developer : RaJesh Thakur
    |-------------------------------------------
*/
function randomReferenceCode(length) {
    var chars = "123456789";
    var value = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        value += chars.charAt(i);
    }
    return value;
}


/**
 *	Check if patient has already submitted the assessment or not.
 *	Input: patient id & date.
 *	Developer : GpSingh
**/
exports.checkAssessmentSubmitted = function(req, res) {
	var outputJSON = "";
	var _serachObj = {};
	var _pObj = {
		'patient': 1
	};
	_serachObj = req.body;
	if (typeof req.body.created_date != 'undefined') {
		_serachObj.created_date = {
			"$gte": new Date(req.body.created_date)
		};
	}
	//console.log('_serachObj ======================== ', _serachObj)
	patientAssessmentObj.find(_serachObj, _pObj, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if (data.length > 0) {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				};
			} else {
				outputJSON = {
					'status': 'warning',
					'messageId': 200,
					'message': constantObj.messages.errorRetreivingData
				};
			}
		}
		res.jsonp(outputJSON);
	});
}