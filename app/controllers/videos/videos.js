var videoObj = require('./../../models/video/video.js');
var constantObj = require('./../../../constants.js');
var mongoose = require('mongoose');


/**
 * List all Videos object in Admin section.
 * Input: 
 * Output: Video json object
 * Developer : RaJesh Thakur
 */


exports.getAllVideos = function(req, res) {
	var outputJSON = "";
	videoObj.find({}, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}


/**
 * Create new Video object
 * Input: Video object
 * Output: Video json object with success
 * developer : RaJesh Thakur
 */


exports.saveVideo = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var VideoModelObj = {};
	VideoModelObj = req.body;
	//console.log('body = ', req.body);
	if(typeof(req.body._id) != 'undefined'){
		var updateData = {};
    	updateData._id = req.body._id;
    	delete(req.body._id);
    	videoObj.update(updateData, {$set: req.body}, function(err, data) {
    		if (err) {
				//console.log('error in update answer option : ', err);
				switch (err.name) {
					case 'ValidationError':

						for (field in err.errors) {
							if (errorMessage == "") {
								errorMessage = field +""+ err.errors[field].message;
							} else {
								errorMessage += ", " +field+ ""+ err.errors[field].message;
							}
						} //for
						break;
				} //switch

				outputJSON = {
					'status': 'failure',
					'messageId': 401,
					'message': errorMessage
				};
			} //if
			else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.answerUpdateSuccess,
					'data': data
				};
			}
			// console.log('outputJSON = ', outputJSON);
			res.jsonp(outputJSON);

		});
	} else { // Add video
		videoObj(VideoModelObj).save(req.body, function(err, data) {
			if (err) {
				//console.log('error in add faq option : ', err);
				switch (err.name) {
					case 'ValidationError':

						for (field in err.errors) {
							if (errorMessage == "") {
								errorMessage = field +""+ err.errors[field].message;
							} else {
								errorMessage += ", " +field+ ""+ err.errors[field].message;
							}
						} //for
						break;
				} //switch
				outputJSON = {
					'status': 'failure',
					'messageId': 401,
					'message': errorMessage
				};
			}else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.answerAddSuccess,
					'data': data
				};
			}
			res.jsonp(outputJSON);
		});
	}
}



/**
* Show Video by id
* Input: Video json object
* Output: Video json object
* This function gets Video json object from exports.question 
* Developer : RaJesh Thakur
*/


exports.findOneVideo = function(req, res) {
	var outputJSON = "";
	var inputData = {};
	inputData._id = req.body._id;
	videoObj.findOne(inputData, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
};

/**
* Bulk Update Status of the Videos
* Input: Video json object
* Output: Video json object
* This function Update single or multiple Videos json object from exports.question 
* Developer : RaJesh Thakur
*/

exports.updateVideoStatus = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	var roleLength = inputData.data.length;
	var bulk = videoObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var videoData = inputData.data[i];
		var id = mongoose.Types.ObjectId(videoData.id);
		bulk.find({
			_id: id
		}).update({
			$set: videoData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
		res.jsonp(outputJSON);
	});
};



/**
* Delete the Video object
* Input: _id
* Created : RaJesh Thakur
* Output: Video json object with success
*/

exports.deleteVideo = function(req, res) {
	var outputJSON = ""; var search = {}; var updateData = {};
	search= {'_id' : req.body};
	updateData = {$set:{'is_deleted':true, 'is_status': false}};
	videoObj.update(search, updateData, function(err, data) {
		//console.log(err, data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.diseaseDeleteSuccess,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}

/**
 * Get Used Video Types
 * Developer : GpSingh
 * Output: Video json object
 */
exports.findVideoTypes = function(req, res) {
	var outputJSON = "";
	var query = {'is_deleted':false};
	var projection = {'video_for':1};
	videoObj.find(query, projection, function(err, data) {
		//console.log('data = ', data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}

/**
 * Get Video by Type Mobile App
 * Developer : RaJesh
 * Output: Video json object
 */
exports.getVideoByTypeForMobile = function(req, res) {
	var outputJSON = "";
	videoObj.find(req.body, function(err, data) {
		//console.log('data = ', data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}


//end.