var checkinObj = require('./../../models/checkin/checkin.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');


/**
 * Find checkin by id
 * Input : checkin id
 * Output: checkin json object
 * It uses load function which has been define in check in  model after that passes control to next calling function.
 */
exports.checkinData = function(req, res, next, id) {
	checkinObj.load(id, function(err, checkinData) {
		if (err) {
			res.jsonp(err);
		} else if (!checkinData) {
			res.jsonp({
				err: 'Failed to load role ' + id
			});
		} else {
			req.checkData = checkinData;
			next();
		}
	});
};

/**
 * List patient/caregiver checkin
 * Input: patient id
 * Output: checkin json object
 * Developed by : Rajesh
 * Modified by : Gurpreet
 */
exports.list = function(req, res) {
	var outputJSON = ""; var inputJSON = {};
	//console.log("inputJSON list ----------->>", req.body);
	inputJSON = req.body;
	checkinObj.find(inputJSON, function(err, data) {
		//console.log(err, data.length);
		// return;
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if (data.length) {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			} else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'No records',
					'data': data
				}
			}
		}
		res.jsonp(outputJSON);
	}).sort({
		'created_date': 1
	});
}


exports.add = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	checkinModelObj = req.body;
	//console.log(JSON.stringify(checkinModelObj));
	checkinObj(checkinModelObj).save(req.body, function(err, data) {
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += ", " + err.errors[field].message;
						}
					}
					break;
			}
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userSuccess,
				'data': data
			};
		}
		res.jsonp(outputJSON);
	});
}


/**
 * Update Check in 
 * Input: 
 * Output: check in data json object
 * Developer : Rajesh
 */

exports.update = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var checkinModelObj = req.checkData;
	checkinModelObj.patient = req.body.patient;
	checkinModelObj.checkin_date = req.body.checkin_date;
	checkinModelObj.general_metric = req.body.general_metric;
	checkinModelObj.static_metric = req.body.static_metric;
	checkinModelObj.weekly_metric = req.body.weekly_metric;
	checkinModelObj.daily_metric = req.body.daily_metric;
	checkinModelObj.save(function(err, data) {
		//console.log(err);
		if (err) {
			switch (err.name) {
				case 'ValidationError':
					for (field in err.errors) {
						if (errorMessage == "") {
							errorMessage = err.errors[field].message;
						} else {
							errorMessage += "\r\n" + err.errors[field].message;
						}
					} //for
					break;
			} //switch
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} //if
		else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userStatusUpdateSuccess
			};
		}
		res.jsonp(outputJSON);
	});
}

/**
 * get Weekly metric Data for patient/caregiver checkin
 * Developed by : Gurpreet
 */
exports.getWeeklyData = function(req, res) {
	var outputJSON = ""; var inputJSON = {};
	inputJSON = req.body;
	checkinObj.find(inputJSON, function(err, data) {
		//console.log('getWeeklyData = ', err, data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if (data.length) {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			} else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'No records'
				}
			}
		}
		res.jsonp(outputJSON);
	}).sort({'created_date': 1});
}