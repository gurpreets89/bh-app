var questionObj = require('./../../models/question/question.js');
var categoryObj = require('./../../models/categories/categories.js');
var answerObj = require('./../../models/answer/answer.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');
var patientAssessmentObj	= require('./../../models/patientAssessment/patientAssessment.js');
var diseaseObj = require('./../../models/disease/disease.js');
var userObj = require('./../../models/users/users.js');
var extraQuestionObj = require('./../../models/extraQuestion/extraQuestion.js');

/**
 * List all disease questions object
 * Input: 
 * Output: Disease questions json object
 * Created: Jatinder Singh
 * Currently used in BH application
 */
exports.diseaseQuestionlist = function(req, res) {
	var outputJSON = "";
	//console.log("disease id is ",req.body);
	questionObj.find(req.body, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if(data.length == 0){
				outputJSON = {
					'status': 'success',
					'messageId': 199,
					'message': constantObj.messages.successRetreivingData
				}
			} else {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successRetreivingData,
					'data': data
				}
			}
		}
		res.jsonp(outputJSON);
	});
}


exports.exportFile = function(req, res){
    
    var json2csv = require('json2csv');
    var fs = require('fs');
    var whereJSON = {};
    whereJSON.is_deleted=false;
    questionObj.find(whereJSON, function(errDetail, dataDetail){
        if(errDetail){
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        }
        else{

            var fields = ['question', 'question_type', 'question_for', 'answers'];
            var mainData = new Array();
            var answerData = new Array();

            for(i=0;i<dataDetail.length;i++){
                var obj = {};
                obj.answers='';


                
                for(j=0;j<dataDetail[i].answers.length;j++){
                   
                  	if(j==dataDetail[i].answers.length-1){
                  		obj.answers +=dataDetail[i].answers[j].answer_text;
                  	}else{
                  		obj.answers +=dataDetail[i].answers[j].answer_text +",";
                  	}
                    

                }
               
                switch(dataDetail[i]['question_type']) {
					    case 1:
					        obj.question_type='screening';
					        break;
					    case 2:
					        obj.question_type='followup';
					        break;
					    case 3:
					        obj.question_type='duration';
					        break;
					    case 4:
					        obj.question_type='Impairment';
					        break;
					    case 5:
					        obj.question_type='Age of onset';
					        break;
				}

				switch(dataDetail[i]['question_for']) {
					    case 1:
					        obj.question_for='patient';
					        break;
					    case 2:
					        obj.question_for='caregiver';
					  
				}
                
              
                obj.question = dataDetail[i]['question'];
                mainData.push(obj);
            }
            var csv = json2csv({ data: mainData, fields: fields});
            fs.writeFile('public/questionlist.csv', csv, function(err){
                if(err){
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': err
                    };
                }
                else{
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                    };
                }
                res.jsonp(outputJSON);
            });
        }
    });
}


       /**
	* Find question by id
	* Input: questionId
	* Output: Question json object
	* This function gets called automatically whenever we have a questionId parameter in route. 
	* It uses load function which has been define in role model after that passes control to next calling function.
	*/
	exports.question = function(req, res, next, id) {
       	questionObj.load(id, function(err, question) {
       	//console.log("in load function");
	       if (err){
		       res.jsonp(err);
	       }
	       else if (!question){
		       res.jsonp({err:'Failed to load question ' + id});
	       }
	       else{
		       
		       req.questionData = question;
		       //console.log(req.user);
		       next();
	       }
       });
	};


       /**
	* Show question by id
	* Input: Question json object
	* Output: Question json object
	* This function gets question json object from exports.question 
	*/
	exports.findOne = function(req, res) {
	       if(!req.questionData) {
		       outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
	       }
	       else {
		       outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		       'data': req.questionData}
	       }
	       res.jsonp(outputJSON);
	};




/**
 * Create new question object
 * Input: Question object
 * Output: Question json object with success
 */
exports.add = function(req, res) {
	var outputJSON = "";
	var errorMessage = "";
	var inputJsonString = req.body;
	// inputJsonString.clinic = req.user._id;

       //console.log('req.body = ', req.body); //return;
       questionData = req.body;
       //console.log('inputJsonString = ', inputJsonString); //return;
       questionObj(questionData).save(inputJsonString, function(err, data) {

		//console.log('------------------', err, data);
		if (err) {
			//console.log('error in add question : ', err);
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} //if
		else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userSuccess,
				'data': data
			};
		}
		//console.log('outputJSON = ', outputJSON);
		res.jsonp(outputJSON);
	});
};

/**
 * List all the question object
 * Input: Null
 * Created :Jatinder Singh
 * Output: Question json object with success
 */
exports.allQuestionlist = function(req, res) {
	var outputJSON = "";
	// questionObj.find({is_deleted:false},{"disease":1}, function(err, data) {
	questionObj.find({is_deleted:false}, function(err, data) {
		//console.log("the list is",data);
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData,
				'err' : err
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	}).populate('disease').sort('question_type');
}


    /**
	* List all questions as per required input
	* Created :Gurpreet Singh
	* Output: Question json object with success
	*/
    exports.questionslist = function(req, res) {
	    var outputJSON = ""; var inputJSON = {};
		if(req.body.user_id && req.body.assessment_type && req.body.assessment_type != 'basic'){
			console.log("HERE --------------->>>"); 
			var errorMessage = "";
		    var reqObj = {};
		    reqObj.patient = req.body.user_id;
		    var reqObj2 = {};
		    reqObj2.question_for = req.body.question_for;
			// Find diagnose diseases based on patient ID
			console.log("reqObj", reqObj);
		    patientAssessmentObj.find(reqObj, function(err, data) {
		    	if (err) {
					outputJSON = {
						'status': 'failure',
						'messageId': 203,
						'message': constantObj.messages.errorRetreivingData
					};
					res.jsonp(outputJSON);
				} else {
					if(data.length != 0){
						var diseaseId = [];
						for(var i = 0; i < data.length; i++){
							for(var j = 0; j < data[i].results.length; j++){
								if(data[i].results[j].is_diagnosed == true){
									//console.log(data[i].results[j].disease);
									diseaseId.push(data[i].results[j].disease);
								}
							}
						}
						reqObj2.disease = { $in: diseaseId };
						reqObj2.is_deleted = false;
						reqObj2.is_status = true;
						console.log("HERE --------------->>", JSON.stringify(reqObj2));
						questionObj.find(reqObj2, function(err, data) {
						    if (err) {
							    outputJSON = {
								     'status': 'failure',
								     'messageId': 203,
								     'message': constantObj.messages.errorRetreivingData
							    }
						    } else {
							     outputJSON = {
								     'status': 'success',
								     'messageId': 200,
								     'message': constantObj.messages.successRetreivingData,
								     'data': data
							    }
							}
							console.log("outputJSON", outputJSON);
						    res.jsonp(outputJSON);
					      }).populate('disease').sort({'disease':1, 'question_type' : 1});
					}
				}
		    })
		}else{
			console.log("HERE ELSE --------------->>>"); 
			var inputData = {}; var findParams = "";
			inputData.is_deleted = false;
			inputData.is_status = true;
			//console.log(JSON.stringify(inputData));
			findParams = {'title':1};
			userObj.findOne({'_id':req.body.user_id}).exec(function(err, data){
			//diseaseObj.find(inputData, findParams, function(err, data){
				userObj.findOne({'_id':data.parent}).populate('parent').exec(function(errClinic, dataClinic){
					if(errClinic){
						outputJSON = {
							 'status': 'failure',
							 'messageId': 203,
							 'message': constantObj.messages.errorRetreivingData
						}
					}
					else{
							/* find questions on basis of diseases ids */
						inputJSON = req.body;
						inputJSON.is_deleted = false;
						inputJSON.is_status = true;
						if (dataClinic == null) {
							return res.status(200).send({ 'status': 'failure', 'messageId': 203, 'message': constantObj.messages.errorRetreivingData })
						  } else {
							inputJSON.disease = { $in: dataClinic.parent.disease };
						  }						delete inputJSON.assessment_type;
						delete inputJSON.user_id;
				//		console.log("inputJSON = ", inputJSON);
						questionObj.find(inputJSON, function(err, data) {
							//console.log("=========", err, data);
							if (err) {
								outputJSON = {
									 'status': 'failure',
									 'messageId': 203,
									 'message': constantObj.messages.errorRetreivingData
								}
							} else {
								
								var queryJSON = {};
								queryJSON.clinic = dataClinic.parent._id;
								queryJSON.is_deleted = false;
								queryJSON.is_status = true;
								extraQuestionObj.find(queryJSON, function(errExtra, dataExtra) {
									if (err) {
										outputJSON = {
											'status': 'failure',
											'messageId': 203,
											'message': constantObj.messages.errorRetreivingData,
											'err' : err
										};
									} 
									else {
										for(eq = 0; eq<dataExtra.length;eq++){
											var obj = {};

											// CODE TO GET HIGHEST MARKS OF ANY QUESTION
											var higestmarks = 0;
											for(ans=0;ans<dataExtra[eq].answers.length;ans++){
												if(higestmarks < dataExtra[eq].answers[ans]['answer_value']){
													higestmarks = dataExtra[eq].answers[ans]['answer_value'];
												}
											}
											//END OF CODE TO GET HIGHTEST MARKS OF ANY QUESTION

											obj._id = dataExtra[eq]._id;
											obj.highest_marks = higestmarks;
											obj.answers = dataExtra[eq].answers;
											obj.created_date = dataExtra[eq].created_date;
											obj.is_deleted = dataExtra[eq].is_deleted;
											obj.is_status = dataExtra[eq].is_status;
											obj.question = dataExtra[eq].question;
											obj.question_type = 6;
											obj.clinic = dataClinic.parent._id;
											console.log(obj);
											data.push(obj);

										}
										outputJSON = {
											'status': 'success',
											'messageId': 200,
											'message': constantObj.messages.successRetreivingData,
											'data': data
										}
									}
									res.jsonp(outputJSON);
								}).lean(); 

								/*
								outputJSON = {
									'status': 'success',
									'messageId': 200,
									'message': constantObj.messages.successRetreivingData,
									'data': data
								}
								*/
							}
							//res.jsonp(outputJSON);
						}).populate('disease').sort({'disease':1, 'question_type' : 1}).lean();
					}
				});
			})
		}
    }
       
       /**
       * Delete the question object
       * Input: Null
       * Created :Jatinder Singh
       * Output: Question json object with success
       */
       exports.deleteQuestion = function(req, res) {
	       var outputJSON = "";
	       questionObj.remove({_id:req.body}, function(err, data) {
		       if (err) {
			       outputJSON = {
				       'status': 'failure',
				       'messageId': 203,
				       'message': constantObj.messages.errorRetreivingData
			       };
		       } else {
			       outputJSON = {
				       'status': 'success',
				       'messageId': 200,
				       'message': constantObj.messages.questionDeleteSuccess,
				       'data': data
			       }
		       }
		       res.jsonp(outputJSON);
	       });
       }

			/*exports.add = function(req, res) {
				var outputJSON = "";
				var errorMessage = "";
				console.log(req.body);
				var answerLength = req.body.answers.length;
				console.log(answerLength);
				var tempAnswerId = []
				//if(answerLength){ 
					//console.log('HELL');
					//bulk = answerObj.collection.initializeOrderedBulkOp();
					/*for(var a = 0; a< answerLength; ++a){
						var answer = req.body.answers;
						var answerData = new answerObj({answer_text : answer[a].answer_text});
						if(req.body.correct_answer == a)
						var correctAnswer = answerData._id
						tempAnswerId.push(answerData._id)
						bulk.insert({"_id": answerData._id, "answer_text": answer[a].answer_text,created: new Date(), enable: true});
					}
					/*bulk.execute(function(err, result) {
						if(!err){
							var questionData = req.body;
							questionData.answers = tempAnswerId;
							questionData.correct_answer = correctAnswer;
							questionObj(questionData).save(function(err, data) {
								if(err) {
									outputJSON = {'status' : 'failure', 'messageId': 400, 'message' : constantObj.messages.questionnaireUpdateQuestionFailure};
								res.jsonp(outputJSON);
								}
								else {
									console.log(questionData.category);
									console.log(data._id)
									categoryObj.findByIdAndUpdate(
										questionData.category,
										{$push: {"questions": data._id}},
										function(err, model) {
											if(err){
												outputJSON = {'status' : 'failure', 'messageId': 400, 'message' : constantObj.messages.questionnaireUpdateQuestionFailure};
											}
											else{
												outputJSON = {'status' : 'success', 'messageId': 200, 'message' : constantObj.messages.questionSuccess};
											}
											res.jsonp(outputJSON);
										}

										);	
								}		    	
							});
						}
					});

		}
	}*/


		/**
		 * Update question object
		 * Input: Question object
		 * Output: Question json object with success
		 * Developer: Rajesh
		 * Date : 20-June-2016
		 */
		exports.update = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var questions = req.questionData;
		 	questions.disease = req.body.disease;
		 	questions.answers = req.body.answers;
		 	questions.question_type = req.body.question_type;
		    questions.question_for = req.body.question_for;
		    questions.question = req.body.question;
		    questions.is_status = req.body.is_status;

		    questions.save(function(err, data) {
		    if(err) {
		      switch(err.name) {
		        case 'ValidationError':
		        for(field in err.errors) {
		          if(errorMessage == "") {
		            errorMessage = err.errors[field].message;
		          }
		          else {              
		            errorMessage+="\r\n" + err.errors[field].message;
		          }
		              }//for
		              break;
		          }//switch
		          outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
		        }//if
		        else {
		          outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.userStatusUpdateSuccess};
		        }
		        res.jsonp(outputJSON);
		    });
		}



		 /**
		 * Update question object(s) (Bulk update)
		 * Input: Question object(s)
		 * Output: Success message
		 * This function is used to for bulk updation for question object(s)
		 */
		exports.bulkUpdate = function(req, res) {
				var outputJSON = "";
				var inputData = req.body;
				//console.log("inputData",inputData);
				var roleLength = inputData.data.length;
				var bulk = questionObj.collection.initializeUnorderedBulkOp();
				for (var i = 0; i < roleLength; i++) {
				var questionData = inputData.data[i];
				var id = mongoose.Types.ObjectId(questionData.id);
				bulk.find({
					_id: id
				}).update({
					$set: questionData
				});
				}
				bulk.execute(function(data) {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.questionSuccess
				};
				res.jsonp(outputJSON);
				});
		} 



			
		/**
		 * Get Answer list
		 * Input: questionId
		 * Output: Success message and question object
		 * This function is used to for answer list for question object(s)
		 */
			exports.getanswerlist = function(req, res) {
				var outputJSON ="";
				var questionID = req.body._id;
				questionObj.findById(questionID, function(err, data) {
					if(err) {
						switch(err.name) {
							case 'ValidationError':
							for(field in err.errors) {
								if(errorMessage == "") {
									errorMessage = err.errors[field].message;
								}
								else {							
									errorMessage+="\r\n" . field.message;
								}
								}//for
								break;
						}//switch

						outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
					}//if
					else {
						outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.questionAnswerSuccess, data: data.answers};
					}
					res.jsonp(outputJSON);
				})
			}
			
