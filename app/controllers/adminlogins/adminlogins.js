// var adminLoginObj = require('./../../models/adminlogins/adminlogin.js');
var userObj = require('./../../models/users/users.js');
var forgotPasswordObj=require('./../../models/forgotPassword/forgotPassword.js');
var emailService = require('./../email/emailService.js');
var constantObj = require('./../../../constants.js');
var nodemailer = require('nodemailer');
var qs = require('querystring') , 
	fs = require('fs');
var request = require('request');
var jwt = require('jwt-simple');
var moment = require('moment');
var CryptoJS = require("crypto-js");

//authentication response functionality
/*Created By: Jatinder Singh
* Date : 23 may 2016
*/
exports.authenticate = function(req, res , done) {
	// console.log("req" , req.id , JSON.stringify(req.user)   /*, res , done*/ );
	var data = req.user; 
	var user_type  = data.user.user_type;
	var outputdataJSON = "";
	if(user_type == 2){ //Clinician User
		outputdataJSON = {'messagID' : 200};
		if(data.user.is_approved == true){ //Approved Account
			outputdataJSON = {
					'status':'approved',
					'userType': 2,
					'messageId':200, 
					'message':constantObj.messages.clinicianApprovedLogin,
					"data":data,
					"access_token": req.user.token
			};
		} else if(data.user.is_approved == false) { //Disapproved/ Yet to be approved account
			outputdataJSON = {
					'status':'disapproved',
					'userType': 2,
					'messageId':200,
					'message':constantObj.messages.clinicianDisapprovedLogin,
					"data":data,
					"access_token": req.user.token
			};
		}
	} else if(user_type == 1) { //Web Admin of Clinician
		outputdataJSON = {
			'messageId':200,
			"data":data,
			"access_token": req.user.token,
			"userType" : 1
		};
		if(data.user.is_status == true){ //Active Account
			outputdataJSON.status = "success";
			outputdataJSON.message = constantObj.messages.WebAdminSuccessfulLogin;
		} else if(data.user.is_status == false) { //In Active account
			outputdataJSON.status = "inactive";
			outputdataJSON.message = constantObj.messages.WebAdminInactiveError;
		}
	} else if(user_type == 5) { //Super Admin
			outputdataJSON = {
					'status':'success',
					'userType': 5,
					'messageId':200,
					'message':constantObj.messages.WebAdminSuccessfulLogin,
					"data":data,
					"access_token": req.user.token
			};
	}else{
		outputdataJSON = {
				'status':'error',
				'messageId':200,
				'message':constantObj.messages.UnauthorizedAccessError
		};
	}
	res.jsonp(outputdataJSON);
	// res.jsonp({'status':status, 'messageId':messagID, 'message':message,"data":data ,"access_token": req.user.token});
}

/*
	Function: forgot password
	Created: Jatinder Singh
*/

exports.forgotPassword = function(req, res) {
	// console.log("ans is",req.body);
	// return;
	var outputJSON = "";
	userObj.findOne({
		email: req.body.email
	}, function(err, data) {
		// console.log("forgotPassword = ", err, data); //return;
		if (err) {
			// console.log(err);
			return res.send(err);
		} else {
			if (!data) {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.emailDoesnotExist
				}
				return res.send(outputJSON);
			}
			//console.log("username:", req.body.username);
			var token = createJWT(data);
			// console.log("token:::", token);
			// return;
			//console.log("Host ");
			var port = ""//process.env.PORT|| 3000 ; 
			var resetUrl = "http://"+ req.headers.host +"/#/reset-password";

				forgotPasswordObj.findOne({email: req.body.email}, function(err, dataExist) {
					//console.log("dataExist ---", JSON.stringify(dataExist));
					if(err){
						return res.send(err);		
					} else {
						if(dataExist){
							var updating = {};
							updating = {
								"token": token,
								"expirationPeriod": moment(new Date()).add(1, 'days').format(),
								"token_used" : false
							};


							forgotPasswordObj.update({"username":dataExist.username},{$set: updating},function(err, post) {
								// console.log("errros is",err,post);
								if (err) {
									var response = {
										"code": 401,
										"messageId":401,
										"messageText": "Token is already generated!"
									};
									return res.status(401).send(response);
								}

								if (post) {
									// console.log("inside post");
									var response = {
										"code": 200,
										"messageId": 200,
										"messageText": "email link sent to " + data.email + " !"
									}
									sendResetLinkEmail(data, resetUrl, token, req);
									return res.status(200).send(response)
								}
							});
						} else {
							var adding = new forgotPasswordObj({
								"userId":data._id,					
								"username": data.username,
								"email": data.email,
								"token": token,
								"expirationPeriod": moment(new Date()).add(1, 'days').format(),
								"token_used" : false
							});

							adding.save(function(err, post) {
								// console.log("errros is",err,post);
								if (err) {
									var response = {
										"code": 401,
										"messageId":401,
										"messageText": "Token is already generated!"
									};
									return res.status(401).send(response);
								}

								if (post) {
									var response = {
										"code": 200,
										"messageId": 200,
										"messageText": "email link sent to " + data.email + " !"
									}
									sendResetLinkEmail(data, resetUrl, token, req);
									return res.status(200).send(response)
								}
							});
						}
					}
				});
			//});
		}
	});
}

/*
 |--------------------------------------------------------------------------
 | Generate JSON Web Token
 |--------------------------------------------------------------------------
 */
function createJWT(user) {
	var payload = {
		sub: user._id,
		iat: moment().unix(),
		exp: moment(new Date()).add(1, 'days').format()
	};
	// console.log("token", payload, constantObj.facebookCredentials.token_secret);
	return jwt.encode(payload, constantObj.facebookCredentials.token_secret);
}


// reset password from forgot password link email
exports.resetPassword = function(req, res) {
	
	//console.log("body:", req.body);
	// return;
	forgotPasswordObj.findOne({
		token: req.body.token
	}, function(err, data) {
		if (err || !data) {
			//console.log("Token not found");
			var response = {
				"code": 400,
				"messageText": "Can't process your request!"
			}
			return res.status(400).send(response);
		} else {
			if(data.token_used){
				var response = {
					"code": 400,
					"messageText": "Token is already used."
				}
				return res.status(400).send(response);
			}
			//console.log("now date:", moment().format());
			//console.log("expired date", moment(data.expirationPeriod).format());
			// return false;
			if (moment(data.expirationPeriod).format() < moment().format()) {//if token expire
				//console.log("Token is expired!");
				var response = {
					"code": 400,
					"messageText": "Token is expired!"
				}
				forgotPasswordObj.remove({
					"token": req.body.token
				}, function(err, data) {
					if (err) {
						var response = {
							"code": 420,
							"messageText": "Error Occurred"
						};
						return res.status(420).send(response);

					} else {
						var response = {
							"code": 400,
							"messageText": "Token is Expired! Try Again"
						};
						return res.status(400).send(response);

					}
					

				})


			} else { // If token is not expired
				//console.log("password", req.body);
				var userName = data.username;
				userObj.update({
					username: data.username
				}, {
					$set: {
						"password": req.body.password
					}
				}, function(err, data) {
					if (err) {
						outputJSON = {
							'sForgottatus': 'failure',
							'messageId': 203,
							'message': constantObj.messages.userStatusUpdateFailure
						}
						return res.status(203).send(err);
					}
					if (data) {
						outputJSON = {
							'status': 'success',
							'messageId': 200,
							'message': constantObj.messages.userStatusUpdateSuccess
						}
						//return res.status(200).send(outputJSON);
						/*forgotPasswordObj.remove({
							"token": req.body.token
						}, function(err, data) {
							if (err) {
								var response = {
									"code": 400,
									"messageText": "error deleting the token"
								};
								return res.status(400).send(response);
							} else {
								var response = {
									"code": 200,
									"messageText": "Token Successfully Deleted"
								};
								return res.status(200).send(outputJSON);
							}
						})*/
						
						// update token used status to true.
						forgotPasswordObj.update({
							username: userName
						}, {
							$set: {
								"token_used": true
							}
						}, function(err, data) {
							if (err) {
								var response = {
									"code": 400,
									"messageText": "Error updating the token."
								};
								return res.status(400).send(response);
							} else {
								var response = {
									"code": 200,
									"messageText": "Token Successfully Updated"
								};
								return res.status(200).send(response);
							}
						})
					}
				})
			}
		}
	})
}



exports.forgotUsername = function(req, res) {
	var outputJSON = "";
	userObj.findOne({
		email: req.body.email
	}, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if (data) {
				var transporter = nodemailer.createTransport({
					service: constantObj.gmailSMTPCredentials.service,
					auth: {
						user: constantObj.gmailSMTPCredentials.username,
						pass: constantObj.gmailSMTPCredentials.password
					}
				});

				transporter.sendMail({
					from: 'rajatg@smartdatainc.net',
					to: data.email,
					subject: 'Your Username',
					text: data.username
				});

				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successSendingForgotPasswordEmail
				}
			} else {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
			}
		}
		res.jsonp(outputJSON);
	});
}

// Developer : RaJesh
exports.changePassword = function(req, res) {
	var outputJSON = "";
	var updatePassword = {};
	updatePassword.password = req.body.newpassword;
	var newpassword = req.body.newpassword;
	//console.log(newpassword);
	userObj.findOne({
		_id: req.body._id
	}, function(err, data) {
		if(err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		}else {
			//console.log("changePassword = ", JSON.stringify(data));
			if(data.password){
				var decryptedData = CryptoJS.AES.decrypt(data.password.toString(), constantObj.messages.encryptionKey);
				var decryptedOldPassword = decryptedData.toString(CryptoJS.enc.Utf8);
			}

			var decryptedData1 = CryptoJS.AES.decrypt(newpassword.toString(), constantObj.messages.encryptionKey);
			var decryptedNewPassword = decryptedData1.toString(CryptoJS.enc.Utf8);

			if (decryptedOldPassword == JSON.stringify(req.body.password)) {
				if(decryptedOldPassword == decryptedNewPassword){
					// Old password and new password can't be same
					outputJSON = {
						'status': 'warning',
						'messageId': 201,
						'message': constantObj.messages.oldNewPasswordSameError
					};
					res.jsonp(outputJSON);
				}else{
					userObj.update({"_id": req.body._id},{$set: updatePassword}, function (err, result) {
				        //console.log("changed password success");
				        outputJSON = {
							'status': 'success',
							'messageId': 200,
							'message': constantObj.messages.passwordChangedSuccess
						};
						res.jsonp(outputJSON);
	    			});
				}
			} else {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.oldPasswordIncorrect
				};
				res.jsonp(outputJSON);
			}
		}
	});
}

function sendResetLinkEmail(data, resetUrl, token, req){
	var userDetails = {};
    userDetails.email = data.email;
    userDetails.full_name = data.first_name + " " + data.last_name;
    userDetails.app_link = resetUrl + "/" + token;
    var frm = 'BH app<noreply@bh-app.com>';
    var emailSubject = 'BH App - Password Reset Link';
    var emailTemplate = 'passwordreset.html';
    emailService.send(userDetails, emailSubject, emailTemplate, frm, req);
}