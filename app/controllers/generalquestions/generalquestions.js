var generalquestionObj = require('./../../models/generalquestions/generalquestions.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');


exports.generalQuestion = function(req, res, next, id) {
   generalquestionObj.load(id, function(err, generalQuestion) {
    if (err){
      //console.log("here1");
      res.jsonp(err);
    }
    else if (!generalQuestion){
      res.jsonp({err:'Failed to load role ' + id});
    }
    else{
      req.generalData = generalQuestion;
      next();
    }
  });
};

exports.patientGeneralQuestion = function(req, res) {
  var outputJSON = "";
  //console.log(req.body);
  generalquestionObj.find(req.body, function(err, data) {
    //console.log(err);
    if(err) {
      outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
    }
    else {
      outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
      'data': data}
    }
    res.jsonp(outputJSON);
  });
}


  /**
   * add general question
   * Input: 
   * Output: general question json object
   * Developer : Rajesh
   */


   exports.add = function(req, res) {
      var errorMessage = "";
      var outputJSON = "";
      obj = req.body;
      //console.log(JSON.stringify(obj));
      generalquestionObj(obj).save(req.body, function(err, data) {
        if(err) {
          switch(err.name) {
            case 'ValidationError':
            for(field in err.errors) {
              if(errorMessage == "") {
                 errorMessage = err.errors[field].message;
              }
              else {                   
                 errorMessage+=", " + err.errors[field].message;
              }
            }
            break;
          }
          outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
        }
        else {
          outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.userSuccess};
        }
        res.jsonp(outputJSON);
      });
   }
   /**
   * Update Goal
   * Input: 
   * Output: goals json object
   * Developer : Rajesh
   */

exports.updatepatientGeneralQuestion = function(req, res) {
  var errorMessage = "";
  var outputJSON = "";
  var general = req.generalData;

  //console.log(req.body);
  general.patient = req.body.patient;
  general.is_alcohal_drugs = req.body.is_alcohal_drugs;
  general.is_exercise = req.body.is_exercise;
  general.is_caffeine = req.body.is_caffeine;
  general.save(function(err, data) {
    //console.log(err);
    if(err) {
      switch(err.name) {
        case 'ValidationError':
        for(field in err.errors) {
          if(errorMessage == "") {
            errorMessage = err.errors[field].message;
          }
          else {              
            errorMessage+="\r\n" + err.errors[field].message;
          }
              }//for
              break;
          }//switch
          outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
        }//if
        else {
          outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.userStatusUpdateSuccess};
        }
        res.jsonp(outputJSON);
   });
}
