var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dailyMetricSchema = new mongoose.Schema({
	goal: {type: Schema.Types.ObjectId, ref: 'Goal'},
	goal_question: {type : String},
	checked: {type: Boolean}
});
var weeklyMetricSchema = new mongoose.Schema({
	goal: {type: Schema.Types.ObjectId, ref: 'Goal'},
	goal_question: {type : String},
	rating: {type:Number}
});
var staticMetricSchema = new mongoose.Schema({
	id: {type: Number},
	title: {type:String},
	rating: {type:Number}
});
var generalMetricSchema = new mongoose.Schema({
	id : {type: Number},
	title: {type:String},
	checked: {type: Boolean, default: false}
});

var checkinSchema = new mongoose.Schema({
	patient:{type:Schema.Types.ObjectId, ref:'User'},
	daily_metric: [dailyMetricSchema],
	weekly_metric: [weeklyMetricSchema],
	static_metric: [staticMetricSchema],
	general_metric: [generalMetricSchema],
	checkin_date: {type:String},
	submitted_by: {type:String}, // {value =  patient or caregiver (in lowercase only)}
	created_date: {
        type: Date,
        default: Date.now
    },
    modified_date: {
        type: Date,
        default: Date.now
	}
});

checkinSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).populate('plan')
    .exec(cb);
};

var checkinObj = mongoose.model('Patient_CheckIn', checkinSchema);
module.exports = checkinObj;