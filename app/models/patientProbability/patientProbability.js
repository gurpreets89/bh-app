var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var diagnoseDiseases = new mongoose.Schema({
    disease                 : {type:Schema.Types.String, ref:'Disease'},
    disease_title           : {type:String},
    severity_score          : {type:Number},
    posterior_probability   : {type:Number},
});
var targetDiseases = new mongoose.Schema({
    disease                 : {type:Schema.Types.String, ref:'Disease'},
    disease_title           : {type:String},
    severity_score          : {type:Number},
    posterior_probability   : {type:Number},
});

var keyfactors = new mongoose.Schema({
    keyfactor_title         : {type:String},
    checked                 : {type: Boolean}
});

var patientProbability = new mongoose.Schema({
    patient                 : {type:Schema.Types.String, ref:'User'},
    patientassessment       : {type:Schema.Types.String, ref:'patientassessment'},
    diagnose_checked        : [diagnoseDiseases],
    target_checked          : [targetDiseases],
    keyfactor_checked       : [keyfactors],
    submit_date             : {type:String},
    other_data              : {type:String},
    created_date            : {type:Date},
    modified_date           : {type:Date}
});

patientProbability.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    })
   .populate('patient')
    .exec(cb);
};

var patientProbabilityObj = mongoose.model('patientProbability' , patientProbability);
module.exports = patientProbabilityObj;