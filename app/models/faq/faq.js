var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var faqSchema = new mongoose.Schema({
	faq_question: {type:String, required : 'Please enter the faq question.'},
	faq_answer: {type: String,  required : 'Please enter the faq answer.'},
	is_status: {type: Boolean, default: false},
	is_deleted : {type : Boolean, default : false},
	created_date: {
		type: Date,
		default: Date.now
	},
    modified_date: {
        type: Date,
        default: Date.now
	}
});

var faqObj = mongoose.model('Faq', faqSchema);
module.exports = faqObj;