var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var goalSchema = new mongoose.Schema({
	patient:{type:Schema.Types.ObjectId, ref:'User'},
	clinician:{type:Schema.Types.ObjectId, ref:'User'},
	clinic: {type: Schema.Types.ObjectId, ref: 'User'},
	user_type: {type:Number},
	title:{type:String},
	//description:[{type:String}],
	goal_type_id:{type:Number},
	goal_type:{type:String},
	goal_question:{type:String},
	is_achieved:{type:Boolean, default:false},
	is_status: {type: Boolean, default: false},
	is_deleted : {type : Boolean, default : false},
	created_date: {
        type: Date,
        default: Date.now
    },
    modified_date: {
        type: Date,
        default: Date.now
	}
});

goalSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).populate('patient')
    .exec(cb);
};

var goalObj = mongoose.model('Goal', goalSchema);
module.exports = goalObj;