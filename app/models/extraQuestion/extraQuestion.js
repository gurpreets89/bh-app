var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var extraAnswerSchema = new mongoose.Schema({
	answer_id: {type: Schema.Types.ObjectId, ref: 'Answer'},
	answer_text: {type:String},
	answer_value: {type: Number, default: 0} // score
});


var extraQuestionSchema = new mongoose.Schema({
	question:{type:String, required : 'Please enter the question name.'},
	clinic: {type: Schema.Types.ObjectId, ref: 'User'},
	answers: [extraAnswerSchema],
	is_status:{type:Boolean, default:false},
	is_deleted:{type:Boolean, default:false},
  	created_date: {
		type: Date,
		default: Date.now
	},
	modified_date: {
		type: Date,
		default: Date.now
	}
});



extraQuestionSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    })
    .populate('answers').populate('disease')
    .exec(cb);
};

var extraQuestionsObj = mongoose.model('ExtraQuestion', extraQuestionSchema);
module.exports = extraQuestionsObj;