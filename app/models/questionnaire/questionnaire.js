var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var questionnaireSchema = new mongoose.Schema({
	questionnaire_name : {type:String, unique: true, required : 'Please enter the questionnaire name.'},
	questionnaire_type: {
		type : Number,
		enum : [0, 1] //[0 = Basic, 1 = Follow up]
	},
	//questions: [{
	//	category: {type: Schema.Types.ObjectId, ref: 'category'},
	//        questions: [{type: Schema.Types.ObjectId, ref: 'Question'}]
	//}],
	questions: [{type: Schema.Types.ObjectId, ref: 'Question'}],
	clinician:{type:Schema.Types.ObjectId, ref:'User'},
	clinic: {type: Schema.Types.ObjectId, ref: 'User'},
	is_deleted : {type : Boolean, default : false},
	enable : {type : Boolean},
	created_date : {type : Date, default : Date.now},
	modified_date: {
            type: Date,
            default: Date.now
	}
});

questionnaireSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    })
    .populate('questions.category')
    .populate('questions.questions')
    .exec(cb);
};
//custom validations

questionnaireSchema.path('questionnaire_name').validate(function(value) {
  var validateExpression = /^[a-zA-Z\.\- ]*$/;
  return validateExpression.test(value);
}, "Please enter valid questionnaire name.");
  

questionnaireSchema.plugin(uniqueValidator, {message:'Questionnaire already exists.'});

var questionnaireObj = mongoose.model('Questionnaire' , questionnaireSchema);
module.exports = questionnaireObj;