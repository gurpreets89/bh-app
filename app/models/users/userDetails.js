var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var userDetailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  emr_number: {
    type: String,
    required: 'Please enter the emr number'
  },
  profile_img: {
    type: String,
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  modified_date: {
    type: Date,
    default: Date.now
  }
});

userSchema.statics.serializeUser = function(user, done) {
  // console.log("serializeUser" , user);
  done(null, user);
};

userSchema.statics.deserializeUser = function(obj, done) {
  done(null, obj);
};

userSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).populate('plan')
    .exec(cb);
};

var userDetailObj = mongoose.model('UserDetail', userDetailSchema);
module.exports = userDetailObj;