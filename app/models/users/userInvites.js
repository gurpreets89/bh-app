var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var userInviteSchema = new Schema({
  user_email    : { type: String},
  parent        : { type:Schema.Types.ObjectId, ref: 'User'},
  user_type     : { type:String}, //either 'patient' or 'clinician'
  created_date  : { type: Date, default: Date.now},
  modified_date : { type: Date, default: Date.now},
  reference_code: { type: String},
  emr_no        : {type:String},
  is_signed_up  : { type: Boolean, default: false}
});

userInviteSchema.statics.serializeUser = function(user, done) {
    done(null, user);
};

userInviteSchema.statics.deserializeUser = function(obj, done) {
    done(null, obj);
};

userInviteSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).exec(cb);
};

var userInviteObj   = mongoose.model('UserInvite', userInviteSchema);
module.exports      = userInviteObj;