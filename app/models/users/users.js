var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var diseaseSpecialities = new mongoose.Schema({
  disease: {type: String, ref: 'Disease'}
});



var userSchema = new Schema({
  first_name: {
    type: String,
    required: 'Please enter the first name.'
  },
  last_name: {
    type: String,
    required: 'Please enter the last name.'
  },
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: 'Please enter the email.'
  },
  username: {
    type: String, 
    unique: true,
    required: 'Please enter the username.'
  },
  password: {
    type: String,
   // select: false,
    required: 'Please enter the password.'
  },
  user_type: {
    type: Number,
    enum: [1, 2, 3, 4, 5]   //[WebAdmin(1), clinician(2), patient(3)/caregiver(4)], Superadmin(5)(if saas based)]
  },
  disease:[{
	type:Schema.Types.ObjectId,
	ref:'Disease'
  }],
  patient_type: {
    type:Number,
    enum: [0, 1] //[in(0)/out(1)][if user_type = patient]
  }, 
  parent: {
    type:Schema.Types.ObjectId,
    ref: 'User'
  }, // if user_type = clinician then it will be Web-Admin id; if user_type = patient then it will be Clinician_id
  hospital_id: {
    type:Schema.Types.ObjectId,
    ref: 'User'
  },
  caregiver: {
    type:Schema.Types.ObjectId,
    ref: 'User',
    default: null
  }, // only if user is an IN PATIENT.
  caregivers_patient_id: {
    type:Schema.Types.ObjectId,
    ref: 'User',
    default:null
  }, // only if any patient assigned to caregiver user.
  dob: {
    type:Date, default:null
  },
  app_no: {
    type:Number,
    default: 0
  }, // [Auto Generated]
  emr_no: {
    type:String
  }, 
  clinic_name: {
    type: String,
    required: 'Please enter the clinic name.'
  },
  clinician_disease_specialities: {
    type: [diseaseSpecialities]
  },
  phone: {
    type: Number
  },
  country: {
    type: String,
    default: 'United States'
  },
  country_id: {
    type: Number,
    default: 0
  },
  country_code: {
    type: String
  },
  address: {
    type: String,
  },
  address1: {
    type: String,
  },
  address2: {
    type: String,
  },
  is_approved: {
    type: Boolean,
    default: false
  }, 
  is_status: {
    type: Boolean,
    default: false
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  is_appointment_showed: {
    type: String, // possible values [yes, no, noanswer]
  },
  next_appointment_date: {
    type: String,
  },
  next_appointment_time: {
    type: String,
  },
  next_assessment_date: {
    type: String,
  },
  is_assessment_completed:{
    type:Boolean,
    default:false
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  modified_date: {
    type: Date,
    default: Date.now
  },
  profile_pic : {type: String},
  device_id : {type: String}, // is used to get User's device ID
  platform_type: {type: String}, // is used to get User's device Platform (Android Or IOS)
  time_zone : {type: String}, // is used to get User's Time Zone
  next_assessment_available: { 
    type: Boolean,
    default: false
  },
  level_of_service: {
    type: String
  },
  assessment_type: {
    type: String
  },
  daily_reminder_time : {
    type: Date
  },
  weekly_reminder_time : {
    type: Date
  }/*,
  weekly_reminder_sent : {
    type: Boolean,
    default: false
  }*/
});

userSchema.statics.serializeUser = function(user, done) {
  // console.log("serializeUser" , user);
  done(null, user);
};

userSchema.statics.deserializeUser = function(obj, done) {
  done(null, obj);
};

userSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
  .populate('caregiver').populate('caregivers_patient_id')
    .exec(cb);
};





//custom validations

userSchema.path('first_name').validate(function(value) {
  var validateExpression = /^[ A-Za-z']*$/;
  return validateExpression.test(value);
}, "Please enter a valid first name.");


userSchema.path("last_name").validate(function(value) {
  var validateExpression = /^[ A-Za-z']*$/;
  return validateExpression.test(value);
}, "Please enter a valid last name.");

userSchema.path("email").validate(function(value) {
  var validateExpression = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return validateExpression.test(value);
}, "Please enter a valid email address.");

userSchema.path("username").validate(function(value) {
  validateExpression = /^[a-zA-Z0-9]*$/;
  return validateExpression.test(value);
}, "Please enter a valid user name");


userSchema.plugin(uniqueValidator, {
  //message: "Username/email already exists."
  message: " already exists."
});

var userObj = mongoose.model('User', userSchema);
module.exports = userObj;