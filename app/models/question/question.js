var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//var uniqueValidator = require('mongoose-unique-validator');

var answerSchema = new mongoose.Schema({
	answer_id: {type: Schema.Types.ObjectId, ref: 'Answer'},
	answer_text: {type:String},
	answer_value: {type: Number, default: 0} // score
	// enable: {type: Boolean, default: true},
	// is_deleted : {type : Boolean, default : false},
	// created_date: {
	// 	type: Date,
	// 	default: Date.now
	// },
});


var questionSchema = new mongoose.Schema({
	question:{type:String, required : 'Please enter the question name.'},
	clinic: {type: Schema.Types.ObjectId, ref: 'User'},
	disease: {type: String, ref: 'Disease'},
	question_type: {type:Number, enum: [1,2,3,4,5]}, //[1 =  screening, 2 = followup, 3 = duration, 4 = Impairment, 5 = Age of onset]
	question_for: {type:Number, enum: [1,2]}, //[1 =  patient, 2 = caregiver]
	//parent_question: {type: Schema.Types.ObjectId, ref: 'Question', default:null},
	//answers: [{type: Schema.Types.ObjectId, ref: 'Answer'}],
	answers: [answerSchema],
	is_status:{type:Boolean, default:false},
	//is_status:{type:Boolean, default:true},
	is_deleted:{type:Boolean, default:false},
  	created_date: {
		type: Date,
		default: Date.now
	},
	modified_date: {
		type: Date,
		default: Date.now
	}
});



questionSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    })
    .populate('answers').populate('disease')
    .exec(cb);
};

//questionSchema.plugin(uniqueValidator, {message:'Question already exists.'});

var questionsObj = mongoose.model('Question', questionSchema);
module.exports = questionsObj;