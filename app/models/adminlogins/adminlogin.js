var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var adminloginSchema = new Schema({
	username: String,
	password: String,
	firstName: String,
	lastName: String,
	email: String,
	planId: { //Link to plan model 
		type: String,
		default: null
	},
	type: {
		type: String,
		enum: [1, 2, 3]   // 1 SuperAdmin , 2 Subscribers , 3 Subscriber's Staff  
	},
	role:{ //Link to role model 
		type: String,
		default:null
	},
	isDeleted: {
		type: Boolean,
		default: false
	},
	enable: {
		type: Boolean,
		default: true
	},
	createdBy: { //Link to same model 
		type: String,
		default: null
	},
	updatedBy: { // Link to same model 
		type: String,
		default: null
	}

}, {
	collection: 'users'
});


adminloginSchema.statics.serializeUser = function(user, done) {
	done(null, user);
};

adminloginSchema.statics.deserializeUser = function(obj, done) {
	done(null, obj);
};

var adminlogin = mongoose.model('adminlogin', adminloginSchema);
module.exports = adminlogin;