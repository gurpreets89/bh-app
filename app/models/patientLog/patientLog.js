var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var diseaseSpecialities = new mongoose.Schema({
  disease: {type: String}
});

var patientLogSchema = new mongoose.Schema({
  patient_id: { type:String},
  first_name: { type: String },
  last_name: { type: String},
  email: { type: String},
  username: { type: String },
  password: { type: String},
  user_type: { type: Number},
  patient_type: {type:Number}, 
  parent: { type:String},
  hospital_id: {type:String},
  caregiver: { type:String},
  caregivers_patient_id: { type:String},
  dob: { type:Date},
  app_no: { type:Number},
  emr_no: { type:String },
  clinic_name: { type: String },
  clinician_disease_specialities: { type: [diseaseSpecialities] },
  phone: { type: Number },
  country: { type: String },
  country_id: { type: Number },
  country_code: { type: Number },
  address1: { type: String},
  address2: { type: String},
  is_approved: { type: Boolean}, 
  is_status: { type: Boolean },
  is_deleted: { type: Boolean },
  is_appointment_showed: { type: String },
  next_appointment_date: { type: String },
  next_appointment_time: { type: String },
  next_assessment_date: { type: String },
  profile_pic : {type: String},
  device_id : {type: String},
  platform_type: {type: String},
  next_assessment_available: { type: Boolean},
  level_of_service: { type: String},
  assessment_type: { type: String},
  created_date: { type: Date, default: Date.now },
  modified_date: { type: Date, default: Date.now },
  modified_by: { type:String },
  modified_by_id: { type:Schema.Types.ObjectId, ref: 'User' }
});

patientLogSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).exec(cb);
};

var patientLogObj = mongoose.model('patientLog', patientLogSchema);
module.exports = patientLogObj;