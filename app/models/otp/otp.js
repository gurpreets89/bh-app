var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var otpSchema = new mongoose.Schema({
	user: {type: Schema.Types.ObjectId, ref: 'User'},
	otp_code: {type: Number},
	generated_time: {
		type: Date
	},
	expiry_time: {
		type: Date
	}
});

var otpObj = mongoose.model('Otp', otpSchema);
module.exports = otpObj;