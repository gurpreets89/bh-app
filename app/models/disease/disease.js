var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var diseaseSchema = new mongoose.Schema({
	title: {type:String},
	description: {type: String},
	starting_probability_outpatient: {type: String},
	starting_probability_pediatrician: {type: String},
	low_severity_threshold: {type: String},
	high_severity_threshold: {type: String},
	low_dlr: {type: String},
	neutral_dlr: {type: String},
	high_dlr: {type: String},
	clinic: {type: Schema.Types.ObjectId, ref: 'User'},
	is_status: { type: Boolean, default: false},
	is_deleted: {type: Boolean, default: false},
	created_date: {type: Date, default: Date.now},
	modified_date: {type: Date, default: Date.now}
});

var diseaseObj = mongoose.model('Disease', diseaseSchema);
module.exports = diseaseObj;