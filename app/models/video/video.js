var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var videoSchema = new mongoose.Schema({
	video_url: {type:String, required : 'Please enter the video URL.'},
	video_for: {type:String, required : 'Please select a video type.'},
	is_status: {type: Boolean, default: false},
	is_deleted : {type : Boolean, default : false},
	created_date: {
		type: Date,
		default: Date.now
	},
    modified_date: {
        type: Date,
        default: Date.now
	}
});

var videoObj = mongoose.model('Video', videoSchema);
module.exports = videoObj;