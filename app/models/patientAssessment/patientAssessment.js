var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var assessmentAnswers = new mongoose.Schema({
    disease         : {type:Schema.Types.String, ref:'Disease'},
    question     	  : {type:Schema.Types.ObjectId, ref:'Question'},
    extra_question  : {type:Schema.Types.ObjectId, ref:'ExtraQuestion'},
    highest_score   : {type:Number},  // It will used in case of extra questions to calcualte average
    question_type   : {type:Number},
    disease_name    : {type:String},
    question_text  	: {type:String},
    answer_score   	: {type:Number},
    answer_text 	  : {type : String}
});

var diseaseResults = new mongoose.Schema({
	  disease                 : {type:Schema.Types.String, ref:'Disease'},//Mania
    severity_score       	  : {type:Number},
    posterior_probability   : {type:Number},
    is_diagnosed            : {type:Boolean}
});

var patientAssessment = new mongoose.Schema({
	  user        	  : {type:Schema.Types.ObjectId, ref:'User'}, //the user_id who is entering data
   	user_type		    : {type:Number}, //the user_type who is entering data
   	patient     	  : {type:String, ref:'User'}, // patient_id in both cases (patient/caregiver)
   	clinic         	: {type: Schema.Types.ObjectId, ref: 'User'},
   	created_date    : {type : Date, default : Date.now},
   	modified_date   : {type: Date, default: Date.now},
   	answers 		    : [assessmentAnswers],
   	results 		    : [diseaseResults],
    level_of_service: {type: String},
    assessment_type : { type: String}
});

patientAssessment.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    })
   .populate('patient').populate('clinic')
    .exec(cb);
};

var questionnaireObj = mongoose.model('patientAssessment' , patientAssessment);
module.exports = questionnaireObj;