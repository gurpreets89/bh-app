var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var generalquestionSchema = new mongoose.Schema({
	patient:{type:Schema.Types.ObjectId, ref:'User'},
	is_alcohal_drugs : {type: Boolean, default: false},
	is_exercise : {type: Boolean, default: false},
	is_caffeine : {type: Boolean, default: false}
});

generalquestionSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).populate('plan')
    .exec(cb);
};

var generalquestionObj = mongoose.model('GeneralQuestion', generalquestionSchema);
module.exports = generalquestionObj;