var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var answerSchema = new mongoose.Schema({
	answer_text: {type:String, required : 'Please enter the answer text.'},
	answer_value: {type: Number, default: 0, required : 'Please enter the score.'}, // score
	is_status: {type: Boolean, default: false},
	position: {type: Number},
	is_deleted : {type : Boolean, default : false},
	created_date: {
		type: Date,
		default: Date.now
	},
});

var answerObj = mongoose.model('Answer', answerSchema);
module.exports = answerObj;