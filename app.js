var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var db = require('./db.js');
var passport = require('passport');
//var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;

var userTokenObj = require('./app/models/users/userTokens.js');
var userLoginObj = require('./app/models/users/users.js');
var constantObj = require('./constants.js');
var tokenService = require('./app/services/tokenAuth.js');
var CryptoJS = require("crypto-js");

/*var routes = require('./routes/index');
var users = require('./routes/users');*/

var app = express();
app.use(cors());
app.use(passport.initialize());
app.use(passport.session());

//API Security - Browser Authentication/Basic Authentication
/*var users = [{username:'taxi', password:'application'}];
passport.use('basic',new BasicStrategy({}, function (username, password, done) {
    findByUsername(username, function(err, user) {
          console.log("The value of user is ",user.body);
          if (err) { return done(err); }
          if (!user) { return done(null, false); }
          if (user.password != password) { return done(null, false); }
          return done(null, user);
      });
  }
));*/

passport.use('bearer', new BearerStrategy(function(token, done) {
  //console.log('token = ',token);
  userTokenObj.findOne({
      token: token
    })
    .populate('user')
    .exec(function(err, user) {
    //console.log("User is ", JSON.stringify(user));
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }
      return done(null, user.user, {
        scope: 'all'
      });
    });
  // });
}));


/*function findByUsername(username, fn) {
  for (var i = 0, len = users.length; i < len; i++) {
    var user = users[i];
    if (user.username === username) {
      return fn(null, user);
    }
  }
  return fn(null, null);
}*/


//admin login
var LocalStrategy = require('passport-local').Strategy;
  passport.use('userObj',new LocalStrategy(
    function(username, password, done) {
      //console.log("1------",username , password) ; 
      userLoginObj.findOne({username:{$regex: username,'$options': 'i'}}, function(err, adminuser) {
        //console.log(adminuser);
       // console.log("----in LocalStrategy----" ,err,JSON.stringify(adminuser.password));
        if(err) {
               return done(err);
        }
        
        if(!adminuser) {
            return done(null, false);
        }
        var decryptedData = CryptoJS.AES.decrypt(adminuser.password.toString(), constantObj.messages.encryptionKey);
        
        var newdecryptedData = decryptedData.toString(CryptoJS.enc.Utf8);
        //if(newdecryptedData != password) {
        // console.log("2-------", newdecryptedData, JSON.stringify(password));
        if(newdecryptedData != JSON.stringify(password)) {
              return done(null, false, { message: 'Incorrect password.' });
        }

        //generate a token here and return 
        var authToken = tokenService.issueToken({sid: adminuser});
        // save token to db  ; 

        var tokenObj = new userTokenObj({
          "user":adminuser._id,          
          "token": authToken
        });

        tokenObj.save(function(e,s){
          // console.log(e,s) ; 
        });
      
        // console.log("auth Token is " , authToken );
        //returning specific data
        return done(null, {user: adminuser, token: authToken});
      });
    }
  ));

passport.serializeUser(userLoginObj.serializeUser);
passport.deserializeUser(userLoginObj.deserializeUser);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



/*app.use('/', routes);
app.use('/users', users);*/

require('./routes/adminlogin')(app, express, passport);
require('./routes/users')(app, express, passport);
//require('./routes/defaultSettings')(app, express, passport);
//require('./routes/permissions')(app, express, passport);
//require('./routes/categories')(app, express, passport);
require('./routes/questionnaires')(app, express, passport);
require('./routes/questions')(app, express, passport);
require('./routes/extraquestions')(app, express, passport);
require('./routes/answers')(app, express, passport);
require('./routes/clinicians')(app, express, passport);
require('./routes/diseases')(app, express, passport);
//require('./routes/answers')(app, express, passport);
require('./routes/faqs')(app, express,passport);
require('./routes/videos')(app, express,passport);
require('./routes/patients')(app, express, passport);
require('./routes/goals')(app, express, passport);
require('./routes/generalquestions')(app, express, passport);
require('./routes/checkin')(app, express, passport);
require('./routes/cron')(app, express);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
