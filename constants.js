const messages = {
	"errorRetreivingData": "Error occured while retreiving the data from collection",
	"emailDoesnotExist": "This email doesn't exist. Please provide a registered email.",
	"successRetreivingData": "Data retreived successfully from the collection",
	"warningRetreivingUser": "Username already exist in system", //used
	"warningRetreivingOtp": "Otp does not match.", //used
	"warningOtpExpiry": "Otp expired.", //used
	"UnauthorizedAccessError": "OOPS! You are not Authorized to access.",
	"invitationSent" : 'Invitation link to clinician has been sent successfully.',
	"invitationResent" : 'Invitation link to clinician has been resent successfully.',

	//forgot password
	"successSendingForgotPasswordEmail": "Password sent successfully",
	"oldNewPasswordSameError": "Old password and new password can't be same.",
	"passwordChangedSuccess": "Password changed successfully.",
	"oldPasswordIncorrect": "Old password is incorrect.",

	//clinician message
	"clinicianApprovedLogin": "Clinician has been logged successfully.",
	"clinicianDisapprovedLogin": "OOPS! Your account has not been approved. Please wait or get in touch with your administrator if problem persists.",
	"clinicianDeleteSuccess": "Clinician deleted successfully",
	"clinicianDeleteError": "Clinician can't be deleted now, please try again later.",

	//Web-Admin messages
	"WebAdminSuccessfulLogin": "Web-Admin has been logged in successfully.",
	"WebAdminInactiveError": "OOPS! Your account is not active. Please contact website's Super Administrator.",

	//appointmentType
	"appointmentTypeSuccess": "Appointment type has been saved successfully.",
	"appointmentTypeUpdateSuccess": "Appointment type has been updated successfully.",
	"appointmentTypeStatusUpdateFailure": "An error has been occurred while updating status.",
	"appointmentTypeStatusUpdateSuccess": "Appointment type status has been updated successfully.",
	"appointmentTypeDeleteFailure": "Error occured while deleting the appointment type.",
	"appointmentTypeDeleteSuccess": "Appointment type has been deleted successfully.",

	//default diagnosis 
	"diagnosisSuccess": "Diagnose has been saved successfully.",
	"diagnosisUpdateSuccess": "Diagnose has been updated successfully.",
	"diagnosisStatusUpdateFailure": "An error has been occurred while updating status.",
	"diagnosisStatusUpdateSuccess": "Diagnose status has been updated successfully.",
	"diagnosisDeleteFailure": "Error occured while deleting the diagnose.",
	"diagnosisDeleteSuccess": "Diagnose has been deleted successfully.",
	
	//default task 
	"taskSuccess": "Task has been saved successfully.",
	"taskUpdateSuccess": "Task has been updated successfully.",
	"taskStatusUpdateFailure": "An error has been occurred while updating status.",
	"taskStatusUpdateSuccess": "Task status has been updated successfully.",
	"taskDeleteFailure": "Error occured while deleting the Task.",
	"taskDeleteSuccess": "Task has been deleted successfully.",

	//patient message
	"diagTargetSuccess": "Diagnose and Target data saved successfully.",
	"inviteOutPatientSuccess": "Invitation link to Out-Patient has been sent successfully",
	"patientUpdateSuccess": "Patient updated successfully",
	"patientTransferSuccess": "Selected patients transferred successfully.",
	"patientTransferError": "Selected patients can't be transferred at the moment.",

	//user message
	"userSuccess": "User saved successfully",
	"userStatusUpdateFailure": "Error occured while updating Status",
	"userStatusUpdateSuccess": "User update successfully",
	"userDeleteFailure": "Error occured while deleting the user",
	"userDeleteSuccess": "User(s) deleted successfully",
	"userUpdateSuccess": "User updated successfully",
	"userNameExist": "Username already exist, Please try another.",
	"userEmailExist": "Email already exist, Please try another.",
	
	//disease messages
	"diseaseSuccess": "Disorder added successfully",
	"diseaseUpdateSuccess": "Disorder updated successfully",
	"diseaseDeleteSuccess": "Disorder deleted successfully",

	//Permission message
	"permissionSuccess": "Permission has been saved successfully.",
	"permissionStatusUpdateFailure": "An error has been occured while updating status.",
	"permissionStatusUpdateSuccess": "Permission has been updated successfully.",
	"permissionDeleteFailure": "An error has been occured while deleting the permission.",
	"permissionDeleteSuccess": "Permission(s) deleted successfully.",

	//category
	"categorySuccess": "Category has been saved successfully.",
	"categoryUpdateSuccess": "Category has been updated successfully.",
	"categoryStatusUpdateFailure": "An error has been occurred while updating status.",
	"categoryStatusUpdateSuccess": "Category status has been updated successfully.",
	"categoryDeleteFailure": "Error occured while deleting the category.",
	"categoryDeleteSuccess": "Category has been deleted successfully.",

	//answer
	"answerAddSuccess": "Answer option saved successfully",
	"answerUpdateSuccess": "Answer option updated successfully",

	//question message
	"questionFailure": "Error occured while saving the data",
	"questionSuccess": "Question saved successfully",
	"questionUpdateSuccess": "Question updated successfully",
	"questionStatusUpdateFailure": "Error occured while updating status",
	"questionStatusUpdateSuccess": "Status updated successfully",
	"questionDeleteFailure": "Error occured while deleting the question",
	"questionDeleteSuccess": "Question deleted successfully",
	"questionAnswerSuccess": "Answers retreived successfully",

	// AES encryption key
	"encryptionKey" :"GFYUFGTYGFTYTY64564545acvbvrttyFG@%#%#%#%#TTRR",

	//questionnaire message
	/*"questionnaireSuccess": "Questionnaire has been saved successfully.",
	"questionnaireUpdateQuestionFailure": "An error has been occured while saving the question in questionnaire.",
	"questionnaireUpdateSuccess": "Questionnaire has been updated successfully.",
	"questionnaireDeleteFailure": "An error has been occured while deleting the questionnaire.",
	"questionnaireDeleteSuccess": "Questionnaire has been deleted successfully.",
	"questionnaireStatusUpdateFailure": "An error has been occured while updating status.",
	"questionnaireStatusUpdateSuccess": "Status has been updated successfully.",*/

	//gmail credentials
	"gmailID":"active.discouting.sdm@gmail.com",
    "gmailpassword":"active@123"

}

const gmailSMTPCredentials = {
	"service": "gmail",
	"host": "smtp.gmail.com",
	"username": "osgroup.sdei@gmail.com",
	"password": "mohali2378"
}

const facebookCredentials = {
	"app_id": "1655859644662114",
	"secret": "62da09d9663d9f8315e873abfdbbe70f",
	"token_secret": process.env.token_secret || 'JWT Token Secret'
}

const twitterCredentials = {
	"consumer_key": "q2doqAf3TC6Znkc1XwLvfSD4m",
	"consumer_secret": "Yrfi1hr84UMoamS2vnJZQn6CeP8dHsv6XjDoyRqsfzSNwyFQBZ"
}

const googleCredentials = {
	"client_secret_key": "leWdLHJOoo9g6B9oLCV1lMqY"
}

var obj = {
	messages: messages,
	gmailSMTPCredentials: gmailSMTPCredentials,
	facebookCredentials: facebookCredentials,
	twitterCredentials: twitterCredentials,
	googleCredentials: googleCredentials
};
module.exports = obj;