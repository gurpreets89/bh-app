var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

	//default Appt Type
	var router = express.Router();
	var defaultAppointmentTpyeObj = require('./../app/controllers/admin/appointmentType.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.add);
	router.param('appointmentTypeId', defaultAppointmentTpyeObj.appointmentType);
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.list);
	router.post('/update/:appointmentTypeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.bulkUpdate);
	router.get('/findOne/:appointmentTypeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.findOne);
	app.use('/defaultAppointmentType', router);

	//default diagnosis
	var router = express.Router();
	var defaultDiagnosisObj = require('./../app/controllers/admin/diagnosis.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.add);
	router.param('diagnosisId', defaultDiagnosisObj.diagnosis);
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.list);
	router.post('/update/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.bulkUpdate);
	router.get('/findOne/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.findOne);
	app.use('/defaultDiagnosis', router);
	
	//default tasks
	var router = express.Router();
	var defaultTaskObj = require('./../app/controllers/admin/task.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.add);
	router.param('taskId', defaultTaskObj.task);
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.list);
	router.post('/update/:taskId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null)], defaultTaskObj.bulkUpdate);
	router.get('/findOne/:taskId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.findOne);
	router.post('/filteredlist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.filterlist);
	app.use('/defaultTasks', router);
}

