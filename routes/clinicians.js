module.exports = function(app, express, passport) {

	var router = express.Router();
	var clinicianObj = require('./../app/controllers/clinicians/clinicians.js');
    
	router.get('/list', passport.authenticate('bearer', {session:false}), clinicianObj.list);
	router.post('/bulkUpdateClinician', passport.authenticate('bearer', {session:false}), clinicianObj.bulkUpdateClinician);
	router.post('/delete', passport.authenticate('bearer', {session:false}), clinicianObj.deleteClinician); //used
	router.post('/checkClinicianPatients', passport.authenticate('bearer', {session:false}), clinicianObj.checkClinicianPatients);
	router.post('/getClinicians', passport.authenticate('bearer', {session:false}), clinicianObj.getClinicians);
	router.post('/updatePatientsClinician', passport.authenticate('bearer', {session:false}), clinicianObj.updatePatientsClinician);
	router.post('/getPatientsInfo', passport.authenticate('bearer', {session:false}), clinicianObj.getPatientsInfo);
	router.post('/logPatientsInfo', passport.authenticate('bearer', {session:false}), clinicianObj.logPatientsInfo);
	router.post('/invite', passport.authenticate('bearer', {session:false}), clinicianObj.invite);
	router.get('/listInvitedUsers', passport.authenticate('bearer', {session:false}), clinicianObj.listInvitedUsers);
        
	app.use('/clinicians', router);

}