var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

	var router = express.Router();

	var questionObj = require('./../app/controllers/questions/questions.js');


	var multer = require('multer');
    var fs = require('fs');

	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, './public/csv');
		},
		filename: function (req, file, cb) {
			//var filnameArr = file.originalname.split('.');
			var fileNameArr=file.originalname.substr(file.originalname.lastIndexOf('.')+1);
			cb(null, file.fieldname + '_' + Date.now() + '.' + fileNameArr);
		}
	});

	var upload = multer({
	    rename: function (fieldname, filename) {
	        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
	    }, storage: storage
	});

	router.post('/add', [passport.authenticate('bearer', {session : false}), middleware.checkAdminPermission([5], null) ], questionObj.add);
	router.post('/questionlist', passport.authenticate('bearer', {session:false}), questionObj.diseaseQuestionlist);
	
	router.get('/listall',[ passport.authenticate('bearer', {session:false}), middleware.checkAdminPermission([5], null) ], questionObj.allQuestionlist);
    /*router.post('/list',[ passport.authenticate('bearer', {session:false}), middleware.checkAdminPermission([5], null) ], questionObj.questionslist);*/
    //router.get('/listall',passport.authenticate('bearer', {session:false}), questionObj.allQuestionlist);
    router.post('/bulkUpdate', passport.authenticate('bearer', {session:false}), questionObj.bulkUpdate);
    router.post('/list', passport.authenticate('bearer', {session:false}), questionObj.questionslist);
	router.post('/delQuestion', passport.authenticate('bearer', {session:false}), questionObj.deleteQuestion);

	router.param('questionId', questionObj.question);
	router.post('/update/:questionId', passport.authenticate('bearer', {session:false}), questionObj.update);
	router.get('/question/:questionId', passport.authenticate('bearer', {session:false}), questionObj.findOne);
    router.get('/exportFile', passport.authenticate('bearer', {session:false}), upload.single('file'), questionObj.exportFile);

	app.use('/questions', router);
}