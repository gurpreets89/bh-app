var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var extraQuestionObj = require('./../app/controllers/extraquestions/extraquestions.js');
	var multer = require('multer');
    var fs = require('fs');

	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, './public/csv');
		},
		filename: function (req, file, cb) {
			//var filnameArr = file.originalname.split('.');
			var fileNameArr=file.originalname.substr(file.originalname.lastIndexOf('.')+1);
			cb(null, file.fieldname + '_' + Date.now() + '.' + fileNameArr);
		}
	});

	var upload = multer({
	    rename: function (fieldname, filename) {
	        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
	    }, storage: storage
	});
	
	router.post('/add', [passport.authenticate('bearer', {session : false}), middleware.checkAdminPermission([1], null) ], extraQuestionObj.add);
	router.get('/list',[ passport.authenticate('bearer', {session:false}), middleware.checkAdminPermission([1], null) ], extraQuestionObj.allQuestionlist);
	router.post('/delQuestion', passport.authenticate('bearer', {session:false}), extraQuestionObj.deleteQuestion);
	router.param('questionId', extraQuestionObj.question);
	router.post('/update/:questionId', passport.authenticate('bearer', {session:false}), extraQuestionObj.update);
	router.get('/question/:questionId', passport.authenticate('bearer', {session:false}), extraQuestionObj.findOne);

	router.post('/importFile', passport.authenticate('bearer', {session:false}), upload.single('file'), extraQuestionObj.importFile);

	router.post('/bulkUpdate', passport.authenticate('bearer', {session:false}), extraQuestionObj.bulkUpdate);
	app.use('/extraquestions', router);
}