var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

    var router = express.Router();
    var patientObj = require('./../app/controllers/patients/patients.js');
    
    router.get('/list/', passport.authenticate('bearer', {session:false}), patientObj.list);
    router.get('/listInvitedUsers', passport.authenticate('bearer', {session:false}), patientObj.listInvitedUsers);
    router.get('/list/:id', passport.authenticate('bearer', {session:false}), patientObj.list);
    router.post('/saveAssessment', passport.authenticate('bearer', {session:false}), patientObj.saveAssessment); //used
    router.post('/saveScoreAndProbability', passport.authenticate('bearer', {session:false}), patientObj.saveScoreAndProbability); //use in bh app
    router.post('/getLatestAssessmentOfPatient', passport.authenticate('bearer', {session:false}), patientObj.getLatestAssessmentOfPatient); //use in bh app
    router.post('/getHistoryData', passport.authenticate('bearer', {session:false}), patientObj.getHistoryData); //use in bh app
    router.post('/saveDiagnoseTarget', passport.authenticate('bearer', {session:false}), patientObj.saveDiagnoseTarget); //use in bh app
    router.post('/patientType', passport.authenticate('bearer', {session:false}), patientObj.patientType);
    router.post('/findOne', passport.authenticate('bearer', {session:false}), patientObj.findOne);
    router.post('/getPatientFilterList', passport.authenticate('bearer', {session:false}), patientObj.getPatientFilterList);
    router.post('/inviteOutPatient', passport.authenticate('bearer', {session:false}), patientObj.inviteOutPatient);
    router.post('/updatePatient', passport.authenticate('bearer', {session:false}), patientObj.updatePatient);
    router.post('/getPatientProbability', passport.authenticate('bearer', {session:false}), patientObj.getPatientProbability); //use in bh app
    router.post('/checkAppointmentForClinician', passport.authenticate('bearer', {session:false}), patientObj.checkAppointmentForClinician); //use in bh app
	router.post('/checkAssessmentSubmitted', passport.authenticate('bearer', {session:false}), patientObj.checkAssessmentSubmitted);
    
    app.use('/patients', router);
}