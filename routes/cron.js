module.exports = function(app,express){
    var router = express.Router();
    var cronObj = require("./../app/controllers/cron/controller.js");
    
    setInterval(function(){ 
	    //console.log('Cron Job');
	    cronObj.list();
	    cronObj.dailyreminderNotification();
	    cronObj.weeklyreminderNotification();
    },600000); // 60000 = 1min, 600000 = 10min
    
    app.use('/cron',router);
}
