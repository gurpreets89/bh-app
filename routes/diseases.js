module.exports = function(app, express, passport) {

	var router = express.Router();
	var diseasesObj = require('./../app/controllers/diseases/diseases.js');
	var multer = require('multer');
    var fs = require('fs');

	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, './public/csv');
		},
		filename: function (req, file, cb) {
			//var filnameArr = file.originalname.split('.');
			var fileNameArr=file.originalname.substr(file.originalname.lastIndexOf('.')+1);
			cb(null, file.fieldname + '_' + Date.now() + '.' + fileNameArr);
		}
	});

	var upload = multer({
	    rename: function (fieldname, filename) {
	        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
	    }, storage: storage
	});

    
	router.get('/list', passport.authenticate('bearer', {session:false}), diseasesObj.list);
	router.get('/adminlist', passport.authenticate('bearer', {session:false}), diseasesObj.adminlist);
	router.get('/diseaseDetail/:title', passport.authenticate('bearer', {session:false}), diseasesObj.findDiseaseDetail);
	router.post('/add', passport.authenticate('bearer', {session:false}), diseasesObj.add);
	router.get('/diseaseOne/:id', passport.authenticate('bearer', {session:false}), diseasesObj.findOne);
    router.post('/updateDisease', passport.authenticate('bearer', {session:false}), diseasesObj.updateDisease);
	router.post('/updateAdminDiseases', passport.authenticate('bearer', {session:false}), diseasesObj.updateAdminDiseases);

	router.post('/importFile', passport.authenticate('bearer', {session:false}), upload.single('file'), diseasesObj.importFile);
	router.get('/exportFile', passport.authenticate('bearer', {session:false}), upload.single('file'), diseasesObj.exportFile);

	
	
	
    router.post('/delDisease', passport.authenticate('bearer', {session:false}), diseasesObj.deleteDisease);
    router.post('/bulkUpdateDiseases', passport.authenticate('bearer', {session:false}), diseasesObj.bulkUpdateDiseases);
        
        /*router.param('id', userObj.user);
	*/
	app.use('/diseases', router);

}