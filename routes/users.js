module.exports = function(app, express, passport) {

	var router = express.Router();
	var userObj = require('./../app/controllers/users/users.js');
	var multer = require('multer');
    var fs = require('fs');

	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, './public/csv');
		},
		filename: function (req, file, cb) {
			//var filnameArr = file.originalname.split('.');
			var fileNameArr=file.originalname.substr(file.originalname.lastIndexOf('.')+1);
			cb(null, file.fieldname + '_' + Date.now() + '.' + fileNameArr);
		}
	});

	var upload = multer({
	    rename: function (fieldname, filename) {
	        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
	    }, storage: storage
	});
        
    /* routes used by gurpreet */
    // route to check if user already exists.
    router.get('/checkUser/:name', userObj.checkUser);
    router.post('/add', userObj.add);
    /*This following route /addPatient is used to add clinician from admin profile and patient/caregiver from clinician profile*/
    router.post('/addPatient', passport.authenticate('bearer', {session:false}), userObj.add);
    router.post('/inviteOutPatient', passport.authenticate('bearer', {session:false}), userObj.inviteOutPatient);
        
	router.post('/authenticate-patient', passport.authenticate('userObj', {session:false}), userObj.authenticate);
	router.get('/list', passport.authenticate('basic', {session:false}), userObj.list); 
	router.get('/getUserProfileData', passport.authenticate('bearer', {session:false}), userObj.getUserProfileData);
	router.post('/updateUserProfile', passport.authenticate('bearer', {session:false}), userObj.updateUserProfile);
	
	router.param('id', userObj.user);
	router.post('/update/:id', passport.authenticate('bearer', {session:false}), userObj.update);
	router.get('/userOne/:id', passport.authenticate('bearer', {session:false}), userObj.findOne);
	router.post('/bulkUpdate', passport.authenticate('bearer', {session:false}), userObj.bulkUpdate);
	router.post('/findPatientByCaregiverId', passport.authenticate('bearer', {session:false}), userObj.findPatientByCaregiverId);
	router.post('/findUserCount', passport.authenticate('bearer', {session:false}), userObj.findUserCount);
	router.post('/findAdminPatient', passport.authenticate('bearer', {session:false}), userObj.findAdminPatient);
	router.post('/updateProfilePic', passport.authenticate('bearer', {session:false}), userObj.updateProfilePic);
	router.post('/checkUserAndEmail', passport.authenticate('bearer', {session:false}), userObj.checkUserAndEmail);
	router.post('/checkUserAndEmailSignup', userObj.checkUserAndEmail);
	router.post('/checkInvitedClinicianSignup', userObj.checkInvitedClinicianSignup);
	router.get('/listAdmin', passport.authenticate('bearer', {session:false}), userObj.listAdmin);
	router.post('/addAdmin', passport.authenticate('bearer', {session:false}), userObj.addAdmin);
	router.post('/forgotPassword', userObj.forgotPassword); // Mobile App API
	router.post('/checkOtp', userObj.checkOtp); // Mobile App API
	router.post('/resetPassword', userObj.resetPassword); // Mobile App API
	router.post('/getAdmin', userObj.getAdmin);
	// Sign Up Patient Mobile
	router.post('/signUpPatient', userObj.signUpPatient);
	router.post('/updateInvitedClinicianStatus', userObj.updateInvitedClinicianStatus);

	router.get('/exportFile', passport.authenticate('bearer', {session:false}), upload.single('file'), userObj.exportFile);

	
	app.use('/users', router);

}