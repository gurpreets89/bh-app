module.exports = function(app, express, passport) {

	var router = express.Router();
	var answersObj = require('./../app/controllers/answers/answers.js');
	var multer = require('multer');
    var fs = require('fs');

	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, './public/csv');
		},
		filename: function (req, file, cb) {
			//var filnameArr = file.originalname.split('.');
			var fileNameArr=file.originalname.substr(file.originalname.lastIndexOf('.')+1);
			cb(null, file.fieldname + '_' + Date.now() + '.' + fileNameArr);
		}
	});

	var upload = multer({
	    rename: function (fieldname, filename) {
	        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
	    }, storage: storage
	});
        
    
	router.get('/answerlist', passport.authenticate('bearer', {session:false}), answersObj.answerlist);
	router.get('/getAllAnswerOptions', passport.authenticate('bearer', {session:false}), answersObj.getAllAnswerOptions);
	router.post('/saveAnswerOption', passport.authenticate('bearer', {session : false}), answersObj.saveAnswerOption);
	
	router.post('/findOneAnswer', passport.authenticate('bearer', {session:false}), answersObj.findOneAnswer);
	router.post('/bulkUpdateAnswer', passport.authenticate('bearer', {session:false}), answersObj.bulkUpdateAnswer);
	router.get('/exportFile', passport.authenticate('bearer', {session:false}), upload.single('file'), answersObj.exportFile);

	app.use('/answers', router);
}