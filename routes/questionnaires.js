module.exports = function(app, express, passport) {

	var router = express.Router();

	var questionnaireObj = require('./../app/controllers/questionnaires/questionnaire.js');
	// router.get('/listquestionnaire', passport.authenticate('basic', {session:false}), questionnaireObj.listquestionnaire);
	// router.post('/addquestionnaire', passport.authenticate('basic', {session:false}), questionnaireObj.addquestionnaire);
	// router.get('/editquestionnaire/:id', passport.authenticate('basic', {session:false}), questionnaireObj.editquestionnaire);
	// router.post('/updatequestionnaire', passport.authenticate('basic', {session: false}), questionnaireObj.updatequestionnaire);
	// router.post('/updateStatus', passport.authenticate('basic', {session:false}), questionnaireObj.updateStatus);

	router.get('/list', passport.authenticate('bearer', {session:false}), questionnaireObj.list); //used
        /* fetch basic questionnaire */
        router.get('/listBasic', passport.authenticate('bearer', {session:false}), questionnaireObj.listBasic); //used
	router.post('/add', passport.authenticate('bearer', {session:false}), questionnaireObj.add); //used
        router.get('/check', passport.authenticate('bearer', {session:false}), questionnaireObj.check); //used
        router.post('/removequestionnaire', passport.authenticate('bearer', {session:false}), questionnaireObj.removequestionnaire); //used
	router.param('questionnarieId', questionnaireObj.questionnaire);
	router.post('/update/:questionnarieId', passport.authenticate('bearer', {session:false}), questionnaireObj.update);
	router.get('/questionnaireOne/:questionnarieId', passport.authenticate('bearer', {session:false}), questionnaireObj.findOne);
	router.post('/bulkUpdate', passport.authenticate('bearer', {session:false}), questionnaireObj.bulkUpdate);	
	app.use('/questionnaire', router);

}