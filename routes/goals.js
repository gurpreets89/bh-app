module.exports = function(app, express, passport) {
	var router = express.Router();
	var goalObj = require('./../app/controllers/goals/goals.js');
	
	router.param('id', goalObj.goal);
	router.post('/list', passport.authenticate('bearer', {session:false}), goalObj.list); //used;	
	router.post('/bulkUpdateGoal', passport.authenticate('bearer', {session:false}), goalObj.bulkUpdateGoal);
  	router.post('/getUserByGoal', passport.authenticate('bearer', {session:false}), goalObj.getUserByGoal);
	router.post('/patientGoal', passport.authenticate('bearer', {session:false}), goalObj.patientGoal); //used

	// web app routes
	router.post('/add', passport.authenticate('bearer', {session:false}), goalObj.add); //used
	router.post('/update/:id', passport.authenticate('bearer', {session:false}), goalObj.update); //used
	router.post('/delGoal', passport.authenticate('bearer', {session:false}), goalObj.deleteGoal); //used
	router.get('/findOneGoal/:id', passport.authenticate('bearer', {session:false}), goalObj.findOne); //used
	
	// mobile app routes
	router.post('/patientGoalForState', passport.authenticate('bearer', {session:false}), goalObj.patientGoalForState);
	
	// common routes {web + mobile app}
	router.post('/getPatientDailyGoals', passport.authenticate('bearer', {session:false}), goalObj.getPatientDailyGoals);
	
	app.use('/goal', router);
}