module.exports = function(app, express, passport) {

	var router = express.Router();
	var faqsObj = require('./../app/controllers/faqs/faqs.js');
	var multer = require('multer');
    var fs = require('fs');

	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, './public/csv');
		},
		filename: function (req, file, cb) {
			//var filnameArr = file.originalname.split('.');
			var fileNameArr=file.originalname.substr(file.originalname.lastIndexOf('.')+1);
			cb(null, file.fieldname + '_' + Date.now() + '.' + fileNameArr);
		}
	});

	var upload = multer({
	    rename: function (fieldname, filename) {
	        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
	    }, storage: storage
	});
        
	router.get('/getAllFaqs', passport.authenticate('bearer', {session:false}), faqsObj.getAllFaqs);
	router.get('/getAllFaqsForMobile', passport.authenticate('bearer', {session:false}), faqsObj.getAllFaqsForMobile);
	router.post('/saveFaq', passport.authenticate('bearer', {session : false}), faqsObj.saveFaq);
	router.post('/findOneFaq', passport.authenticate('bearer', {session:false}), faqsObj.findOneFaq);
	router.post('/updateFaqStatus', passport.authenticate('bearer', {session:false}), faqsObj.updateFaqStatus);
	router.post('/deleteFaq', passport.authenticate('bearer', {session:false}), faqsObj.deleteFaq);
	router.get('/exportFile', passport.authenticate('bearer', {session:false}), upload.single('file'), faqsObj.exportFile);

	app.use('/faqs', router);
}