module.exports = function(app, express, passport) {

	var router = express.Router();
	var videosObj = require('./../app/controllers/videos/videos.js');
        
	router.get('/getAllVideos', passport.authenticate('bearer', {session:false}), videosObj.getAllVideos);
	router.get('/findVideoTypes', passport.authenticate('bearer', {session:false}), videosObj.findVideoTypes);
	router.post('/getVideoByTypeForMobile', passport.authenticate('bearer', {session:false}), videosObj.getVideoByTypeForMobile);
	router.post('/saveVideo', passport.authenticate('bearer', {session : false}), videosObj.saveVideo);
	router.post('/findOneVideo', passport.authenticate('bearer', {session:false}), videosObj.findOneVideo);
	router.post('/updateVideoStatus', passport.authenticate('bearer', {session:false}), videosObj.updateVideoStatus);
	router.post('/deleteVideo', passport.authenticate('bearer', {session:false}), videosObj.deleteVideo);
	app.use('/videos', router);
}