module.exports = function(app, express, passport) {
	var router = express.Router();
	var checkinObj = require('./../app/controllers/checkin/checkin.js');
	router.param('id', checkinObj.checkinData);	
	router.post('/add', passport.authenticate('bearer', {session:false}), checkinObj.add); //used
	router.post('/update/:id', passport.authenticate('bearer', {session:false}), checkinObj.update); //used
	router.post('/list', passport.authenticate('bearer', {session:false}), checkinObj.list); //used
	
	// mobile app routes
	router.post('/getWeeklyData', passport.authenticate('bearer', {session:false}), checkinObj.getWeeklyData);
	
	app.use('/checkin', router);

}