module.exports = function(app, express, passport) {

	var router = express.Router();

	var adminLoginObj = require('./../app/controllers/adminlogins/adminlogins.js');
	router.post('/authenticate', passport.authenticate('userObj', {session:false}), adminLoginObj.authenticate);
	router.post('/forgot_password', /*passport.authenticate('basic', {session:false}),*/ adminLoginObj.forgotPassword);
	router.post('/forgot_username', /*passport.authenticate('basic', {session:false}),*/ adminLoginObj.forgotUsername);
	router.post('/reset_password/:token',/* passport.authenticate('basic', {session:false}),*/ adminLoginObj.resetPassword);
	router.post('/change_password',/* passport.authenticate('basic', {session:false}),*/ adminLoginObj.changePassword);
	// router.post('/auth/facebook', adminLoginObj.facebookLogin);
	// router.post('/auth/twitter', adminLoginObj.twitterLogin);
	// router.post('/auth/google', adminLoginObj.googeLogin);
	app.use('/adminlogin', router);

}