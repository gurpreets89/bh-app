module.exports = function(app, express, passport) {
	var router = express.Router();
	var generalquestionObj = require('./../app/controllers/generalquestions/generalquestions.js');
	router.param('id', generalquestionObj.generalQuestion);
	router.post('/add', passport.authenticate('bearer', {session:false}), generalquestionObj.add); //used
	router.post('/patientGeneralQuestion', passport.authenticate('bearer', {session:false}), generalquestionObj.patientGeneralQuestion); //used
	router.post('/updatepatientGeneralQuestion/:id', passport.authenticate('bearer', {session:false}), generalquestionObj.updatepatientGeneralQuestion); //used
	app.use('/generalquestions', router);

}