"use strict"

angular.module("Videos")

.factory('VideoService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};
	
	service.getAllVideos = function(callback) {
		communicationService.resultViaGet(webservices.getAllVideos, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.saveVideo = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.saveVideo, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	service.findOneVideo =  function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.findOneVideo, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.findVideoTypes = function(callback) {
		communicationService.resultViaGet(webservices.findVideoTypes, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.updateVideoStatus =  function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.updateVideoStatus, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.deleteVideo = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.deleteVideo, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	return service;
}]);
