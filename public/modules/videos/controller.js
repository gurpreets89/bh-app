"use strict";

angular.module("Videos")

bhapp.controller("videoController", ['$scope', '$rootScope', '$localStorage','VideoService', 'UserService','ngTableParams', 'SweetAlert','$timeout', '$stateParams', '$location', '$state', 'toastr','$uibModal',  function($scope, $rootScope, $localStorage, VideoService, UserService, ngTableParams, SweetAlert,  $timeout, $stateParams, $location, $state, toastr, $uibModal){

	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	}
	else {
		$rootScope.userLoggedIn = false;
	}
	$scope.isFiltersVisible = false;
	//Toggle multilpe checkbox selection
	$scope.selection = [];
	$scope.selectionAll = false;
	$scope.toggleSelection = function toggleSelection(id) {
	//Check for single checkbox selection
		if(id){
			var idx = $scope.selection.indexOf(id);
			// is currently selected
			if (idx > -1) {
			    $scope.selection.splice(idx, 1);
			    if($scope.listForToggle.length != $scope.selection.length){
			     	$scope.selectionAll = false;
			    }
			}// is newly selected
			else {
			    $scope.selection.push(id);
			    if($scope.listForToggle.length == $scope.selection.length){
			     	$scope.selectionAll = true;
			    }else{
			    	$scope.selectionAll = false;
			    }
			}

	    } else { //Check for all checkbox selection
	    	if($scope.selection.length > 0 && $scope.selectionAll){
	    		$scope.selection = [];
	    		$scope.checkboxes = {
	    			checked: false,
	    			items:{}
	    		};	
	    		$scope.selectionAll = false;
	    	} else {      	//Check for all un checked checkbox for check
	    		$scope.selectionAll = true
	    		$scope.selection = [];
	    		angular.forEach($scope.simpleList, function(item) {
	    			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
	    			$scope.selection.push(item._id);
	    		});
	    	}
		}
		//console.log($scope.selection)
	};

	// Update Answer options status
	$scope.performAction = function() {				
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		$scope.showloader = true;
		if($scope.selectedAction == 0){
			toastr.warning(messagesConstants.selectAction);
			$scope.showloader = false;
		}else if ($scope.selection == "") {
			toastr.warning(messagesConstants.selectActionField);
			$scope.showloader = false;
		}
		else{	
			for(var i = 0; i< roleLength; i++){
				var id =  $scope.selection[i];
				if($scope.selectedAction == 1) {
					updatedData.push({id: id, is_status: true});
				}
				else if($scope.selectedAction == 2) {
				  updatedData.push({id: id, is_status: false});
				}
			}
			var inputJson = {data: updatedData};
			VideoService.updateVideoStatus(inputJson, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(messagesConstants.VideoUpdateSuccess);
					$state.reload();
				} else {
					toastr.error(messagesConstants.VideoUpdateError);
				}
			});
		}
	}
	
	$scope.videoObj = {};
	$scope.videoObj.video_for = "";
	$scope.frmSubmitted = false;

	$scope.getAllVideos = function(){
		$scope.showloader = true;
		VideoService.getAllVideos (function(response) {
			//console.log(response);
			$scope.showloader = false;
			if(response.messageId == 200) {
				if(response.data.length != 0){
					$scope.isFiltersVisible = true;
					$scope.filter = {
						video_url: ''
					};
					$scope.noVideos = false;
					$scope.VideoExist = true;
					$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{is_status:"desc"}}, { total:response.data.length, counts: [10, 25, 50, 100], data: response.data});
					$scope.simpleList = response.data;
					$scope.listForToggle = new Array();
					for(var i = 0; i < response.data.length; i++){
						if(response.data[i].is_deleted == false){
							$scope.listForToggle.push(response.data[i]);
						}
					}
					$scope.checkboxes = {
						checked: false,
						items:{}
					};
				}else{
					$scope.noVideos = true;
					$scope.VideoExist = false;
				}
			}
		});	
	}

	// Video Type JSON Object
	$scope.videoType = [
		{
			id: 1,
			name: "Assessment",
			enabled : true
		},{
			id: 2,
			name: "Goals",
			enabled : true
		},{
			id: 3,
			name: "Check In",
			enabled : true
		},{
			id: 4,
			name: "State of mind",
			enabled : true
		},{
			id: 5,
			name: "Settings",
			enabled : true
		},{
			id: 6,
			name: "FAQ",
			enabled : true
		}
	];

	/* Function to get the one Video data
	*  Created By: Rajesh Thakur
	*/
	$scope.getVideoData = function() {
		$scope.showloader = true;
		// get available Video Types.
		VideoService.findVideoTypes(function(response) {
			//console.log('response = ', response);
			if(response.messageId == 200) {
				//var videoTypeArray = response.data.video_for;
				for(var i = 0; i < $scope.videoType.length; i++){
					for(var j = 0; j < response.data.length; j++){
						if($scope.videoType[i].name == response.data[j].video_for){
							//$scope.videoType = _.omit($scope.videoType, fq[j]._id);
							$scope.videoType[i].enabled = false;
						}
					}
				}
			}
		});
		//console.log('$scope.videoType = ', $scope.videoType);
		
		if($stateParams.id){
			$scope.add_edit_video_screen_title = messagesConstants.EditVideoScreenTitle;
			var inputJsonString = {};
			inputJsonString._id = $stateParams.id;
			VideoService.findOneVideo(inputJsonString, function(response) {
				$scope.showloader = false;
	    		if(response.messageId == 200) {
					$scope.videoObj = response.data;
					var videoTypeArray = response.data.video_for;
					for(var i = 0; i < $scope.videoType.length; i++){
						for(var j = 0; j < videoTypeArray.length; j++){
							if($scope.videoType[i].id == videoTypeArray[j].id){
								$scope.videoType[i].selected = true;
							}
						}
					}
				} else {
					toastr.error(messagesConstants.NoVideoFound);
			 	}
			});
	    }else{
	    	$scope.add_edit_video_screen_title = messagesConstants.AddVideoScreenTitle;
	    	$scope.showloader = false;
	    } 
	}

	/* Function to Add/Edit the Video
	*  Created By: Rajesh Thakur
	*/
	
	$scope.addVideo = function() {
		$scope.frmSubmitted = true;
		$scope.showloader = true;
		var inputJsonString = {};
		inputJsonString = $scope.videoObj;
	//console.log('$scope.videoObj = ', $scope.videoObj); //return;
		/*$scope.videoTypeArray = [];
	    angular.forEach($scope.videoType, function(x){
	      	if (x.selected) {
	      		$scope.videoTypeArray.push(x)
	      	};
	    });*/
		if($scope.videoObj.video_for == undefined){
			$scope.showloader = false;
			toastr.warning(messagesConstants.SelectVideoType);
		}else{
			//console.log('..in else..');
			//inputJsonString.video_for = $scope.videoTypeArray;
			//console.log(inputJsonString);
			if($stateParams.id){
				// Update Video
				inputJsonString._id = $stateParams.id;
				inputJsonString.modified_date = Date.now();
				VideoService.saveVideo(inputJsonString, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.updateVideo);
						$timeout(function() {
					        $location.path('/videos');
					    }, 2000);
					}else{
						toastr.error(messagesConstants.VideoEditError);
					}
				});
			} else {
				// SAVE Video
				VideoService.saveVideo(inputJsonString, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.saveVideo);
						$timeout(function() {
					        $location.path('/videos');
					    }, 2000);
					}else{
						toastr.error(messagesConstants.VideoAddError);
					}
				});
			}
		}
	}

	/*
	* Del Video in Web-Admin
	* Created: RaJesh Thakur
	* Used in BH APP project
    */
    $scope.delVideo = function(id, qname){
		SweetAlert.swal({
			title: "Confirmation",
			text: "Are you sure you want to delete video ("+qname+")",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No!",
			closeOnConfirm: true}, 
			function(isConfirm){ 
		  		if(isConfirm) {
			  		var delete_object = {'_id':id};
			  		$scope.showloader = true;
		            VideoService.deleteVideo(delete_object, function(response) {
		            	$scope.showloader = false;
		            	$timeout(function() {
		            		$scope.getAllVideos();
					        $location.path('/videos');
					    }, 100);
						toastr.success(messagesConstants.VideoDeleteSuccess);
					});
	        	}
			}
		);
	}

	$scope.openVideoModal = function (url) {
		$rootScope.videoUrl = url.video_url;
	    var modalInstance = $uibModal.open({
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      templateUrl: 'myModalContent.html',
	      controller: 'ModalInstanceCtrl',
	      resolve: {}
	    });
   	}
}])

bhapp.controller('ModalInstanceCtrl', ['$uibModalInstance', '$scope', '$sce', '$rootScope', function ($uibModalInstance, $scope, $sce, $rootScope) {

	var video = $rootScope.videoUrl;
	var video_Id = video.substring(video.lastIndexOf("/") + 1, video.length);

	$scope.videoSrc = 'https://player.vimeo.com/video/' + video_Id + '?autoplay=1&title=0&byline=0&portrait=0';

	$scope.currentVideoUrl = $sce.trustAsResourceUrl($scope.videoSrc);
    $scope.modalClose = function () {
   		$uibModalInstance.close();
   	}
}]);