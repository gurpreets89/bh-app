"use strict"

angular.module("Users")

.factory('UserService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.getAdminList = function(callback) {
			communicationService.resultViaGet(webservices.listAdmin, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.exportUserList = function(callback) {
		communicationService.resultViaGet(webservices.exportuserList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.getUserList = function(callback) {
			communicationService.resultViaGet(webservices.userList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.saveUser = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addUser, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.savePatient = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addPatient, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.addAdmin = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addAdmin, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
/*
	service.inviteOutPatient = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.inviteOutPatient, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
*/

	service.checkUserAndEmail = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.checkUserAndEmail, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.checkUserAndEmailSignup = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.checkUserAndEmailSignup, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.checkInvitedClinicianSignup = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.checkInvitedClinicianSignup, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateInvitedClinicianStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.updateInvitedClinicianStatus, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	/* check user by gurpreet */
	service.checkUser = function(username, callback) {
		var serviceURL = webservices.checkUser + "/" + username;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.getUser = function(userId, callback) {
		var serviceURL = webservices.findOneUser + "/" + userId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateUser = function(inputJsonString, userId, callback) {
		var serviceURL = webservices.update + "/" + userId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateUserStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateUser, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getUserProfileData = function(callback) {
			communicationService.resultViaGet(webservices.getUserProfileData, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.updateUserProfile = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.updateUserProfile, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateProfilePic = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.updateProfilePic, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getAdmin = function (inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getAdmin, appConstants.authorizationKey, headerConstants.json, inputJsonString, function (response) {
			callback(response.data);
		});
	}

	return service;


}]);
