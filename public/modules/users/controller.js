"use strict";
angular.module("Users")

bhapp.controller("userController", ['$scope', '$rootScope', '$localStorage', 'UserService', 'DiseaseService', 'ngTableParams', '$stateParams', '$state', '$location', 'toastr', '$timeout', 'Upload', function($scope, $rootScope, $localStorage, UserService, DiseaseService, ngTableParams, $stateParams, $state, $location, toastr, $timeout, Upload) {

    $scope.user = {};
    // $scope.enableChecked = true;
    // function to sign up and add new clinician 
    $scope.addUser = function() {
        $scope.showloader = true;
        if (typeof($stateParams.id) == 'undefined') {
            var checkExist = {};
            checkExist.username = $scope.user.username;
            checkExist.email = $scope.user.email;
            if ($scope.user.admin_added) {
                // Add New Clinician Code
                UserService.checkUserAndEmail(checkExist, function(resp1) {
                    $scope.showloader = false;
                    $scope.errordetail = {};
                    if (resp1.messageId == 200) {
                        if (resp1.status == 'warning-username') {
                            $scope.errordetail.usernameexist = true;
                        }
                        if (resp1.status == 'warning-email') {
                            $scope.errordetail.emailexist = true;
                        }
                        if (resp1.status == 'success') {
                            var inputString = {};
                            inputString = $scope.user;
                            inputString.user_type = 2;
                            inputString.is_status = true;
                            // inputString.clinic_name = 'Test Clinic';
                            inputString.created = Date.now();
                            var admin_added = inputString.admin_added;
                            // delete inputString.admin_added;
                            if (admin_added) { //If add by admin, use patient add service because of re-useability.
                                inputString.password = randomPassword(8);
                                inputString.originalPassword = inputString.password;
                                var passwordEncrpt = CryptoJS.AES.encrypt(JSON.stringify(inputString.password), messagesConstants.encryptionKey);
                                inputString.password = passwordEncrpt.toString();
                                var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(inputString), messagesConstants.encryptionKey);
                                var encryptedJSON = {};
                                encryptedJSON.inputData = ciphertext.toString();
                                $scope.showloader = true;
                                UserService.savePatient(encryptedJSON, function(response) {
                                    $scope.showloader = false;
                                    if (response.messageId == 200) {
                                        toastr.success(messagesConstants.saveClinician);
                                        $timeout(function() {
                                            $location.path('/clinicians');
                                        }, 1000);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                });
                            }
                        }
                    }
                });
            } else {
                //console.log(JSON.stringify($scope.user)); // Check Clinician Invited or Not Start Here
                var checkInvitedClinician = {};
                checkInvitedClinician.user_email = $scope.user.email;
                checkInvitedClinician.reference_code = $scope.user.parent;
                checkInvitedClinician.is_signed_up = false;
                UserService.checkInvitedClinicianSignup(checkInvitedClinician, function(response1) {
                    $scope.showloader = false;
                    if(response1.status == "warning"){
                        toastr.warning(messagesConstants.notInvited);
                    }else{
                        // Email and Reference Code Matched and Found DB, 
                        // Clinician Sign-up here 
                        //console.log(response1.data[0].parent);
                        $scope.user.parent = response1.data[0].parent;
                        //console.log(checkExist);
                        UserService.checkUserAndEmailSignup(checkExist, function(resp1) {
                            $scope.showloader = false;
                            $scope.errordetail = {};
                            if (resp1.messageId == 200) {
                                if (resp1.status == 'warning-username') {
                                    $scope.errordetail.usernameexist = true;
                                    toastr.warning("Username already exist.");
                                    $scope.user.parent = "";
                                    $scope.user.password = "";
                                    $scope.user.password2 = "";
                                }
                                if (resp1.status == 'warning-email') {
                                    $scope.errordetail.emailexist = true;
                                    toastr.warning("Email already exist.");
                                    $scope.user.parent = "";
                                    $scope.user.password = "";
                                    $scope.user.password2 = "";
                                }
                                if (resp1.status == 'success') {
                                    var inputString = {};
                                    inputString = $scope.user;
                                    inputString.user_type = 2;
                                    inputString.is_status = true;
                                    // inputString.clinic_name = 'Test Clinic';
                                    // password encryption when clinician register from login page
                                    inputString.originalPassword = inputString.password;
                                    var passwordEncrpt = CryptoJS.AES.encrypt(JSON.stringify(inputString.password), messagesConstants.encryptionKey);
                                    inputString.password = passwordEncrpt.toString();
                                    inputString.password2= passwordEncrpt.toString();
                                    
                                    inputString.created = Date.now();
                                    var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(inputString), messagesConstants.encryptionKey);
                                    var encryptedJSON = {};
                                    encryptedJSON.inputData = ciphertext.toString();
                                    $scope.showloader = true;
                                   // console.log(encryptedJSON);
                                    // If data came from sign-up form, this code will execute as of no-session required.
                                    UserService.saveUser(encryptedJSON, function(response) {
                                        //console.log(response);
                                        $scope.showloader = false;
                                        $scope.errormessage = '';
                                        $scope.message = '';
                                        if (response.messageId == 200) {
                                            // Update Invited User Status
                                            var sendInvitedClinicianId = {};
                                            sendInvitedClinicianId._id = response1.data[0]._id;
                                            UserService.updateInvitedClinicianStatus(sendInvitedClinicianId, function(response4) {
                                                //console.log(response4);
                                                if(response4.status == "success"){
                                                    $scope.user = '';
                                                    toastr.success(messagesConstants.addClinician);
                                                    $state.go("login");
                                                }
                                            });    

                                        } else {
                                            // customized error messages for firstname, lastname, email 
                                            var eMsg = response.message;
                                            var msgSet = false;
                                            var eMsgArr = eMsg.split(",");
                                            if (typeof eMsgArr[0] != 'undefined') {
                                                var e1 = eMsgArr[0];
                                                var e1Arr = e1.split(" ");
                                                if (e1Arr[0] == 'parent') {
                                                    toastr.warning(messagesConstants.ReferanceCodeError);
                                                    msgSet = true;
                                                }
                                            }
                                            if (msgSet == false && (typeof eMsgArr[1] != 'undefined')) {
                                                var e2 = eMsgArr[1];
                                                var e2Arr = e2.split(" ");
                                                if (e2Arr[0] == 'email') {
                                                    toastr.warning(messagesConstants.validEmailError);
                                                    msgSet = true;
                                                }
                                            }
                                        }
                                    });
                                }else{
                                    //console.log("HERE --- ");
                                }
                            }
                        }); 
                    }
                })
            }
        } else { /* If Edit Clinician code starts */
            var inputString = {};
            inputString = $scope.user;
            inputString.modified = Date.now();
            UserService.updateUserProfile(inputString, function(response) {
                $scope.showloader = false;
                if (response.messageId == 200) {
                    toastr.success(messagesConstants.updateClinician);
                    $timeout(function() {
                        $location.path('/clinicians');
                    }, 1000);
                }
            });
        }
    }


    $scope.downloadCSV = function(){
        UserService.exportUserList(function(response){
            console.log(response)
            $scope.showloader = false;
            if(response.messageId == 200) {
                window.open('/adminlist.csv');
            }
        });
    }

    /*
    |-------------------------------------------
    |   Generate Random password function
    |   developer : gurpreet
    |-------------------------------------------
    */
    function randomPassword(length) {
        var chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNP123456789";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }


    if ($localStorage.userLoggedIn) {
        $rootScope.userLoggedIn = true;
        $rootScope.loggedInUser = $localStorage.loggedInUsername;
    } else {
        $rootScope.userLoggedIn = false;
    }


    if ($rootScope.message != "") {

        $scope.message = $rootScope.message;
    }

    // Developer Rajesh 
    // Linking Change password screen on the basis of user type
    $scope.userType = "";
    if ($localStorage.loggedInUsertype == 1) {
        $scope.homescreen = "/#/adminhome";
        $scope.userType = "Admin";
    } else if ($localStorage.loggedInUsertype == 2) {
        $scope.homescreen = "/#/home";
        $scope.userType = "Clinician";
    } else if ($localStorage.loggedInUsertype == 5) {
        $scope.homescreen = "/#/superhome";
        $scope.userType = "Super-Admin";
    }

    //empty the $scope.message so the field gets reset once the message is displayed.
    $scope.message = "";
    $scope.activeTab = 0;
    //$scope.user = {first_name: "", last_name: "", username: "", password: "", email: "", display_name: "", role: []}

    //Toggle multilpe checkbox selection
    $scope.selection = [];
    $scope.selectionAll;
    $scope.toggleSelection = function toggleSelection(id) {
        //Check for single checkbox selection
        if (id) {
            var idx = $scope.selection.indexOf(id);
            // is currently selected
            if (idx > -1) {
                $scope.selection.splice(idx, 1);
            }
            // is newly selected
            else {
                $scope.selection.push(id);
            }
        } else { //Check for all checkbox selection 
            if ($scope.selection.length > 0 && $scope.selectionAll) {
                $scope.selection = [];
                $scope.checkboxes = {
                    checked: false,
                    items: {}
                };
                $scope.selectionAll = false;
            } else { //Check for all un checked checkbox for check

                $scope.selectionAll = true
                $scope.selection = [];
                angular.forEach($scope.simpleList, function(item) {
                    $scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
                    $scope.selection.push(item._id);
                });
            }
        }

    };


    //apply global Search
    $scope.applyGlobalSearch = function() {
        var term = $scope.globalSearchTerm;
        if (term != "") {
            if ($scope.isInvertedSearch) {
                term = "!" + term;
            }
            $scope.tableParams.filter({ $: term });
            $scope.tableParams.reload();
        }
    }


    $scope.getAllUsers = function() {
        $scope.showloader = true;
        UserService.getUserList(function(response) {
            $scope.showloader = false;
            if (response.messageId == 200) {
                $scope.filter = { firstname: '', lastname: '', email: '' };

                $scope.tableParams = new ngTableParams({ page: 1, count: 10, sorting: { firstname: "asc" }, filter: $scope.filter }, { total: response.data.length, counts: [10, 25, 50, 100], data: response.data });
                //multiple checkboxes

                $scope.simpleList = response.data;
                $scope.roleData = response.data;
                $scope.checkboxes = {
                    checked: false,
                    items: {}
                };
            }
        });
    }


    $scope.getAllRoles = function() {
        RoleService.getRoleList(function(response) {
            if (response.messageId == 200) {
                var len = response.data.length;
                var rolePermissionlen = $scope.user.role.length;
                for (var i = 0; i < len; i++) {
                    if (($scope.user.role.indexOf(response.data[i]._id)) == -1)
                        response.data[i].used = false;
                    else
                        response.data[i].used = true;
                }
                $scope.roleData = response.data;

            }
        });
    }

    $scope.activeTab = 0;
    $scope.findOne = function() {
        if ($stateParams.id) {
            UserService.getUser($stateParams.id, function(response) {
                if (response.messageId == 200) {
                    $scope.user = response.data;
                }
            });
        }
        $scope.getAllRoles()
    }


    $scope.checkStatus = function(yesNo) {
        if (yesNo)
            return "pickedEven";
        else
            return "";
    }

    $scope.moveTabContents = function(tab) {
        $scope.activeTab = tab;
    }

    $scope.selectRole = function(id) {
        var index = $scope.user.role.indexOf(id);
        if (index == -1)
            $scope.user.role.push(id)
        else
            $scope.user.role.splice(index, 1)

        var roleLen = $scope.roleData.length;
        for (var a = 0; a < roleLen; ++a) {
            if ($scope.roleData[a]._id == id) {
                if ($scope.roleData[a].used) {
                    $scope.roleData[a].used = false;
                } else {
                    $scope.roleData[a].used = true;
                }
                break;
            }
        }

    }


    $scope.updateData = function(type) {
        $scope.showloader = true;
        if ($scope.user._id) {
            var inputJsonString = $scope.user;
            UserService.updateUser(inputJsonString, $scope.user._id, function(response) {
                $scope.showloader = false;
                if (response.messageId == 200) {
                    if (type)
                        $location.path("/users");
                    else {
                        ++$scope.activeTab
                    }

                } else {
                    $scope.message = err.message;
                }
            });
        } else {
            var inputJsonString = $scope.user;
            UserService.saveUser(inputJsonString, function(response) {
                $scope.showloader = false;
                if (response.messageId == 200) {
                    $scope.message = '';
                    $stateParams.id = response.data
                    $scope.user = response.data;
                    $scope.activeTab = 1;
                } else {
                    $scope.message = response.message;
                }
            });
        }
    }




    //perform action
    $scope.performAction = function() {
        $scope.showloader = true;
        var roleLength = $scope.selection.length;
        var updatedData = [];
        $scope.selectedAction = selectedAction.value;
        if ($scope.selectedAction == 0) {
            toastr.warning(messagesConstants.selectAction);
            $scope.showloader = false;
        } else if ($scope.selection == "") {
            toastr.warning(messagesConstants.selectActionField);
            $scope.showloader = false;
        } else {
            for (var i = 0; i < roleLength; i++) {
                var id = $scope.selection[i];
                if ($scope.selectedAction == 3) {
                    updatedData.push({ id: id, is_deleted: true });
                } else if ($scope.selectedAction == 1) {
                    updatedData.push({ id: id, is_status: true });
                } else if ($scope.selectedAction == 2) {
                    updatedData.push({ id: id, is_status: false });
                }
            }
            var inputJson = { data: updatedData }
            UserService.updateUserStatus(inputJson, function(response) {
                $scope.showloader = false;
                if (response.messageId == 200) {
                    toastr.success(messagesConstants.AdminSuccessStatus);
                    $state.reload();
                } else {
                    toastr.error(messagesConstants.AdminSuccessError);
                }
            });
        }
    }



    //perform action

    $scope.performAction1 = function() {
        var data = $scope.checkboxes.items;
        var records = [];
        var inputJsonString = "";
        var jsonString = "";

        var actionToPerform = "";

        $scope.selectAction = selectAction.value;

        if ($scope.selectAction == "disable") {
            actionToPerform = false;
        } else if ($scope.selectAction == "enable") {
            actionToPerform = true;
        } else if ($scope.selectAction == "delete") {

            actionToPerform = "delete";
        }


        for (var id in data) {
            if (data[id]) {
                if (actionToPerform == "delete") {
                    if (jsonString == "") {

                        jsonString = '{"_id": "' + id + '", "is_deleted":"true"}';
                    } else {
                        jsonString = jsonString + "," + '{"_id": "' + id + '", "is_deleted":"true"}';
                    }
                } else {
                    if (jsonString == "") {

                        jsonString = '{"_id": "' + id + '", "enable":"' + actionToPerform + '"}';
                    } else {
                        jsonString = jsonString + "," + '{"_id": "' + id + '", "enable":"' + actionToPerform + '"}';
                    }
                }
            }

        }

        inputJsonString = "[" + jsonString + "]";

        if (actionToPerform == "delete") {

            UserService.deleteUser(inputJsonString, function(response) {
                $rootScope.message = messagesConstants.deleteUser;
                $state.reload();
            });
        } else {
            UserService.statusUpdateUser(inputJsonString, function(response) {
                $rootScope.message = messagesConstants.updateStatus;
                $state.reload();
            });
        }
    }

    /*Get Logged-in user profile data*/
    /*Created By: Jatinder Singh*/

    $scope.profileData = {};
    $scope.profileData.diseases = {};
    $scope.profileImgPath = 'images/profile/';
    $scope.profileImg = 'avatar5.png';
    $scope.getUserProfileData = function() {
        $scope.showloader = true;
        UserService.getUserProfileData(function(response1) {
            $scope.showloader = false;
            $scope.errprofileImage = 'images/avatar5.png';
            $scope.showSpeciality = false;
            if (response1.messageId == 200) {
                $scope.profileData = response1.data;
                if (typeof $scope.profileData.profile_pic != 'undefined') {
                    $scope.profileImg = $scope.profileData.profile_pic;
                }
                $scope.first_name = response1.data.first_name;
                $scope.last_name = response1.data.last_name;
                $scope.clinic_name = response1.data.clinic_name;
                var speciality = response1.data.clinician_disease_specialities;
                if ($scope.profileData.user_type == 2) {
                    $scope.showSpeciality = true;
                } else {
                    $scope.showSpeciality = false;
                }
                if ($scope.profileData.user_type == 5) {
                    $scope.showClinicName = false;
                } else {
                    $scope.showClinicName = true;
                }
                DiseaseService.getDiseaseList(function(response2) {
                    if (response2.messageId == 200) {
                        $scope.diseases = response2.data;
                        //console.log(JSON.stringify($scope.diseases));
                        for (var i = 0; i < $scope.diseases.length; i++) {
                            for (var j = 0; j < speciality.length; j++) {
                                if ($scope.diseases[i]._id == speciality[j].disease) {
                                    speciality[j].selected = true;
                                    speciality[j].title = $scope.diseases[i].title;
                                    $scope.diseases[i] = speciality[j];
                                }
                            }
                        }
                    }
                });
            }
        });
    }


    /* Created By: Jatinder Singh */
    /** function to update logged-in user profile data **/

    $scope.updateProfileData = function() {
        $scope.showloader = true;
        var inputString = {};
        inputString = $scope.profileData;
        inputString.country_code = "+1";
        if ($scope.profileData.user_type == 2) {

            var specialityDone = $scope.profileData.clinician_disease_specialities.length;
            $scope.selectedSpecialities = [];
            angular.forEach($scope.diseases, function(x) {
                if (x.selected) {
                    var obj = {};
                    if (typeof(x.disease) != 'undefined') {
                        obj.disease = x.disease;
                    } else {
                        obj.disease = x._id;
                    }
                    $scope.selectedSpecialities.push(obj);
                    // $scope.selectedSpecialities.push(x._id);
                };
            });
            inputString.clinician_disease_specialities = $scope.selectedSpecialities;

            //console.log(inputString.clinician_disease_specialities);
        }


        UserService.updateUserProfile(inputString, function(response) {
            $scope.showloader = false;
            if (response.messageId == 200) {
                toastr.success(messagesConstants.profileUpdated);
                $timeout(function() {
                    $state.go($state.current, {}, { reload: true });
                }, 100);
            }
        });
    }
    /*
        |------------------------------------------------------
        |   Upload image | developer - gurpreet
        |------------------------------------------------------
    */

    $scope.invalidImage = true;
    var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".png", ".gif"];    
    $scope.ValidateSingleInput = function(oInput) {
        if (oInput == "file") {
            if($scope.picFile != null){
                var sFileName = $scope.picFile.name;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                     
                    if (!blnValid) {
                        toastr.warning(messagesConstants.profilePicWarning);
                        $scope.picFile.name = "";
                        $scope.invalidImage = true;
                        return false;
                    }else{
                        $scope.invalidImage = false;
                    }
                }
            }
        }
        return true;
    }
    $scope.upload = function(dataUrl, name) {
        $scope.showloader = true;
        if (name != undefined) {
            var inputString = {};
            inputString = { "img": dataUrl };
            UserService.updateProfilePic(inputString, function(response) {
                $scope.showloader = false;
                if (response.status == 'success') {
                    toastr.success(messagesConstants.profilePicUpdated);
                    $scope.profileImg = response.imgName;
                    $localStorage.loggedInUser.user.profile_pic = response.imgName;
                    $rootScope.profilePic = "images/profile/" + $localStorage.loggedInUser.user.profile_pic;
                } else {
                    toastr.error(messagesConstants.profilePicError);
                }
            });
        } else {
            $scope.showloader = false;
            toastr.warning(messagesConstants.profilePicWarning);
        }
    }



    /* Function to Edit the Clinician Profile
     *  Created By: Jatinder Singh
     */
    $scope.getClinicianDataById = function() {
        $scope.showloader = true;
        $scope.readonly = false;
        // $scope.user.mode = 'edit';
        if ($stateParams.id) {
            $scope.readonly = true;
            $scope.mode = "Edit";
            UserService.getUser($stateParams.id, function(response) {
                $scope.showloader = false;
                if (response.messageId == 200) {
                    var d = new Date(response.data.dob);
                    var formatdate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                    response.data.dob = formatdate;
                    $scope.user = response.data;
                } else {
                    toastr.error(messagesConstants.getCliniciansError);
                }
            });
        } else {
            $scope.showloader = false;
            $scope.mode = "Add";
            $scope.readonly = false;
        }
    }

    /* Function to Get List of the Total Amdins
     *  Created By: Rajesh
     */
    $scope.getAllAdmins = function() {
        $scope.showloader = true;
        UserService.getAdminList(function(response) {
            $scope.showloader = false;
            if (response.messageId == 200) {
                if (response.data.length != 0) {
                    $scope.noAdmin = false;
                    $scope.adminExist = true;
                    $scope.isFiltersVisible = true;
                    $scope.filter = { firstname: '', lastname: '', email: '', username: '' };
                    $scope.tableParams = new ngTableParams({ page: 1, count: 10, sorting: { is_status: "desc" }, filter: $scope.filter }, { total: response.data.length, counts: [10, 25, 50, 100], data: response.data });
                    $scope.simpleList = response.data;
                    $scope.checkboxes = {
                        checked: false,
                        items: {}
                    };
                } else {
                    $scope.isFiltersVisible = false;
                    $scope.noAdmin = true;
                    $scope.adminExist = false;
                }
            }
        });
    }

    /* Function to Add new admin
     *  Created By: Rajesh
     */

    $scope.addAdminToApp = function() {
        $scope.showloader = true;
        var checkExist = {};
        if($scope.user.hasOwnProperty('_id'))
        checkExist._id = $scope.user._id;
        checkExist.username = $scope.user.username;
        checkExist.email = $scope.user.email;
        UserService.checkUserAndEmail(checkExist, function(resp1) {
            $scope.showloader = false;
            if (resp1.messageId == 200) {
                if (resp1.status == 'warning-username') {
                    toastr.warning(messagesConstants.UsernameWarning);
                } else if (resp1.status == 'warning-email') {
                    toastr.warning(messagesConstants.EmailWarning);
                }
                if (resp1.status == 'success') {
                    var inputString = {};
                    inputString = $scope.user;
                    inputString.country_code = "+1";
                    inputString.user_type = 1;
                    inputString.created = Date.now();
                    
                    inputString.password = $scope.user.password;

                    inputString.originalPassword = inputString.password;
                    var passwordEncrpt = CryptoJS.AES.encrypt(JSON.stringify(inputString.password), messagesConstants.encryptionKey);
 
                    inputString.password = passwordEncrpt.toString();
                    var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(inputString), messagesConstants.encryptionKey);
                    var encryptedJSON = {};
                    encryptedJSON.inputData = ciphertext.toString();
                    // save admin in DB
                    $scope.showloader = true;
                    UserService.addAdmin(encryptedJSON, function(response) {
                        $scope.showloader = false;
                        //console.log("response = ", response);
                        if (response.messageId == 200) {
                           $scope.user.hasOwnProperty('_id')?toastr.success(messagesConstants.updateAdmin) :toastr.success(messagesConstants.saveAdmin);
                            $timeout(function() {
                                $location.path('/users/listadmin');
                            }, 1000);
                        } else {
                            toastr.error(response.message);
                        }
                    });
                }
            }
        });
    }


    if ($stateParams.admin_id) {

        UserService.getAdmin({ admin_id: $stateParams.admin_id }, function (response) {
            if (response.messageId === 200) {
                $scope.user = response.data;
                var decrypted = CryptoJS.AES.decrypt(response.data.password, messagesConstants.encryptionKey);
                $scope.user.password = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
            } else {
                toastr.error(response.message);
            }
        })

    }

    $scope.goToBack = function() {
        window.history.back();
    }

    $scope.openSignUp = function(){
        $scope.$emit('logoutCallforCanvas');
        $localStorage.$reset();
        $rootScope.loggedInUser = false;
        $rootScope.loggedInUserType = false;
        $rootScope.userLoggedIn = false;
    }

}])

bhapp.directive('compareTo', [function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
}]);

bhapp.directive('numbersOnly', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
