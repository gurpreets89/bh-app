"use strict";

angular.module("Home")

bhapp.controller("homeController", ['$scope', '$rootScope', '$localStorage', 'HomeService', function($scope, $rootScope, $localStorage, HomeService) {

	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn 	= true;
		// $rootScope.loggedInUser 	= $localStorage.loggedInUsername;
		$rootScope.loggedInUserType 	= $localStorage.loggedInUsertype;
		var created_Date = $localStorage.loggedInUser.user.created_date;
		// if($localStorage.loggedInUser.user.first_name != undefined && $localStorage.loggedInUser.user.last_name != undefined){
		// 	$rootScope.fullName = $localStorage.loggedInUser.user.first_name + " " + $localStorage.loggedInUser.user.last_name;
		// }else{
		$rootScope.fullName = $localStorage.loggedInUsername;
		if($localStorage.loggedInUser.user.profile_pic){
			$rootScope.profilePic = "images/profile/" + $localStorage.loggedInUser.user.profile_pic;
		}else{
			$rootScope.errprofileImage = "images/avatar5.png";
			$rootScope.profilePic = "images/avatar5.png";
		}
		//}
		var newDate = new Date(created_Date);
		$rootScope.memberSinceDate = formatDate(newDate);
		$rootScope.user_id = $localStorage.loggedInUser.user._id;
	}
	else {
		$rootScope.userLoggedIn = false;
	}

	function formatDate(date) {
	 	var monthsArray = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	  	var monthName = monthsArray[date.getMonth()];
	  	var dayDate = date.getDate();
	  	dayDate = dayDate < 10 ? '0'+dayDate : dayDate;
	  	var dateReturn =  dayDate + " " + monthName + ", " + date.getFullYear();
	  	return dateReturn;
	}

	// Find users count
	var inputJSONString = {};

	inputJSONString._id = $localStorage.loggedInUser.user._id;
	inputJSONString.user_type = $localStorage.loggedInUsertype;
	
	$scope.adminCount = 0;
	$scope.clinicianCount = 0;
	var patientCounter = 0;
	$scope.adminPatientCount = 0;
	var caregivercount = 0;
	$scope.patientCount = 0;
	$scope.showloader = true;
	HomeService.findUserCount(inputJSONString, function(response) {
		$scope.showloader = false;
		if(response.messageId == 200) {
			for(var i = 0; i < response.data.length; i++){
				if(response.data[i].user_type == 2){
					$scope.clinicianCount = $scope.clinicianCount + 1;
				}else if (response.data[i].user_type == 3){
					patientCounter = patientCounter + 1;
				}else if(response.data[i].user_type == 4){
					caregivercount = caregivercount + 1;
				}
				else if (response.data[i].user_type == 1){
					$scope.adminCount = $scope.adminCount + 1;
				}

				$scope.patientCount = patientCounter + caregivercount;
			}
		}
	});
	if($localStorage.loggedInUsertype == 1){
		HomeService.findAdminPatient(inputJSONString, function(response) {
			$scope.showloader = false;
			if(response.messageId == 200) {
				for(var i = 0; i < response.data.length; i++){
					$scope.adminPatientCount = $scope.adminPatientCount + 1;
				}
			}
		});
	}
	
}])

/* directive to display default image if image url is not available */
.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
      attrs.$observe('ngSrc', function(value) {
        if (!value && attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
})