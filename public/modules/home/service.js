"use strict"

angular.module("Home")

.factory('HomeService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.findUserCount = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.findUserCount, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.findAdminPatient = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.findAdminPatient, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	return service;


}]);
