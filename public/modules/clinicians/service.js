"use strict"

angular.module("Users")

.factory('ClinicianService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.getClinicianList = function(callback) {
		communicationService.resultViaGet(webservices.clinicianList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.saveUser = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.addUser, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getUser = function(userId, callback) {
		var serviceURL = webservices.findOneUser + "/" + userId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateUser = function(inputJsonString, userId, callback) {
		var serviceURL = webservices.update + "/" + userId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	//In Use for BH APP
	service.bulkUpdateClinician = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.bulkUpdateClinician, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	//In Use for BH APP
	service.checkClinicianPatients = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.checkClinicianPatients, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	//In Use for BH APP
	service.deleteClinician = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.delClinician, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	//In Use for BH APP
	service.getClinicians = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getClinicians, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	//In Use for BH APP
	service.updatePatientsClinician = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.updatePatientsClinician, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	//In Use for BH APP
	service.getPatientsInfo = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getPatientsInfo, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	//In Use for BH APP
	service.logPatientsInfo = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.logPatientsInfo, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.invite = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.invite, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.getInvitedUsersList = function(callback) {
		communicationService.resultViaGet(webservices.invitedClinicians, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	return service;


}]);