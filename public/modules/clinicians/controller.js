"use strict";

angular.module("Users")

bhapp.controller("clinicianController", ['$scope', '$rootScope', '$localStorage', 'ClinicianService', 'UserService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', 'SweetAlert', 'toastr', function($scope, $rootScope, $localStorage, ClinicianService, UserService, ngTableParams, $stateParams, $state, $location, $timeout, SweetAlert, toastr) {

	if ($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	} else {
		$rootScope.userLoggedIn = false;
	}

	if ($rootScope.message != "") {

		$scope.message = $rootScope.message;
	}

	$scope.initialDateObj = new Date("January 01, 1980 01:15:00");

	//empty the $scope.message so the field gets reset once the message is displayed.
	$scope.message = "";
	$scope.activeTab = 0;
	$scope.user = {
		first_name: "",
		last_name: "",
		username: "",
		password: "",
		email: "",
		display_name: "",
		role: []
	}

	/* ++ angular-calendar settings ++ */
	/*var minimumdate = new Date("Mon Jan 01 1900 12:00:00");
	var dd1 = minimumdate.getDate();
	var mm1 = minimumdate.getMonth() + 1; //January is 0!
	var yyyy1 = minimumdate.getFullYear();
	if (dd1 < 10) {
		dd1 = '0' + dd1
	}
	if (mm1 < 10) {
		mm1 = '0' + mm1
	}
	minimumdate = mm1 + '/' + dd1 + '/' + yyyy1;*/
	var minimumdate = "01/01/1900";
	$scope.minDate = minimumdate;
	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd
	}
	if (mm < 10) {
		mm = '0' + mm
	}
	today = mm + '/' + dd + '/' + yyyy;
	$scope.maxDate = today;
	
	/* -- angular-calendar settings -- */

	//Toggle multilpe checkbox selection
	$scope.selection = [];
	$scope.selectionAll;
	$scope.toggleSelection = function toggleSelection(id) {
		//Check for single checkbox selection
		if (id) {
			var idx = $scope.selection.indexOf(id);
			// is currently selected
			if (idx > -1) {
				$scope.selection.splice(idx, 1);
			}
			// is newly selected
			else {
				$scope.selection.push(id);
			}
		} else { //Check for all checkbox selection
			//Check for all checked checkbox for uncheck
			if ($scope.selection.length > 0 && $scope.selectionAll) {
				$scope.selection = [];
				$scope.checkboxes = {
					checked: false,
					items: {}
				};
				$scope.selectionAll = false;
			} else {
				//Check for all un checked checkbox for check

				$scope.selectionAll = true
				$scope.selection = [];
				angular.forEach($scope.simpleList, function(item) {
					$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
					$scope.selection.push(item._id);
				});
			}
		}
	};


	//apply global Search
	$scope.applyGlobalSearch = function() {
		var term = $scope.globalSearchTerm;
		if (term != "") {
			if ($scope.isInvertedSearch) {
				term = "!" + term;
			}
			$scope.tableParams.filter({
				$: term
			});
			$scope.tableParams.reload();
		}
	}

	$scope.simpleList = [];

	$scope.getAllClinicians = function() {
		$scope.showloader = true;
		ClinicianService.getClinicianList(function(response) {
			//console.log('getAllClinicians = ',response);
			$scope.showloader = false;
			if (response.messageId == 200) {
				if (response.data.length != 0) {
					$scope.noClinician = false;
					$scope.isFiltersVisible = true;
					$scope.clinicianExist = true;
					$scope.filter = {
						firstname: '',
						lastname: '',
						email: '',
						username : ''
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 10,
						sorting: {
							is_status:"desc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [10, 25, 50, 100],
						data: response.data
					});
					//multiple checkboxes

					for (var i = 0; i < response.data.length; i++) {
						var data1 = response.data[i];
						if (data1.is_deleted == false) {
							$scope.simpleList.push(data1);
						}
					}
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				} else {
					$scope.noClinician = true;
					$scope.clinicianExist = false;
					$scope.isFiltersVisible = false;
				}
			}else{
				$scope.noClinician = true;
				$scope.clinicianExist = false;
				$scope.isFiltersVisible = false;
			}
		});
	}


	$scope.activeTab = 0;
	$scope.findOne = function() {
		if ($stateParams.id) {
			ClinicianService.getUser($stateParams.id, function(response) {
				if (response.messageId == 200) {
					$scope.user = response.data;
				}
			});
		}
		$scope.getAllRoles()
	}




	//perform action - Approve Clinician
	$scope.performAction = function() {
		$scope.showloader = true;
		var roleLength = $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		if ($scope.selectedAction == 0) {
			$scope.showloader = false;
			toastr.warning(messagesConstants.selectAction);
		} else if ($scope.selection == "") {
			$scope.showloader = false;
			toastr.warning(messagesConstants.selectActionField);
		} else {
			for (var i = 0; i < roleLength; i++) {
				var id = $scope.selection[i];
				if ($scope.selectedAction == 1) {
					updatedData.push({
						id: id,
						is_approved: true
					});
				} else if ($scope.selectedAction == 2) {
					updatedData.push({
						id: id,
						is_status: true
					});
				} else if ($scope.selectedAction == 3) {
					updatedData.push({
						id: id,
						is_status: false
					});
				}
			}
			var inputJson = {
				data: updatedData
			}
			ClinicianService.bulkUpdateClinician(inputJson, function(response) {
				$scope.showloader = false;
				$rootScope.message = messagesConstants.updateStatus;
				toastr.success(messagesConstants.ClinicianStatusSuccess);
				$state.reload();
			});
		}
	}


	/*
	|-----------------------------------------
	|	Delete Clinician : developer - Jatinder Singh
	|  	Function to Delete the Clinician User not physically but by status/flag
	|-----------------------------------------
	*/
	$scope.deleteClinician = function(clinicianId, email) {
		SweetAlert.swal({
				title: messagesConstants.ClinicianSweetAlertTitle,
				text: messagesConstants.ClinicianSweetAlertText1 + email + messagesConstants.ClinicianSweetAlertText2,
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: messagesConstants.ClinicianSweetAlertConfirmText,
				cancelButtonText: messagesConstants.ClinicianSweetAlertCancelText,
				closeOnConfirm: true
			},
			function(isConfirm) {
				if (isConfirm) {
					if (clinicianId) {
						// check if clinician has patients.
						var clinicianInputJson = {};
						clinicianInputJson.parent = clinicianId;
						$scope.showloader = true;
						ClinicianService.checkClinicianPatients(clinicianInputJson, function(response) {
							$scope.showloader = false;
							if (response.messageId == 200) {
								if (response.data.length > 0) {
									toastr.error(messagesConstants.TransferPatientError, messagesConstants.RemoveClinicError);
								} else {
									$scope.deleteClinicianNow(clinicianId);
								}
							} else {
								$scope.deleteClinicianNow(clinicianId);
							}
						});
					}
				}
			}
		);
	}

	/*
	|-----------------------------------------
	|	This function will work after deleteClinician checks that no patients exists.
	|	developer - Gurpreet Singh
	|  	Function to Delete the Clinician not physically but by status/flag
	|-----------------------------------------
	*/

	$scope.deleteClinicianNow = function(clinicianId) {
		$scope.showloader = true;
		var inputJsonString = {};
		inputJsonString._id = clinicianId;
		inputJsonString.is_deleted = 1;
		inputJsonString.is_status = 0; //So that he/she can not able to login again in our platform.
		UserService.updateUserProfile(inputJsonString, function(response) {

			$scope.showloader = false;
			if (response.messageId == 200) {
				toastr.success(messagesConstants.deleteClinician);
				$state.reload();
			} else {
				toastr.error(messagesConstants.RemoveClinicError);
			}
		});
	}


	/*
	|-----------------------------------------
	|	Transfer Patients
	|	developer - Gurpreet Singh
	|-----------------------------------------
	*/
	$scope.transferrablePatients = {};
	$scope.isPatients = false;
	$scope.availableClinicians = {};
	$scope.transferObj = {};
	$scope.transferPatients = function() {
		$scope.getClinicianInfoById();
		$scope.showloader = true;
		if ($stateParams.id) {
			var clinicianInputJson = {};
			clinicianInputJson.parent = $stateParams.id;
			// check if clinician has patients.
			ClinicianService.checkClinicianPatients(clinicianInputJson, function(response) {
				$scope.showloader = false;
				if (response.messageId == 200) {
					if (response.data.length > 0) {
						$scope.transferrablePatients = response.data;
						$scope.isPatients = true;
					}
					/*else{
										toastr.error("No patients exist under this clinician.");
									    }*/
				}
				$scope.transferObj.clinician = '';
				ClinicianService.getClinicians(clinicianInputJson, function(response) {
					if (response.messageId == 200) {
						if (response.data.length > 0) {
							$scope.availableClinicians = response.data;
						}
					}
				});
			});
		} else {
			$scope.showloader = false;
		}
	}

	/*
	|-----------------------------------------
	|	Save Transfer Patients data
	|	developer - Gurpreet Singh
	|-----------------------------------------
	*/
	$scope.transferSave = function() {
		$scope.showloader = true;
		var patientsArr = [];
		var clinicianInputJson = {};
		if ($scope.transferObj.patients) {
			var array = $.map($scope.transferObj.patients, function(value, index) {
				if(value)
					patientsArr.push(index);
			});
			if(patientsArr.length > 0){
				clinicianInputJson.parent = $stateParams.id;
				clinicianInputJson.clinician = $scope.transferObj.clinician;
				clinicianInputJson.patients = patientsArr;
				/** get transferrable patients data first to update in "patientLog" table. **/
				ClinicianService.getPatientsInfo(clinicianInputJson, function(response) {
					$scope.showloader = false;
					if (response.messageId == 200) {
						clinicianInputJson.data = response.data;
						ClinicianService.logPatientsInfo(clinicianInputJson, function(response) {});
					}
				});
				ClinicianService.updatePatientsClinician(clinicianInputJson, function(response) {
					$scope.showloader = false;
					if (response.messageId == 200) {
						toastr.success(response.message);
					} else if (response.messageId == 203) {
						toastr.error(response.message);
					}
					$state.reload();
				});
			}else{
				$scope.showloader = false;
				toastr.error(messagesConstants.selectPatientsError);
			}
		} else {
			$scope.showloader = false;
			toastr.error(messagesConstants.selectPatientsError);
		}
	}

	/* Created by Jatinder on 14-07-2016 To show clinician details in model */
	$scope.getDetails = function(user) {
		$scope.userData = user;
	}

	/*
	 *	Function to Edit the Clinician Profile
	 *  Created By: GpSingh
	 */
	$scope.getClinicianInfoById = function() {
		$scope.showloader = true;
		$scope.readonly = false;
		$scope.user.dob = '';
		if ($stateParams.id) {
			$scope.readonly = true;
			$scope.mode = "Edit";
			UserService.getUser($stateParams.id, function(response) {
				$scope.showloader = false;
				if (response.messageId == 200) {
					var d = new Date(response.data.dob);
					var formatdate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
					response.data.dob = formatdate;
					$scope.user = response.data;
					var decrypted = CryptoJS.AES.decrypt(response.data.password, messagesConstants.encryptionKey);
					$scope.user.password = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));

				} else {
					toastr.error(messagesConstants.getCliniciansError);

				}
			});
		} else {
			$scope.showloader = false;
			$scope.mode = "Add";
			$scope.readonly = false;
		}
	}

	/*
	 *	Function to Add/Update a Clinician(Profile).
	 *  Created By: GpSingh
	 */
	$scope.addUser = function() {
		$scope.showloader = true;
		if (typeof($stateParams.id) == 'undefined') {
			var checkExist = {};
			checkExist.username = $scope.user.username;
			checkExist.email = $scope.user.email;
			if ($scope.user.admin_added) {
				// Add New Clinician Code
				// UserService.checkUser($scope.user.username, function(resp1) {
				UserService.checkUserAndEmail(checkExist, function(resp1) {
					$scope.showloader = false;
					$scope.errordetail = {};
	//console.log('==', resp1.messageId, typeof resp1.messageId, 200, typeof 200)
					if (resp1.messageId === 200) {
						if (resp1.status === 'warning-username') {
							$scope.errordetail.usernameexist = true;
						}
						if (resp1.status === 'warning-email') {
							$scope.errordetail.emailexist = true;
						}
						if (resp1.status === 'success') {
							var inputString = {};
							inputString = $scope.user;
							inputString.user_type = 2;
							inputString.country_code = "+1";
							inputString.is_status = true;
							// inputString.clinic_name = 'Test Clinic';
							inputString.created = Date.now();
							var admin_added = inputString.admin_added;
							// delete inputString.admin_added;
							if (admin_added) { //If add by admin, use patient add service because of re-useability.
								inputString.password = $scope.user.password;
								inputString.originalPassword = inputString.password;
								var passwordEncrpt = CryptoJS.AES.encrypt(JSON.stringify(inputString.password), messagesConstants.encryptionKey);
								inputString.password = passwordEncrpt.toString();
								var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(inputString), messagesConstants.encryptionKey);
								var encryptedJSON = {};
								encryptedJSON.inputData = ciphertext.toString();
								$scope.showloader = true;
								UserService.savePatient(encryptedJSON, function(response) {
									$scope.showloader = false;
									if (response.messageId == 200) {
										toastr.success(messagesConstants.saveClinician);
										$timeout(function() {
											$location.path('/clinicians');
										}, 1000);
									} else {
										toastr.error(response.message);
									}
								});
							}
						}
					}
				});
			} else {

				// Clinician Sign-up form start here 
				UserService.checkUserAndEmailSignup(checkExist, function(resp1) {
					$scope.showloader = false;
					$scope.errordetail = {};
					if (resp1.messageId == 200) {
						if (resp1.status == 'warning-username') {
							$scope.errordetail.usernameexist = true;
						}
						if (resp1.status == 'warning-email') {
							$scope.errordetail.emailexist = true;
						}
						if (resp1.status == 'success') {
							var inputString = {};
							inputString = $scope.user;
							inputString.user_type = 2;
							inputString.is_status = true;
							// inputString.clinic_name = 'Test Clinic';
							// password encryption when clinician register from login page
							inputString.originalPassword = inputString.password;
							var passwordEncrpt = CryptoJS.AES.encrypt(JSON.stringify(inputString.password), messagesConstants.encryptionKey);
							inputString.password = passwordEncrpt.toString();
							inputString.password2 = passwordEncrpt.toString();

							inputString.created = Date.now();
							var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(inputString), messagesConstants.encryptionKey);
							var encryptedJSON = {};
							encryptedJSON.inputData = ciphertext.toString();
							$scope.showloader = true;
							// If data came from sign-up form, this code will execute as of no-session required.
							UserService.saveUser(encryptedJSON, function(response) {
								$scope.showloader = false;
								$scope.errormessage = '';
								$scope.message = '';
								if (response.messageId == 200) {
									$scope.message = messagesConstants.AccountRegisterSuccess;
									$scope.user = '';
									$scope.userSaved = true;
								} else {
									// customized error messages for firstname, lastname, email 
									var eMsg = response.message;
									var msgSet = false;
									var eMsgArr = eMsg.split(",");
									if (typeof eMsgArr[0] != 'undefined') {
										var e1 = eMsgArr[0];
										var e1Arr = e1.split(" ");
										if (e1Arr[0] == 'parent') {
											$scope.errormessage = messagesConstants.ReferanceCodeError;
											msgSet = true;
										}
									}
									if (msgSet == false && (typeof eMsgArr[1] != 'undefined')) {
										var e2 = eMsgArr[1];
										var e2Arr = e2.split(" ");
										if (e2Arr[0] == 'email') {
											$scope.errormessage = messagesConstants.validEmailError;
											msgSet = true;
										}
									}
								}
							});
						}
					}
				});
			}
		} else { /* If Edit Clinician code starts */

			var checkExist = {};
			if ($scope.user.hasOwnProperty('_id'))
				checkExist._id = $scope.user._id;
			checkExist.username = $scope.user.username;
			checkExist.email = $scope.user.email;
			// Add New Clinician Code
			// UserService.checkUser($scope.user.username, function(resp1) {
			UserService.checkUserAndEmail(checkExist, function (resp1) {
				$scope.showloader = false;
				$scope.errordetail = {};
				//console.log('==', resp1.messageId, typeof resp1.messageId, 200, typeof 200)
				if (resp1.messageId === 200) {
					if (resp1.status === 'warning-username') {
						$scope.errordetail.usernameexist = true;
					}
					if (resp1.status === 'warning-email') {
						$scope.errordetail.emailexist = true;
					}
					if (resp1.status === 'success') {

						var inputString = {};
						inputString = $scope.user;
						var passwordEncrpt = CryptoJS.AES.encrypt(JSON.stringify(inputString.password), messagesConstants.encryptionKey);
							inputString.password = passwordEncrpt.toString();
						inputString.modified = Date.now();
						UserService.updateUserProfile(inputString, function (response) {
							$scope.showloader = false;
							if (response.messageId == 200) {
								toastr.success(messagesConstants.updateClinician);
								$timeout(function () {
									$location.path('/clinicians');
								}, 1000);
							}
						});



					}
				}
			})
		}
	}

	/*
    |-------------------------------------------
    |   Generate Random password function
    |   developer : gurpreet
    |-------------------------------------------
    */
	function randomPassword(length) {
		var chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNP123456789";
		var pass = "";
		for (var x = 0; x < length; x++) {
			var i = Math.floor(Math.random() * chars.length);
			pass += chars.charAt(i);
		}
		return pass;
	}
	
	/** function to send invite to new clinicians from the Admin platform.*/
		$scope.clinicianEmail = {};
		$scope.inviteClinician = function() {
			$scope.showloader = true;
			var inputString = {};
			inputString = $scope.clinicianEmail;
			ClinicianService.invite(inputString, function(response) {
				$scope.showloader = false;
				if (response.messageId == 200 && response.status == 'success') {
					//console.log(response.message);
					toastr.success(response.message);
					$timeout(function() {
						$location.path('/clinicians');
					}, 1000);
				} else if (response.messageId == 200 && response.status == 'warning') {
					toastr.warning(messagesConstants.EmailExistWarning);
				}
			});
		}
		
	/**
	 *	function to get list of Invited Users
	 *	developer : Gurpreet Singh
	 **/
		$scope.InvitedUsersCount = false;
		$scope.getInvitedUsers = function() {
			$scope.showloader = true;
			ClinicianService.getInvitedUsersList(function(response) {
				if (response.messageId == 200) {
					$scope.showloader = false;
					if (response.data.length == 0) {
						$scope.InvitedUsersCount = false;
						$scope.errorMessageAvailable = true;
						$scope.noDataMessage = messagesConstants.NoInvitedUsers;
					} else {
						$scope.InvitedUsersCount = true;
						$scope.errorMessageAvailable = false;
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 10,
							sorting: {
								email: "asc",
								created_date: "desc"
							}
						}, {
							total: response.data.length,
							counts: [10, 25, 50, 100],
							data: response.data,
							defaultSort: "desc"
						});
					}
				}
			});
		}

}])

bhapp.directive('numbersOnly', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, ngModelCtrl) {
			function fromUser(text) {
				if (text) {
					var transformedInput = text.replace(/[^0-9]/g, '');

					if (transformedInput !== text) {
						ngModelCtrl.$setViewValue(transformedInput);
						ngModelCtrl.$render();
					}
					return transformedInput;
				}
				return undefined;
			}
			ngModelCtrl.$parsers.push(fromUser);
		}
	};
});