"use strict"

angular.module("Goals")

.factory('GoalService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


	service.getGoalList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.goalList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveGoal = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addGoal, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.getGoal = function(goalId, callback) {
		var serviceURL = webservices.findOneGoal + "/" + goalId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateGoal = function(inputJsonString, goalId, callback) {
		var serviceURL = webservices.updateGoal + "/" + goalId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateGoalStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateGoal, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.deleteGoal = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.delGoal, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	service.getUserByGoal = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getUserByGoal, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	return service;


}]);
