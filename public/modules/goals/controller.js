"use strict";
angular.module("Users")
bhapp.controller("goalController", ['$scope', '$stateParams', '$rootScope', '$localStorage','GoalService', 'UserService', 'PatientService', 'ngTableParams', '$state','$location', 'SweetAlert', '$timeout', 'toastr', function($scope, $stateParams, $rootScope, $localStorage, GoalService, UserService, PatientService , ngTableParams, $state, $location, SweetAlert, $timeout, toastr){
		if($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
		}
		else {
			$rootScope.userLoggedIn = false;
		}

		if($rootScope.message != "") {

			$scope.message = $rootScope.message;
		}

		//empty the $scope.message so the field gets reset once the message is displayed.
		$scope.message = "";
		$scope.activeTab = 0;
    	$scope.getAllGoals = function(){
    		
    		// Filter Records on the basic of user type added by RaJesh (Start)
			//Contains the filter options
		  	$scope.filterOptions = {
		    	stores: [
		    		{id : 1, name : 'Show All', type: 0},
		      		{id : 2, name : 'Patient', type: 3},
					{id : 3, name : 'Caregiver', type: 4},
		    	]
		  	};
		  	//Mapped to the model to filter
			$scope.filterItem = {
			    store: $scope.filterOptions.stores[0]
			}
			// Filter Records on the basic of user type added by RaJesh (End)

    		var inputJsonString = {};
    		inputJsonString.clinician = $localStorage.loggedInUser.user._id;
    		$scope.showloader = true;
    		GoalService.getGoalList(inputJsonString, function(response) {
    			$scope.showloader = false;
				if(response.messageId == 200) {
					if(response.data.length == 0){
						$scope.noGoals = true;
						$scope.goalExists = false;
					}else{
						$scope.noGoals=false;
						$scope.goalExists=true;
					}

					$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{_id:"desc"}}, { total:response.data.length, counts: [10, 25, 50, 100], data: response.data});
					$scope.simpleList = response.data;
					$scope.checkboxes = {
						checked: false,
						items:{}
					};
				}
			});
    	}


//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.toggleSelection = function toggleSelection(id) {
		//Check for single checkbox selection
			if(id){
				var idx = $scope.selection.indexOf(id);
				// is currently selected
				if (idx > -1) {
				    $scope.selection.splice(idx, 1);
				}
				// is newly selected
				else {
				    $scope.selection.push(id);
				}
		    } else { //Check for all checkbox selection
		    	if($scope.selection.length > 0 && $scope.selectionAll){
		    		$scope.selection = [];
		    		$scope.checkboxes = {
		    			checked: false,
		    			items:{}
		    		};	
		    		$scope.selectionAll = false;
		    	} else {      	//Check for all un checked checkbox for check

		    		$scope.selectionAll = true
		    		$scope.selection = [];
		    		angular.forEach($scope.simpleList, function(item) {
		    			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
		    			$scope.selection.push(item._id);
		    		});
		    	}
			}
		};
	// //Toggle multilpe checkbox selection

		// Update Answer options status
		$scope.performAction = function() {				
			var roleLength =  $scope.selection.length;
			var updatedData = [];
			$scope.selectedAction = selectedAction.value;
			$scope.showloader = true;
			if($scope.selectedAction == 0){
				toastr.warning(messagesConstants.selectAction);
				$scope.showloader = false;
			}else if ($scope.selection == "") {
				toastr.warning(messagesConstants.selectActionField);
				$scope.showloader = false;
			}
			else{	
				for(var i = 0; i< roleLength; i++){
					var id =  $scope.selection[i];
					if($scope.selectedAction == 1) {
						updatedData.push({id: id, is_status: true});
					}
					else if($scope.selectedAction == 2) {
					  updatedData.push({id: id, is_status: false});
					}
				}
				var inputJson = {data: updatedData}
				GoalService.updateGoalStatus(inputJson, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.GoalsStatusSuccess);
						$state.reload();
					} else {
						toastr.error(messagesConstants.GoalsStatusError);
					}
				});
			}
		}


    	$scope.filterGoal = function(data){
    		$scope.showloader = true;
    		var inputJsonString = {};
    		inputJsonString.clinician = $localStorage.loggedInUser.user._id;
			if(data.type == 3 || data.type == 4){
				inputJsonString.user_type = data.type;
			}
			if(data.type == 3){
				$scope.noDataMessage = "No goals available for patients.";
			}else if(data.type == 4){
				$scope.noDataMessage = "No goals available for caregivers.";
			}
			//console.log(inputJsonString);
    		GoalService.getGoalList(inputJsonString, function(response) {
    			//console.log("HERE");
    			$scope.showloader = false;
    			//console.log(response);
				if(response.messageId == 200) {
					if(response.data.length == 0){
						$scope.errorMessageAvailable = true;
					}else{
						$scope.errorMessageAvailable = false;
					}

					$scope.tableParams = new ngTableParams({page:1, count:10}, { total:response.data.length, counts: [10, 25, 50, 100], data: response.data});
					$scope.simpleList = response.data;	
				}
			});
    	}

    	$scope.goalType = [
		   {
		      "id": 1,
		      "title": "Next Session Goals"
		    },
		    {
		      "id": 2,
		      "title": "Patient Midterm Goals"
		    },
		    {
		      "id": 3,
		      "title": "Therapy Goals"
		    }
		];

		$scope.userType = [
			{
				"id": 3,
				"type": 'Patient'
			},
			{
				"id": 4,
				"type": 'Caregiver'
			}
		];

    	$scope.goals = {};

    	
    	$scope.addGoals = function(){
    		$scope.showloader = true;
    		var sendData = {};
    		if($scope.goals.goal_type == 1){
    			sendData.goal_type = "Next Session Goals";
    		}else if($scope.goals.goal_type == 2){
    			sendData.goal_type = "Patient Midterm Goals";
    		}else if($scope.goals.goal_type == 3){
    			sendData.goal_type = "Therapy Goals";
    		}
    		sendData.patient = $scope.goals.patient_id;
    		sendData.clinician = $localStorage.loggedInUser.user._id;
    		sendData.user_type = $scope.goals.user_type;
    		sendData.title = $scope.goals.goal_title;
    		sendData.is_status = true;
    		sendData.created_date = Date.now();
			sendData.modified_date = Date.now();
			sendData.is_achieved = false;
			sendData.is_deleted = false;
			sendData.goal_type_id =  $scope.goals.goal_type,
			sendData.goal_question = $scope.goals.goal_question;

			// Add new Goals for a single Patient/Caregiver
			 if($stateParams.id && $stateParams.type){
				// save for new Patient goal 
				GoalService.saveGoal(sendData, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.AddGoalSuccess);
						window.history.back();
					}
					else{
						toastr.error(messagesConstants.AddGoalError);
					}
				});
			}
			else if($stateParams.g_id && $stateParams.p_id) {
				// Edit/Update Patient existing goal
				GoalService.updateGoal(sendData,$stateParams.g_id, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.UpdateGoalSuccess);
						window.history.back();
					}else{
						toastr.error(messagesConstants.UpdateGoalError);
					}
				});
			}
			else{
				// save new goal 
				GoalService.saveGoal(sendData, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.AddGoalSuccess);
						window.history.back();
					}else{
						toastr.error(messagesConstants.AddGoalError);
					}
				});
			}
    	}


    $scope.DisableSelect = true;
    $scope.DisableUserTypeSelect = false;
    $scope.getUserList = function(data){
    	$scope.showloader = true;
    	if(!data){
    		$scope.DisableSelect = true;
    	}else{
    		$scope.DisableSelect = false;
    	}
    	$scope.patientDetails = {};
		$scope.patientName = [];
    	$scope.inputJsonString = {};
    	$scope.inputJsonString.user_type = data;
    	$scope.inputJsonString.parent = $localStorage.loggedInUser.user._id;
    	GoalService.getUserByGoal($scope.inputJsonString, function(response) {
    		$scope.showloader = false;
			if(response.messageId == 200) {
				if(!response.data){
					if(data == 4){
						toastr.error(messagesConstants.CaregiverError);
					}else if(data == 3){
						toastr.error(messagesConstants.addPatientError);
					}
				}else{
					for(var i = 0; i < response.data.length; i++){
						$scope.patientDetails[i + 1] = {
							"id" : response.data[i]._id,
							"name" : response.data[i].first_name + " " + response.data[i].last_name
						}
					}

					for( var i in $scope.patientDetails ) {
					    if ($scope.patientDetails.hasOwnProperty(i)){
					       $scope.patientName.push($scope.patientDetails[i]);
					    }
					}
				}
				
			}
		});
    }

	$scope.getGoal = function(){
		$scope.showloader = true;
		// Add new Goals for a single Patient/Caregiver
		if($stateParams.id && $stateParams.type){
			//console.log("HERE");
			UserService.getUser($stateParams.id, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					$scope.userDetails = response.data;
					$scope.add_edit_goal_title = "Add New Goal - ";
					$scope.goals.patient_id = $stateParams.id;
					$scope.goals.user_type =  parseInt($stateParams.type);
					$scope.DisableUserTypeSelect = true;
					var uName = response.data.first_name + " " + response.data.last_name;
					$scope.patientDetails = {};
					$scope.patientDetails[1] = {
						"id" : $stateParams.id,
						"name" : uName
					}

					$scope.STATE_PARAM = response.data;
				}
			});
		}
		// Edit Patient/Caregiver Goals
		else if ($stateParams.g_id && $stateParams.p_id) {
			GoalService.getGoal ($stateParams.g_id, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					$scope.add_edit_goal_title = "Edit Goal - ";
					$scope.getUserList(response.data.user_type);
					$scope.goals.goal_title = response.data.title;
					$scope.goals.goal_question = response.data.goal_question;
					$scope.goals.goal_type = response.data.goal_type_id;
					$scope.goals.patient_id = response.data.patient._id;
					$scope.goals.user_type = response.data.user_type;
					$scope.DisableUserTypeSelect = true;
					$scope.DisableSelect = true;
					$scope.STATE_PARAM = response.data.patient;
				}
			});
		}
		// Add new Goals for All Patient/Caregiver
		else{
			$scope.showloader = false;
			$scope.add_edit_goal_title = messagesConstants.AddEditGoals;
		}
	}

	// Cancel Button in Goals
	$scope.goToBack = function(){
		window.history.back();
	}

	$scope.delGoal = function(id, name){
		SweetAlert.swal({
			title: "Confirmation",
			text: "Are you sure you want to delete goal ("+name+")",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No!",
			closeOnConfirm: true}, 
			function(isConfirm){ 
		  		if(isConfirm) {
		  			$scope.showloader = true;
			  		var delete_object = {'_id':id};
		            GoalService.deleteGoal(delete_object, function(response) {
		            	$scope.showloader = false;
		            	$timeout(function() {
		            		$scope.getAllGoals();
					        $state.go('/goals');
					    }, 100);
						toastr.success(messagesConstants.GoalDeleteSuccess);
					});
	        	}
			}
		);
	}
}]);