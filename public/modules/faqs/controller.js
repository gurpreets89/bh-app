"use strict";

angular.module("Faqs")

bhapp.controller("faqController", ['$scope', '$rootScope', '$localStorage','FaqService', 'UserService','ngTableParams', 'SweetAlert','$timeout', '$stateParams', '$location', '$state', 'toastr',  function($scope, $rootScope, $localStorage, FaqService, UserService, ngTableParams, SweetAlert,  $timeout, $stateParams, $location, $state, toastr){

	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	}
	else {
		$rootScope.userLoggedIn = false;
	}
	$scope.isFiltersVisible = false;
	//Toggle multilpe checkbox selection
	$scope.selection = [];
	$scope.selectionAll = false;
	$scope.toggleSelection = function toggleSelection(id) {
	//Check for single checkbox selection
		if(id){
			var idx = $scope.selection.indexOf(id);
			// is currently selected
			if (idx > -1) {
			    $scope.selection.splice(idx, 1);
			    if($scope.listForToggle.length != $scope.selection.length){
			     	$scope.selectionAll = false;
			    }
			}// is newly selected
			else {
			    $scope.selection.push(id);
			    if($scope.listForToggle.length == $scope.selection.length){
			     	$scope.selectionAll = true;
			    }else{
			    	$scope.selectionAll = false;
			    }
			}

	    } else { //Check for all checkbox selection
	    	if($scope.selection.length > 0 && $scope.selectionAll){
	    		$scope.selection = [];
	    		$scope.checkboxes = {
	    			checked: false,
	    			items:{}
	    		};	
	    		$scope.selectionAll = false;
	    	} else {      	//Check for all un checked checkbox for check
	    		$scope.selectionAll = true
	    		$scope.selection = [];
	    		angular.forEach($scope.simpleList, function(item) {
	    			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
	    			$scope.selection.push(item._id);
	    		});
	    	}
		}
		//console.log($scope.selection)
	};

	$scope.downloadCSV = function(){
        FaqService.exportFaqList(function(response){
            $scope.showloader = false;
            if(response.messageId == 200) {
                window.open('/faqlist.csv');
            }
        });
    }

	// Update Answer options status
	$scope.performAction = function() {				
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		$scope.showloader = true;
		if($scope.selectedAction == 0){
			toastr.warning(messagesConstants.selectAction);
			$scope.showloader = false;
		}else if ($scope.selection == "") {
			toastr.warning(messagesConstants.selectActionField);
			$scope.showloader = false;
		}
		else{	
			for(var i = 0; i< roleLength; i++){
				var id =  $scope.selection[i];
				if($scope.selectedAction == 1) {
					updatedData.push({id: id, is_status: true});
				}
				else if($scope.selectedAction == 2) {
				  updatedData.push({id: id, is_status: false});
				}
			}
			var inputJson = {data: updatedData};
			FaqService.updateFaqStatus(inputJson, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(messagesConstants.FAQUpdateSuccess);
					$state.reload();
				} else {
					toastr.error(messagesConstants.FAQUpdateError);
				}
			});
		}
	}

	$scope.getAllFaqs = function(){
		$scope.showloader = true;
		FaqService.getAllFaqs (function(response) {
			$scope.showloader = false;
			if(response.messageId == 200) {
				if(response.data.length != 0){
					$scope.isFiltersVisible = true;
					$scope.filter = {
						faq_question: '',
						faq_answer: ''
					};
					$scope.noFaq = false;
					$scope.FaqExist = true;
					$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{is_status:"desc"}}, { total:response.data.length, counts: [10, 25, 50, 100], data: response.data});
					$scope.simpleList = response.data;
					$scope.listForToggle = new Array();
					for(var i = 0; i < response.data.length; i++){
						if(response.data[i].is_deleted == false){
							$scope.listForToggle.push(response.data[i]);
						}
					}
					$scope.checkboxes = {
						checked: false,
						items:{}
					};
				}else{
					$scope.noFaq = true;
					$scope.FaqExist = false;
				}
			}
		});	
	}

	/* Function to get the one Faq data
	*  Created By: Rajesh Thakur
	*/
	$scope.getFaqData = function() {
		$scope.showloader = true;
		if($stateParams.id){
			$scope.add_edit_faq_screen_title = messagesConstants.EditFaqScreenTitle;
			var inputJsonString = {};
			inputJsonString._id = $stateParams.id;
			FaqService.findOneFaq(inputJsonString, function(response) {
				$scope.showloader = false;
	    		if(response.messageId == 200) {
					$scope.faqObj = response.data;
				} else {
					toastr.error(messagesConstants.NoFAQFound);
			 	}
			});
	    }else{
	    	$scope.add_edit_faq_screen_title = messagesConstants.AddFaqScreenTitle;
	    	$scope.showloader = false;
	    } 
	}

	/* Function to Add/Edit the Faq
	*  Created By: Rajesh Thakur
	*/

	$scope.addFAQ = function() {
		$scope.showloader = true;
		var inputJsonString = {};
		inputJsonString = $scope.faqObj;


		var temp = CKEDITOR.instances['faq_answer'];
		inputJsonString.faq_answer = temp.getData();
		
		if($stateParams.id){
			// Update FAQ
			inputJsonString._id = $stateParams.id;
			inputJsonString.modified_date = Date.now();
			FaqService.saveFaq(inputJsonString, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(messagesConstants.updateFAQ);
					$timeout(function() {
				        $location.path('/faqs');
				    }, 2000);
				}else{
					toastr.error(messagesConstants.FAQEditError);
				}
			});
		} else {
			// SAVE FAQ
			FaqService.saveFaq(inputJsonString, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(messagesConstants.saveFAQ);
					$timeout(function() {
				        $location.path('/faqs');
				    }, 2000);
				}else{
					toastr.error(messagesConstants.FAQAddError);
				}
			});
		}
	}

	/*
	* Del FAQ Question in Web-Admin
	* Created: RaJesh Thakur
	* Used in BH APP project
    */
    var editor_attributes = document.getElementById("faq_answer");
    if(editor_attributes != null){
	    var editor = CKEDITOR.instances.faq_answer;
	    if (editor) {
	        editor.destroy(true); 
	    }   
	    CKEDITOR.replace('faq_answer');
		CKEDITOR.editorConfig = function(config) {
			config.language = 'es';
			config.uiColor = '#F7B42C';
			config.height = 300;
			config.toolbarCanCollapse = true;
		};
	}
	
    $scope.delQuestion = function(id, qname){
		SweetAlert.swal({
			title: "Confirmation",
			text: "Are you sure you want to delete question ("+qname+")",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No!",
			closeOnConfirm: true}, 
			function(isConfirm){ 
		  		if(isConfirm) {
			  		var delete_object = {'_id':id};
			  		$scope.showloader = true;
		            FaqService.deleteFaq(delete_object, function(response) {
		            	$scope.showloader = false;
		            	$timeout(function() {
		            		$scope.getAllFaqs();
					        $location.path('/faqs');
					    }, 100);
						toastr.success(messagesConstants.QuestionDeleteSuccess);
					});
	        	}
			}
		);
	}

}])

bhapp.directive('maximumWordsValidation', function () {
    'use strict';
	return {
	  	require: 'ngModel',
	  	link: function (scope, element, attrs, ngModelCtrl) {
	    // Figure out name of count variable we will set on parent scope
	    var wordCountName = attrs.ngModel.replace('.', '_') + '_words_count';

	    scope.$watch(function () {
	      return ngModelCtrl.$modelValue;
	    }, function (newValue) {
	      var str = newValue && newValue.replace('\n', '');
	      // Dont split when string is empty, else count becomes 1
	      var wordCount = str ? str.split(' ').length : 0;
	      // Set count variable
	      scope.$parent[wordCountName] = wordCount;
	      // Update validity
	      var max = attrs.maximumWordsValidation;
	      ngModelCtrl.$setValidity('maximumWords', wordCount <= max);
	    });
	  }
	};
});