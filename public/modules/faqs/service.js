"use strict"

angular.module("Faqs")

.factory('FaqService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};
	
	service.getAllFaqs = function(callback) {
		communicationService.resultViaGet(webservices.getAllFaqs, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.saveFaq = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.saveFaq, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	service.exportFaqList = function(callback) {
		
		communicationService.resultViaGet(webservices.exportfaqList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.findOneFaq =  function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.findOneFaq, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateFaqStatus =  function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.updateFaqStatus, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.deleteFaq = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.deleteFaq, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	return service;
}]);
