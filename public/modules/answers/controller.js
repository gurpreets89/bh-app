"use strict";

angular.module("Answers")

bhapp.controller("answerController", ['$scope', '$rootScope', '$localStorage','AnswerService', 'UserService','ngTableParams', 'SweetAlert','$timeout', '$stateParams','$state','$location', 'toastr',  function($scope, $rootScope, $localStorage, AnswerService, UserService, ngTableParams, SweetAlert,  $timeout, $stateParams, $state, $location, toastr){

		if($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
		}
		else {
			$rootScope.userLoggedIn = false;
		}
		
		if($rootScope.message != "") {
		
			$scope.message = $rootScope.message;
		}

		$scope.downloadCSV = function(){
	        AnswerService.exportAnswerList(function(response){
	            console.log(response)
	            $scope.showloader = false;
	            if(response.messageId == 200) {
	                window.open('/answerlist.csv');
	            }
	        });
        }


		//empty the $scope.message so the field gets reset once the message is displayed.
		$scope.message = "";
		$scope.activeTab = 0;
		$scope.add_answer_screen_title = messagesConstants.AddAnswerScreenTitle;
		$scope.user = {first_name: "", last_name: "", username: "", password: "", email: "", display_name: "", role: []}

		/*
		*
		* Created By : Jatinder singh
		* Created on : 11 July 2016
		* Function: Get list of all answer options to display in admin porfile.
		*
		*
		*/

		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.toggleSelection = function toggleSelection(id) {
		//Check for single checkbox selection
			if(id){
				var idx = $scope.selection.indexOf(id);
				// is currently selected
				if (idx > -1) {
				    $scope.selection.splice(idx, 1);
				}
				// is newly selected
				else {
				    $scope.selection.push(id);
				}
		    } else { //Check for all checkbox selection
		    	if($scope.selection.length > 0 && $scope.selectionAll){
		    		$scope.selection = [];
		    		$scope.checkboxes = {
		    			checked: false,
		    			items:{}
		    		};	
		    		$scope.selectionAll = false;
		    	} else {      	//Check for all un checked checkbox for check

		    		$scope.selectionAll = true
		    		$scope.selection = [];
		    		angular.forEach($scope.simpleList, function(item) {
		    			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
		    			$scope.selection.push(item._id);
		    		});
		    	}
			}
		};


		$scope.getAllAnswerOptions = function(){
			$scope.showloader = true;
			$scope.add_answer_screen_title = messagesConstants.AddEditAnswerScreenTitle;
			AnswerService.getAllAnswerOptions (function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					if(response.data.length != 0){
						$scope.noAnswer = false;
						$scope.answerExist = true;
					$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{is_status:"desc"}}, { total:response.data.length, counts: [10, 25, 50, 100], data: response.data});
					$scope.simpleList = response.data;
					$scope.checkboxes = {
						checked: false,
						items:{}
					};
				}else{
						$scope.noAnswer = true;
						$scope.answerExist = false;
					}
				}
			});	
    	}

    	/*
		* Add/Update Answer Text and score data in Web-Admin
		* Created: Jatinder Singh 
		* Used in BH APP project
	    */
	   
		$scope.addAnswerOption = function() {
			$scope.showloader = true;
			var inputJsonString = {};
			inputJsonString = $scope.answerObj;
			if($stateParams.id){
				inputJsonString._id = $stateParams.id;
				AnswerService.saveAnswerOption(inputJsonString, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.updateanswerOption);
						$timeout(function() {
					        $location.path('/answers');
					    }, 2000);
					}else{
						toastr.error(messagesConstants.AnswerEditError);
					}
				});
			} else {
				AnswerService.saveAnswerOption(inputJsonString, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.saveAnswerOption);
						$timeout(function() {
					        $location.path('/answers');
					    }, 2000);
					}else{
						toastr.error(messagesConstants.AnswerAddError);
					}
				});
			}
		}


		/* Function to Edit the Answer options
		*  Created By: Jatinder Singh
		*/
		$scope.getAnswerOptionData = function() {
			$scope.showloader = true;
			if($stateParams.id){
				$scope.add_answer_screen_title = messagesConstants.EditAnswerOption;
				var inputJsonString = {};
				inputJsonString._id = $stateParams.id;
				AnswerService.findOneAnswer(inputJsonString, function(response) {
					$scope.showloader = false;
		    		if(response.messageId == 200) {
						$scope.answerObj = response.data;
					} else {
						toastr.error(messagesConstants.AnswerEditError);
				 	}
				});
		    }else{
		    	$scope.showloader = false;
		    } 
		}

		/* Function to Edit the Answer options status
		*  Created By: RaJesh
		*/

		// Update Answer options status
		$scope.performAction = function() {					
			var roleLength =  $scope.selection.length;
			var updatedData = [];
			$scope.showloader = true;
			$scope.selectedAction = selectedAction.value;
			if($scope.selectedAction == 0){
				$scope.showloader = false;
				toastr.warning(messagesConstants.selectAction);
			} else if ($scope.selection == "") {
				$scope.showloader = false;
				toastr.warning(messagesConstants.selectActionField);
			} else {	
				for(var i = 0; i< roleLength; i++){
					var id =  $scope.selection[i];
					if($scope.selectedAction == 1) {
						updatedData.push({id: id, is_status: true});
					}
					else if($scope.selectedAction == 2) {
					  updatedData.push({id: id, is_status: false});
					}
				}
				var inputJson = {data: updatedData}
				AnswerService.bulkUpdateAnswer(inputJson, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.AnswerUpdateSuccess);
						$state.reload();
					} else {
						toastr.error(messagesConstants.AnswerUpdateError);
					}
				});
			}
		}


	        	
    	$scope.activeTab = 0;
    	$scope.findOne = function () {
    		if ($stateParams.id) {
    			UserService.getUser ($stateParams.id, function(response) {
    				if(response.messageId == 200) {
    					$scope.user = response.data;
    				}
    			});
    		}
    		$scope.getAllRoles()
    	}


    	$scope.selectRole = function (id) {
			var index = $scope.user.role.indexOf(id);
			if(index == -1)	
				$scope.user.role.push(id)
			else
				$scope.user.role.splice(index, 1)

			var roleLen = $scope.roleData.length;
			for (var a = 0; a < roleLen; ++a) {
				if ($scope.roleData[a]._id == id) {
					if ($scope.roleData[a].used) {
						$scope.roleData[a].used = false;
					} else {
						$scope.roleData[a].used = true;
					}
					break;
				}
			}
			
		}


		$scope.updateData = function (type) {
			$scope.showloader = true;
			if ($scope.user._id) {
				var inputJsonString = $scope.user;
				UserService.updateUser(inputJsonString, $scope.user._id, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						if(type)
						$location.path( "/users" );
					else{
						++$scope.activeTab
					}
						
					}	
					else{
						$scope.message = err.message;
					} 
				});
			}
			else{
				var inputJsonString = $scope.user;
				UserService.saveUser(inputJsonString, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						$scope.message = '';
						$stateParams.id = response.data
						$scope.user = response.data;
						$scope.activeTab = 1;
					}	
					else{
						$scope.message = response.message;
					} 
				});
			}
		}
	
	}
]);