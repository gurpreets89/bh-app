"use strict"

angular.module("Answers")

.factory('AnswerService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.getAnswerList = function(callback) {
		communicationService.resultViaGet(webservices.answerList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.exportAnswerList = function(callback) {
		communicationService.resultViaGet(webservices.exportAnswerList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.getAllAnswerOptions = function(callback) {
		communicationService.resultViaGet(webservices.getAllAnswerOptions, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.saveAnswerOption = function(inputJsonString, callback) {
		// alert("in save");
		communicationService.resultViaPost(webservices.saveAnswerOption, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	service.findOneAnswer =  function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.findOneAnswer, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.bulkUpdateAnswer =  function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.bulkUpdateAnswer, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	return service;
}]);
