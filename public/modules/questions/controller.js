"use strict"

angular.module("Questions")
angular.module("Diseases")

bhapp.controller('questionController', ['$scope', '$rootScope', '$localStorage', 'AnswerService','QuestionService', 'DiseaseService', 'ngTableParams','$stateParams','$state', 'SweetAlert', '$timeout', '$location','toastr', function($scope, $rootScope, $localStorage, AnswerService, QuestionService, DiseaseService, ngTableParams, $stateParams, $state, SweetAlert, $timeout, $location, toastr) {



	
	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	}
	else {
		$rootScope.userLoggedIn = false;
	}

	if($rootScope.message != "") {

		$scope.message = $rootScope.message;
	}

	//empty the $scope.message so the field gets reset once the message is displayed.
	$scope.message = "";
	$scope.questionObj = {
		answers: {},
	};

	$scope.colorCount = 0;
	$scope.countCheck =  function(color){
		if(color.checked){
			$scope.colorCount++;//opposite
		}else{
			$scope.colorCount--;
		}
	};
		
	
	$scope.allQuestions = "";
	$scope.isFiltersVisible = false;
	//show the dependency question and answer fields
	$scope.showDependents = false;
	$scope.questionOptionData = [];
	$scope.answerRadio = {
	 		correct: ""
 	}

 	$scope.downloadCSV = function(){
        QuestionService.exportQuestionList(function(response){
            console.log(response)
            $scope.showloader = false;
            if(response.messageId == 200) {
                window.open('/questionlist.csv');
            }
        });
    }

 	/*
	* Add Question page in Web-Admin
	* Created: Jatinder Singh 
	* Used in BH APP project
    */
    $scope.getAllDiseases = function(){	
    	$scope.showloader = true;
    	$scope.allDiseases = [];
		$scope.questionObj.disease = '';
		$scope.questionObj.question_type = '2';
		$scope.questionObj.question_for = '1';
		// get all Disease List
    	DiseaseService.getDiseaseList(function(response) {
    		$scope.showloader = false;
			if(response.messageId == 200) {
				$scope.allDiseases = response.data;
			}
		});
		// Get all answers List
		AnswerService.getAnswerList(function(response) {
			$scope.showloader = false;
			if(response.messageId == 200) {
				$scope.allAnswers = response.data;
				//console.log($scope.allAnswers);
			}
		});

		// find one question by ID
    	if($stateParams.id){
    		$scope.showloader = true;
    		$scope.question_screen_title = messagesConstants.EditQuestion;
    		QuestionService.findOneQuestion($stateParams.id, function(response) {
    			$scope.showloader = false;
				if(response.messageId == 200) {
					if(response.data.disease){
						$scope.questionObj.disease = response.data.disease._id;
					}
					$scope.questionObj.question = response.data.question;
					if(response.data.question_type){
						$scope.questionObj.question_type = response.data.question_type.toString();
					}
					if(response.data.question_for){
						$scope.questionObj.question_for = response.data.question_for.toString();
					}
					$scope.questionObj.is_status = response.data.is_status;
					var ansArray = response.data.answers;
					for(var i = 0; i < $scope.allAnswers.length; i++){
						for(var j = 0; j < ansArray.length; j++){
							if($scope.allAnswers[i]._id == ansArray[j]._id){
								ansArray[j].selected = true;
								$scope.allAnswers[i] = ansArray[j];
							}
						}
					}
				}
			});
    	}else{
    		// Add new question
    		$scope.showloader = false;
    		$scope.question_screen_title = messagesConstants.AddQuestion;
    	}
     }

	/*
	* Populate Question in Parent Question field
	* Created: Jatinder Singh 
	* Used in BH APP project
    */
    $scope.populateQuestions = function(){
    	$scope.showloader = true;
    	var inputJsonString = {'disease':$scope.questionObj.disease};
    	var parentQuestion = {};
    	QuestionService.getDiseaseQuestionList(inputJsonString, function(response) {
    		$scope.showloader = false;
    		if(response.messageId == 200) { // If questions available for this disease.
				$scope.parentQuestion = response.data;
			}
		});
	}

    
    /*
	* Add Question page in Web-Admin
	* Created: Jatinder Singh 
	* Used in BH APP project
    */
   
	$scope.addQuestion = function() {
		$scope.showloader = true;
		var inputJsonString = {};
		if(typeof($scope.questionObj.disease) == 'undefined' || $scope.questionObj.disease==''){
			toastr.error(messagesConstants.SelectDiseases);
			$scope.showloader = false;
			return;
		}
		inputJsonString = $scope.questionObj;

		$scope.questionNameArray = [];
	    angular.forEach($scope.allAnswers, function(x){
	      	if (x.selected) {
	      		$scope.questionNameArray.push(x)
	      	};
	    });
		if($scope.questionNameArray.length == 0){
			$scope.showloader = false;
			toastr.warning(messagesConstants.SelectAnAnswer);
		}else{
			inputJsonString.answers = $scope.questionNameArray;
			if($stateParams.id){
				// Edit Question
				QuestionService.updateQuestion(inputJsonString, $stateParams.id, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.updateQuestion);
						$timeout(function() {
					        $location.path('/questions');
					    }, 2000);
					}else{
						toastr.error(messagesConstants.EditQuestionError);
					}
				});
			}else{
				// Add new Question
				QuestionService.saveQuestion(inputJsonString, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.saveQuestion);
						$timeout(function() {
					        $location.path('/questions');
					    }, 2000);
					}else{
						toastr.error(messagesConstants.AddQuestionError);
					}
				});
			}
		}	
	}


	/*
	* Get All Questions page in Web-Admin
	* Created: Jatinder Singh 
	* Used in BH APP project
	*/
	$scope.getAllQuestions = function(){
		$scope.showloader = true;
	    QuestionService.getAllQuestionsList(function(response) {
	    	$scope.showloader = false;
			if (response == 'unauthorize') {
				$location.path('/404');
			}
			if(response.messageId == 200) {
				if(response.data.length != 0){
					$scope.isFiltersVisible = true;
					$scope.noQuestion = false;
					$scope.QuestionExist = true;
					$scope.filter = {
							custom_question_type: '',
							custom_question_for: '',
							question: '',
							custom_disease_title : ''

					};

					var customResponseData = [];
					customResponseData = response.data;
					//console.log(JSON.stringify(customResponseData.length));
					for(var i = 0; i < customResponseData.length; i++){
						if(customResponseData[i].question_for == 1){
							customResponseData[i].custom_question_for = "Patient";
						}else if(customResponseData[i].question_for == 2){
							customResponseData[i].custom_question_for = "Caregiver";
						}

						if(customResponseData[i].question_type == 1){
							customResponseData[i].custom_question_type = "Screening";
						}else if(customResponseData[i].question_type == 2){
							customResponseData[i].custom_question_type = "Follow up";
						}else if(customResponseData[i].question_type == 3){
							customResponseData[i].custom_question_type = "Duration";
						}else if(customResponseData[i].question_type == 4){
							customResponseData[i].custom_question_type = "Impairment";
						}else if(customResponseData[i].question_type == 5){
							customResponseData[i].custom_question_type = "Age of onset";
						}


						customResponseData[i].custom_disease_title = customResponseData[i].disease.title;
					}


					$scope.tableParams = new ngTableParams(
						{page:1, count:10, sorting:{_id:"desc"}, filter: $scope.filter}, 
						{
							total:response.data.length,
						 	counts: [10, 25, 50, 100],
						  	data: customResponseData
						}
					);
					$scope.simpleList = response.data;
					$scope.checkboxes = {
						checked: false,
						items:{}
					};
				}else{
					$scope.noQuestion = true;
					$scope.QuestionExist = false;
				}	
			}
	    });
	}	



    /*
	* Del Question page in Web-Admin
	* Created: Jatinder Singh 
	* Used in BH APP project
    */
    $scope.delQuestion = function(id, qname){
		SweetAlert.swal({
			title: "Confirmation",
			text: "Are you sure you want to delete question ("+qname+")",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No!",
			closeOnConfirm: true}, 
			function(isConfirm){ 
		  		if(isConfirm) {
			  		var delete_object = {'_id':id};
			  		$scope.showloader = true;
		            QuestionService.deleteQuestion(delete_object, function(response) {
		            	$scope.showloader = false;
		            	$timeout(function() {
		            		$scope.getAllQuestions();
					        $location.path('/questions');
					    }, 100);
						toastr.success(messagesConstants.QuestionDeleteSuccess);
					});
	        	}
			}
		);
	}



	//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.toggleSelectionForQuestion = function toggleSelectionForQuestion(id) {
		//Check for single checkbox selection
			if(id){
				var idx = $scope.selection.indexOf(id);
				// is currently selected
				if (idx > -1) {
				    $scope.selection.splice(idx, 1);
				}
				// is newly selected
				else {
				    $scope.selection.push(id);
				}
		    } else { //Check for all checkbox selection   
		    	if($scope.selection.length > 0 && $scope.selectionAll){
		    		$scope.selection = [];
		    		$scope.checkboxes = {
		    			checked: false,
		    			items:{}
		    		};	
		    		$scope.selectionAll = false;
		    	} else {      	//Check for all un checked checkbox for check

		    		$scope.selectionAll = true
		    		$scope.selection = [];
		    		angular.forEach($scope.simpleList, function(item) {
		    			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
		    			$scope.selection.push(item._id);
		    		});
		    	}
			}
	       
		};


	//perform action
	$scope.performAction = function() {
		$scope.showloader = true;
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		if($scope.selectedAction == 0){
			toastr.warning(messagesConstants.selectAction);
			$scope.showloader = false;
		}else if($scope.selection == ""){
			toastr.warning(messagesConstants.selectActionField);
			$scope.showloader = false;
		}
		else{	
			for(var i = 0; i< roleLength; i++){
				var id =  $scope.selection[i];
				  if($scope.selectedAction == 3) {
				  updatedData.push({id: id, is_deleted: true});
				}
				else if($scope.selectedAction == 1) {
					updatedData.push({id: id, is_status: true});
				}
				else if($scope.selectedAction == 2) {
					updatedData.push({id: id, is_status: false});
				}
			}
			var inputJson = {data: updatedData}
			QuestionService.updateQuestionStatus(inputJson, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(messagesConstants.QuestionStatusSuccess);
					$state.reload();
				} else {
					toastr.error(messagesConstants.QuestionSuccessError);
				}
			});
		}
	}
}]);