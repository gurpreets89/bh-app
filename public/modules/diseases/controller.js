"use strict";

angular.module("Diseases")

bhapp.controller("diseaseController", ['$scope', '$rootScope', '$localStorage', 'UserService','DiseaseService','ngTableParams', '$stateParams','$state','$location','SweetAlert', 'toastr', '$http', function($scope, $rootScope, $localStorage, UserService, DiseaseService, ngTableParams, $stateParams, $state, $location, SweetAlert, toastr, $http){

	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	}
	else {
		$rootScope.userLoggedIn = false;
	}
	
	if($rootScope.message != "") {
	
		$scope.message = $rootScope.message;
	}

	//empty the $scope.message so the field gets reset once the message is displayed.
	$scope.message = "";
	$scope.activeTab = 0;
	$scope.user = {first_name: "", last_name: "", username: "", password: "", email: "", display_name: "", role: []}
	$scope.invalidFile = false;

	$scope.inputContainsFile = false;
    $scope.fileNameChanged = function(element){
        if(element.files.length > 0)
          $scope.inputContainsFile = true;
        else
          $scope.inputContainsFile = false;
    }
    console.log(window);
    $scope.downloadCSV = function(){
    	DiseaseService.exportDisease(function(response){
			$scope.showloader = false;
			if(response.messageId == 200) {
				window.open('/import_disorder.csv');
			}
		});
    }


	$scope.uploadFile = function(myfile, callback) {
		var file = $scope.myFile;
		var fileNameArr = [];
		var ext = file.name.split('.');
		var currE = ext[ext.length-1];
		if (currE == "csv") {
			$scope.loadingclass = 'loadingclass';
			$scope.progressbardiv = 'progressbardiv';
			$rootScope.noscrollclass = 'noscroll';
			$scope.current = 90;

			var uploadUrl = "/diseases/importFile";
			var fd = new FormData();
			fd.append('file', file);
			fd.append('user', $localStorage.userId);
			$http.post(uploadUrl, fd, {
				transformRequest: angular.identity,
				headers: { 'Content-Type': undefined },
			}).success(function(res) {
				//////// end loader class //////////
				$scope.loadingclass = ''; 
				$scope.progressbardiv = ''; 
				$rootScope.noscrollclass = '';  
				$scope.current = 0;
				//////// end loader class //////////
				$scope.showloader = false;
				if(res.messageId == 200) {
					toastr.success(res.message);
					$location.path('/diseases');
				}
			}).error(function(err) {
				$scope.error_message = "Error! There are some problem in file uploading. Check if csv file is valid.";
				toastr.success($scope.error_message);
			});
		} 
		else {
			alert("You can upload CSV files only!");
			$scope.error_message = "Error! Uploading file should be correct format only!";
			return false;
		}
	}

	//Toggle multilpe checkbox selection
	$scope.selection = [];
	$scope.selectionAll;
	$scope.toggleSelection = function toggleSelection(id) {
		//Check for single checkbox selection
		if(id){
			var idx = $scope.selection.indexOf(id);
			// is currently selected
			if (idx > -1) {
			    $scope.selection.splice(idx, 1);
			}
			// is newly selected
			else {
			    $scope.selection.push(id);
			}
	        }
	        //Check for all checkbox selection
	        else{
	        	//Check for all checked checkbox for uncheck
	        	if($scope.selection.length > 0 && $scope.selectionAll){
	        		$scope.selection = [];
	        		$scope.checkboxes = {
	        			checked: false,
	        			items:{}
	        		};	
	        		$scope.selectionAll = false;
	        	}
	        	//Check for all un checked checkbox for check
	        	else{
	        		$scope.selectionAll = true
	        		$scope.selection = [];
	        		angular.forEach($scope.simpleList, function(item) {
	        			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
	        			$scope.selection.push(item._id);
	        		});
	        	}
	        }
	       
	};
	
	/*
	* Add /Edit Disease
	* Created by : Gurpreet Singh
	* Used by: BH Application
	*/
	$scope.addDisease = function(){
		$scope.showloader = true;
		var inputString = {};
		
		inputString = $scope.disease;
		inputString.modified_date = Date.now();
		
		if ($stateParams.id) { // update if $stateParams.id exists
			DiseaseService.updateDisease(inputString, function(response){
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(response.message);
					$location.path('/diseases');
				}
			});
		}else{ // add new disease
			inputString.created_date = Date.now();
			DiseaseService.saveDisease(inputString, function(response){
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(response.message);
					$location.path('/diseases');
				}
			});
		}
    }

	/*
	* Get all Disease listing for Admin platform
	* Created by : Jatinder Singh
	* Used by: BH Application
	*/
	$scope.simpleList = [];
 	$scope.getAllDiseases = function(){
 		$scope.showloader = true;
 		$scope.allDiseases = [];
		DiseaseService.getDiseaseList(function(response) {
			$scope.showloader = false;
    		if(response.messageId == 200) {
    			if(response.data.length != 0){
					$scope.noDiseases = false;
					$scope.diseasesExist = true;
					$scope.filter = {title: ''};
					$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{is_status:"desc"}, filter:$scope.filter}, { total:response.data.length, counts: [10, 25, 50, 100], data: response.data});
				//multiple checkboxes
                    var data1 = "";
					//console.log("response.data.length = ", response.data.length);
                    for (var i = 0; i < response.data.length; i++) {
                        data1 = response.data[i];
                        if (data1.is_deleted == false) {
                            $scope.simpleList.push(data1);
                        }
                    }
					//console.log("response.data.length 2= ", $scope.simpleList.length);
					$scope.checkboxes = {
						checked: false,
						items:{}
					};
				}else{
						$scope.noDiseases = true;
						$scope.diseasesExist = false;
				}
			}
		});
	}


	    
	$scope.activeTab = 0;
	$scope.findOne = function () {
		if ($stateParams.id) {
			UserService.getUser ($stateParams.id, function(response) {
				if(response.messageId == 200) {
					$scope.user = response.data;
				}
			});
		}
		$scope.getAllRoles()
	}


	$scope.checkStatus = function (yesNo) {
			if (yesNo)
				return "pickedEven";
			else
				return "";
	}

	$scope.moveTabContents = function(tab){
		$scope.activeTab = tab;
	}

	$scope.selectRole = function (id) {
		var index = $scope.user.role.indexOf(id);
		if(index == -1)	
			$scope.user.role.push(id)
		else
			$scope.user.role.splice(index, 1)

		var roleLen = $scope.roleData.length;
		for (var a = 0; a < roleLen; ++a) {
			if ($scope.roleData[a]._id == id) {
				if ($scope.roleData[a].used) {
					$scope.roleData[a].used = false;
				} else {
					$scope.roleData[a].used = true;
				}
				break;
			}
		}
		
	}


	$scope.updateData = function (type) {
		$scope.showloader = true;
		if ($scope.user._id) {
			var inputJsonString = $scope.user;
			UserService.updateUser(inputJsonString, $scope.user._id, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					if(type)
					$location.path( "/users" );
				else{
					++$scope.activeTab
				}
					
				}	
				else{
					$scope.message = err.message;
				} 
			});
		}
		else{
			var inputJsonString = $scope.user;
			UserService.saveUser(inputJsonString, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					$scope.message = '';
					$stateParams.id = response.data
					$scope.user = response.data;
					$scope.activeTab = 1;
				}	
				else{
					$scope.message = response.message;
				} 
			});
		}
	}

	// Update diseases options status
	$scope.performAction = function() {	
		$scope.showloader = true;			
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		if($scope.selectedAction == 0){
			$scope.showloader = false;
			toastr.warning(messagesConstants.selectAction);
		} else if ($scope.selection == "") {
			$scope.showloader = false;
			toastr.warning(messagesConstants.selectActionField);
		} 
		else{	
			for(var i = 0; i< roleLength; i++){
				var id =  $scope.selection[i];
				if($scope.selectedAction == 1) {
					updatedData.push({id: id, is_status: true});
				}else if($scope.selectedAction == 2) {
				  updatedData.push({id: id, is_status: false});
				}else if($scope.selectedAction == 3) {
				  updatedData.push({id: id, is_deleted: true});
				}
			}
			var inputJson = {data: updatedData}
			DiseaseService.bulkUpdateDiseases(inputJson, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(messagesConstants.DiseasesUpdateSuccess);
					$state.reload();
				} else {
					toastr.error(messagesConstants.DiseasesUpdateError);
				}
			});
		}
	}
	/*
	* Get a Disease detail.
	* Created by : Gurpreet Singh
	*/
	$scope.disease = {};
	$scope.getDisease = function(){
		$scope.showloader = true;
		if($stateParams.id){
			$scope.disease_title = messagesConstants.EditDisorder;
			DiseaseService.findOneDisease($stateParams.id, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					$scope.disease = response.data;
				}
			});
		}else{
			$scope.showloader = false;
			// Add new question
			$scope.disease_title = messagesConstants.AddDisorder;
		}
	}
	
	/*
	* Del disease
	* Created: Gurpreet Singh 
	* Used in BH APP project
	*/
	$scope.delDisease = function(id, qname){
		SweetAlert.swal({
			title: "Confirmation",
			text: "Are you sure you want to delete disorder ("+qname+")",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No!",
			closeOnConfirm: true}, 
			function(isConfirm){ 
		  		if(isConfirm) {
		  			$scope.showloader = true;
			  		var delete_object = {'_id':id};
					DiseaseService.deleteDisease(delete_object, function(response) {
						$scope.showloader = false;
						$state.reload();
						toastr.success(response.message);
					});
				}
			}
		);
	}
}
]);

bhapp.controller("admindiseaseController", ['$scope', '$rootScope', '$localStorage', 'UserService','DiseaseService','ngTableParams', '$stateParams','$state','$location','SweetAlert', 'toastr', function($scope, $rootScope, $localStorage, UserService, DiseaseService, ngTableParams, $stateParams, $state, $location, SweetAlert, toastr){
	
	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	}
	else {
		$rootScope.userLoggedIn = false;
	}
	if($rootScope.message != "") {
	
		$scope.message = $rootScope.message;
	}

	//empty the $scope.message so the field gets reset once the message is displayed.
	$scope.message = "";
	$scope.activeTab = 0;
	
	/*
	* Get all Disease listing for Admin platform
	* Created by : Jatinder Singh
	* Used by: BH Application
	*/
	$scope.simpleList = [];
 	$scope.getAllDiseases = function(){
 		$scope.showloader = true;
 		$scope.allDiseases = [];
		DiseaseService.getAdminDiseaseList(function(response) {
			$scope.showloader = false;
    		if(response.messageId == 200) {
    			if(response.data.length != 0){
					$scope.noDiseases = false;
					$scope.diseasesExist = true;
					$scope.filter = {title: ''};
					$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{_id:"desc"}, filter:$scope.filter}, { total:response.data.length, counts: [10, 25, 50, 100], data: response.data});
				//multiple checkboxes
                    var data1 = "";
					//console.log("response.data.length = ", response.data.length);
                    for (var i = 0; i < response.data.length; i++) {
                        data1 = response.data[i];
						$scope.simpleList.push(data1);
                    }
					//console.log("response.data.length 2= ", $scope.simpleList.length);
					$scope.checkboxes = {
						checked: false,
						items:{}
					};
				}else{
						$scope.noDiseases = true;
						$scope.diseasesExist = false;
				}
			}
		});
	}
	
	
	//Toggle multilpe checkbox selection
	$scope.selection = [];
	$scope.selectionAll;
	$scope.toggleSelection = function toggleSelection(id) {
		//Check for single checkbox selection
		if(id){
			var idx = $scope.selection.indexOf(id);
			// is currently selected
			if (idx > -1) {
			    $scope.selection.splice(idx, 1);
			}
			// is newly selected
			else {
			    $scope.selection.push(id);
			}
		}
		//Check for all checkbox selection
		else{
			//Check for all checked checkbox for uncheck
			if($scope.selection.length > 0 && $scope.selectionAll){
				$scope.selection = [];
				$scope.checkboxes = {
					checked: false,
					items:{}
				};	
				$scope.selectionAll = false;
			}
			//Check for all un checked checkbox for check
			else{
				$scope.selectionAll = true
				$scope.selection = [];
				angular.forEach($scope.simpleList, function(item) {
					$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
					$scope.selection.push(item._id);
				});
			}
			
		}
	       
	};
		
	
	
	$scope.checkStatus = function (yesNo) {
			if (yesNo)
				return "pickedEven";
			else
				return "";
	}

	$scope.moveTabContents = function(tab){
		$scope.activeTab = tab;
	}
	
	// Update diseases options status
	$scope.performAction = function() {	
		$scope.showloader = true;			
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		if($scope.selectedAction == 0){
			$scope.showloader = false;
			toastr.warning(messagesConstants.selectAction);
		} else if ($scope.selection == "") {
			$scope.showloader = false;
			toastr.warning(messagesConstants.selectActionField);
		} 
		else{
			$scope.showloader = false;
			var inputJson = {'diseaseId': $scope.selection, 'operation':$scope.selectedAction};
			DiseaseService.updateAdminDiseases(inputJson, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(messagesConstants.DiseasesUpdateSuccess);
					$state.reload();
				} else {
					toastr.error(messagesConstants.DiseasesUpdateError);
				}
			});
		}
	}
	/*
	$scope.selectRole = function (id) {
		var index = $scope.user.role.indexOf(id);
		if(index == -1)	
			$scope.user.role.push(id)
		else
			$scope.user.role.splice(index, 1)

		var roleLen = $scope.roleData.length;
		for (var a = 0; a < roleLen; ++a) {
			if ($scope.roleData[a]._id == id) {
				if ($scope.roleData[a].used) {
					$scope.roleData[a].used = false;
				} else {
					$scope.roleData[a].used = true;
				}
				break;
			}
		}
		
	}
	*/
}
]);