"use strict"

angular.module("Diseases")

.factory('DiseaseService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.getDiseaseList = function(callback) {
		communicationService.resultViaGet(webservices.diseaseList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}
	service.getAdminDiseaseList = function(callback) {
		communicationService.resultViaGet(webservices.adminDiseaseList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}
	

	service.saveDisease = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.addDisease, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	service.exportDisease = function(callback) {
		communicationService.resultViaGet(webservices.exportDiseaseList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

        
	service.findOneDisease = function(Id, callback) {
		var serviceURL = webservices.findOneDisease + "/" + Id;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}
	

	service.updateDisease = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.updateDisease, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				callback(response.data);
		});
	}
	
	service.updateAdminDiseases = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.updateAdminDiseases, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				callback(response.data);
		});
	}
	
	service.deleteDisease = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.delDisease, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	service.getDiseaseDetail = function(diseaseName, callback) {
		var serviceURL = webservices.findDiseaseDetail + "/" + diseaseName;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.bulkUpdateDiseases =  function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.bulkUpdateDiseases, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	return service;


}]);
