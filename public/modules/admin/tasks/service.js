"use strict"

angular.module("DefaultTasks")

.factory('TaskService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


	service.getTaskList = function(callback) {
			communicationService.resultViaGet(webservices.tasksList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}
	
	service.getFilteredTask = function(searchJsonString, callback) {
			communicationService.resultViaPost(webservices.filterTasksList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveTask = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addTask, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.getTask = function(taskId, callback) {
		var serviceURL = webservices.findOneTask + "/" + taskId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateTask = function(inputJsonString, taskId, callback) {
		var serviceURL = webservices.updateTask + "/" + taskId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateTaskStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateTask, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	return service;


}]);
