	"use strict";

	angular.module("DefaultTasks").controller("defaultTaskController", ['$scope', '$rootScope', '$localStorage','DefaultDiagnosisService','DefaultAppointmentTypeService', 'TaskService', 'ngTableParams', '$stateParams', '$state','$location',  function($scope, $rootScope, $localStorage, DefaultDiagnosisService, DefaultAppointmentTypeService, TaskService, ngTableParams, $stateParams, $state, $location){
		
		
		if($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
		}
		else {
			$rootScope.userLoggedIn = false;
		}

		
		if($rootScope.message != "") {

			$scope.message = $rootScope.message;
		}

		//empty the $scope.message so the field gets reset once the message is displayed.
		$scope.message = "";
		$scope.activeTab = 0;
		$scope.task = {diagnosis: "", appointmentType: "", title: "", code: "", time: ""}

		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		
		$scope.listAllDiagnosis = function(){
			DefaultDiagnosisService.listAllDiagnosis(function(response) {
				if (response.messageId == 200) {
					$scope.diags = response.data;
				}
			});
		}
		
		$scope.getAllAppt = function(){
			DefaultAppointmentTypeService.listAllAppointmentTypes(function(response) {
				if (response.messageId == 200) {
					$scope.appt = response.data;
				}
			});
		}
		
		$scope.listAllDiagnosis();
		$scope.getAllAppt();
		
		
		$scope.toggleSelection = function toggleSelection(id) {
				//Check for single checkbox selection
				if(id){
					var idx = $scope.selection.indexOf(id);
			    // is currently selected
			    if (idx > -1) {
				$scope.selection.splice(idx, 1);
			    }
			    // is newly selected
			    else {
				$scope.selection.push(id);
			    }
			}
			//Check for all checkbox selection
			else{
				//Check for all checked checkbox for uncheck
				if($scope.selection.length > 0 && $scope.selectionAll){
					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items:{}
					};	
					$scope.selectionAll = false;
				}
				//Check for all un checked checkbox for check
				else{
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						$scope.selection.push(item._id);
					});
				}
			}
			console.log($scope.selection)
		    };


	        	//apply global Search
	        	$scope.applyGlobalSearch = function() {
	        		var term = $scope.globalSearchTerm;
	        		if(term != "") {
	        			if($scope.isInvertedSearch) {
	        				term = "!" + term;
	        			}
	        			$scope.tableParams.filter({$ : term});
	        			$scope.tableParams.reload();			
	        		}
	        	}


	        	$scope.getAllTasks = function(){
	        		TaskService.getTaskList (function(response) {
	        			if(response.messageId == 200) {
	        				$scope.filter = {title: '', code : '', time : ''};
	        				$scope.tableParams = new ngTableParams({page:1, count:20, sorting:{title:"asc"}, filter:$scope.filter}, { total:response.data.length, counts:[], data: response.data});
						$scope.simpleList = response.data;
						$scope.taskData = response.data;;
						$scope.checkboxes = {
							checked: false,
							items:{}
						};	
					}
				});
	        	}
			
			$scope.tasksSearch = function() {
	        		var searchJsonString = $scope.searchtask;
				TaskService.getFilteredTask (searchJsonString, function(response) {
	        				console.log(response);
	        				if(response.messageId == 200) {
	        				$scope.filter = {title: '', code : '', time : ''};
	        				$scope.tableParams = new ngTableParams({page:1, count:20, sorting:{title:"asc"}, filter:$scope.filter}, { total:response.data.length, counts:[], data: response.data});
						$scope.simpleList = response.data;
						$scope.taskData = response.data;;
						$scope.checkboxes = {
							checked: false,
							items:{}
						};	
					}
	        		});
	        	}


	        	$scope.activeTab = 0;
	        	$scope.findOne = function () {
				console.log('jhg');
				console.log($stateParams.id);
	        		if ($stateParams.id) {
	        			TaskService.getTask ($stateParams.id, function(response) {
	        				console.log(response);
	        				if(response.messageId == 200) {
	        					$scope.task = response.data;
	        				}
	        			});
	        		}
	        	}


	        	$scope.checkStatus = function (yesNo) {
					if (yesNo)
						return "pickedEven";
					else
						return "";
					}

				$scope.moveTabContents = function(tab){
				$scope.activeTab = tab;
				}

				$scope.selectRole = function (id) {
				var index = $scope.task.indexOf(id);
				if(index == -1)	
					$scope.task.push(id)
				else
					$scope.task.splice(index, 1)

				var taskLen = $scope.taskData.length;
				for (var a = 0; a < taskLen; ++a) {
					if ($scope.taskData[a]._id == id) {
						if ($scope.taskData[a].used) {
							$scope.taskData[a].used = false;
						} else {
							$scope.taskData[a].used = true;
						}
						break;
					}
				}
				
				console.log($scope.task);
				}


				$scope.updateData = function (type) {
				if ($scope.task._id) {
					var inputJsonString = $scope.task;
					TaskService.updateTask(inputJsonString, $scope.task._id, function(response) {
						if(response.messageId == 200) {
							$scope.message = response.message;
							$scope.alerttype = 'alert-success';
						}	
						else{
							$scope.message = response.message;
							$scope.alerttype = 'alert-danger';
						} 
					});
				}
				else{
					var inputJsonString = $scope.task;
					console.log(inputJsonString)
					TaskService.saveTask(inputJsonString, function(response) {
						if(response.messageId == 200) {
							$scope.message = '';
							$stateParams.id = response.data
							$scope.task = response.data;
							$scope.message = response.message;
							$scope.alerttype = 'alert-success';
						}	
						else{
							$scope.message = response.message;
							$scope.alerttype = 'alert-danger';
						} 
					});
				}
				}


						

		//perform action
		$scope.performAction = function() {						
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		console.log($scope.selectedAction);
		console.log($scope.selection);
		if($scope.selectedAction == 0)
		$scope.message = messagesConstants.selectAction;
		else{	
		for(var i = 0; i< roleLength; i++){
			var id =  $scope.selection[i];
			  if($scope.selectedAction == 3) {
			  updatedData.push({id: id, is_deleted: true});
			}
			else if($scope.selectedAction == 1) {
				updatedData.push({id: id, enable: true});
			}
			else if($scope.selectedAction == 2) {
				updatedData.push({id: id, enable: false});
			}
		}
		var inputJson = {data: updatedData}
			TaskService.updateTaskStatus(inputJson, function(response) {
				$rootScope.message = messagesConstants.updateStatus;
				$state.reload();
			});
		}
		}
		

	}

	]);