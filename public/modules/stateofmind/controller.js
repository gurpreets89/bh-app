"use strict";
//angular.module("stateofmind")

bhapp.controller('stateofmindController', ['$scope', '$rootScope', '$localStorage', '$timeout', '$location', 'UserService', 'PatientService', 'toastr', '$stateParams', function($scope, $rootScope, $localStorage, $timeout, $location, UserService, PatientService, toastr, $stateParams) {

	if ($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	} else {
		$rootScope.userLoggedIn = false;
	}

	$scope.findOne = function(cb) {
		if ($stateParams.id) {
			UserService.getUser($stateParams.id, function(response) {
				if (response.messageId == 200) {
					$scope.user = response.data;
					if (cb) {
						cb(response.data);
					}
				}
			});
		}
	}
	
	$scope.roundNum = function(str){
		return Math.round(str);
	}

	/* ++ angular-calendar settings ++ */
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();
		if (dd < 10) {
			dd = '0' + dd
		}
		if (mm < 10) {
			mm = '0' + mm
		}
		today = mm + '/' + dd + '/' + yyyy;
		$scope.minDate = today;
		$scope.todayDate = today;
		var maximumdate = new Date("Jan 01 2100 12:00:00");
	var dd1 = maximumdate.getDate();
	var mm1 = maximumdate.getMonth() + 1; //January is 0!
	var yyyy1 = maximumdate.getFullYear();
	if (dd1 < 10) {
		dd1 = '0' + dd1
	}
	if (mm1 < 10) {
		mm1 = '0' + mm1
	}
	maximumdate = mm1 + '/' + dd1 + '/' + yyyy1;
	$scope.maxDate = maximumdate;
	/* -- angular-calendar settings -- */

	/**
	 *	function to get Patient Data - checkIn, goals, charts, etc.
	 *	developer : gurpreet
	 **/
	// parameters set here with initial value
	$scope.probabilityDataAvailable = false;
	$scope.probabilityData = '';
	$scope.isWeeklyMetricsPatient = false;
	$scope.goalsAvailable = false; $scope.showGraphData = false;

	// function
	$scope.getPatientData = function() {
		$scope.showloader = true;
		//console.log($stateParams);
		if ($stateParams.id) {
			//console.log("IN getPatientData");
			UserService.getUser($stateParams.id, function(response) {
				//console.log(response);
				if (response.messageId == 200) {
					$scope.userDetails = response.data;
					$scope.findOne(function(res) {
						$scope.appointmentData = res;
						if (res.caregiver != null) {
							$scope.caregiverAssigned = "TRUE";
							$scope.caregiverId = res.caregiver;
						} else {
							$scope.caregiverAssigned = "FALSE";
						}

						if (typeof res.level_of_service != 'undefined') {
							$scope.appointmentData.level_of_service = res.level_of_service;
						}else{
							$scope.appointmentData.level_of_service = "inpatient";
						}
						if (typeof res.assessment_type != 'undefined') {
							$scope.appointmentData.assessment_type = res.assessment_type;
						}else{
							$scope.appointmentData.assessment_type = "baseline";
						}
						if (typeof res.next_appointment_time != 'undefined') {
							//console.log('res.next_appointment_time = ', typeof res.next_appointment_time, res.next_appointment_time);
							var nat = new Date();
							var tim = res.next_appointment_time;
							var ts = tim.split(":");
							nat.setHours(ts[0]);
							nat.setMinutes(ts[1]);
							$scope.appointmentData.next_appointment_time = nat;
						}else{
							$scope.appointmentData.next_appointment_time = new Date();
						}

						if (typeof(res.is_appointment_showed) == 'undefined') {
							$scope.appointmentData.is_appointment_showed = 'noanswer';
						} else if (res.is_appointment_showed == '') {
							$scope.appointmentData.is_appointment_showed = 'noanswer';
						}
						var inputJSONcheckIn = {};
						var inputJsonString = {};
						var d = new Date();
						d.setDate(d.getDate() - 9); // mongo query is $gte so subtracting 9 days to get last 10 days data.
						var newdate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						inputJsonString.patient = $stateParams.id;
						inputJSONcheckIn.patient = $stateParams.id;
						inputJSONcheckIn.created_date = {
							$gte: d.toISOString()
						};
						/**++ get all goals for patients ++**/
						$scope.dailyMetricData = {};
						$scope.weeklyMetricData = {};
						$scope.patient_goal_data = {};
						$scope.patientAllGoals = [];
						/**++ listCheckInData ++**/
						$scope.CheckInDailyMetricData = {};
						$scope.CheckInStaticMetricData2 = [];
						$scope.CheckInWeeklyMetricData2 = [];
						$scope.checkInData = {};
						PatientService.listCheckInData(inputJSONcheckIn, function(response) {
							if (response.messageId == 200) {
								if (response.data.length) {
									$scope.totalDays = response.data.length;
									$scope.showGraphData = true;
									// get all patient goals for listing of Next Session Goals
									PatientService.getPatientDailyGoals(inputJsonString, function(resp) {
										if (resp.messageId == 200) {
											if (resp.data.length > 0) {
												$scope.goalsAvailable = true;
												for (var i = 0; i < resp.data.length; i++) {
													$scope.patient_goal_data = resp.data;
													if (resp.data[i].goal_type_id == 1) {
														$scope.dailyMetricData[i] = {
															"goal_id": $scope.patient_goal_data[i]._id,
															"goal_question": $scope.patient_goal_data[i].goal_question,
															"title": $scope.patient_goal_data[i].title,
															"checked": false
														}
														$scope.patientAllGoals[$scope.patient_goal_data[i]._id] = 0;
													}
												}
											}

											var daily_metric_data = [];
											for (var i = 0; i < response.data.length; i++) {
												var checkInData = response.data[i];
												//console.log("checkInData = ", i, checkInData);
												if (checkInData.daily_metric) {
													var checkIn_daily_metric = checkInData.daily_metric;
													//console.log("checkIn_daily_metric = ",i, checkIn_daily_metric);
													for (var j = 0; j < checkIn_daily_metric.length; j++) {
														var checkIn_dm = checkIn_daily_metric[j];
														//console.log("checkIn_dm = ", checkIn_dm);
														if (typeof $scope.patientAllGoals[checkIn_dm.goal] == 'undefined') {
															$scope.patientAllGoals[checkIn_dm.goal] = 0;
														}
														if (checkIn_dm.checked == true) {
															//console.log(typeof $scope.patientAllGoals[checkIn_dm.goal], " = ", $scope.patientAllGoals[checkIn_dm.goal]);
															$scope.patientAllGoals[checkIn_dm.goal] = $scope.patientAllGoals[checkIn_dm.goal] + 1;
														}
													}
												}
											}
											$scope.CheckInDailyMetricData = $scope.patientAllGoals;
											//console.log("------------- $scope.CheckInDailyMetricData ---------- \n", $scope.CheckInDailyMetricData);
											$scope.CheckInDailyMetricDataKeys = Object.keys($scope.CheckInDailyMetricData);

											/* graph data here for state of mind */
											if (typeof(response.data[0].static_metric) != 'undefined') {
												for (var i = 0; i < response.data.length; i++) {
													$scope.checkInData = response.data;
													$scope.CheckInStaticMetricData = {};
													if ($scope.checkInData[i].static_metric) {
														var checkIn_static_metric = $scope.checkInData[i].static_metric;
														$scope.moodEnergyStress = [];
														for (var j = 0; j < checkIn_static_metric.length; j++) {
															$scope.checkIn_dm = checkIn_static_metric[j];
															$scope.CheckInStaticMetricData['date'] = $scope.checkInData[i].checkin_date;
															$scope.CheckInStaticMetricData[checkIn_static_metric[j].title] = checkIn_static_metric[j].rating;
														}
														$scope.CheckInStaticMetricData2[i] = $scope.CheckInStaticMetricData;
													}
												}

												/* Data Manipulation to set data for every date starts here*/
												var start_day = new Date(new Date().getTime() - 10 * 24 * 60 * 60 * 1000);
												var current_day = new Date(new Date().getTime());
												var current_date = (current_day.getMonth() + 1) + '/' + current_day.getDate() + '/' + current_day.getFullYear();
												var days_count = (current_day - start_day) / (1000 * 60 * 60 * 24);
												var dates_slot = {};

												var j = 0;
												for (var i = days_count; i >= 0; i--) {
													var start_day = new Date(new Date().getTime() - i * 24 * 60 * 60 * 1000);
													var start_date = (start_day.getMonth() + 1) + '/' + start_day.getDate() + '/' + start_day.getFullYear();
													dates_slot[j] = start_date;
													j++;
												}

												$scope.CheckInStaticMetricData3 = {};
												var tempObj = {};
												var tempArr = {};
												$.each(dates_slot, function(index1, value1) {
													$.each($scope.CheckInStaticMetricData2, function(index2, value2) {
														if (value1 == value2.date) {
															$scope.CheckInStaticMetricData3[index1] = $scope.CheckInStaticMetricData2[index2];
															tempArr[value1] = $scope.CheckInStaticMetricData2[index2];
														} else {
															tempObj = {};
															tempObj = {
																date: value1,
																'Good Mood?': 0,
																'Good Energy?': 0,
																'Low Stress?': 0,
																'Good Sleep?': 0,
																'Eating Healthy?': 0
															};
															if (tempArr[value1]) {} else {
																tempArr[value1] = tempObj;
															}
														}
													});
												});

												/* Data Manipulation starts to set data for every date starts here*/
												/*Set Map data start here*/
												var graphColors = {
													0: '#DB4437',
													1: '#1B9E77',
													2: '#FFD700'
												};
												var newarr1 = {};
												newarr1.cols = [{
													id: "date",
													label: "Date",
													type: "string"
												}, {
													id: "mood",
													label: "Good Mood",
													type: "number"
												}, {
													id: "energy",
													label: "Good Energy",
													type: "number"
												}, {
													id: "stress",
													label: "Low Stress",
													type: "number"
												}, {
													role: "style",
													type: "string"
												}];
												newarr1.rows = [];

												$.each(tempArr, function(index, value) {
													var obj = {};
													obj.c = [{
														v: value.date
													}, {
														v: value['Good Mood?']
													}, {
														v: value['Good Energy?']
													}, {
														v: value['Low Stress?']
													}, ]
													newarr1.rows.push(obj);
												});

												var newarr2 = {};
												newarr2.cols = [{
													id: "date",
													label: "Date",
													type: "string"
												}, {
													id: "mood",
													label: "Good Mood",
													type: "number"
												}, {
													id: "sleep",
													label: "Good Sleep",
													type: "number"
												}, {
													id: "eat",
													label: "Eating Healthy",
													type: "number"
												}, {
													role: "style",
													type: "string"
												}];
												newarr2.rows = [];
												$.each(tempArr, function(index, value) {
													var obj = {};
													obj.c = [{
														v: value.date
													}, {
														v: value['Good Mood?']
													}, {
														v: value['Good Sleep?']
													}, {
														v: value['Eating Healthy?']
													}, ]
													newarr2.rows.push(obj);
												});

												$scope.setGraphSettings('myChartObject1', 'LineChart', 'Daily Metrics', 50, newarr1);
												$scope.setGraphSettings('myChartObject2', 'LineChart', 'Daily Metrics', 50, newarr2);
												$scope.showloader = false;
												/*Set Map data ends here*/
											}
										} //resp.messageId == 200
									});
								} else { // If no data exists for patient
									$scope.showloader = false;
									$scope.totalDays = 0;
									if ($scope.showNoGoalsAvailable) {
										$scope.showNoCheckinsAvailable = false;
										//$scope.goalsAvailable = true;
									} else {
										$scope.showNoCheckinsAvailable = true;
										//$scope.goalsAvailable = false;
									}
									$scope.showGraphData = false;
								}
							}
						});
						/**-- listCheckInData --**/

						/** ++ Call getCaregiverCheckInData() for Chart 3 ++ **/
						$scope.getPatientCheckInData(inputJSONcheckIn);
						/** -- Call getCaregiverCheckInData() for Chart 3 -- **/

						/** ++ Call getCaregiverCheckInData() for Chart 4 ++ **/
						if ($scope.userDetails.caregiver != null && typeof $scope.userDetails.caregiver != 'undefined' && typeof $scope.userDetails.caregiver._id != 'undefined') {
							$scope.getCaregiverCheckInData(inputJSONcheckIn, $scope.userDetails.caregiver._id);
						}
						/** -- Call getCaregiverCheckInData() for Chart 4 -- **/

						PatientService.getPatientProbability(inputJsonString, function(response) {
							//console.log(response);
							if (response.messageId == 200) {
								$scope.showloader = false;
								if (response.data.length) {
									$scope.probabilityDataAvailable = true;
									$scope.probabilityData = response.data;
									//console.log("HERE ------------>> ",  JSON.stringify($scope.probabilityData));
								}
							}
						});
						/**-- get last 5 patient probability records for a patient --**/
					});
				}
			});
		}
	}


	/**
	 *	set graph settings for state of mind
	 *	developer : gurpreet
	 **/
	$scope.myChartObject1 = {};
	$scope.myChartObject2 = {};
	$scope.myChartObject3 = {};
	$scope.myChartObject4 = {};
	$scope.setGraphSettings = function(myChartObject, graphType, graphTitle, chartArea, data) {
		var dataLength = [];
		for (var i = 0; i < data.cols.length; i++) {
			dataLength.push(i);
		}
		var commonChartObj = {};

		commonChartObj.type = graphType;
		commonChartObj.data = data;
		commonChartObj.displayed = false;
		commonChartObj.view = {
			columns: dataLength
		};
		commonChartObj.options = {
			legend: false,
			title: graphTitle,
			vAxis: {
				title: 'Rating',
				"gridlines": {
					"count": 10
				},
				titleTextStyle: {
					color: 'black',
					fontSize: '15',
					fontName: '"Arial"'
				},
				viewWindowMode: 'explicit',
				viewWindow: {
					max: 5,
					min: 0
				}
			},
			hAxis: {
				title: 'Date',
				titleTextStyle: {
					color: 'black',
					fontSize: '15',
					fontName: '"Arial"'
				}
			},
			// chartArea:{width:chartArea+'%'},
			defaultColors: ['#0000FF', '#009900', '#CC0000', '#DD9900', '#0000FF', '#009900', '#CC0000', '#DD9900'],
			isStacked: "false",
			fill: 20,
			displayExactValues: true,
		};

		if (myChartObject == 'myChartObject1') {
			$scope.myChartObject1 = commonChartObj;
		}
		if (myChartObject == 'myChartObject2') {
			$scope.myChartObject2 = commonChartObj;
		}
		if (myChartObject == 'myChartObject3') {
			$scope.myChartObject3 = commonChartObj;
		}
		if (myChartObject == 'myChartObject4') {
			$scope.myChartObject4 = commonChartObj;
		}
	}


	/**-----------------------------------------------------
	 *	Function : getPatientCheckInData
	 *	Get patient's check in data if available.
	 *	developer : Gurpreet Singh
	 *------------------------------------------------------**/
	$scope.isWeeklyMetricsPatient = false;
	$scope.getPatientCheckInData = function(inputJSONcheckIn) {
		//console.log("inputJSONcheckIn = ", inputJSONcheckIn);
		var inputData = {};
		inputData.submitted_by = 'patient';
		inputData.patient = inputJSONcheckIn.patient;
		inputData.weekly_metric = {
			$ne: []
		}; // weekly metric should not be empty
		//console.log("inputData = ", inputData);
		PatientService.listCaregiverCheckInData(inputData, function(response) {
			//console.log('getPatientCheckInData =---------- ', response);
			if (response.messageId == 200) {
				if (response.data.length) {
					//Weekly Metrics start here
					if (response.data[0].weekly_metric.length != 0) {
						$scope.isWeeklyMetricsPatient = true;
						for (var i = 0; i < response.data.length; i++) {
							$scope.checkInData = response.data;
							//console.log("$scope.checkInData = \n", $scope.checkInData);
							$scope.CheckInWeeklyMetricData = {};
							if ($scope.checkInData[i].weekly_metric) {
								var checkIn_weekly_metric = $scope.checkInData[i].weekly_metric;
								for (var j = 0; j < checkIn_weekly_metric.length; j++) {
									$scope.checkIn_dm = checkIn_weekly_metric[j];
									$scope.CheckInWeeklyMetricData['date'] = $scope.checkInData[i].checkin_date;
									$scope.CheckInWeeklyMetricData[checkIn_weekly_metric[j].goal_question] = checkIn_weekly_metric[j].rating;
								}
								$scope.CheckInWeeklyMetricData2[i] = $scope.CheckInWeeklyMetricData;
							}
						}
						//console.log("$scope.CheckInWeeklyMetricData2 +++++++ ", $scope.CheckInWeeklyMetricData2);

						/*Set Map data start here*/
						var graphColors = {
							0: '#DB4437',
							1: '#1B9E77',
							2: '#FFD700'
						};
						var newarr3 = {};
						newarr3.rows = [];
						$scope.CheckInWeeklyMetricData2.forEach(function(index, value) {

							var obj = {};
							var colsObj;
							newarr3.cols = [];
							var tempArr = [];
							var objTempArr = [];

							tempArr = Object.keys(index);
							for (var key in tempArr) {

								colsObj = {};
								if (tempArr[key] != 'date') {
									colsObj = {
										id: tempArr[key],
										label: tempArr[key],
										type: "number"
									};
								} else {
									colsObj = {
										id: tempArr[key],
										label: tempArr[key],
										type: "string"
									};
								}
								newarr3.cols.push(colsObj);
								objTempArr[key] = {
									v: index[tempArr[key]]
								};
							};

							obj.c = objTempArr;

							newarr3.cols.push({
								role: "style",
								type: "string"
							});

							newarr3.rows.push(obj);
						});
						$scope.setGraphSettings('myChartObject3', 'LineChart', 'Weekly Metrics - Patient', 50, newarr3);
						/*Set Map data ends here*/
					}
				}
			} //(response.messageId == 200) 
		});
	}


	/**-----------------------------------------------------
	 *	Function : getCaregiverCheckInData
	 *	Get patient's caregiver check in data if available.
	 *	developer : Gurpreet Singh
	 *------------------------------------------------------**/
	$scope.isWeeklyMetricsCaregiver = false;
	var caregiverCheckInWeeklyMetricData2 = [];
	$scope.getCaregiverCheckInData = function(inputJSONcheckIn, caregiverId) {
		var inputData = {};
		inputData.submitted_by = 'caregiver';
		inputData.patient = caregiverId;
		inputData.weekly_metric = {
			$ne: []
		}; // weekly metric should not be empty
	//console.log("inputData ---------- ", inputData);
		PatientService.listCaregiverCheckInData(inputData, function(response) {
			//console.log('getCaregiverCheckInData =---------- ', response);
			if (response.messageId == 200) {
				if (response.data.length) {
					//Weekly Metrics start here
					if (response.data[0].weekly_metric.length != 0) {
						$scope.isWeeklyMetricsCaregiver = true;
						for (var i = 0; i < response.data.length; i++) {
							var checkInData = response.data[i];
					//console.log(".........checkInData = \n", checkInData);
							var CheckInWeeklyMetricData = {};
							if (checkInData.weekly_metric) {
								var checkIn_weekly_metric = checkInData.weekly_metric;
								for (var j = 0; j < checkIn_weekly_metric.length; j++) {
									var cwm = checkIn_weekly_metric[j];
							//console.log("cwm = \n", cwm);
									CheckInWeeklyMetricData['date'] = checkInData.checkin_date;
									CheckInWeeklyMetricData[cwm.goal_question] = cwm.rating;
								}
								caregiverCheckInWeeklyMetricData2[i] = CheckInWeeklyMetricData;
							}
						}
			//console.log("caregiverCheckInWeeklyMetricData2 = \n", caregiverCheckInWeeklyMetricData2);
						/*Set Map data start here*/
						var graphColors = {
							0: '#DB4437',
							1: '#1B9E77',
							2: '#FFD700'
						};
						var newarr4 = {};
						newarr4.rows = [];
						caregiverCheckInWeeklyMetricData2.forEach(function(index, value) {

							var obj = {};
							var colsObj;
							newarr4.cols = [];
							var tempArr = [];
							var objTempArr = [];

							tempArr = Object.keys(index);
							for (var key in tempArr) {

								colsObj = {};
								if (tempArr[key] != 'date') {
									colsObj = {
										id: tempArr[key],
										label: tempArr[key],
										type: "number"
									};
								} else {
									colsObj = {
										id: tempArr[key],
										label: tempArr[key],
										type: "string"
									};
								}
								newarr4.cols.push(colsObj);
								objTempArr[key] = {
									v: index[tempArr[key]]
								};
							};

							obj.c = objTempArr;

							newarr4.cols.push({
								role: "style",
								type: "string"
							});

							newarr4.rows.push(obj);
						});
						$scope.setGraphSettings('myChartObject4', 'LineChart', 'Weekly Metrics - Caregiver', 50, newarr4);
						/*Set Map data ends here*/
					}
				}
			} //(response.messageId == 200) 
		});
	}
	
	/**
	*	function to save Patient appointment details
	*	developer : gurpreet
	**/
	$scope.saveOrUpdateAppointment = function() {
		$scope.appointmentData.next_assessment_available = true;
		var inputJsonString = {};
		//$scope.appointmentData.next_appointment_time = $scope.appointmentData.next_appointment_time.replace(/[\s]/g, '');
		inputJsonString = $scope.appointmentData;

		var d = new Date($scope.appointmentData.next_appointment_date);
		var d1 = new Date($scope.appointmentData.next_assessment_date);
		var currentdate = new Date();
		currentdate.setHours(0, 0, 0, 0);
		if(currentdate > d && currentdate > d1){
			toastr.warning("Please select valid appointment date & time and assessment date.");
		}else if(currentdate > d){
			toastr.warning("Please select valid appointment date & time.");
		}else if(currentdate > d1){
			toastr.warning("Please select valid assessment date.");
		}else{
			if(inputJsonString.next_appointment_time){
				var myDate = new Date(inputJsonString.next_appointment_time);
				var minutes = myDate.getMinutes();
				var hours = myDate.getHours();
				inputJsonString.next_appointment_time = hours+":"+ (minutes < 10 ? '0'+minutes : minutes);
			}
			var inputJSON = {};
			inputJSON.parent = inputJsonString.parent;
			inputJSON.next_appointment_date = inputJsonString.next_appointment_date;
			inputJSON.next_appointment_time = inputJsonString.next_appointment_time;

			console.log("inputJSON", inputJSON);
			PatientService.checkAppointmentForClinician(inputJSON, function(response) {
				//console.log(response);
				if(response.data.length > 0){
					toastr.error("Sorry! This time is not available for appointment. Please select different time.");
				}else{
					// save patient next apoointment and assessment
					inputJsonString.is_assessment_completed = false;
					PatientService.updateUser(inputJsonString, $stateParams.id, function(response) {
						if (response.messageId == 200) {
							toastr.success(messagesConstants.nextApmntSaved);
						}
					});
				}
			})
		}
	}
	
	/**--------------------------------
	*	Check time is valid or not
	*	developer : Gurpreet Singh
	*--------------------------------**/
	$scope.timeError = false;
	$scope.checkTime = function() {
		var selectedDate = $scope.appointmentData.next_appointment_date;
		var selectedTime = $scope.appointmentData.next_appointment_time;

		var myDate = new Date(selectedTime);
		var arrDateVal = selectedDate.split("/");
		myDate.setDate(arrDateVal[1]);
		myDate.setMonth(arrDateVal[0]-1);
		myDate.setUTCFullYear(arrDateVal[2]);
	
		var d = new Date();
		if (myDate.getTime() <= d.getTime()) {
			$scope.timeError = true;
		} else {
			$scope.timeError = false;
		}

		/*return;
		if (typeof selectedDate != 'undefined' && typeof selectedTime != 'undefined' && selectedDate == currentdate) {
			//if (selectedTime <= currentTime) {
			
			if (myDate.getTime() <= d.getTime()) {
				$scope.timeError = true;
			} else {
				$scope.timeError = false;
			}
		} else {
			$scope.timeError = false;
		}*/
	}

}]);