"use strict"

angular.module("Questionnaire")

.factory('questionnaireService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

		service.getQuestionnaireList = function(callback) {
				//communicationService.resultViaGet(webservices.basicQuestionnaireList, appConstants.authorizationKey, headerConstants.json, function(response) {
				/* commented for testing BASIC questionnaire. Need to uncomment later. */
				communicationService.resultViaGet(webservices.questionnaireList, appConstants.authorizationKey, headerConstants.json, function(response) {
				callback(response.data);
			});

		}
		
		service.checkQuestionnaires = function(callback) {
				communicationService.resultViaGet(webservices.checkQuestionnaires, appConstants.authorizationKey, headerConstants.json, function(response) {
				callback(response.data);
			});

		}

		service.saveQuestionnaire = function(inputJsonString, callback) {
		        communicationService.resultViaPost(webservices.addquestionnaire, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				callback(response.data);
			});
		}

		service.findOneQuestionnaire = function(questionnarieId , callback) {
			var webservice = webservices.findOneQuestionnaire + "/" + questionnarieId;
			communicationService.resultViaGet(webservice, appConstants.authorizationKey, headerConstants.json, function(response) {
				callback(response.data);
			});
		}

		service.updateQuestionnaire = function(inputJsonString, questionnarieId, callback) {
			var webservice = webservices.updateQuestionnaire + "/" + questionnarieId;
			console.log(webservice);
			communicationService.resultViaPost(webservice, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				callback(response.data);
			});
		}

		
		service.updateQuestionnaireStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateQuestionnaire, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				callback(response.data);
			});
		}

		

		service.deleteQuestionnaire = function(inputJsonString, callback) {
		        communicationService.resultViaPost(webservices.deleteQuestionnaire, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				callback(response.data);
			});
		}

		service.statusUpdateQuestions = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.updatestatusquestion, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				callback(response.data);
			});
		}


	return service;

}]);