'use strict'

angular.module("Questionnaire")
bhapp.controller('questionnaireController', ['$scope', '$rootScope', '$localStorage', 'ngTableParams', 'questionnaireService', '$location', 'Flash', '$stateParams', 'QuestionService', 'SweetAlert', '$state', function($scope, $rootScope, $localStorage, ngTableParams, questionnaireService, $location, Flash, $stateParams, QuestionService, SweetAlert, $state) {
	
	$scope.ass_quqes = {};
	$scope.ques_quesnaire = '';
	$scope.selected_questions = [];
	$scope.selected_ques={};
	
	/*** Toggle multilpe checkbox selection **/
	$scope.selection = [];
	$scope.selectionAll;
	$scope.toggleSelection = function toggleSelection(id) {
		//Check for single checkbox selection
		if(id){
			var idx = $scope.selection.indexOf(id);
			// is currently selected
			if (idx > -1) {
			    $scope.selection.splice(idx, 1);
			}
			// is newly selected
			else {
			    $scope.selection.push(id);
			}
		} else { //Check for all checkbox selection
		        //Check for all checked checkbox for uncheck
			if($scope.selection.length > 0 && $scope.selectionAll){
				$scope.selection = [];
				$scope.checkboxes = {
					checked: false,
					items:{}
				};	
				$scope.selectionAll = false;
			} else {      	//Check for all un checked checkbox for check
	
				$scope.selectionAll = true
				$scope.selection = [];
				angular.forEach($scope.simpleList, function(item) {
					$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
					$scope.selection.push(item._id);
				});
			}
		}
		
	};
	
	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	}
	else {
		$rootScope.userLoggedIn = false;
	}

	if($rootScope.message != "") {
		$scope.message = $rootScope.message;
	}
	$scope.questionnaire = {};
	$scope.questionnaire.questionnaire_type = '0';

	//empty the $scope.message so the field gets reset once the message is displayed.
	//$scope.message = "";
	//$scope.activeTab = 0;
	//$scope.sorted_categories = [];
	//$scope.categories = [];
	//$scope.questionnaire = {questionnaire_name: "", enable: false}
	//$scope.isCategorySelected = 0;
	
	$scope.applyGlobalSearch = function() {
		var term = $scope.globalSearchTerm;
		if(term != "") {
			if($scope.isInvertedSearch) {
				term = "!" + term;
			}
			$scope.tableParams.filter({$ : term});
			$scope.tableParams.reload();			
		}
	}

	//$scope.selection = [];
	//$scope.selectionAll;
	//$scope.selectionCategory = [];
	//$scope.selectionAllCategory;
	//$scope.selectedCategoryOnly = [];
	
	

	//getQuestionnaireList
	$scope.getAllQuestionnarire = function(){
		questionnaireService.getQuestionnaireList (function(response) {
			if(response.messageId == 200) {
				$scope.filter = {questionnaire_name: ''};
				$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{questionnaire_name:"asc"}, filter:$scope.filter}, { total:response.data.length, counts:[], data: response.data});
				$scope.simpleList = response.data;
				$scope.checkboxes = {
					checked: false,
					items:{}
				};		
			}
		});
	}

	//check if basic questionnaire already exists.
	$scope.checkQuestionnaires = function(){
		$scope.is_basic = true;
		
		questionnaireService.checkQuestionnaires (function(response) {
			if(response.messageId == 200) {
				if(response.data > 0) {
					$scope.is_basic = false;
					$scope.questionnaire.questionnaire_type = '1';
				}
				//$scope.filter = {questionnaire_name: ''};
				//$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{questionnaire_name:"asc"}, filter:$scope.filter}, { total:response.data.length, counts:[], data: response.data});
				//$scope.simpleList = response.data;
				//$scope.checkboxes = {
				//	checked: false,
				//	items:{}
				//};
			}
			if ($stateParams.id) {
			    questionnaireService.findOneQuestionnaire($stateParams.id, function(response) {
				$scope.questionnaire = response.data;
				//$scope.is_basic = true;
				$scope.questionnaire.questionnaire_type = response.data.questionnaire_type.toString();
			    });
			}
		});
	}

	//adding questionnaire : gurpreet
	$scope.addQuestionnaire = function() {
		if ($stateParams.id){
		    $scope.questionnaireID = $stateParams.id;
		}
		var inputJsonString = "";
		if(typeof $scope.questionnaire.enable == 'undefined') {
			$scope.questionnaire.enable = false;
		}
		// for adding questionnaire
		if($scope.questionnaireID == undefined) {
			inputJsonString = $scope.questionnaire;
			questionnaireService.saveQuestionnaire(inputJsonString, function(response) {
				if(response.messageId == 200) {
				    //$scope.message = messagesConstants.saveQuestionnaire;
				    Flash.create('success', 'Questionnaire has been added successfully.', 'alert alert-success');
				    $location.path('/questionnaires');
				}else{
					Flash.create('danger', "Questionnaire can't be added.", 'alert alert-danger');
				}
			});
		
		}// for edit questionnaire
		else {
			inputJsonString = $scope.questionnaire;
			questionnaireService.updateQuestionnaire(inputJsonString, $scope.questionnaireID, function(response) {
				if(response.messageId == 200) {
					Flash.create('success', messagesConstants.updateQuestionnaire, 'alert alert-success');
					$location.path('/questionnaires');
				}else{
					Flash.create('danger', err.message, 'alert alert-danger');
				}
			});
		}
	}
	
	
	
	$scope.assign_questions = function(){
		$scope.ass_ques_criteria = {};
		if(!$scope.ques_quesnaire){
			
		    questionnaireService.findOneQuestionnaire($stateParams.id, function(response) {
			$scope.ques_quesnaire = response.data;
			if($scope.ques_quesnaire.questions){
			    for(var k=0; k< $scope.ques_quesnaire.questions.length; k++){
				$scope.selected_questions[k] = $scope.ques_quesnaire.questions[k];
			    }
			}
			if($scope.ques_quesnaire.questions.length != 0){
			    $scope.ass_quqes.search_cre = {'is_deleted':0,_id: {$nin: $scope.ques_quesnaire.questions}};
			    $scope.selected_ques.search_cre = {_id: {$in: $scope.ques_quesnaire.question}};
			    $scope.selected_ques.sort_order = '-';
			} else {
			    $scope.ass_quqes.search_cre = {'is_deleted':0};
			}
			QuestionService.getQuestionsList($scope.ass_quqes, function(quesArr) {
			    $scope.questionnair_ques = quesArr.data;
			    /*for(var i=0; i< $scope.questionnair_ques.length; i++){
				$scope.questionnair_ques[i].order = i;
				$scope.queOrder[i] = i;
			    }*/
			});
			//if($scope.selected_ques.search_cre){
			//    QuestionService.getQuestionsList($scope.selected_ques.search_cre, function(quesArr) {
			//    //QuestionService.getList().query($scope.selected_ques, function(quesArr) {
			//	$scope.selected_questions = quesArr;
			//    });
			//}
		    });
		} else {
	
			var questionnairedata = {};
			questionnairedata._id = $stateParams.id;
			var cnt = 0;
			var selected_ques = [];
			for(cnt; cnt < $scope.selected_questions.length; cnt++){
			    selected_ques[cnt] = $scope.selected_questions[cnt]._id;
			}
			questionnairedata.selected_questions = selected_ques;
			
			questionnaireService.updateQuestionnaire(questionnairedata, questionnairedata._id, function(data){
			//questionnaireService.update().save(questionnairedata, function(data){
				//console.log('data---', data);
				if (data.messageId == 200) {
				   Flash.create('success', 'Questions have been assigned to questionnaire successfully.', 'alert alert-success');
				   $location.path('/questionnaires');
				} else {
				   //console.log('errors ==== ', data);
				}
			});
		}
	}
	
	//$scope.selectQuestion = function(qindex){
	$scope.selectQuestion = function(ques_id){
	    var qq = 0;
	    for(qq; qq < $scope.questionnair_ques.length; qq++){
		if($scope.questionnair_ques[qq]._id == ques_id){
		    $scope.selected_questions.push($scope.questionnair_ques[qq]);
		    $scope.questionnair_ques.splice(qq,1);
		}
	    }
	};
	
	$scope.unselectQuestion = function(qindex){
	    $scope.questionnair_ques.push($scope.selected_questions[qindex]);
	    /*$scope.Sorted = orderByFilter($scope.questionnair_ques, function(item) {
		return $scope.queOrder.indexOf(item.order);
	    });*/
	   // $scope.questionnair_ques = $scope.Sorted;
	    $scope.selected_questions.splice(qindex,1); 
	};
	
	$scope.onDrop = function($event,$data,index){
	    var current_position;
	    for(var sq = 0; sq < $scope.selected_questions.length; sq++){
		if($scope.selected_questions[sq]._id == $data._id){
		    current_position = sq;
		}
	    }
	    if(current_position < index){
		for (var i = current_position; i <= index; i++){
		    if(i == index){
			$scope.selected_questions[i] = $data;
		    } else {
			$scope.selected_questions[i] = $scope.selected_questions[i+1]; 
		    }
		}
	    }
	    if(current_position > index){
		for (var i = current_position; i >= index ; i--){
		    if(i == index){
			$scope.selected_questions[i] = $data;
		    } else {
			$scope.selected_questions[i] = $scope.selected_questions[i-1]; 
		    }
		}
	    }
	};
	
	/*
	* Del Questionnaire page in Web-Admin
	* Created: Gurpreet Singh
	* Used in BH APP project
	*/
	$scope.delQuestionnaire = function(id, qname){
		SweetAlert.swal({
			title: "Confirmation",
			text: "Are you sure you want to delete questionnaire ("+qname+")",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No!",
			closeOnConfirm: true
		}, 
		function(isConfirm){
			if(isConfirm) {
				var delete_object = {'_id':id};
				questionnaireService.deleteQuestionnaire(delete_object, function(response) {
					//$scope.getAllQuestions();
					//$location.path('/questionnaires');
					Flash.create('success', 'Questionnaire has been deleted successfully.', 'alert alert-success');	
					$state.reload();
					
				});
			}
		}
		);
	}
	
	/* perform checkbox actions */
	$scope.performAction = function() {					
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		if($scope.selectedAction == 0)
			$scope.message = messagesConstants.selectAction;
		else{	
			for(var i = 0; i< roleLength; i++){
				var id =  $scope.selection[i];
				if($scope.selectedAction == 3) {
					updatedData.push({id: id, is_deleted: true});
				}
				else if($scope.selectedAction == 1) {
					updatedData.push({id: id, enable: true});
				}
				else if($scope.selectedAction == 2) {
					updatedData.push({id: id, enable: false});
				}
			}
			var inputJson = {data: updatedData}
			questionnaireService.updateQuestionnaireStatus(inputJson, function(response) {
				if (response.messageId == 200) {
					Flash.create('success', messagesConstants.updateStatus, 'alert alert-success');
					$state.reload();
				}
				//$rootScope.message = messagesConstants.updateStatus;
				//Flash.create('success', 'Clinician has been approved successfully.', 'alert alert-success');
				//$state.reload(); 
				// $location.path('/clinicians');
			});
		}
	}
}]);