"use strict"

angular.module("ExtraQuestions")

bhapp.controller('extraQuestionController', ['$scope', '$rootScope', '$localStorage', 'AnswerService','ExtraQuestionService', 'ngTableParams','$stateParams','$state', 'SweetAlert', '$timeout', '$location','toastr','$http', function($scope, $rootScope, $localStorage, AnswerService, ExtraQuestionService, ngTableParams, $stateParams, $state, SweetAlert, $timeout, $location, toastr, $http) {

	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
	}
	else {
		$rootScope.userLoggedIn = false;
	}
	if($rootScope.message != "") {
		$scope.message = $rootScope.message;
	}
	//empty the $scope.message so the field gets reset once the message is displayed.
	$scope.message = "";
	$scope.questionObj = {
		answers: {},
	};

	$scope.colorCount = 0;
	$scope.countCheck =  function(color){
		if(color.checked){
			$scope.colorCount++;//opposite
		}else{
			$scope.colorCount--;
		}
	};
		
	
	$scope.allQuestions = "";
	$scope.isFiltersVisible = false;
	//show the dependency question and answer fields
	$scope.showDependents = false;
	$scope.questionOptionData = [];
	$scope.answerRadio = {
	 		correct: ""
 	}

 	$scope.invalidFile = false;
	$scope.inputContainsFile = false;
    $scope.fileNameChanged = function(element){
        if(element.files.length > 0)
          $scope.inputContainsFile = true;
        else
          $scope.inputContainsFile = false;
    }


	$scope.uploadFile = function(myfile, callback) {
		var file = $scope.myFile;
		var fileNameArr = [];
		var ext = file.name.split('.');
		var currE = ext[ext.length-1];
		if (currE == "csv") {
			$scope.loadingclass = 'loadingclass';
			$scope.progressbardiv = 'progressbardiv';
			$rootScope.noscrollclass = 'noscroll';
			$scope.current = 90;

			var uploadUrl = "/extraquestions/importFile";
			var fd = new FormData();
			fd.append('file', file);
			fd.append('user', $localStorage.userId);
			$http.post(uploadUrl, fd, {
				transformRequest: angular.identity,
				headers: { 'Content-Type': undefined },
			}).success(function(res) {
				//////// end loader class //////////
				$scope.loadingclass = ''; 
				$scope.progressbardiv = ''; 
				$rootScope.noscrollclass = '';  
				$scope.current = 0;
				//////// end loader class //////////
				$scope.showloader = false;
				if(res.messageId == 200) {
					toastr.success(res.message);
					$location.path('/extraquestions');
				}
			}).error(function(err) {
				$scope.error_message = "Error! There are some problem in file uploading. Check if csv file is valid.";
				toastr.success($scope.error_message);
			});
		} 
		else {
			alert("You can upload CSV files only!");
			$scope.error_message = "Error! Uploading file should be correct format only!";
			return false;
		}
	}

 	/*
	* Add Question page in Web-Admin
	* Created: Jatinder Singh 
	* Used in BH APP project
    */
    $scope.getAllDiseases = function(){	
    	$scope.showloader = true;
    	
		// Get all answers List
		AnswerService.getAnswerList(function(response) {
			$scope.showloader = false;
			if(response.messageId == 200) {
				$scope.allAnswers = response.data;
			}

			// find one question by ID
			if($stateParams.id){
				$scope.showloader = true;
				$scope.question_screen_title = messagesConstants.EditQuestion;
				ExtraQuestionService.findOneQuestion($stateParams.id, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						$scope.questionObj.question = response.data.question;
						$scope.questionObj.is_status = response.data.is_status;
						var ansArray = response.data.answers;
						for(var i = 0; i < $scope.allAnswers.length; i++){
							for(var j = 0; j < ansArray.length; j++){
								if($scope.allAnswers[i]._id == ansArray[j].answer_id){
									ansArray[j].selected = true;
									$scope.allAnswers[i] = ansArray[j];
								}
							}
						}
						console.log($scope.allAnswers);
					}
				});
			}else{
				// Add new question
				$scope.showloader = false;
				$scope.question_screen_title = messagesConstants.AddQuestion;
			}
		});

	}

	
	/*
	* Add Question page in Web-Admin
	* Created: Jatinder Singh 
	* Used in BH APP project
    */
	$scope.addQuestion = function() {
		$scope.showloader = true;
		var inputJsonString = {};
		inputJsonString = $scope.questionObj;

		$scope.questionNameArray = [];
	    angular.forEach($scope.allAnswers, function(x){
	      	if (x.selected) {
	      		$scope.questionNameArray.push(x)
	      	};
	    });
		if($scope.questionNameArray.length == 0){
			$scope.showloader = false;
			toastr.warning(messagesConstants.SelectAnAnswer);
		}else{
			inputJsonString.answers = new Array();
			for(var p=0;p<$scope.questionNameArray.length;p++){
				var obj = {};
				obj.answer_id = $scope.questionNameArray[p]['_id'];
				obj.answer_text = $scope.questionNameArray[p]['answer_text'];
				obj.answer_value = $scope.questionNameArray[p]['answer_value'];
				inputJsonString.answers.push(obj);
			}
			//inputJsonString.answers = $scope.questionNameArray;
			if($stateParams.id){
				// Edit Question
				ExtraQuestionService.updateQuestion(inputJsonString, $stateParams.id, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.updateQuestion);
						$timeout(function() {
					        $location.path('/extraquestions');
					    }, 2000);
					}else{
						toastr.error(messagesConstants.EditQuestionError);
					}
				});
			}else{
				// Add new Question
				ExtraQuestionService.saveQuestion(inputJsonString, function(response) {
					$scope.showloader = false;
					if(response.messageId == 200) {
						toastr.success(messagesConstants.saveQuestion);
						$timeout(function() {
					        $location.path('/extraquestions');
					    }, 2000);
					}else{
						toastr.error(messagesConstants.AddQuestionError);
					}
				});
			}
		}	
	}


	/*
	* Get All Questions page in Web-Admin
	* Created: Jatinder Singh 
	* Used in BH APP project
	*/
	$scope.getAllQuestions = function(){
		$scope.showloader = true;
	    ExtraQuestionService.getAllQuestionsList(function(response) {
	    	$scope.showloader = false;
			if (response == 'unauthorize') {
				$location.path('/404');
			}
			if(response.messageId == 200) {
				if(response.data.length != 0){
					$scope.isFiltersVisible = true;
					$scope.noQuestion = false;
					$scope.QuestionExist = true;
					$scope.filter = {
							custom_question_type: '',
							custom_question_for: '',
							question: '',
							custom_disease_title : ''

					};

					var customResponseData = [];
					customResponseData = response.data;
					$scope.tableParams = new ngTableParams(
						{page:1, count:10, sorting:{_id:"desc"}, filter: $scope.filter}, 
						{
							total:response.data.length,
						 	counts: [10, 25, 50, 100],
						  	data: customResponseData
						}
					);
					$scope.simpleList = response.data;
					$scope.checkboxes = {
						checked: false,
						items:{}
					};
				}else{
					$scope.noQuestion = true;
					$scope.QuestionExist = false;
				}	
			}
	    });
	}	



    /*
	* Del Question page in Web-Admin
	* Created: Jatinder Singh 
	* Used in BH APP project
    */
    $scope.delQuestion = function(id, qname){
		SweetAlert.swal({
			title: "Confirmation",
			text: "Are you sure you want to delete question ("+qname+")",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No!",
			closeOnConfirm: true}, 
			function(isConfirm){ 
		  		if(isConfirm) {
			  		var delete_object = {'_id':id};
			  		$scope.showloader = true;
		            ExtraQuestionService.deleteQuestion(delete_object, function(response) {
		            	$scope.showloader = false;
		            	$timeout(function() {
		            		$scope.getAllQuestions();
					        $location.path('/extraquestions');
					    }, 100);
						toastr.success(messagesConstants.QuestionDeleteSuccess);
					});
	        	}
			}
		);
	}



	//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.toggleSelectionForQuestion = function toggleSelectionForQuestion(id) {
		//Check for single checkbox selection
			if(id){
				var idx = $scope.selection.indexOf(id);
				// is currently selected
				if (idx > -1) {
				    $scope.selection.splice(idx, 1);
				}
				// is newly selected
				else {
				    $scope.selection.push(id);
				}
		    } else { //Check for all checkbox selection   
		    	if($scope.selection.length > 0 && $scope.selectionAll){
		    		$scope.selection = [];
		    		$scope.checkboxes = {
		    			checked: false,
		    			items:{}
		    		};	
		    		$scope.selectionAll = false;
		    	} else {      	//Check for all un checked checkbox for check

		    		$scope.selectionAll = true
		    		$scope.selection = [];
		    		angular.forEach($scope.simpleList, function(item) {
		    			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
		    			$scope.selection.push(item._id);
		    		});
		    	}
			}
	       
		};


	//perform action
	$scope.performAction = function() {
		$scope.showloader = true;
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		$scope.selectedAction = selectedAction.value;
		if($scope.selectedAction == 0){
			toastr.warning(messagesConstants.selectAction);
			$scope.showloader = false;
		}else if($scope.selection == ""){
			toastr.warning(messagesConstants.selectActionField);
			$scope.showloader = false;
		}
		else{	
			for(var i = 0; i< roleLength; i++){
				var id =  $scope.selection[i];
				  if($scope.selectedAction == 3) {
				  updatedData.push({id: id, is_deleted: true});
				}
				else if($scope.selectedAction == 1) {
					updatedData.push({id: id, is_status: true});
				}
				else if($scope.selectedAction == 2) {
					updatedData.push({id: id, is_status: false});
				}
			}
			var inputJson = {data: updatedData}
			ExtraQuestionService.updateQuestionStatus(inputJson, function(response) {
				$scope.showloader = false;
				if(response.messageId == 200) {
					toastr.success(messagesConstants.QuestionStatusSuccess);
					$state.reload();
				} else {
					toastr.error(messagesConstants.QuestionSuccessError);
				}
			});
		}
	}
}]);