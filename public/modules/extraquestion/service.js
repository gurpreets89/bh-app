"use strict"

angular.module("ExtraQuestions")

.factory('ExtraQuestionService', ['communicationService', function(communicationService) {

	var service = {};

	service.saveQuestion = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.addExtraQuestion, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}
	
	service.deleteQuestion = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.delExtraQuestion, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	service.getAllQuestionsList = function(callback) {
		communicationService.resultViaGet(webservices.listAllExtraQuestion, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);	
		});	
	}
	
	service.getQuestionsList = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.listExtraQuestions, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);	
		});	
	}

	service.findOneQuestion =  function(questionId, callback) {
		var webservice = webservices.findOneExtraQuestion + "/" + questionId;
		communicationService.resultViaGet(webservice, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.updateQuestion = function(inputJsonString, questionId, callback) {
			var webservice = webservices.updateExtraQuestion + "/" + questionId;
			communicationService.resultViaPost(webservice, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getAnswerList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.getAnswerList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.updateQuestionStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateExtraQuestion, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	return service;

}]);