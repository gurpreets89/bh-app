"use strict"

angular.module("Subscribers")

.factory('SubscriberService', ['$http','communicationService' ,   function($http , communicationService) {

	var service = {};

	service.getSubscriberList = function(callback) {
			communicationService.resultViaGet(webservices.subscriberList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}	

	service.saveSubscriber = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addSubscriber, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getSubscriber = function(userId, callback) {
		var serviceURL = webservices.findOneSubscriber + "/" + userId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateSubscriber = function(inputJsonString, userId, callback) {
		var serviceURL = webservices.updateSubscriber + "/" + userId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateSubscriberStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateSubscriber, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				console.log(response.data);
			callback(response.data);
		});
	}
	
	service.getSubscriberHistory = function(userId, callback) {
		var serviceURL = webservices.findSubscriberHistory + "/" + userId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	return service;


}]);
