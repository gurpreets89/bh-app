	"use strict";
	//console.log("File Load ");
	angular.module("Subscribers")
		.controller("subscriberController", ['$scope', '$rootScope', '$localStorage', 'SubscriberService', 'RoleService', 'PlanService', 'ngTableParams', '$stateParams', '$state', '$location', function($scope, $rootScope, $localStorage, SubscriberService, RoleService, PlanService, ngTableParams, $stateParams, $state, $location) {


				if ($localStorage.userLoggedIn) {
					$rootScope.userLoggedIn = true;
					$rootScope.superadminLoggedIn = true;
					$rootScope.loggedInUserType = $localStorage.loggedInUsertype;
				} else {
					$rootScope.userLoggedIn = false;
				}


				if ($rootScope.message != "") {

					$scope.message = $rootScope.message;
				}
				
				//empty the $scope.message so the field gets reset once the message is displayed.
				$scope.message = "";
				$scope.activeTab = 0;
				$scope.user = {
					company : "",
					first_name: "",
					last_name: "",
					username: "",
					password: "",
					email: "",
					display_name: "",
					role: []
				}
				$scope.getAllPlans = function(){
					PlanService.getPlanList(function(response) {
						if (response.messageId == 200) {
							$scope.plans = response.data;
						}
					});
				}
				$scope.getAllPlans();

				//Toggle multilpe checkbox selection
				$scope.selection = [];
				$scope.selectionAll;
				$scope.toggleSelection = function toggleSelection(id) {
					//Check for single checkbox selection
					if (id) {
						var idx = $scope.selection.indexOf(id);
						// is currently selected
						if (idx > -1) {
							$scope.selection.splice(idx, 1);
						}
						// is newly selected
						else {
							$scope.selection.push(id);
						}
					}
					//Check for all checkbox selection
					else {
						//Check for all checked checkbox for uncheck
						if ($scope.selection.length > 0 && $scope.selectionAll) {
							$scope.selection = [];
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
							$scope.selectionAll = false;
						}
						//Check for all un checked checkbox for check
						else {
							$scope.selectionAll = true
							$scope.selection = [];
							angular.forEach($scope.simpleList, function(item) {
								$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
								$scope.selection.push(item._id);
							});
						}
					}
					//console.log($scope.selection)
				};


				//apply global Search
				$scope.applyGlobalSearch = function() {
					var term = $scope.globalSearchTerm;
					if (term != "") {
						if ($scope.isInvertedSearch) {
							term = "!" + term;
						}
						$scope.tableParams.filter({
							$: term
						});
						$scope.tableParams.reload();
					}
				}


				$scope.getAllUsers = function() {
					SubscriberService.getSubscriberList(function(response) {
						if (response.messageId == 200) {
							$scope.filter = {
								firstname: '',
								lastname: '',
								email: '',
								company: ''
							};
							
							$scope.tableParams = new ngTableParams({
								page: 1,
								count: 20,
								sorting: {
									firstname: "asc"
								},
								filter: $scope.filter
							}, {
								total: response.data.length,
								counts: [],
								data: response.data
							});
							//multiple checkboxes

							$scope.simpleList = response.data;
							$scope.roleData = response.data;;
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
						}
					});
				}
				$scope.getFullName = function(user) {
				    return user.fullName = user.first_name + ' ' + user.last_name;
				}
				
				$scope.subscriberhistory = function(){
					if ($stateParams.id) {
						SubscriberService.getSubscriberHistory($stateParams.id, function(response) {
							//console.log(response);
							if (response.messageId == 200) {
								//console.log(response.data)
								
								angular.forEach(response.data,function(value,key){
									var days = value.plan_id.active_period;
									var startPeriodDate = value.startdate;
									var enddate = moment(startPeriodDate).add(days, 'days').format('MMMM D, YYYY');
									response.data[key].expiredate = enddate;
								});
								$scope.subscriberinfo = response.data;
							}
						});
					}
				}
				
				$scope.activeTab = 0;
				$scope.findOne = function() {
					if ($stateParams.id) {
						SubscriberService.getSubscriber($stateParams.id, function(response) {
							//console.log(response);
							if (response.messageId == 200) {
								//console.log(response.data)
								$scope.user = response.data;
							}
						});
					}
					
				}


				$scope.checkStatus = function(yesNo) {
					if (yesNo)
						return "pickedEven";
					else
						return "";
				}

				$scope.moveTabContents = function(tab) {
					$scope.activeTab = tab;
				}

				
				$scope.updateData = function(type) {
					if ($scope.user._id) {
						//console.log($scope.user);
						var inputJsonString = $scope.user;
						SubscriberService.updateSubscriber(inputJsonString, $scope.user._id, function(response) {
							if (response.messageId == 200) {
								$scope.message = response.message;
								$scope.alerttype = 'alert-success';

							} else {
								$scope.message = response.message;
								$scope.alerttype = 'alert-danger';
							}
						});
					} else {
						var inputJsonString = $scope.user;
						//console.log(inputJsonString)
						SubscriberService.saveSubscriber(inputJsonString, function(response) {
							if (response.messageId == 200) {
								$scope.message = '';
								$stateParams.id = response.data
								$scope.user = response.data;
								$scope.message = response.message;
								$scope.alerttype = 'alert-success';
							} else {
								$scope.message = response.message;
								$scope.alerttype = 'alert-danger';
							}
						});
					}
				}



				//perform action
				$scope.performAction = function() {
					var roleLength = $scope.selection.length;
					var updatedData = [];
					$scope.selectedAction = selectedAction.value;
					//console.log($scope.selectedAction);
					//console.log($scope.selection);
					if ($scope.selectedAction == 0)
						$scope.message = messagesConstants.selectAction;
					else {
						for (var i = 0; i < roleLength; i++) {
							var id = $scope.selection[i];
							if ($scope.selectedAction == 3) {
								updatedData.push({
									id: id,
									is_deleted: true
								});
							} else if ($scope.selectedAction == 1) {
								updatedData.push({
									id: id,
									enable: true
								});
							} else if ($scope.selectedAction == 2) {
								updatedData.push({
									id: id,
									enable: false
								});
							}
						}
						var inputJson = {
							data: updatedData
						}
						SubscriberService.updateSubscriberStatus(inputJson, function(response) {
							$rootScope.message = messagesConstants.updateStatus;
							$state.reload();
						});
					}
				}

			}

		]);