"use strict"

angular.module("Users")

.factory('PatientService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


	service.getPatientList = function(filter, callback) {
		var serviceURL = webservices.patientList;
		if(typeof filter != 'undefined'){
			serviceURL = webservices.patientList + "/" + filter;
		}
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, headerConstants.json, function(response) {
			//console.log('in getPatientList---->>', response);
			callback(response.data);
		});
	}

	service.getPatientFilterList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.getPatientFilterList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveUser = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addUser, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveDiagnoseTarget = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.saveDiagnoseTarget, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getLatestAssessmentOfPatient = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.getLatestAssessmentOfPatient, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getHistoryData = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.getHistoryData, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.getUser = function(userId, callback) {
		var serviceURL = webservices.findOneUser + "/" + userId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateUser = function(inputJsonString, userId, callback) {
		var serviceURL = webservices.update + "/" + userId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.getPatientType = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getPatientType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.getPatientGoal = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getPatientGoal, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateUserStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateUser, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveScoreProbability = function(inputJsonString, callback) {
		alert("in patient service");
			communicationService.resultViaPost(webservices.saveScoreProbability, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveGeneralQuestions = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addGeneralQuestions, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.listPatientGeneralQuestions = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.patientGeneralQuestion, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updatePatientGeneralQuestions = function(inputJsonString, generalQuestionId, callback) {
		var serviceURL = webservices.updatepatientGeneralQuestion + "/" + generalQuestionId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveCheckInData = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addCheckInData, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.listCheckInData = function(inputJsonString, callback) {
			var serviceURL = webservices.listCheckInData;
			communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateCheckInData = function(inputJsonString, checkInId, callback) {
			var serviceURL = webservices.updateCheckInData + "/" + checkInId;
			communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	/* added by gurpreet */
	service.inviteOutPatient = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.inviteOutPatient, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	/* added by gurpreet */
	service.getInvitedUsersList = function(callback) {
		communicationService.resultViaGet(webservices.invitedUsersList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}
	
	/* added by gurpreet */
	service.updatePatient = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.updatePatient, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	/* added by gurpreet */
	service.getPatientProbability = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getPatientProbability, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	/* added by gurpreet */
	service.listCaregiverCheckInData = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.listCheckInData, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.getPatientDailyGoals = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getPatientDailyGoals, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.checkAppointmentForClinician = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.checkAppointmentForClinician, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	return service;


}]);
