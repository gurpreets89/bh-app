"use strict";

angular.module("Users")

bhapp.controller("patientController", ['$scope', '$rootScope', '$localStorage', 'GoalService', 'PatientService', 'DiseaseService', 'UserService', 'HomeService', 'ngTableParams', '$state', '$location', '$stateParams', 'SweetAlert', '$timeout', 'toastr', 'ClinicianService', function($scope, $rootScope, $localStorage, GoalService, PatientService, DiseaseService, UserService, HomeService, ngTableParams, $state, $location, $stateParams, SweetAlert, $timeout, toastr, ClinicianService) {

		if ($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
		} else {
			$rootScope.userLoggedIn = false;
		}

		if ($rootScope.message != "") {
			$scope.message = $rootScope.message;
		}

		$scope.showloader = false;
		$scope.caregivers = {};
		$scope.isEmailDisabled = false;
		$scope.isUsernameDisabled = false;
		//empty the $scope.message so the field gets reset once the message is displayed.
		$scope.message = "";
		$scope.activeTab = 0;
		$scope.user = {
			first_name: "",
			last_name: "",
			username: "",
			password: "",
			email: "",
			display_name: "",
			country_code: "+1",
			role: []
		}
		$scope.appointmentData = {};
		$scope.assessmentData = {};
		$scope.recordsCount = 25;
		var that = this;

		this.isOpen = false;

		this.openCalendar = function(e) {
			e.preventDefault();
			e.stopPropagation();

			that.isOpen = true;
		};

		/* ++ angular-calendar settings ++ */
		var minimumdate = new Date("Mon Jan 01 1900 12:00:00");
	var dd1 = minimumdate.getDate();
	var mm1 = minimumdate.getMonth() + 1; //January is 0!
	var yyyy1 = minimumdate.getFullYear();
	if (dd1 < 10) {
		dd1 = '0' + dd1
	}
	if (mm1 < 10) {
		mm1 = '0' + mm1
	}
	minimumdate = mm1 + '/' + dd1 + '/' + yyyy1;
	$scope.minDate = minimumdate;
	//console.log("$scope.minDate = ", $scope.minDate);
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();
		if (dd < 10) {
			dd = '0' + dd
		}
		if (mm < 10) {
			mm = '0' + mm
		}
		today = mm + '/' + dd + '/' + yyyy;
		$scope.maxDate = today;
		/* -- angular-calendar settings -- */

		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.toggleSelection = function toggleSelection(id) {
			//Check for single checkbox selection
			if (id) {
				var idx = $scope.selection.indexOf(id);
				// is currently selected
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				}
				// is newly selected
				else {
					$scope.selection.push(id);
				}
			}
			//Check for all checkbox selection
			else {
				//Check for all checked checkbox for uncheck
				if ($scope.selection.length > 0 && $scope.selectionAll) {
					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				}
				//Check for all un checked checkbox for check
				else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						$scope.selection.push(item._id);
					});
				}
			}
		};

		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;
			if (term != "") {
				if ($scope.isInvertedSearch) {
					term = "!" + term;
				}
				$scope.tableParams.filter({
					$: term
				});
				$scope.tableParams.reload();
			}
		}

		$scope.getAllPatients = function() {
			$scope.showloader = true;
			// Filter Records on the basic of user type added by RaJesh (Start)
			//Contains the filter options
			$scope.filterOptions = {
				stores: [{
					id: 1,
					name: 'Show All',
					type: 0
				}, {
					id: 2,
					name: 'Patient',
					type: 3
				}, {
					id: 3,
					name: 'Caregiver',
					type: 4
				}, ]
			};
			//Mapped to the model to filter
			$scope.filterItem = {
					store: $scope.filterOptions.stores[0]
				}
				// Filter Records on the basic of user type added by RaJesh (End)
			$scope.noDataMessage = "No users available.";
			var inputString = "";
			$scope.searchKey = '';
			if ($stateParams.id) {
				inputString = $stateParams.id;
				$scope.searchKey = $stateParams.id;
			}
			PatientService.getPatientList(inputString, function(response) {
				if (response.messageId == 200) {
					$scope.showloader = false;
					if (response.data.length == 0) {
						$scope.noPatients = true;
						$scope.patientExist = false;
						$scope.DataAvailable = false;
						$scope.filterAvailable = false;
						$scope.errorMessageAvailable = true;
					} else {
						//console.log("HERE ---------->>", JSON.stringify(response.data));
						$scope.noPatients = false;
						$scope.patientExist = true;
						$scope.DataAvailable = true;
						$scope.filterAvailable = true;
						$scope.errorMessageAvailable = false;
						$scope.filter = {
							firstname: '',
							lastname: '',
							email: '',
							'parent': ''
						};
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 10,
							sorting: { created_date: "desc" },
							filter: $scope.filter
						}, {
							total: response.data.length,
							counts: [10, 25, 50, 100],
							data: response.data,
							defaultSort: "asc"
						});
						$scope.simpleList = response.data;
						$scope.roleData = response.data;
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
					}
				}
			});
		}

		$scope.convertTime = function(time){
			var timeString = time;
			var H = +timeString.substr(0, 2);
			var h = H % 12 || 12;
			var ampm = H < 12 ? " AM" : " PM";
			return timeString = h + timeString.substr(2, 3) + ampm;
		}

		$scope.getAdminPatients = function() {
			$scope.showloader = true;
			/*
			// Filter Records on the basic of user type added by RaJesh (Start)
				//Contains the filter options
				$scope.filterOptions = {
					stores: [
						{id : 1, name : 'Show All', type: 0},
						{id : 2, name : 'Patient', type: 3},
						{id : 3, name : 'Caregiver', type: 4},
					]
				};
				//Mapped to the model to filter
				$scope.filterItem = {
					store: $scope.filterOptions.stores[0]
				}
			// Filter Records on the basic of user type added by RaJesh (End)
			*/
			$scope.noDataMessage = messagesConstants.NoPatientsAviliable;
			var inputString = {};
			inputString._id = $localStorage.loggedInUser.user._id;
			HomeService.findAdminPatient(inputString, function(response) {
				$scope.showloader = false;
				if (response.messageId == 200) {
					if (response.data.length == 0) {
						$scope.DataAvailable = false;
						$scope.filterAvailable = false;
						$scope.errorMessageAvailable = true;
					} else {
						var customResponseData = [];
						customResponseData = response.data;
						//console.log(JSON.stringify(customResponseData.length));
						for(var i = 0; i < customResponseData.length; i++){
							//console.log(customResponseData[i].parent.first_name + " " + customResponseData[i].parent.last_name);
							var fullname = customResponseData[i].parent.first_name + " " + customResponseData[i].parent.last_name;
							customResponseData[i].clinician_fullname = fullname;
						}
						$scope.DataAvailable = true;
						$scope.filterAvailable = true;
						$scope.errorMessageAvailable = false;
						//console.log(response.data);
						$scope.filter = {
							firstname: '',
							lastname: '',
							email: '',
							clinician_fullname: ''
						};
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 10,
							sorting: {
								firstname: "asc"
							},
							filter: $scope.filter
						}, {
							total: customResponseData.length,
							counts: [10, 25, 50, 100],
							data: customResponseData,
							defaultSort: "asc"
						});
					}

				}
			});
		}

		/*
		|------------------------------------------------------------
		| function : Filter Records on the basis of user type
		| developer :  RaJesh
		|------------------------------------------------------------
		*/
		$scope.filterData = function(data) {
			$scope.showloader = true;
			var inputString = {};
			if (data.type == 3 || data.type == 4) {
				inputString.user_type = data.type;
			} else {
				inputString = "";
			}
			PatientService.getPatientFilterList(inputString, function(response) {
				if (response.messageId == 200) {
					$scope.showloader = false;
					if (response.data.length == 0) {
						$scope.DataAvailable = false;
						$scope.errorMessageAvailable = true;
						if (data.type == 3) {
							$scope.noDataMessage = "No patients are available.";
						} else if (data.type == 4) {
							$scope.noDataMessage = "No caregivers are available.";
						}
					} else {
						$scope.DataAvailable = true;
						$scope.filterAvailable = true;
						$scope.errorMessageAvailable = false;
						$scope.filter = {
							firstname: '',
							lastname: '',
							email: ''
						};
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 10,
							sorting: { created_date: "desc"},
							filter: $scope.filter
						}, {
							total: response.data.length,
							counts: [10, 25, 50, 100],
							data: response.data,
							defaultSort: "asc"
						});
					}
				}
			});
		};


		/* Created by Rajesh on 27-05-2016 To show patient details in model */
		$scope.getDetails = function(user) {
			$scope.userData = user;
		}

		/* Created by Rajesh on 24-08-2016 To show Missed Check in model */
		$scope.getMissedCheckIn = function(user) {
			$scope.missedCheckdata = user;
		}


		/* Created by Jatinder Singh on 04-07-2016 To show assigned caregiver/patient details in model */
		$scope.getAssignedDetails = function(user) {
			$scope.userData = {};

			if (user.user_type == 3) {
				$scope.userData.caregivers_patient_id = {};
				$scope.userData.first_name = user.caregiver.first_name;
				$scope.userData.last_name = user.caregiver.last_name;
				$scope.userData.username = user.caregiver.username;
				$scope.userData.email = user.caregiver.email;
				$scope.userData.user_type = user.caregiver.user_type;
				$scope.userData.caregivers_patient_id.first_name = user.first_name;
				$scope.userData.caregivers_patient_id.last_name = user.last_name;
				$scope.userData.patient_type = 0;
			} else if (user.user_type == 4) {
				$scope.userData.caregiver = {};
				$scope.userData.first_name = user.caregivers_patient_id.first_name;
				$scope.userData.last_name = user.caregivers_patient_id.last_name;
				$scope.userData.username = user.caregivers_patient_id.username;
				$scope.userData.email = user.caregivers_patient_id.email;
				$scope.userData.user_type = user.caregivers_patient_id.user_type;
				$scope.userData.caregiver.first_name = user.first_name;
				$scope.userData.caregiver.last_name = user.last_name;
				$scope.userData.emr_no = user.emr_no;
				$scope.userData.patient_type = 0;
			}
		}


		/* get Patient Goal */
		$scope.getGoals = function() {
			$scope.showloader = true;
			if ($stateParams.id) {
				UserService.getUser($stateParams.id, function(response) {
					if (response.messageId == 200) {
						$scope.userDetails = response.data;
					}
				});
			}
			var inputJsonString = {};
			inputJsonString.patient = $stateParams.id;
			$scope.STATE_PARAM = $stateParams;
			PatientService.getPatientGoal(inputJsonString, function(response) {
				if (response.messageId == 200) {
					$scope.showloader = false;
					if (response.data.length == 0) {
						$scope.showNorecord = true;
					}
					$scope.showNextSession = false;
					$scope.showMidTerm = false;
					$scope.showTherapy = false;
					for (var i = 0; i < response.data.length; i++) {
						if (response.data[i].goal_type_id == 1) {
							$scope.showNextSession = true;
						}
						if (response.data[i].goal_type_id == 2) {
							$scope.showMidTerm = true;
						}
						if (response.data[i].goal_type_id == 3) {
							$scope.showTherapy = true;
						}
					}
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 10
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
				}
			});
		}


		$scope.delGoal = function(id, name) {
			SweetAlert.swal({
					title: "Confirmation",
					text: "Are you sure you want to delete goal (" + name + ")",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No!",
					closeOnConfirm: true
				},
				function(isConfirm) {
					if (isConfirm) {
						var delete_object = {
							'_id': id
						};
						GoalService.deleteGoal(delete_object, function(response) {
							$timeout(function() {
								$scope.getGoals();
								$state.reload();
							}, 100);
							toastr.success(messagesConstants.deleteGoalSuccess);
						});
					}
				}
			);
		}

		$scope.newAssesment = function() {
			PatientService.getPatientList(function(response) {
				if (response.messageId == 200) {
					$scope.filter = {
						firstname: '',
						lastname: '',
						email: ''
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 2,
						sorting: {
							firstname: "asc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
					//multiple checkboxes
					$scope.simpleList = response.data;
					$scope.roleData = response.data;;
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				}
			});
		}

		$scope.getAllRoles = function() {
			RoleService.getRoleList(function(response) {
				if (response.messageId == 200) {
					var len = response.data.length;
					var rolePermissionlen = $scope.user.role.length;
					for (var i = 0; i < len; i++) {
						if (($scope.user.role.indexOf(response.data[i]._id)) == -1)
							response.data[i].used = false;
						else
							response.data[i].used = true;
					}
					$scope.roleData = response.data;
				}
			});
		}
		$scope.activeTab = 0;
		$scope.findOne = function(cb) {
			if ($stateParams.id) {
				UserService.getUser($stateParams.id, function(response) {
					if (response.messageId == 200) {
						$scope.user = response.data;
						if (cb) {
							cb(response.data);
						}
					}
				});
			}
		}


		$scope.checkStatus = function(yesNo) {
			if (yesNo)
				return "pickedEven";
			else
				return "";
		}

		$scope.moveTabContents = function(tab) {
			$scope.activeTab = tab;
		}




		$scope.updateData = function(type) {
			if ($scope.user._id) {
				var inputJsonString = $scope.user;
				UserService.updateUser(inputJsonString, $scope.user._id, function(response) {
					if (response.messageId == 200) {
						if (type)
							$location.path("/users");
						else {
							++$scope.activeTab
						}

					} else {
						$scope.message = err.message;
					}
				});
			} else {
				var inputJsonString = $scope.user;
				UserService.saveUser(inputJsonString, function(response) {
					if (response.messageId == 200) {
						$scope.message = '';
						$stateParams.id = response.data
						$scope.user = response.data;
						$scope.activeTab = 1;
					} else {
						$scope.message = response.message;
					}
				});
			}
		}




		//perform action
		$scope.performAction = function() {
			var roleLength = $scope.selection.length;
			var updatedData = [];
			$scope.selectedAction = selectedAction.value;
			if ($scope.selectedAction == 0) {
				toastr.warning(messagesConstants.selectAction);
			} else if ($scope.selection == "") {
				toastr.warning(messagesConstants.selectActionField);
			} else {
				for (var i = 0; i < roleLength; i++) {
					var id = $scope.selection[i];
					if ($scope.selectedAction == 3) {
						updatedData.push({
							id: id,
							is_deleted: true
						});
					} else if ($scope.selectedAction == 1) {
						updatedData.push({
							id: id,
							is_status: true
						});
					} else if ($scope.selectedAction == 2) {
						updatedData.push({
							id: id,
							is_status: false
						});
					}
				}
				var inputJson = {
					data: updatedData
				}
				UserService.updateUserStatus(inputJson, function(response) {
					$rootScope.message = messagesConstants.updateStatus;
					toastr.success(messagesConstants.UsersSuccessMsg);
					$state.reload();
				});
			}
		}

		//perform action

		$scope.performAction1 = function() {
			var data = $scope.checkboxes.items;
			var records = [];
			var inputJsonString = "";
			var jsonString = "";

			var actionToPerform = "";

			$scope.selectAction = selectAction.value;

			if ($scope.selectAction == "disable") {
				actionToPerform = false;
			} else if ($scope.selectAction == "enable") {
				actionToPerform = true;
			} else if ($scope.selectAction == "delete") {

				actionToPerform = "delete";
			}


			for (var id in data) {
				if (data[id]) {
					if (actionToPerform == "delete") {
						if (jsonString == "") {

							jsonString = '{"_id": "' + id + '", "is_deleted":"true"}';
						} else {
							jsonString = jsonString + "," + '{"_id": "' + id + '", "is_deleted":"true"}';
						}
					} else {
						if (jsonString == "") {

							jsonString = '{"_id": "' + id + '", "enable":"' + actionToPerform + '"}';
						} else {
							jsonString = jsonString + "," + '{"_id": "' + id + '", "enable":"' + actionToPerform + '"}';
						}
					}
				}

			}

			inputJsonString = "[" + jsonString + "]";

			if (actionToPerform == "delete") {

				UserService.deleteUser(inputJsonString, function(response) {
					$rootScope.message = messagesConstants.deleteUser;
					$state.reload();
				});
			} else {
				UserService.statusUpdateUser(inputJsonString, function(response) {
					$rootScope.message = messagesConstants.updateStatus;
					$state.reload();
				});
			}

		}

		/** function to add new patients from the clinician platform.*/
		$scope.errordetail = {};
		$scope.errordetail.usernameexist = false;
		$scope.errordetail.emailexist = false;
		$scope.errordetail.usernameexistMsg = '';
		$scope.errordetail.emailexistMsg = '';

		$scope.addPatient = function() {
			$scope.showloader = true;
			$scope.user.is_status = true;
			if ($stateParams.id) {
				var inputJsonString = {};
				inputJsonString = $scope.user;
				inputJsonString.country_code = "+1";
				inputJsonString.modified_date = Date.now();
				var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(inputJsonString), messagesConstants.encryptionKey);
				var encryptedJSON = {};
				encryptedJSON.inputData = ciphertext.toString();
				PatientService.updatePatient(encryptedJSON, function(response) {
					//console.log('response = ', response, $scope.user);
					if (response.messageId == 200) {
						$scope.showloader = false;
						if($scope.user.user_type==4){
							toastr.success(messagesConstants.updateCaregiver);
						}else{
							toastr.success(messagesConstants.updatePatient);
						}
						$location.path('/patients');
					}
				});
			} else {
				var checkExist = {};
				checkExist.username = $scope.user.username;
				checkExist.email = $scope.user.email;
				UserService.checkUserAndEmail(checkExist, function(resp1) {
					$scope.errordetail = {};
					if (resp1.messageId == 200) {
						if (resp1.status == 'warning-username') {
							$scope.showloader = false;
							$scope.errordetail.usernameexist = true;
							toastr.error(resp1.message);
							$scope.errordetail.usernameexistMsg = resp1.message;
						} else if (resp1.status == 'warning-email') {
							$scope.showloader = false;
							$scope.errordetail.emailexist = true;
							toastr.error(resp1.message);
							$scope.errordetail.emailexistMsg = resp1.message;
						} else if (resp1.status == 'success') {
							var inputString = {};
							inputString = $scope.user;
							inputString.country_code = "+1";
							inputString.password = randomPassword(8);
							inputString.originalPassword = inputString.password;
							var passwordEncrpt = CryptoJS.AES.encrypt(JSON.stringify(inputString.password), messagesConstants.encryptionKey);
							inputString.password = passwordEncrpt.toString();
							inputString.patient_type = 0;
							inputString.clinic_name = 'Test Clinic';
							inputString.created_date = Date.now();
							inputString.modified_date = Date.now();

							var msgString = ''
							if (inputString.user_type == 3) {
								msgString = messagesConstants.savePatient;
							} else if (inputString.user_type == 4) {
								msgString = messagesConstants.saveCaregiver;
							}

							var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(inputString), messagesConstants.encryptionKey);
							var encryptedJSON = {};
							encryptedJSON.inputData = ciphertext.toString();
							UserService.savePatient(encryptedJSON, function(response) {
								$scope.showloader = false;
								if (response.messageId == 200) {
									/** log newly added patients info in patientLog table **/
									var clinicianInputJson = {};
									clinicianInputJson.patients = [response.data._id];
									clinicianInputJson.data = [response.data];
									ClinicianService.logPatientsInfo(clinicianInputJson, function(response) {
										if (response.messageId == 200) {
											toastr.success(msgString);
											$location.path('/patients');
										}
									});
								} else {
									/** customized error messages for firstname, lastname, email **/
									var eMsg = response.message;
									var msgSet = false;
									var eMsgArr = eMsg.split(",");
									if (typeof eMsgArr[0] != 'undefined') {
										var e1 = eMsgArr[0];
										var e1Arr = e1.split(" ");
										if (e1Arr[0] == 'first_name') {
											$scope.errormessage = messagesConstants.FirstNameValidatedError;
											msgSet = true;
										} else if (e1Arr[0] == 'last_name') {
											$scope.errormessage = messagesConstants.LastNameValidatedError;
											msgSet = true;
										}
										toastr.error($scope.errormessage);
									}
									if (msgSet == false && (typeof eMsgArr[1] != 'undefined')) {
										var e2 = eMsgArr[1];
										var e2Arr = e2.split(" ");
										if (e2Arr[0] == 'last_name') {
											$scope.errormessage = messagesConstants.EmailValidationError;
											msgSet = true;
										}
										toastr.error($scope.errormessage);
									}
								}
							});
						}
					}
				});
			}
		}

		/*
		|-------------------------------------------
		|	Add CAREGIVER when adding patient
		|	developer : gurpreet
		|-------------------------------------------
		*/
		$scope.modalClose = false;
		$scope.addCaregiver = function() {
			$scope.showloader = true;
			var checkExist = {};
			checkExist.username = $scope.caregiver.username;
			checkExist.email = $scope.caregiver.email;
			UserService.checkUserAndEmail(checkExist, function(resp1) {
				$scope.cgErrorDetail = {};
				$scope.showloader = false;
				if (resp1.messageId == 200) {
					if (resp1.status == 'warning-username') {
						$scope.cgErrorDetail.usernameexist = true;
						toastr.error(resp1.message);
					}
					if (resp1.status == 'warning-email') {
						$scope.cgErrorDetail.emailexist = true;
						toastr.error(resp1.message);
					}
					if (resp1.status == 'success') {
						var inputString = {};
						inputString = $scope.caregiver;
						inputString.password = randomPassword(8);
						inputString.originalPassword = inputString.password;
						var passwordEncrpt = CryptoJS.AES.encrypt(JSON.stringify(inputString.password), messagesConstants.encryptionKey);
						inputString.password = passwordEncrpt.toString();
						inputString.patient_type = 0;
						inputString.clinic_name = 'Test Clinic';
						inputString.created_date = Date.now();
						inputString.modified_date = Date.now();

						var msgString = ''
						msgString = messagesConstants.saveCaregiver;
						var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(inputString), messagesConstants.encryptionKey);
						var encryptedJSON = {};
						encryptedJSON.inputData = ciphertext.toString();
						UserService.savePatient(encryptedJSON, function(response) {
							$scope.showloader = false;
							if (response.messageId == 200) {
								toastr.success(msgString);
								$scope.caregiver = '';
								$scope.modalClose = true;
								$scope.getCaregivers(4);
							} else {
								var rm = response.message.split(" ");
								rm.splice(0, 1);
								toastr.error(rm.join(" "));
							}
						});
					}
				}
			});
		}

		/*
		|-------------------------------------------
		|	Generate Random password function
		|	developer : gurpreet
		|-------------------------------------------
		*/
		function randomPassword(length) {
			var chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNP123456789";
			var pass = "";
			for (var x = 0; x < length; x++) {
				var i = Math.floor(Math.random() * chars.length);
				pass += chars.charAt(i);
			}
			return pass;
		}


		/** function to apply algorithm on the Assessment solved by the patient.*/
		$scope.processAssessment = function() {
			$scope.user_score = 47;
			$scope.title = 'ADHD';
			var inputString = {};
			DiseaseService.getDiseaseDetail($scope.title, function(resp) {
				if (resp.messageId == 200) {
					if (resp.status == 'success') {

						$scope.diseaseData = resp.data;
						/*High Severity Check*/
						if ($scope.user_score >= $scope.diseaseData.high_severity_threshold) {
							inputString.posterior_probability = $scope.diseaseData.op_high_risk_posterior;
						} else if ($scope.user_score >= $scope.diseaseData.low_severity_threshold && $scope.user_score < $scope.diseaseData.high_severity_threshold) {
							/*Low Severity Check*/
							inputString.posterior_probability = $scope.diseaseData.op_low_risk_posterior;
						} else {
							inputString.posterior_probability = 0;
						}
						inputString.severity_score = $scope.user_score;
						inputString.patient = "57345ce347addb3b87536662";
						// inputString.created_date:  Date.now();
						// inputString.modified_date:  Date.now();
						PatientService.saveScoreProbability(inputString, function(response) {

							// console.log('in saveScoreProbability = ', response);
						});
					}
				} else if (resp.messageId == 203) {
					//if error in retriving data
					if (resp1.status == 'failure') {}
				}
			});
		}

		// General Questions

		$scope.generalQuestionCheckIn = [{
			"id": 1,
			"title": "Alcohol/Drugs?",
			"is_optout": false,
			"checked": false
		}, {
			"id": 2,
			"title": "Exercise?",
			"is_optout": false,
			"checked": false
		}, {
			"id": 3,
			"title": "Caffeine?",
			"is_optout": false,
			"checked": false
		}];
		$scope.staticQuestions = [{
			"id": "1",
			"title": "Good Mood?",
			"rating": 0
		}, {
			"id": "2",
			"title": "Good Energy?",
			"rating": 0
		}, {
			"id": "3",
			"title": "Low Stress?",
			"rating": 0
		}, {
			"id": "4",
			"title": "Good Sleep?",
			"rating": 0
		}, {
			"id": "5",
			"title": "Eating Healthy?",
			"rating": 0
		}];


		

		/**
		 *	set graph settings for state of mind
		 *	developer : gurpreet
		 **/
		$scope.myChartObject1 = {};
		$scope.myChartObject2 = {};
		$scope.myChartObject3 = {};
		$scope.myChartObject4 = {};
		$scope.setGraphSettings = function(myChartObject, graphType, graphTitle, chartArea, data) {
			var dataLength = [];
			for (var i = 0; i < data.cols.length; i++) {
				dataLength.push(i);
			}
			var commonChartObj = {};

			commonChartObj.type = graphType;
			commonChartObj.data = data;
			commonChartObj.displayed = false;
			commonChartObj.view = {
				columns: dataLength
			};
			commonChartObj.options = {
				legend: false,
				title: graphTitle,
				vAxis: {
					title: 'Rating',
					"gridlines": {
						"count": 10
					},
					titleTextStyle: {
						color: 'black',
						fontSize: '15',
						fontName: '"Arial"'
					},
					viewWindowMode: 'explicit',
					viewWindow: {
						max: 5,
						min: 0
					}
				},
				hAxis: {
					title: 'Date',
					titleTextStyle: {
						color: 'black',
						fontSize: '15',
						fontName: '"Arial"'
					}
				},
				// chartArea:{width:chartArea+'%'},
				defaultColors: ['#0000FF', '#009900', '#CC0000', '#DD9900', '#0000FF', '#009900', '#CC0000', '#DD9900'],
				isStacked: "false",
				fill: 20,
				displayExactValues: true,
			};

			if (myChartObject == 'myChartObject1') {
				$scope.myChartObject1 = commonChartObj;
			}
			if (myChartObject == 'myChartObject2') {
				$scope.myChartObject2 = commonChartObj;
			}
			if (myChartObject == 'myChartObject3') {
				$scope.myChartObject3 = commonChartObj;
			}
			if (myChartObject == 'myChartObject4') {
				$scope.myChartObject4 = commonChartObj;
			}
		}



		


		// get Check in screen data
		$scope.getCheckInData = function() {
			$scope.showloader = true;
			if ($stateParams.id) {
				UserService.getUser($stateParams.id, function(response) {
					if (response.messageId == 200) {
						$scope.userDetails = response.data;
					}
				});
			}
			$scope.showGoalAvailable = true;
			$scope.showNorecord = false;
			$scope.dailyMetricAvailable = false;
			$scope.weeklyMetricAvailable = false;
			$scope.checkinExist = false;
			$scope.STATE_PARAM = $stateParams;
			var inputJsonString = {};
			inputJsonString.patient = $stateParams.id;
			$scope.dailyMetricData = [];
			$scope.weeklyMetricData = []
			$scope.weekly_Metric_Data = {};
			$scope.daily_metric_data = {};
			PatientService.getPatientGoal(inputJsonString, function(response) {
				if (response.messageId == 200) {
					if (response.data.length == 0) {
						$scope.showGoalAvailable = false;
						$scope.showNorecord = true;
					} else {
						for (var i = 0; i < response.data.length; i++) {
							$scope.checkInData = response.data;
							if (response.data[i].goal_type_id == 1) {
								$scope.dailyMetricAvailable = true;
								$scope.daily_metric_data[i] = {
									"goal": $scope.checkInData[i]._id,
									"goal_question": $scope.checkInData[i].goal_question,
									"checked": false
								}
							}
							if (response.data[i].goal_type_id == 2) {
								$scope.weeklyMetricAvailable = true;
								$scope.weekly_Metric_Data[i] = {
									"goal": $scope.checkInData[i]._id,
									"goal_question": $scope.checkInData[i].goal_question,
									"rating": 0
								}
							}
						}
						for (var i in $scope.daily_metric_data) {
							if ($scope.daily_metric_data.hasOwnProperty(i)) {
								$scope.dailyMetricData.push($scope.daily_metric_data[i]);
							}
						}

						for (var i in $scope.weekly_Metric_Data) {
							if ($scope.weekly_Metric_Data.hasOwnProperty(i)) {
								$scope.weeklyMetricData.push($scope.weekly_Metric_Data[i]);
							}
						}
					}
					$scope.checkIndataMethod();
				}
			});

			//  Get Patient General Questions OptOut
			PatientService.listPatientGeneralQuestions(inputJsonString, function(response) {
				if (response.messageId == 200) {
					if (response.data.length == 0) {
						$scope.updateOptOutdata = false;
					} else {
						$scope.generalQuestionCheckIn = [{
							"id": 1,
							"title": "Alcohol/Drugs?",
							"is_optout": response.data[0].is_alcohal_drugs,
							"checked": false
						}, {
							"id": 2,
							"title": "Exercise?",
							"is_optout": response.data[0].is_exercise,
							"checked": false
						}, {
							"id": 3,
							"title": "Caffeine?",
							"is_optout": response.data[0].is_caffeine,
							"checked": false
						}];
						$scope.updateOptOutdata = true;
						$scope.updateFieldId = response.data[0]._id;
					}
				}
			});
		}

		// Check In Data
		$scope.checkIndataMethod = function() {
			if (!$scope.showNorecord) {
				$scope.findPatientCheckIn = {};
				$scope.findPatientCheckIn.patient = $stateParams.id;
				$scope.findPatientCheckIn.checkin_date = date;
				// LIST CHECK IN
				PatientService.listCheckInData($scope.findPatientCheckIn, function(response) {
					if (response.messageId == 200) {
						$scope.showloader = false;
						if (response.data.length != 0) {
							$scope.checkinExist = true;
							$scope.showGoalAvailable = false;
						} else {
							$scope.checkinExist = false;
							$scope.showGoalAvailable = true;
						}
					}
				});
			}else{
				$scope.showloader = false;
			}
		}


		var currentDt = new Date();
		var mm = currentDt.getMonth() + 1;
		var dd = currentDt.getDate();
		var yyyy = currentDt.getFullYear();
		var date = mm + '/' + dd + '/' + yyyy;

		var inputJsonData = {};
		inputJsonData.static_metric = [];
		inputJsonData.weekly_metric = [];
		inputJsonData.daily_metric = [];
		inputJsonData.general_metric = [];

		$scope.submitCheckInData = function() {
			inputJsonData.patient = $stateParams.id;
			inputJsonData.checkin_date = date;
			inputJsonData.submitted_by = 'patient';
			for (var i in $scope.dailyMetricData) {
				if ($scope.dailyMetricData.hasOwnProperty(i)) {
					inputJsonData.daily_metric.push($scope.dailyMetricData[i]);
				}
			}

			for (var i in $scope.weeklyMetricData) {
				if ($scope.weeklyMetricData.hasOwnProperty(i)) {
					inputJsonData.weekly_metric.push($scope.weeklyMetricData[i]);
				}
			}


			for (var i in $scope.staticQuestions) {
				if ($scope.staticQuestions.hasOwnProperty(i)) {
					inputJsonData.static_metric.push($scope.staticQuestions[i]);
				}
			}
			for (var i in $scope.generalQuestionCheckIn) {
				if ($scope.generalQuestionCheckIn.hasOwnProperty(i)) {
					if ($scope.generalQuestionCheckIn[i].is_optout != true) {
						inputJsonData.general_metric.push($scope.generalQuestionCheckIn[i]);
					}
				}
			}

			// Save check in on daily basis
			PatientService.saveCheckInData(inputJsonData, function(response) {
				if (response.messageId == 200) {
					$scope.saveOptOutValues();
					toastr.success(messagesConstants.CheckInUpdateSuccess);
					$timeout(function() {
						$location.path('/patients');
					}, 2000);
				} else {
					toastr.error(messagesConstants.CheckInUpdateError);
				}
			});
		}

		$scope.saveOptOutValues = function() {
			$scope.inputJsonString = {};
			$scope.inputJsonString.patient = $stateParams.id;
			for (var i = 0; i < $scope.generalQuestionCheckIn.length; i++) {
				if ($scope.generalQuestionCheckIn[i].id == 1) {
					$scope.inputJsonString.is_alcohal_drugs = $scope.generalQuestionCheckIn[i].is_optout;
				} else if ($scope.generalQuestionCheckIn[i].id == 2) {
					$scope.inputJsonString.is_exercise = $scope.generalQuestionCheckIn[i].is_optout;
				} else if ($scope.generalQuestionCheckIn[i].id == 3) {
					$scope.inputJsonString.is_caffeine = $scope.generalQuestionCheckIn[i].is_optout;
				}
			}

			if (!$scope.updateOptOutdata) {
				PatientService.saveGeneralQuestions($scope.inputJsonString, function(response) {});
			} else {
				PatientService.updatePatientGeneralQuestions($scope.inputJsonString, $scope.updateFieldId, function(response) {
					if (response.messageId == 200) {
						//console.log("General questions has been updated successfully.");
					} else {
						//console.log("General questions can't be updated.");
					}
				});
			}
		}


		/*
		 *Created by : Jatinder Singh
		 *function to show the probability screen data algorithm on the Assessment solved by the patient.
		 */

		$scope.currentProbability = function(inputString, inputString2) {
			$scope.probability = {};
			$scope.isAssessmentData = false;
			//console.log(inputString2);
			PatientService.getLatestAssessmentOfPatient(inputString2, function(response) {
				if (response.messageId == 200 && response.status == 'success') {
					$scope.data = response.data;
					$scope.currentData = response.data[0].results;
					//console.log(JSON.stringify($scope.currentData));
					$scope.assessmentData = response.data[0].answers;
					if (response.data[0].answers.length > 0) {
						$scope.isAssessmentData = true;
					}

					//console.log(response.data);


					// get Extra question percentage
					var extra_question_highest = 0;
					var extra_question_get = 0;
					for(var p=0;p<$scope.assessmentData.length;p++){
						if(typeof $scope.assessmentData[p]['extra_question'] != "undefined"){
							extra_question_highest +=  $scope.assessmentData[p]['highest_score'];
							extra_question_get +=  $scope.assessmentData[p]['answer_score'];
						}
					}
					$scope.extraQuestionPer = (extra_question_get*100)/extra_question_highest;
					$scope.extraQuestionPer = $scope.extraQuestionPer.toFixed(2);
					//End of code to get extra questions

						
					if ($scope.data[0].patient.next_assessment_available) {
						$scope.assessmentDateAvailable = true;
						$scope.assessmentDateNotAvailable = false;
					} else {
						$scope.assessmentDateAvailable = false;
						$scope.assessmentDateNotAvailable = true;
					}

					if ($scope.data[0].patient.next_appointment_date) {
						$scope.appointmentDateAvailable = true;
						$scope.appointmentDateNotAvailable = false;
					} else {
						$scope.appointmentDateAvailable = false;
						$scope.appointmentDateNotAvailable = true;
					}
					if (response.data[0].user_type == 3) {
						$scope.user_who_submitted_Assessment = "Patient Submitted Questions & Answers";
					} else if (response.data[0].user_type == 4) {
						$scope.user_who_submitted_Assessment = "Caregiver Submitted Questions & Answers";
					}


					/*Bar Chart Functionality start here*/
					var json = response.data[0].results;
					var graphColors = {
						0: '#DB4437',
						1: '#1B9E77',
						2: '#FFD700'
					};
					var newarr = {};
					newarr.cols = [{
						id: "t",
						label: "Conditions",
						type: "string"
					}, {
						id: "s",
						label: "Percentage",
						type: "number"
					}, {
						role: "style",
						type: "string"
					}];
					newarr.rows = [];
					var key = '';
					json.forEach(function(index, value) {
						if (index.posterior_probability >= 80) {
							key = 0;
						} else if (index.posterior_probability >= 31 && index.posterior_probability <= 80) {
							key = 2;
						} else {
							key = 1;
						}
						var obj = {};
						obj.c = [{
							v: index.disease.title
						}, {
							v: index.posterior_probability
						}, {
							v: 'color:' + graphColors[key]
						}, ]
						newarr.rows.push(obj);
					});
					/*Bar Chart Functionality ends here*/
					$scope.createGoogleGraph('BarChart', 'Patient Probability', 65, newarr);
					$scope.DataAvailable = true;
					$scope.NoShowData = false;
					PatientService.getHistoryData(inputString, function(response1) {
							if (response1.status == "success") {
								response1.data.forEach(function(index, value) {
									if ($scope.data[0]._id != response1.data[value]._id) {
										$scope.historyData[value] = index;
										//console.log($scope.historyData);
										$scope.previousAvailable = false;
									}
								});
							}
						})
						// PatientService.getLatestAssessmentOfPatient(inputString, function(response2) {
						// 	console.log(response2);
						// 		if(response2.messageId == 200 && response2.status == 'success') {
						// 		response2.data.forEach(function(index, value) {
						// 			if(index._id != response.data[0]._id){
						// 				$scope.historyData[value] = index;
						// 			}
						// 		});
						// 	} else {
						// 		$scope.DataAvailable = false;
						// 		$scope.NoShowData = true;
						// 	}
						// });
				} else {
					$scope.DataAvailable = false;
					$scope.NoShowData = true;
				}
				$scope.showloader = false;
			});
		};

		/*Created by Jatinder Singh*/
		//google charts
		$scope.createGoogleGraph = function(graphType, graphTitle, chartArea, data) {
			$scope.myChartObject = {};
			$scope.myChartObject.type = graphType;
			$scope.myChartObject.data = data;
			$scope.myChartObject.options = {
				legend: false,
				title: graphTitle,
				vAxis: {
					title: 'Conditions',
					titleTextStyle: {
						color: 'black',
						fontSize: '15',
						fontName: '"Arial"'
					}
				},
				hAxis: {
					title: 'Percentage',
					titleTextStyle: {
						color: 'black',
						fontSize: '15',
						fontName: '"Arial"'
					}
				},
				chartArea: {
					width: chartArea + '%'
				},
				colors: ['#ffffff'],
			};
		}

		$scope.Math = window.Math;

		/** function to send invite to new out patients from the clinician platform.*/
		$scope.outpatient = {};
		$scope.outpatient.user_type = "patient";

		$scope.addOPatient = function() {
			console.log($scope.outpatient);
			$scope.showloader = true;
			var inputString = {};
			inputString = $scope.outpatient;
			PatientService.inviteOutPatient(inputString, function(response) {
				//console.log(response.messageId);
				$scope.showloader = false;
				if (response.messageId == 200 && response.status == 'success') {
					// Invitation sent
					var message = "";
					if($scope.outpatient.user_type == 'patient'){
						message = "Invitation link to patient has been sent successfully.";
					}else{
						message = "Invitation link to caregiver has been sent successfully.";
					}
					toastr.success(message);
					$timeout(function() {
						$location.path('/patients');
					}, 1000);
				}else if(response.messageId == 205 && response.status == 'success'){
					// Invitation Resent
					var message = "";
					if($scope.outpatient.user_type == 'patient'){
						message = "Invitation link to patient has been resent successfully.";
					}else{
						message = "Invitation link to caregiver has been resent successfully.";
					}
					toastr.success(message);
					$timeout(function() {
						$location.path('/patients');
					}, 1000);

				}else if (response.messageId == 200 && response.status == 'warning') {
					toastr.warning(messagesConstants.EmailExistWarning);
				}
			});
		}

		/**
		 *	function to get list of Invited Users
		 *	developer : Gurpreet Singh
		 **/
		$scope.InvitedUsersCount = false;
		$scope.getInvitedUsers = function() {
			$scope.showloader = true;
			PatientService.getInvitedUsersList(function(response) {
				if (response.messageId == 200) {
					$scope.showloader = false;
					if (response.data.length == 0) {
						$scope.InvitedUsersCount = false;
						$scope.errorMessageAvailable = true;
						$scope.noDataMessage = "No invited users are available.";
					} else {
						console.log("response.data", response.data);
						$scope.InvitedUsersCount = true;
						$scope.errorMessageAvailable = false;
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 10,
							sorting: {
								email: "asc",
								created_date: "desc"
							}
						}, {
							total: response.data.length,
							counts: [10, 25, 50, 100],
							data: response.data,
							defaultSort: "asc"
						});
					}
				}
			});
		}


		/*Created by : Jatinder Singh*/
		/** function to show the probability screen data algorithm on the Assessment solved by the patient.*/
		$scope.currentData = {};
		$scope.historyData = {};
		$scope.buttonShow = true;
		$scope.latestDate = '';
		$scope.getPatientProbability = function() {
			$scope.showloader = true;
			if ($stateParams.id) {
				UserService.getUser($stateParams.id, function(response) {
					if (response.messageId == 200) {
						$scope.patient_id = '';
						var inputString = {};
						var inputString2 = {};
						inputString.patient = $stateParams.id;

						if (response.data.caregivers_patient_id != null) {
							$scope.userDetails = response.data.caregivers_patient_id;
							$scope.user_parent = response.data;
							$scope.assignedPatient = true;
							inputString.patient = response.data.caregivers_patient_id._id;
						} else {
							$scope.userDetails = response.data;
							$scope.assignedPatient = false;
						}
						if ($stateParams.aid) {
							$scope.buttonShow = false;
							inputString2._id = $stateParams.aid;
							/*Function call here*/
							$scope.currentProbability(inputString, inputString2);
						} else {
							/*Function call here*/
							$scope.allProbability(inputString);
						}
					}else{
						$scope.showloader = false;
					}
				});
			}
		}

		/*
		 *Created by : Jatinder Singh
		 *function to show the probability screen data algorithm on the Assessment solved by the patient.
		 */

		$scope.allProbability = function(inputString) {
			$scope.previousAvailable = true;
			$scope.probability = {};
			$scope.isAssessmentData = false;
			PatientService.getLatestAssessmentOfPatient(inputString, function(response) {
				if (response.messageId == 200 && response.status == 'success') {
					$scope.data = response.data;
					if ($scope.data.length != 0) {
						if ($scope.data[0].patient.next_assessment_available) {
							$scope.assessmentDateAvailable = true;
							$scope.assessmentDateNotAvailable = false;
						} else {
							$scope.assessmentDateAvailable = false;
							$scope.assessmentDateNotAvailable = true;
						}
						if ($scope.data[0].patient.next_appointment_date) {
							$scope.appointmentDateAvailable = true;
							$scope.appointmentDateNotAvailable = false;
						} else {
							$scope.appointmentDateAvailable = false;
							$scope.appointmentDateNotAvailable = true;
						}
						if (response.data[0].user_type == 3) {
							$scope.user_who_submitted_Assessment = "Patient Submitted Questions & Answers";
						} else if (response.data[0].user_type == 4) {
							$scope.user_who_submitted_Assessment = "Caregiver Submitted Questions & Answers";
						}
						$scope.currentData = response.data[0].results;
						$scope.assessmentData = response.data[0].answers;
						$scope.isSuicidePlan = false;
						$scope.isSuicideBehavior = false;
						$scope.isPhysicalAbuse = false;
						$scope.isSexualAbuse = false;
						$scope.isTrauma = false;
						$scope.isAlcohol = false;
						$scope.isOtherSubstance = false;
						$scope.isNSSI = false;

						// get Extra question percentage
						var extra_question_highest = 0;
						var extra_question_get = 0;
						for(var p=0;p<$scope.assessmentData.length;p++){
							if(typeof $scope.assessmentData[p]['extra_question'] != "undefined"){
								extra_question_highest +=  $scope.assessmentData[p]['highest_score'];
								extra_question_get +=  $scope.assessmentData[p]['answer_score'];
							}
						}
						$scope.extraQuestionPer = (extra_question_get*100)/extra_question_highest;
						$scope.extraQuestionPer = $scope.extraQuestionPer.toFixed(2);
						//End of code to get extra questions



						if (response.data[0].answers.length > 0) {
							$scope.isAssessmentData = true;
							var allAnswers = [];
							allAnswers = response.data[0].answers;
							allAnswers.forEach(function(value, index) {
								/** checking for Key Factors mapping **/
								if ((value.question_text == "I have a plan of how I would kill myself." || value.question_text == "I have a plan of how I would kill myself") && value.answer_score == 1) {
									$scope.isSuicidePlan = true;
								}
								if ((value.question_text == "I have tried to kill myself." || value.question_text == "I have tried to kill myself") && value.answer_score == 1) {
									$scope.isSuicideBehavior = true;
								}
								if ((value.question_text == "I have been hit so hard it left a mark." || value.question_text == "I have been hit so hard it left a mark") && value.answer_score == 1) {
									$scope.isPhysicalAbuse = true;
								}
								if ((value.question_text == "Someone has touched me where they are not supposed to." || value.question_text == "Someone has touched me where they are not supposed to") && value.answer_score == 1) {
									$scope.isSexualAbuse = true;
								}
								if ((value.question_text == "I had something bad happen to me (like a bad accident, or getting attacked)." || value.question_text == "I had something bad happen to me (like a bad accident, or getting attacked)") && value.answer_score == 1) {
									$scope.isTrauma = true;
								}
								if ((value.question_text == "I drink alcohol." || value.question_text == "I drink alcohol") && (value.answer_score == 1 || value.answer_score == 2 || value.answer_score == 3)) {
									$scope.isAlcohol = true;
								}
								if ((value.question_text == "I smoke" || value.question_text == "I use things to get high") && (value.answer_score == 1 || value.answer_score == 2 || value.answer_score == 3)) {
									$scope.isOtherSubstance = true;
								}
								if ((value.question_text == "I cut or burn myself" || value.question_text == "cuts or burns self (or other non-suicidal self-injury)") && (value.answer_score == 1 || value.answer_score == 2 || value.answer_score == 3)) {
									$scope.isNSSI = true;
								}
							});
						}
						$scope.probability.patientassessment_id = response.data[0]._id;
						$scope.latestDate = response.data[0].created_date;
						/*Bar Chart Functionality start here*/
						var json = response.data[0].results;
						var graphColors = {
							0: '#DB4437',
							1: '#1B9E77',
							2: '#FFD700'
						};
						var newarr = {};
						newarr.cols = [{
							id: "t",
							label: "Conditions",
							type: "string"
						}, {
							id: "s",
							label: "Percentage",
							type: "number"
						}, {
							role: "style",
							type: "string"
						}];
						newarr.rows = [];
						var key = '';
						json.forEach(function(index, value) {
							if (index.posterior_probability >= 80) {
								key = 0;
							} else if (index.posterior_probability >= 31 && index.posterior_probability <= 80) {
								key = 2;
							} else {
								key = 1;
							}
							var obj = {};
							obj.c = [{
								v: index.disease.title
							}, {
								v: index.posterior_probability
							}, {
								v: 'color:' + graphColors[key]
							}, ]
							newarr.rows.push(obj);
						});
						/*Bar Chart Functionality ends here*/

						$scope.createGoogleGraph('BarChart', 'Patient Probability', 65, newarr);
						$scope.DataAvailable = true;
						$scope.NoShowData = false;

						// Get all assessment if available 
						var inputData = {};
						inputData.patient = inputString.patient;

						PatientService.getHistoryData(inputData, function(response1) {
							if (response1.status == "success") {
								response1.data.forEach(function(index, value) {
									if ($scope.data[0]._id != response1.data[value]._id) {
										$scope.historyData[value] = index;
										//console.log($scope.historyData);
										$scope.previousAvailable = false;
									}
								});
							}
						});
						$scope.showloader = false;
					} else {
						$scope.DataAvailable = false;
						$scope.NoShowData = true;
					}
					$scope.showloader = false;
				} else {
					$scope.DataAvailable = false;
					$scope.NoShowData = true;
				}
			});
		}

		/**
		 *	Created by : Jatinder Singh
		 *	function to submit the probability screen data for target and diagnose checkboxes.
		 **/

		$scope.submitProabilityPage = function() {
			var inputJsonString = {};
			inputJsonString.patient = $stateParams.id;
			inputJsonString.created_date = Date.now();
			$scope.probability.diagnose = [];
			angular.forEach($scope.currentData, function(disease) {
                if (disease.is_diagnosed) {
                    $scope.probability.diagnose.push(disease);
                };
            });
			if(!$scope.probability.target){
				toastr.warning(messagesConstants.TargetCheckboxSelection);
				return;
			}

			var answerLength = $scope.probability;
			$scope.newAnswer = [];
			$scope.newAnswer.diagnose_checked = [];
			$scope.newAnswer.target_checked = [];
			$scope.newAnswer.keyfactor_checked = [];
			var temp = '';

			// for Diagnoses checkboxes
			for (var i = 0;  i < answerLength.diagnose.length; i++) {
				$scope.newAnswer.diagnose_checked.push({
						"_id": answerLength.diagnose[i].disease._id,
						"disease_title": answerLength.diagnose[i].disease.title,
						"severity_score": answerLength.diagnose[i].posterior_probability,
						"posterior_probability": answerLength.diagnose[i].severity_score,
				});
			}
			// for Target checkboxes
			for (var index in answerLength.target) {
				if (answerLength.target[index]) {
					temp = index.split("_");
					$scope.newAnswer.target_checked.push({
						"_id": temp[0],
						"disease_title": temp[1],
						"severity_score": temp[2],
						"posterior_probability": temp[3],
					});
				}
			}
			// for Key Factor checkboxes
			//console.log(answerLength.keyfactor);
			if(answerLength.keyfactor){
				for (var index in answerLength.keyfactor) {
					//console.log(answerLength.keyfactor[index]);
					if (answerLength.keyfactor[index]) {
						temp = index.split(" ");
						if(temp[1]){
							$scope.newAnswer.keyfactor_checked.push({
								"keyfactor_title": temp[0] +" "+ temp[1],
								"checked": true
							});
						}else{
							$scope.newAnswer.keyfactor_checked.push({
								"keyfactor_title": temp[0],
								"checked": true
							});
						}
					}
				}
			}
			if ($scope.newAnswer.diagnose_checked.length) {
				inputJsonString.diagnose_checked = $scope.newAnswer.diagnose_checked;
			}
			if ($scope.newAnswer.target_checked.length) {
				inputJsonString.target_checked = $scope.newAnswer.target_checked;
			}else{
				toastr.warning(messagesConstants.TargetCheckboxSelection);
				return;
			}
			if ($scope.newAnswer.keyfactor_checked.length) {
				inputJsonString.keyfactor_checked =  $scope.newAnswer.keyfactor_checked;
			}
			var d = new Date();
			var newdate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
			inputJsonString.submit_date = newdate;
			inputJsonString.patientassessment = $scope.probability.patientassessment_id;
			inputJsonString.other_data = $scope.probability.other_data;
			$scope.showloader = true;
			PatientService.saveDiagnoseTarget(inputJsonString, function(response) {
				$scope.showloader = false;
				if (response.messageId == 200) {
					toastr.success(messagesConstants.diagTargetSuccess);
				}
			});
		}

		/**
		 *	get all caregivers for a clinic
		 *	developer : gurpreet 
		 **/

		$scope.getCaregivers = function(user_type) {
			$scope.showloader = true;
			$scope.patientDetails = {};
			$scope.patientName = [];
			$scope.inputJsonString = {};
			$scope.caregiverText = 'Add';
			$scope.patientText = 'Add';
			$scope.inputJsonString.user_type = user_type;
			PatientService.getPatientType($scope.inputJsonString, function(response) {
				if (response.messageId == 200) {
					$scope.showloader = false;
					if (response.data != 'undefined') {
						$scope.caregivers = response.data;

					}
				}
			});
			if ($stateParams.id) {
				$scope.caregiverText = 'Edit';
				$scope.patientText = 'Edit';
				$scope.isEmailDisabled = true;
				$scope.isUsernameDisabled = true;
				PatientService.getUser($stateParams.id, function(response) {
					if (response.messageId == 200) {
						$scope.showloader = false;
						var d = new Date(response.data.dob);
						var t_month = d.getMonth() + 1;
						var t_date = d.getUTCDate();
						if (t_month < 10) {
							t_month = "0" + t_month;
						}
						if (t_date < 10) {
							t_date = "0" + d.getDate();
						}
						var formatdate = (t_month) + '/' + t_date + '/' + d.getFullYear();
						response.data.dob = formatdate;
						$scope.user = response.data;
					}
				});
			}
		}


		/**--------------------------------
		 *	Check time is valid or not
		 *	developer : Gurpreet Singh
		 *--------------------------------**/
		$scope.timeError = false;
		$scope.checkTime = function() {
			var selectedDate = $scope.appointmentData.next_appointment_date;
			var selectedTime = $scope.appointmentData.next_appointment_time;
			var d = new Date();
			var currentdate = (d.getMonth() + 1 < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1) + '/' + (d.getDate() < 10 ? "0" + d.getDate() : d.getDate()) + '/' + d.getFullYear();
			var currentTime = d.getHours() + " : " + (d.getMinutes() < 10 ? ("0" + d.getMinutes()) : d.getMinutes());
			if (typeof selectedDate != 'undefined' && typeof selectedTime != 'undefined' && selectedDate == currentdate) {
				if (selectedTime <= currentTime) {
					$scope.timeError = true;
				} else {
					$scope.timeError = false;
				}
			} else {
				$scope.timeError = false;
			}
		}

}]) //controller end here
