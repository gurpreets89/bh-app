// var baseUrl = "http://localhost:3000";

var webservices = {	

	"authenticate" : baseUrl + "/adminlogin/authenticate",
	"authenticate-patient" : baseUrl + "/users/authenticate", /*Used: For Patient login Authentication API*/
	"forgot_password" : baseUrl + "/adminlogin/forgot_password",
	"forgot_username" : baseUrl + "/adminlogin/forgot_username",
	"reset_password":  baseUrl + "/adminlogin/reset_password/:token",
	"change_password":  baseUrl + "/adminlogin/change_password",
	
	//"listVehicleTypes" : baseUrl + "/vehicletypes/list",
	//"addVehicleType": baseUrl + "/vehicletypes/add",
	//"editVehicleType": baseUrl + "/vehicletypes/edit",
	//"updateVehicleType": baseUrl + "/vehicletypes/update",
	//"statusUpdateVehicleType": baseUrl + "/vehicletypes/update_status",
	//"deleteVehicleType": baseUrl + "/vehicletypes/delete",

	//Clinician : Added by Jatinder
		"clinicianList" : baseUrl + "/clinicians/list",
		"bulkUpdateClinician" : baseUrl + "/clinicians/bulkUpdateClinician",
		"delClinician": baseUrl + "/clinicians/delete",
		"checkClinicianPatients" : baseUrl + "/clinicians/checkClinicianPatients",
		"getClinicians" : baseUrl + "/clinicians/getClinicians",
		"updatePatientsClinician" : baseUrl + "/clinicians/updatePatientsClinician",
		"getPatientsInfo" : baseUrl + "/clinicians/getPatientsInfo",
		"logPatientsInfo" : baseUrl + "/clinicians/logPatientsInfo",
		"invite" : baseUrl + "/clinicians/invite",
		"invitedClinicians" : baseUrl + "/clinicians/listInvitedUsers", //In use for Bh APP
		"updateInvitedClinicianStatus" : baseUrl + "/users/updateInvitedClinicianStatus", //In use for Bh APP

	//Disease List
	"diseaseList" : baseUrl + "/diseases/list", //In use for Bh APP
	"adminDiseaseList" : baseUrl + "/diseases/adminlist", //In use for Bh APP
	"exportDiseaseList":baseUrl + "/diseases/exportFile", //In use for Bh APP
	"findDiseaseDetail" : baseUrl + "/diseases/diseaseDetail", //In useused
	"addDisease": baseUrl + "/diseases/add", // in use.
	"findOneDisease" : baseUrl + "/diseases/diseaseOne", //used
	"updateDisease" : baseUrl + "/diseases/updateDisease", //In use for Bh APP
	"updateAdminDiseases" : baseUrl + "/diseases/updateAdminDiseases", //In use for Bh APP
	"delDisease" : baseUrl + "/diseases/delDisease",
	"bulkUpdateDiseases" : baseUrl + "/diseases/bulkUpdateDiseases",  

	//In use for Bh APP//Questions
	"answerList" : baseUrl + "/answers/answerlist", //In use for Bh APP
	"getAllAnswerOptions" : baseUrl + "/answers/getAllAnswerOptions", //In use for Bh APP
	"saveAnswerOption" : baseUrl + "/answers/saveAnswerOption", //In use for Bh APP
	"findOneAnswer" : baseUrl + "/answers/findOneAnswer",
	"bulkUpdateAnswer" : baseUrl + "/answers/bulkUpdateAnswer",

	//In use for Bh APP//FAQs
	"getAllFaqs" : baseUrl + "/faqs/getAllFaqs", //In use for Bh APP
	"findOneFaq" : baseUrl + "/faqs/findOneFaq", //In use for Bh APP
	"saveFaq" : baseUrl + "/faqs/saveFaq", //In use for Bh APP
	"updateFaqStatus" : baseUrl + "/faqs/updateFaqStatus", //In use for Bh APP
	"deleteFaq" : baseUrl + "/faqs/deleteFaq", //In use for Bh APP
	"exportfaqList":baseUrl + "/faqs/exportFile",

	



	//In use for Bh APP // Videos
	"getAllVideos" : baseUrl + "/videos/getAllVideos",
	"findOneVideo" : baseUrl + "/videos/findOneVideo",
	"saveVideo" : baseUrl + "/videos/saveVideo",
	"deleteVideo" : baseUrl + "/videos/deleteVideo",
	"updateVideoStatus" : baseUrl + "/videos/updateVideoStatus",
	"findVideoTypes" : baseUrl + "/videos/findVideoTypes",

	//Patients : Added by RaJesh
	"patientList" : baseUrl + "/patients/list", //In use for Bh APP
	"invitedUsersList" : baseUrl + "/patients/listInvitedUsers", //In use for Bh APP
	"addPatient" : baseUrl + "/users/addPatient", //In use for Bh APP
	"saveScoreProbability" : baseUrl + "/patients/saveScoreAndProbability", //In use for Bh APP
	"getPatientType" : baseUrl + "/patients/patientType", //In use for Bh APP
	"getLatestAssessmentOfPatient" : baseUrl + "/patients/getLatestAssessmentOfPatient", //In use for Bh APP
	"getHistoryData" : baseUrl + "/patients/getHistoryData", //In use for Bh APP
	"saveDiagnoseTarget" : baseUrl + "/patients/saveDiagnoseTarget", //In use for Bh APP
	//"inviteOutPatient" : baseUrl + "/users/inviteOutPatient", // commented by gurpreet
	"inviteOutPatient" : baseUrl + "/patients/inviteOutPatient", //In use for Bh APP
	"getPatientFilterList" : baseUrl + "/patients/getPatientFilterList", //In use for Bh APP
	"updatePatient" : baseUrl + "/patients/updatePatient", //In use for Bh APP
	"getPatientProbability" : baseUrl + "/patients/getPatientProbability", //In use for Bh APP
	"checkAppointmentForClinician" : baseUrl + "/patients/checkAppointmentForClinician", //In use for Bh APP

	//Goals : Added by RaJesh
	"goalList" : baseUrl + "/goal/list", //In use for Bh APP
	"addGoal" : baseUrl + "/goal/add", //In use for Bh APP
	"findOneGoal" : baseUrl + "/goal/findOneGoal", //In use for Bh APP
	"updateGoal" : baseUrl + "/goal/update", //In use for Bh APP
	"delGoal": baseUrl + "/goal/delGoal", //In use for Bh APP
	"getPatientGoal" : baseUrl + "/goal/patientGoal", //In use for Bh APP
	"bulkUpdateGoal" : baseUrl + "/goal/bulkUpdateGoal",
	"getUserByGoal" : baseUrl + "/goal/getUserByGoal",
	"getPatientDailyGoals" : baseUrl + "/goal/getPatientDailyGoals", //In use for Bh APP

	// general questions : Added by RaJesh
	"addGeneralQuestions" : baseUrl + "/generalquestions/add", //In use for Bh APP
	"patientGeneralQuestion" : baseUrl + "/generalquestions/patientGeneralQuestion", //In use for Bh APP
	"updatepatientGeneralQuestion" : baseUrl + "/generalquestions/updatepatientGeneralQuestion", //In use for Bh APP
	
	// checkin screen : Added by RaJesh
	"addCheckInData" :   baseUrl + "/checkin/add", //In use for Bh APP
	"listCheckInData" :   baseUrl + "/checkin/list", //In use for Bh APP
	"updateCheckInData" :   baseUrl + "/checkin/update", //In use for Bh APP


	//user
	"addUser" : baseUrl + "/users/add",
	"userList" : baseUrl + "/users/list",
	"findOneUser" : baseUrl + "/users/userOne", //used
	"bulkUpdateUser" : baseUrl + "/users/bulkUpdate",
	"update" : baseUrl + "/users/update",
	"checkUser" : baseUrl + "/users/checkUser", //used
	"checkUserAndEmail" : baseUrl + "/users/checkUserAndEmail", //used
	"checkUserAndEmailSignup" : baseUrl + "/users/checkUserAndEmailSignup", //used
	"checkInvitedClinicianSignup" : baseUrl + "/users/checkInvitedClinicianSignup", //used
	"getUserProfileData" : baseUrl + "/users/getUserProfileData", //In use for Bh APP
	"updateUserProfile" : baseUrl + "/users/updateUserProfile", //In use for Bh APP
	"updateProfilePic" : baseUrl + "/users/updateProfilePic", //In use for Bh APP
	"findUserCount" : baseUrl + "/users/findUserCount", //used
	"listAdmin" : baseUrl + "/users/listAdmin", //used
	"exportuserList":baseUrl + "/users/exportFile",
	"addAdmin" : baseUrl + "/users/addAdmin", //used
	"findAdminPatient" : baseUrl + "/users/findAdminPatient", //used
	"getAdmin" : baseUrl + "/users/getAdmin", //used 
	
	//subscribers
	"addSubscriber" : baseUrl + "/subscribers/add",
	"subscriberList" : baseUrl + "/subscribers/list",
	"findOneSubscriber" : baseUrl + "/subscribers/userOne",
	"bulkUpdateSubscriber" : baseUrl + "/subscribers/bulkUpdate",
	"updateSubscriber" : baseUrl + "/subscribers/update",
	"findSubscriberHistory" : baseUrl + "/subscribers/userHistory",

	//default Appt Type
	"defaultApptTypeList" : baseUrl + "/defaultAppointmentType/list",
	"addDefaultApptType" : baseUrl + "/defaultAppointmentType/add",
	"updateDefaultApptType" : baseUrl + "/defaultAppointmentType/update",
	"bulkUpdateDefaultApptType" : baseUrl + "/defaultAppointmentType/bulkUpdate",
	"findOneDefaultApptType" : baseUrl + "/defaultAppointmentType/findOne",

	//default Diagnosis
	"defaultDiagnosisList" : baseUrl + "/defaultDiagnosis/list",
	"addDefaultDiagnosis" : baseUrl + "/defaultDiagnosis/add",
	"updateDefaultDiagnosis" : baseUrl + "/defaultDiagnosis/update",
	"bulkUpdateDefaultDiagnosis" : baseUrl + "/defaultDiagnosis/bulkUpdate",
	"findOneDefaultDiagnosis" : baseUrl + "/defaultDiagnosis/findOne",
	
	//default Tasks
	"tasksList" : baseUrl + "/defaultTasks/list",
	"filterTasksList" : baseUrl + "/defaultTasks/filteredlist",
	"addTask" : baseUrl + "/defaultTasks/add",
	"updateTask" : baseUrl + "/defaultTasks/update",
	"bulkUpdateTask" : baseUrl + "/defaultTasks/bulkUpdate",
	"findOneTask" : baseUrl + "/defaultTasks/findOne",

	//vehicle webservice listing
	//"listVehicles": baseUrl + "/vehicles/list",

	//category
	"allQuestions" : baseUrl + "/categories/allQuestions",
	"categoryList" : baseUrl + "/categories/list",
	"addCategory" : baseUrl + "/categories/add",
	"updateCategory" : baseUrl + "/categories/update",
	"bulkUpdateCategory" : baseUrl + "/categories/bulkUpdate",
	"findOne" : baseUrl + "/categories/findOne",

	// "questionnaireList" : baseUrl + "/questionnaire/listquestionnaire",
	// "addquestionnaire" : baseUrl + "/questionnaire/addquestionnaire",
	// "editquestionnaire" : baseUrl + "/questionnaire/editquestionnaire",
	// "updatequestionnaire" : baseUrl + "/questionnaire/updatequestionnaire",
	// "updatestatusquestionnaire" : baseUrl + "/questionnaire/updateStatus",

	//questionnaire webservice listing
	"questionnaireList" : baseUrl + "/questionnaire/list", //used
	"addquestionnaire" : baseUrl + "/questionnaire/add", //used
	"checkQuestionnaires" : baseUrl + "/questionnaire/check",
	"findOneQuestionnaire" : baseUrl + "/questionnaire/questionnaireOne", //used
	"bulkUpdateQuestionnaire" : baseUrl + "/questionnaire/bulkUpdate",
	"updateQuestionnaire" : baseUrl + "/questionnaire/update",
	"basicQuestionnaireList" : baseUrl + "/questionnaire/listBasic", //used
	"deleteQuestionnaire" : baseUrl + "/questionnaire/removequestionnaire",

	//extra question webservice listing
	"updateExtraQuestion" : baseUrl + "/extraquestions/update",
	"findOneExtraQuestion" : baseUrl + "/extraquestions/question",
	"addExtraQuestion" : baseUrl + "/extraquestions/add",   //In use for BH App
	"listAllExtraQuestion" : baseUrl + "/extraquestions/list",   //In use for BH App
	"listExtraQuestions" : baseUrl + "/extraquestions/list",   //In use for BH App
	"delExtraQuestion" : baseUrl + "/extraquestions/delQuestion",
	"bulkUpdateExtraQuestion" : baseUrl + "/extraquestions/bulkUpdate",

	
	//question webservice listing
	"updateQuestion" : baseUrl + "/questions/update",
	"findOneQuestion" : baseUrl + "/questions/question",
	"addQuestion" : baseUrl + "/questions/add",   //In use for BH App
	"listAllQuestion" : baseUrl + "/questions/listall",   //In use for BH App
	"listQuestions" : baseUrl + "/questions/list",   //In use for BH App
	"bulkUpdateQuestion" : baseUrl + "/questions/bulkUpdate",
	"getAnswerList" : baseUrl + "/questions/getanswerlist",
	"diseaseQuestionList" : baseUrl + "/questions/questionlist", 
	"delQuestion" : baseUrl + "/questions/delQuestion", 
	"exportQuestionList":baseUrl + "/questions/exportFile", //In use for Bh APP


	//answertype webservice listing
	"answerTypeList" : baseUrl + "/answer_type/list",
	"exportAnswerList":baseUrl + "/answers/exportFile",
  
  //role
	"roleList" : baseUrl + "/roles/list",
	"addRole" : baseUrl + "/roles/add",
	"updateRole" : baseUrl + "/roles/update",
	"findOneRole" : baseUrl + "/roles/role",
	"bulkUpdateRole" : baseUrl + "/roles/bulkUpdate",

	//permission
	"permissionList" : baseUrl + "/permissions/list",
	"createPermission" : baseUrl + "/permissions/create",
	"updatePermission" : baseUrl + "/permissions/update",
	"findOnePermission" : baseUrl + "/permissions/permission",
	"bulkUpdatePermission" : baseUrl + "/permissions/bulkUpdate",
	
	//user
	"addPlan" : baseUrl + "/plans/add",
	"plansList" : baseUrl + "/plans/list",
	"findOnePlan" : baseUrl + "/plans/planOne",
	"bulkUpdatePlan" : baseUrl + "/plans/bulkUpdate",
	"updatePlan" : baseUrl + "/plans/update"

}

var facebookConstants = {
	"facebook_app_id": "1655859644662114"
}

var googleConstants = {

	"google_client_id" : "54372597586-09u72notkj8g82vl3jt77h7cbutvr7ep.apps.googleusercontent.com",
	
}

var appConstants = {

	"authorizationKey": "dGF4aTphcHBsaWNhdGlvbg=="	
}


var headerConstants = {

	"json": "application/json"

}

var pagingConstants = {
	"defaultPageSize": 10,
	"defaultPageNumber":1
}

var messagesConstants = {
	//common
	
	//users
	"saveUser" : "User has been saved successfully.",
	"savePatient" : "Patient has been saved successfully.",
	"saveCaregiver" : "Caregiver has been saved successfully.",
	"saveClinician" : "New Clinician has been saved successfully.",
	"addClinician" : "Your account has been registered successfully. You will be able to login once your administrator approve your account.",
	"saveAdmin" : "New Admin has been saved successfully.",
	"updateAdmin" : "Admin updated successfully.",
	"updateClinician" : "Clinician has been updated successfully.",
	"deleteClinician" : "Clinician has been removed successfully.",
	"updateUser" : "User has been updated successfully.",
	"updateStatus" : "Status has been updated successfully.",
	"deleteUser": "User(s) has been deleted successfully.",
	"profileUpdated": "Profile has been updated successfully.",
	"profilePicUpdated": "Profile picture has been updated successfully.",
	"profilePicError": "Profile picture can't be updated now. Please try again later.",
	"profilePicWarning": "Please select a valid image.",
	"nextApmntSaved": "Next Appointment & Assessment date has been saved successfully.",
	"accountDeactivated": "Your account has been de-activated, please contact your administrator.",
	"usernamePasswordError": "Either Username or Password is incorrect",
	"EmailWarning":"Email already exist.",
	"UsernameWarning":"Username already exist.",
	"AccountRegisterSuccess":"Your account has been registered successfully",
	"ReferanceCodeError":"Reference Code is invalid. Please try again.",
	"validEmailError":"Please enter a valid email address.",

	//questionnaires
	//"saveQuestionnaire" : "Questionnaire saved successfully.",
	//"updateQuestionnaire" : "Questionnaire updated successfully.",
	//"deleteQuestionnaire" : "Questionnaire deleted successfully.",

	//questions
	"saveQuestion" : "Question has been saved successfully",
	"updateQuestion" : "Question has been updated successfully",
	"deleteQuestion": "Question has been deleted successfully",
	"EditQuestion":"Edit Question",
	"AddQuestion":"Add Question",
	"EditQuestionError":"Unable to edit question. Please try again",
	"AddQuestionError":"Unable to add question. Please try again",
	"QuestionDeleteSuccess":"Question has been deleted successfully.",
	"QuestionStatusSuccess":"Question status updated successfully.",
	"QuestionSuccessError":"Unable to update Question status. Please try again",

	//Admin user
	"AdminSuccessStatus":"Admin status has been updated successfully.",
	"AdminSuccessError":"Unable to update admin status. Please try again",

	//clinician
	"ClinicianStatusSuccess":"Clinician status has been updated.",
	"selectPatientsError":"Please select some patients to transfer.",
	"TransferPatientError":"Please transfer patients to other clinician first.",
	"RemoveClinicError":"Unable to remove clinician.",
	"getCliniciansError":"Unable to get clinician data. Please try again.",
	"inviteClinicianSuccess" : "Invitation link to clinician has been sent successfully.",
	"ClinicianSweetAlertTitle" : "Confirmation",
	"ClinicianSweetAlertText1" : "Are you sure you want to delete clinician with email id : (",
	"ClinicianSweetAlertText2" : ")",
	"ClinicianSweetAlertConfirmText" : "Yes, remove it!",
	"ClinicianSweetAlertCancelText" : "No!",
	"NoInvitedUsers" : "No invited users are available.",


	//diseases
	"SelectDiseases":"Please select a disease.",
    "DiseasesUpdateSuccess":"Diseases option status has been updated.",
    "DiseasesUpdateError":"Unable to update diseases option status. Please try again.",
    "AddDisorder":"Add Disorder",
    "EditDisorder":"Edit Disorder",


	//answers
	"saveAnswerOption" : "Answer option has been saved successfully",
	"updateanswerOption" : "Answer option has been updated successfully",
	"disableAnswerOption": "Answer Option has been disabled successfully",
	"enableAnswerOption": "Answer Option has been enabled successfully",
	"AnswerUpdateError":"Unable to update answer option status. Please try again",
	"AnswerUpdateSuccess":"Answer option status has been updated.",
	"AnswerEditError":"Unable to edit answer. Please try again",
	"AnswerAddError":"Unable to add answer option. Please try again",
	"AddAnswerScreenTitle":"Add Answer",
	 "AddEditAnswerScreenTitle":"Add/Edit Answer Option",
	 "EditAnswerOption":"Edit Answer",
	 "SelectAnAnswer":"Please select at least an answer.",
	 

	"AddFaqScreenTitle":"Add FAQ",
	"EditFaqScreenTitle":"Edit FAQ",
	"FAQUpdateError":"Unable to update question(s) status. Please try again.",
	"FAQUpdateSuccess":"Question(s) status has been updated.",
	"FAQEditError":"Unable to edit question. Please try again.",
	"FAQAddError":"Unable to add question. Please try again.",
	"saveFAQ" : "Question has been saved successfully.",
	"updateFAQ" : "Question has been updated successfully.",
	"NoFAQFound" : "No question found with this ID.",


	"AddVideoScreenTitle":"Add Video",
	"EditVideoScreenTitle":"Edit Video",
	"SelectVideoType":"Please select at least one video type.",
	"VideoUpdateError":"Unable to update video(s) status. Please try again.",
	"VideoUpdateSuccess":"Video(s) status has been updated.",
	"VideoEditError":"Unable to edit video. Please try again.",
	"VideoAddError":"Unable to add video. Please try again.",
	"saveVideo" : "Video has been saved successfully.",
	"updateVideo" : "Video has been updated successfully.",
	"NoVideoFound" : "No video found with this ID.",
	"VideoDeleteSuccess":"Video has been deleted successfully.",

	
	//Error
	"enterQuestion" : "Please enter the question.",
	"selectAnswerType" : "Please select the answer type.",
	"enterAnswer" : "Please enter the answer.",
	"selectAnswerCorrect" : "Please choose the answer as correct.",
	"enterKeyword" : "Please enter the Keyword.",
	"selectAction" : "Please select an action to change status.",
	"selectActionField" : "Please select a record to change status.",
	"InvalidTimeError" : "Please enter a valid Time.",

	//Tasks
	"deleteTask": "Task(s) has been deleted successfully.",
	"deletePlan": "Plan(s) has been deleted successfully.",

	//Goals
	"AddGoalSuccess":"Goal has been added successfully.",
	"AddGoalError":"Goal can't be added.",
	"UpdateGoalSuccess":"Goal has been updated successfully.",
	"UpdateGoalError":"Goal can't be updated.",
	"GoalDeleteSuccess":"Goal has been deleted successfully.",
	"AddEditGoals":"Add New Goal",
	"addPatientError":"No patient found. Please add patient.",
	"CaregiverError":"No caregiver found. Please add caregiver.",
	"GoalsStatusSuccess":"Goal status has been updated.",
	"GoalsStatusError":"Unable to update goal option status. Please try again",

	//caregiver messages
	"updateCaregiver" : "Caregiver has been updated successfully.",

	//patient message
	"diagTargetSuccess": "Diagnose and Target data has been saved successfully.",
	"inviteOutPatient" : "Invitation link to Out-Patient has been sent successfully.",
	"updatePatient" : "Patient has been updated successfully.",
	"CheckInUpdateSuccess":"Check-in updated successfully.",
	"CheckInUpdateError":"Can't update check-in.",
	"EmailExistWarning":"Email id already exist, please try another.",
	"FirstNameValidatedError":"First name is invalid. Please enter alphabets only.",
    "LastNameValidatedError":"Last name is invalid. Please enter alphabets only.",
   	"EmailValidationError": "Please enter a valid email address.",
   	"deleteGoalSuccess":"Goal has been deleted successfully.",
   	"CheckboxSelection":"No checkboxes is selected.",
   	"TargetCheckboxSelection":"Please select atleast one target.",
   	"NoPatientsAviliable":"No patients are available.",
   	"PatientsStatusSuccess":"Patients status has been updated.",
	"UsersSuccessMsg":"User(s) status has been updated.",

	// changepassword
	"oldPasswordError": "Your old password is incorrect.",
	"passwordChangeSuccess" : "Password has been changed successfully. Please login again to continue.",

	// passwordreset

	"passwordResetSuccess" : "Password has been reset successfully.",
	"passwordResetLinkSuccess" : "Password reset sent link has been sent successfully. Please check your email.",
	"passwordResetEmailError" : "Email can't be empty.",
	"oldNewPasswordSameError" : "Old password and new password can't be same.",
	// AES encryption key
	"encryptionKey" :"GFYUFGTYGFTYTY64564545acvbvrttyFG@%#%#%#%#TTRR",

	"notInvited": "The email and reference code doesn't match. Please check the invitation email message and try again."
}