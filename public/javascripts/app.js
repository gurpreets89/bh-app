"use strict";

angular.module("Authentication", []);
angular.module("Home", []);
angular.module("communicationModule", []);
angular.module("Users", []);
angular.module("DefaultAppointmentType", []);
angular.module("DefaultDiagnosis", []);
angular.module("DefaultTasks", []);
angular.module("Permissions", []);
angular.module("Questions", []);
angular.module("ExtraQuestions", []);
//angular.module("Questionnaire", []);
// angular.module("Categories", []);
angular.module("Goals", []);
angular.module("Diseases", []);
angular.module("Answers", []);
angular.module("Faqs", []);
angular.module("Videos", []);

var bhapp = angular.module('bhapp', [
	'ui.router',
	'ngStorage',
	'oc.lazyLoad',
	'ngTable',
	'ngResource',
	'ui.grid',
	'Authentication',
	'Home',
	'communicationModule',
	'Users',
	'DefaultAppointmentType',
	'DefaultDiagnosis',
	'DefaultTasks',
	'Permissions',
	'satellizer',
	// 'Questionnaire',
	'Goals',
	'Questions',
	'ExtraQuestions',
	// 'Categories',
	'Diseases',
	'Answers',
	'Faqs',
	'Videos',
	'flash',
	'angular-accordion',
	'oitozero.ngSweetAlert',
	'googlechart',
	'ngFileUpload',
	'ngImgCrop',
	'toastr',
	'720kb.datepicker',
	'ngMask',
	'ngAnimate', 'ngSanitize', 'ui.bootstrap'
]);

function checkloggedIn($rootScope, $localStorage, $http) {
    //console.log('in checkloggedIn ----------- \n', $rootScope, '\n>>>\n', $localStorage);
    /*$http.get('/adminlogin/checkloggedin').success(function(data) {
	console.log("checkloggedin data = ", data);
        //if (data.error) {
        //    $location.path('/login');
        //}
        //else{
        //    $rootScope.user = data;
        //}
    });*/
    
    //console.log('in checkloggedIn ----------- \n', $rootScope, '\n>>>\n', $localStorage);
    
    if($localStorage.userLoggedIn) {
	    $rootScope.userLoggedIn 	= true;
	    $rootScope.loggedInUser 	= $localStorage.loggedInUsername;
	    $rootScope.loggedInUserType = $localStorage.loggedInUsertype;
	    $rootScope.fullName 		= $localStorage.loggedInUsername;
	    var created_Date 	= $localStorage.loggedInUser.user.created_date;
	    var newDate 		= new Date(created_Date);
	    $rootScope.memberSinceDate 	= formatDate(newDate);
	    $rootScope.profilePic = $localStorage.loggedInUser.user.profile_pic ? "images/profile/"+ $localStorage.loggedInUser.user.profile_pic : "images/profile/avatar5.png";

    } else {
	    $rootScope.userLoggedIn = false;
    }
}

function checkSuperAdminloggedIn($rootScope, $localStorage, $http, $location) {
    if($localStorage.userLoggedIn) {
	$rootScope.userLoggedIn 	= true;
	$rootScope.loggedInUser 	= $localStorage.loggedInUsername;
	$rootScope.loggedInUserType = $localStorage.loggedInUsertype;
	$rootScope.fullName 	= $localStorage.loggedInUsername;
	var created_Date 		= $localStorage.loggedInUser.user.created_date;
	var newDate 		= new Date(created_Date);
	$rootScope.memberSinceDate 	= formatDate(newDate);
	$rootScope.profilePic = $localStorage.loggedInUser.user.profile_pic ? "images/profile/"+ $localStorage.loggedInUser.user.profile_pic : "images/profile/avatar5.png";
	if ($localStorage.loggedInUsertype != 5) {
	    $location.path('/login');
	}
    } else {
	    $rootScope.userLoggedIn = false;
    }
}

function checkAdminloggedIn($rootScope, $localStorage, $http, $location) {
    if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn 	= true;
		$rootScope.loggedInUser 	= $localStorage.loggedInUsername;
		$rootScope.loggedInUserType = $localStorage.loggedInUsertype;
		$rootScope.fullName 	= $localStorage.loggedInUsername;
		var created_Date 		= $localStorage.loggedInUser.user.created_date;
		var newDate 		= new Date(created_Date);
		$rootScope.memberSinceDate 	= formatDate(newDate);
		$rootScope.profilePic = $localStorage.loggedInUser.user.profile_pic ? "images/profile/"+ $localStorage.loggedInUser.user.profile_pic : "images/profile/avatar5.png";
		if ($localStorage.loggedInUsertype != 1) {
		    $location.path('/login');
		}
    } else {
	    $rootScope.userLoggedIn = false;
    }
}

function checkClinicianloggedIn($rootScope, $localStorage, $http, $location) {
    if($localStorage.userLoggedIn) {
	$rootScope.userLoggedIn 	= true;
	$rootScope.loggedInUser 	= $localStorage.loggedInUsername;
	$rootScope.loggedInUserType = $localStorage.loggedInUsertype;
	$rootScope.fullName 	= $localStorage.loggedInUsername;
	var created_Date 		= $localStorage.loggedInUser.user.created_date;
	var newDate 		= new Date(created_Date);
	$rootScope.memberSinceDate 	= formatDate(newDate);
	$rootScope.profilePic = $localStorage.loggedInUser.user.profile_pic ? "images/profile/"+ $localStorage.loggedInUser.user.profile_pic : "images/profile/avatar5.png";
	if ($localStorage.loggedInUsertype != 2) {
	    $location.path('/login');
	}
    } else {
	    $rootScope.userLoggedIn = false;
    }
}

function formatDate(date) {
	 	var monthsArray = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	  	var monthName = monthsArray[date.getMonth()];
	  	var dayDate = date.getDate();
	  	dayDate = dayDate < 10 ? '0'+dayDate : dayDate;
	  	var dateReturn =  dayDate + " " + monthName + ", " + date.getFullYear();
	  	return dateReturn;
	}

bhapp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
bhapp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
        // debug:true,
        // events:true
    });
}]);

/* Toaster Settings  */
 bhapp.config(function(toastrConfig) {
   angular.extend(toastrConfig, {
     autoDismiss: false,
     closeButton: true,
     containerId: 'toast-container',
     maxOpened: 0,    
     newestOnTop: true,
     positionClass: 'toast-top-right',
     preventDuplicates: false,
     preventOpenDuplicates: true,
     target: 'body'
   });
 });

bhapp.factory('basicAuthenticationInterceptor', ['$localStorage' , function($localStorage) {
	
	var basicAuthenticationInterceptor = {
		request:function(config) {
			if($localStorage.authorizationToken)
			config.headers['Authorization'] = 'Bearer ' + $localStorage.authorizationToken;
			// config.headers['Authentication'] = 'Basic ' + appConstants.authorizationKey;
 			//config.headers['Content-Type'] = headerConstants.json;
			return config;
		}
	};
	return basicAuthenticationInterceptor;
}]);

bhapp.config([/*'$routeProvider'*/'$stateProvider', '$urlRouterProvider' , '$httpProvider', '$authProvider', '$locationProvider', function(/*$routeProvider*/ $stateProvider , $urlRouterProvider , $httpProvider, $authProvider, $locationProvider) {

	$httpProvider.interceptors.push('basicAuthenticationInterceptor');

	$stateProvider
	.state("404", {
		url: "/404",
		controller: 'loginController',
		templateUrl: '/modules/authentication/views/404.html',
		loginRequired: true,
		resolve:{'logged_in':checkloggedIn}
	})
	.state("home", {
		url: "/home",
		controller: 'homeController',
		templateUrl: '/modules/home/views/home.html',
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})
	.state("adminhome", {
		url: "/adminhome",
		controller: 'homeController',
		templateUrl: '/modules/home/views/adminDashboard.html',
		loginRequired: true,
		resolve:{'logged_in':checkAdminloggedIn}
	})
	.state("superhome", {
		url: "/superhome",
		controller: 'homeController',
		templateUrl: '/modules/home/views/superAdminDashboard.html',
		loginRequired: true,
		resolve:{'logged_in':checkSuperAdminloggedIn}
	})
	.state("homestate", {
		url: "/",
		controller: 'homeController',
		templateUrl: '/modules/home/views/home.html',
		loginRequired: true,
		resolve:{'logged_in':checkloggedIn}
	})
	.state('login', {
		url: "/login",
		controller: 'loginController',
		templateUrl: '/modules/authentication/views/login.html',
		loginRequired: false,
		//resolve:{'logged_in':checkloggedIn}
	})
	.state('forgot-password', {
		url: "/forgot-password",
		controller: 'loginController',
		templateUrl: '/modules/authentication/views/forgot-password.html',
		loginRequired: false,
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Authentication",
					files: [
						'/modules/authentication/service.js',
						'/modules/authentication/controller.js',
					]
				}).then(function() {
					//console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}
	})
	.state('/reset-password/:token', {
		url: "/reset-password/:token",
		controller: 'loginController',
		templateUrl: '/modules/authentication/views/reset-password.html',
		loginRequired: false
		// resolve:{'logged_in':checkloggedIn}
	})

	.state('/changepassword', {
		url: "/changepassword",
		controller: 'loginController',
		templateUrl: '/modules/authentication/views/changepassword.html',
		loginRequired: true,
		resolve:{'logged_in':checkloggedIn}
	})

	.state('forgot-username', {
		url: "/forgot-username",		
		controller:'loginController',
		templateUrl:'/modules/authentication/views/forgot-username.html',
		loginRequired: false,
		resolve:{'logged_in':checkloggedIn}
	})
	
	/** states for clinicians **/
	    .state('clinicians', {
			url: "/clinicians",		
			controller : "clinicianController",
			templateUrl : "/modules/clinicians/views/list.html",
			loginRequired: true,
			resolve:{'logged_in':checkloggedIn}
			//templateUrl : "/pages/tables/data.html"
	    })
	    .state('/clinicians/addclinician', {
			url: '/clinicians/addclinician' ,
			controller : "clinicianController",
			templateUrl : "/modules/clinicians/views/addclinician.html",
			loginRequired: true,
			resolve:{'logged_in':checkloggedIn}
	    })
	    .state('/clinicians/transfer/:id' , {
			url: '/clinicians/transfer/:id' ,		
			controller: "clinicianController",
			templateUrl : "/modules/clinicians/views/transferPatients.html",
			loginRequired: true,
			resolve:{'logged_in':checkloggedIn}
	    })
		.state('/clinicians/invite', {
			url: '/clinicians/invite' ,
			controller : "clinicianController",
			templateUrl : "/modules/clinicians/views/inviteClinician.html",
			loginRequired: true,
			resolve:{'logged_in':checkloggedIn}
	    })
	    
	/** states for diseases **/
	.state('admindiseases', {
		    url: "/admindiseases",		
		    controller : "admindiseaseController",
		    templateUrl : "/modules/diseases/views/adminlist.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkAdminloggedIn},
	})
	.state('diseases', {
		    url: "/diseases",		
		    controller : "diseaseController",
		    templateUrl : "/modules/diseases/views/list.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkSuperAdminloggedIn},
	})
	.state('diseases/add', {
		    url: '/diseases/add' ,
		    controller : "diseaseController",
		    templateUrl : "/modules/diseases/views/add.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkSuperAdminloggedIn},
	})
	.state('diseases/import', {
		    url: '/diseases/import' ,
		    controller : "diseaseController",
		    templateUrl : "/modules/diseases/views/import.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkSuperAdminloggedIn},
	})
	.state('/diseases/edit/:id', {
		url: '/diseases/edit/:id' ,
		controller : "diseaseController",
		templateUrl : "/modules/diseases/views/add.html",
		loginRequired: true,
		resolve:{'logged_in':checkSuperAdminloggedIn}
	})
	
	/** states for Users **/
	.state('users', {
		url: "/users",		
		controller : "userController",
		templateUrl : "/modules/users/views/listuser.html",
		loginRequired: true,
		resolve:{'logged_in':checkloggedIn}
	})

	.state('/users/addadmin', {
		url: '/users/addadmin' ,
		controller : "userController",
		templateUrl : "/modules/users/views/addadmin.html",
		loginRequired: true,
		resolve:{'logged_in':checkSuperAdminloggedIn}
	})
	.state('/users/editadmin/:admin_id', {
		url: '/users/editadmin/:admin_id' ,
		controller : "userController",
		templateUrl : "/modules/users/views/addadmin.html",
		loginRequired: true,
		resolve:{'logged_in':checkSuperAdminloggedIn}
	})
	.state('/users/add', {
		url: '/users/add' ,
		controller : "userController",
		templateUrl : "/modules/users/views/adduser.html",
		loginRequired: true,
		resolve:{'logged_in':checkloggedIn}
	})

	.state('/users/listadmin', {
		url: '/users/listadmin' ,
		controller : "userController",
		templateUrl : "/modules/users/views/listadmin.html",
		loginRequired: true,
		resolve:{'logged_in':checkSuperAdminloggedIn}
	})

	
	.state('/users/signup', {
		url: '/users/signup' ,
		controller : "userController",
		templateUrl : "/modules/users/views/signup.html",
		loginRequired: false,
		resolve:{'logged_in':checkloggedIn}
	})

	.state('/users/edit/:id' , {
		url: '/users/edit/:id' ,		
		controller: "userController",
		templateUrl : "/modules/users/views/adduser.html",
		loginRequired: true,
		resolve:{'logged_in':checkloggedIn}
	})

	/** states for Patients **/
	.state('patients', {
		url: "/patients",		
		controller : "patientController",
		templateUrl : "/modules/patients/views/listpatients.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('patientslist', {
		url: "/patientslist",		
		controller : "patientController",
		templateUrl : "/modules/patients/views/listpatientadmindashboard.html",
		loginRequired: true,
		resolve:{'logged_in':checkAdminloggedIn}
	})
	.state('patients/search/:id', {
		url: '/patients/search/:id' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/listpatients.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})
    
	.state('/patients/add', {
		url: '/patients/add' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/addpatient.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/patients/addcaregiver', {
		url: '/patients/addcaregiver' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/addcaregiver.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/patients/addop', {
		url: '/patients/addop' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/addopatient.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/patients/addassesment', {
		url: '/patients/addassesment' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/addassesment.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/patients/processassesment', {
		url: '/patients/processassesment' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/processAssessment.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/patients/goals/:id/:type', {
		url: '/patients/goals/:id/:type' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/listgoals.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/patients/check-in/:id/:type', {
		url: '/patients/check-in/:id/:type' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/check-in.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})
	
	.state('/patients/probability/:id/:type', {
		url: '/patients/probability/:id/:type' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/patientProbability.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/patients/probability/:id/:aid/:name', {
		url: '/patients/probability/:id/:aid/:name' ,
		controller : "patientController",
		templateUrl : "/modules/patients/views/patientProbability.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})
	
	//.state('patients/stateofmind/:id/:type', {
	//	url: '/patients/stateofmind/:id/:type' ,
	//	controller : "patientController",
	//	templateUrl : "/modules/patients/views/stateofmind.html",
	//	loginRequired: true,
	//	resolve:{'logged_in':checkClinicianloggedIn}
	//})
	
	.state('patients/stateofmind/:id/:type', {
		url: '/patients/stateofmind/:id/:type' ,
		controller : "stateofmindController",
		templateUrl : "/modules/stateofmind/views/stateofmind.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})
	
	.state('/patients/edit/:id' , {
		url: '/patients/edit/:id' ,		
		controller: "patientController",
		templateUrl : "/modules/patients/views/addpatient.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})
	.state('/patients/editCaregiver/:id' , {
		url: '/patients/editCaregiver/:id' ,		
		controller: "patientController",
		templateUrl : "/modules/patients/views/addcaregiver.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	
	/** states for goals **/
	.state('/goals', {
		url: '/goals' ,
		controller : "goalController",
		templateUrl : "/modules/goals/views/listgoals.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/goals/add', {
		url: '/goals/add' ,
		controller : "goalController",
		templateUrl : "/modules/goals/views/add.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})

	.state('/goals/addpatientgoal/:id/:type', {
		url: '/goals/addpatientgoal/:id/:type',
		controller : "goalController",
		templateUrl : "/modules/goals/views/add.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})
	.state('/goals/edit/:g_id/:p_id', {
		url: '/goals/edit/:g_id/:p_id' ,
		controller : "goalController",
		templateUrl : "/modules/goals/views/add.html",
		loginRequired: true,
		resolve:{'logged_in':checkClinicianloggedIn}
	})	

	/** states for questions **/
	.state('questions', {
	    url: "/questions",		
		controller : "questionController",
		templateUrl : "/modules/questions/views/listQuestions.html",
		loginRequired: true,
		resolve:{'logged_in':checkSuperAdminloggedIn}
	})

	.state('/questions/add', {
		url: '/questions/add' ,
		controller : "questionController",
		templateUrl : "/modules/questions/views/add_question.html",
		loginRequired: true,
		resolve:{'logged_in':checkSuperAdminloggedIn}
	})
	
	.state('/questions/edit/:id' , {
	    url: '/questions/edit/:id' ,		
	    controller: "questionController",
	    templateUrl : "/modules/questions/views/add_question.html",
	    loginRequired: true,
	    resolve:{'logged_in':checkSuperAdminloggedIn}
	})

	.state('/questions/delete/:id' , {
		    url: '/questions/delete/:id' ,		
		    controller: "questionController",
		    templateUrl : "/modules/questions/views/add_question.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkSuperAdminloggedIn}
	})
	
	/** states for extra questions **/
	.state('extraquestions', {
	    url: "/extraquestions",		
		controller : "extraQuestionController",
		templateUrl : "/modules/extraquestion/views/listQuestions.html",
		loginRequired: true,
		resolve:{'logged_in':checkAdminloggedIn}
	})

	.state('/extraquestions/add', {
		url: '/extraquestions/add' ,
		controller : "extraQuestionController",
		templateUrl : "/modules/extraquestion/views/add_question.html",
		loginRequired: true,
		resolve:{'logged_in':checkAdminloggedIn}
	})
	
	.state('/extraquestions/edit/:id' , {
	    url: '/extraquestions/edit/:id' ,		
	    controller: "extraQuestionController",
	    templateUrl : "/modules/extraquestion/views/add_question.html",
	    loginRequired: true,
	    resolve:{'logged_in':checkAdminloggedIn}
	})

	.state('extraquestions/import', {
		    url: '/extraquestions/import' ,
		    controller : "extraQuestionController",
		    templateUrl : "/modules/extraquestion/views/import.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkAdminloggedIn},
	})

	.state('/extraquestions/delete/:id' , {
		    url: '/extraquestions/delete/:id' ,		
		    controller: "extraQuestionController",
		    templateUrl : "/modules/extraquestion/views/add_question.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkAdminloggedIn}
	})
	/** End of states for extra questions **/   
	   

	    .state('answers', {
		    url: "/answers",
		    controller : "answerController",
		    templateUrl : "/modules/answers/views/listAnswerOptions.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkloggedIn}
	    })

	    .state('/answers/add', {
		    url: '/answers/add' ,
		    controller : "answerController",
		    templateUrl : "/modules/answers/views/add_answer_option.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkloggedIn}
	    })


	    .state('/answers/edit/:id' , {
			url: '/answers/edit/:id' ,		
			controller: "answerController",
			templateUrl : "/modules/answers/views/add_answer_option.html",
			loginRequired: true,
			resolve:{'logged_in':checkloggedIn}
	    })

	    .state('faqs', {
		    url: "/faqs",
		    controller : "faqController",
		    templateUrl : "/modules/faqs/views/list_faq.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkloggedIn}
	    })

	    .state('/faqs/add', {
		    url: '/faqs/add' ,
		    controller : "faqController",
		    templateUrl : "/modules/faqs/views/add_faq.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkloggedIn}
	    })


	    .state('/videos/edit/:id' , {
			url: '/videos/edit/:id' ,		
			controller: "videoController",
			templateUrl : "/modules/videos/views/add_video.html",
			loginRequired: true,
			resolve:{'logged_in':checkloggedIn}
	    })

	    .state('videos', {
		    url: "/videos",
		    controller : "videoController",
		    templateUrl : "/modules/videos/views/list_video.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkloggedIn}
	    })

	    .state('/videos/add', {
		    url: '/videos/add' ,
		    controller : "videoController",
		    templateUrl : "/modules/videos/views/add_video.html",
		    loginRequired: true,
		    resolve:{'logged_in':checkloggedIn}
	    })


	    .state('/faqs/edit/:id' , {
			url: '/faqs/edit/:id' ,		
			controller: "faqController",
			templateUrl : "/modules/faqs/views/add_faq.html",
			loginRequired: true,
			resolve:{'logged_in':checkloggedIn}
	    })





	    .state('/clinicians/edit/:id' , {
			url: '/clinicians/edit/:id' ,		
			controller: "clinicianController",
			templateUrl : "/modules/clinicians/views/addclinician.html",
			loginRequired: true,
			resolve:{'logged_in':checkloggedIn}
	    })


	.state('/profile', {
		url: '/profile' ,
		controller : "userController",
		templateUrl : "/modules/users/views/profile.html",
		loginRequired: true,
		resolve:{'logged_in':checkloggedIn}
	})

	
	.state('default-appointmenttype', {
		url: '/default-appointmenttype' ,
		controller : "defaultAppointmentTypeController",
		templateUrl : "/modules/admin/appointmentType/views/listappointmenttype.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"DefaultAppointmentType" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/admin/appointmentType/service.js',
						'/modules/admin/appointmentType/controller.js',
					]
				}).then(function() {
					//console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('default-appointmenttype-add', {
		url: '/default-appointmenttype/add' ,
		controller : "defaultAppointmentTypeController",
		templateUrl : "/modules/admin/appointmentType/views/addappointment-type.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"DefaultAppointmentType" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/admin/appointmentType/service.js',
						'/modules/admin/appointmentType/controller.js',
					]
				}).then(function() {
					//console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('default-appointmenttype-edit' , {
		url: '/default-appointmenttype/edit/:id' ,		
		controller: "defaultAppointmentTypeController",
		templateUrl : "/modules/admin/appointmentType/views/addappointment-type.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultAppointmentType" , 
					files: [
						'/modules/admin/appointmentType/service.js',
						'/modules/admin/appointmentType/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('default-diagnosis', {
		url: '/default-diagnosis' ,
		controller : "defaultDiagnosisController",
		templateUrl : "/modules/admin/diagnosis/views/listdiagnosis.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultDiagnosis" , 
					files: [
						'/modules/admin/diagnosis/service.js',
						'/modules/admin/diagnosis/controller.js',
					]
				}).then(function() {
					//console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})	
	.state('default-diagnosis-add', {
		url: '/default-diagnosis/add' ,
		controller : "defaultDiagnosisController",
		templateUrl : "/modules/admin/diagnosis/views/add-diagnosis.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultDiagnosis" , 
					files: [
						'/modules/admin/diagnosis/service.js',
						'/modules/admin/diagnosis/controller.js',
					]
				}).then(function() {
					//console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('default-diagnosis-edit', {
		url: '/default-diagnosis/edit/:id' ,
		controller : "defaultDiagnosisController",
		templateUrl : "/modules/admin/diagnosis/views/add-diagnosis.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultDiagnosis" , 
					files: [
						'/modules/admin/diagnosis/service.js',
						'/modules/admin/diagnosis/controller.js',
					]
				}).then(function() {
					//console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('default-tasks', {
		url: '/default-tasks' ,
		controller : "defaultTaskController",
		templateUrl : "/modules/admin/tasks/views/listtask.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"DefaultTasks" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/admin/tasks/service.js',
						'/modules/admin/tasks/controller.js',
					]
				}).then(function() {
					//console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('default-tasks-add', {
		url: '/default-tasks/add' ,
		controller : "defaultTaskController",
		templateUrl : "/modules/admin/tasks/views/addtask.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"DefaultTasks" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/admin/tasks/service.js',
						'/modules/admin/tasks/controller.js',
					]
				}).then(function() {
					//console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('default-tasks-edit' , {
		url: '/default-tasks/edit/:id' ,		
		controller: "defaultTaskController",
		templateUrl : "/modules/admin/tasks/views/addtask.html",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultTasks" , 
					files: [
						'/modules/admin/tasks/service.js',
						'/modules/admin/tasks/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})

	// default redirect for any url in app.
	$urlRouterProvider.otherwise("/home");
}])

/* for new pagination */
.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
})

.run(['$rootScope', '$location', '$http', '$localStorage' , '$state', function($rootScope, $location, $http, $localStorage , $state) {
	//console.log("State is" , $state);
	$rootScope.$on("$stateChangeStart", function(event, nextRoute, currentRoute) {
		//Implement authorization check here 
		//console.log("nextRoute is " , nextRoute.access   , nextRoute);

	});

	$rootScope.$on('$stateChangeSuccess', function(event, nextRoute, currentRoute) {
	    //console.log("nextRoute is " , nextRoute.access   , nextRoute);

	    if(!$localStorage.userLoggedIn) {
			if($state.current.loginRequired){
				$location.path('/login');
			}
		}

	});




	
}]);
