angular.module('bhapp').directive('fancyCheckbox', ['$parse', function($parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				//console.log(attrs.class);
				angular.element(element).iCheck({
					checkboxClass: 'icheckbox_' + attrs.class,
					radioClass: 'iradio_' + attrs.class
				});
				scope.$watch('DataAvailable', function(newValue, oldValue) {
					if (newValue) {
						if (attrs.check == 'true') {
							angular.element(element).parent().addClass('checked');
						}
					}
				})
			}
		};
	}]).directive('myTimepicker', ['$parse', function($parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				angular.element(element).wickedpicker();
			}
		};
	}])
	.directive('numbersOnly', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attr, ngModelCtrl) {
				function fromUser(text) {
					if (text) {
						var transformedInput = text.replace(/[^0-9]/g, '');

						if (transformedInput !== text) {
							ngModelCtrl.$setViewValue(transformedInput);
							ngModelCtrl.$render();
						}
						return transformedInput;
					}
					return undefined;
				}
				ngModelCtrl.$parsers.push(fromUser);
			}
		};
	})
	.directive('bootstrapSwitch', [
		function() {
			return {
				restrict: 'A',
				require: '?ngModel',
				link: function(scope, element, attrs, ngModel) {
					element.bootstrapSwitch();

					element.on('switchChange.bootstrapSwitch', function(event, state) {
						if (ngModel) {
							scope.$apply(function() {
								ngModel.$setViewValue(state);
							});
						}
					});

					scope.$watch(attrs.ngModel, function(newValue, oldValue) {
						if (newValue) {
							element.bootstrapSwitch('state', true, true);
						} else {
							element.bootstrapSwitch('state', false, true);
						}
					});
				}
			};
		}
	])
	.directive('reevioRating', function() {
		return {
			restrict: 'A',
			scope: {
				value: '=reevioRating'
			},
			templateUrl: 'patients/views/rating.html',
			link: function($scope, element, attrs) {
				$scope.stars = new Array(parseInt(attrs.reevioRatingLength));
				$scope.disabled = (attrs.reevioRatingDisabled == "true") ? true : false;
				$scope.userChoice = false;
				$scope.hoverValue = 0;

				$scope.starHover = function(i) {
					if ($scope.disabled) return;
					$scope.hoverValue = i + 1;
				}
				$scope.stopHover = function() {
					if ($scope.disabled) return;
					$scope.hoverValue = 0;
				}
				$scope.starClick = function(i) {
					if ($scope.disabled) return;
					$scope.value = i + 1;
					$scope.userChoice = true;
				}
			}
		}
	})
	.directive('iCheckRed', ['$timeout', '$parse', function($timeout, $parse) {
		return {
			require: 'ngModel',
			link: function($scope, element, $attrs, ngModel) {
				return $timeout(function() {
					var value;
					value = $attrs['value'];

					$scope.$watch($attrs['ngModel'], function(newValue) {
						$(element).iCheck('update');
					})

					return $(element).iCheck({
						checkboxClass: 'icheckbox_flat-red',
						radioClass: 'iradio_flat-red'

					}).on('ifChanged', function(event) {
						if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
							$scope.$apply(function() {
								return ngModel.$setViewValue(event.target.checked);
							});
						}
						if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
							return $scope.$apply(function() {
								return ngModel.$setViewValue(value);
							});
						}
					});
				});
			}
		};
	}])
	.directive('iCheckGreen', ['$timeout', '$parse', function($timeout, $parse) {
		return {
			require: 'ngModel',
			link: function($scope, element, $attrs, ngModel) {
				return $timeout(function() {
					var value;
					value = $attrs['value'];

					$scope.$watch($attrs['ngModel'], function(newValue) {
						if($(element)){
							$(element).iCheck('update');
						}
					})

					return $(element).iCheck({
						checkboxClass: 'icheckbox_flat-green',
						radioClass: 'iradio_flat-green'

					}).on('ifChanged', function(event) {
						if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
							$scope.$apply(function() {
								return ngModel.$setViewValue(event.target.checked);
							});
						}
						if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
							return $scope.$apply(function() {
								return ngModel.$setViewValue(value);
							});
						}
					});
				});
			}
		};
	}])
	.directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }])
	.directive('validfile', function validFile() {
	    var validFormats = ['csv'];
    	return {
        	require: 'ngModel',
        	link: function (scope, elem, attrs, ctrl) {
                elem.bind('change', function () {
                   var value = elem.val(),
                   ext = value.substring(value.lastIndexOf('.') + 1).toLowerCase();   
                   scope.invalidFile = (validFormats.indexOf(ext) === -1);
                });
        }
    };
});